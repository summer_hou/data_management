package cloud.xhmy.service;

import cloud.xhmy.pojo.Admins;
import cloud.xhmy.pojo.Users;

import java.math.BigInteger;
import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface AdminsService {

    Admins login(Admins user);
    Integer register(Admins user);
    int forgot(Admins user);

    int logout(Admins user);
    int modify(Admins user);
    List<Admins> judge(String email);

    int avatar(Admins user);
    int pro(Admins pro);

    int modifyTel(Admins admins);
    int modifyPwd(Admins admins);
    int realUser(Admins admins);

    Admins queryByEmail(String admins);

    Admins refresh (Admins user);
}
