package cloud.xhmy.service;

import cloud.xhmy.dao.MediaMapper;
import cloud.xhmy.pojo.Media;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public class MediaServiceImpl implements MediaService {

    private MediaMapper mediaMapper;

    public void setMediaMapper(MediaMapper mediaMapper) {
        this.mediaMapper = mediaMapper;
    }

    public int addMedia(Media media) {
        return this.mediaMapper.addMedia(media);
    }

    public int deleteById(int id) {
        return this.mediaMapper.deleteById(id);
    }

    public List<Media> queryByName(String name, int page, int size) {
        return this.mediaMapper.queryByName(name, page, size);
    }

    public List<Media> listAll(String author, int page, int size) {
        return this.mediaMapper.listAll(author, page, size);
    }

    public Media modifyById(int id) {
        return this.mediaMapper.modifyById(id);
    }

    public Media queryById(int id) {
        return this.mediaMapper.queryById(id);
    }

    public List<Media> listAllMedia(int page, int size) {
        return this.mediaMapper.listAllMedia(page, size);
    }

    public int push(int id,String state) {
        return this.mediaMapper.push(id,state);
    }

    public Media countNumber() {
        return this.mediaMapper.countNumber();
    }

    public List<Media> listAllAudit(String state, int page, int size) {
        return this.mediaMapper.listAllAudit(state, page, size);
    }

    public List<Media> listQueryAllAudit(String state, String name, int page, int size) {
        return this.mediaMapper.listQueryAllAudit(state, name, page, size);
    }
}
