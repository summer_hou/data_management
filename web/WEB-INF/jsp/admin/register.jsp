<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/7
  Time: 13:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>adminRegister</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 网页图标 ico格式 -->
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico">
    <!-- jquery -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>
    <!-- 引入 bootstrap js文件 -->
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- 引入 bootstrap css文件 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css"
          type="text/css">
    <!-- 密码的显示与隐藏 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/password-show-hidden.js"></script>

    <style>
        html, body {
            overflow: hidden;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            background: url("${pageContext.request.contextPath}/static/images/235228-1549900348f5ed.jpg");
            margin: 0 auto;
        }
        .img-back{
            filter:alpha(opacity=50);
            -moz-opacity:0.5;
            -khtml-opacity: 0.5;
            opacity: 0.5;
            width:100%;
            height: 100%;
            position:absolute;
            z-index: 10;
            background: rgba(255, 255, 255, 0.5);

        }
        /* canvas {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background:#000;
            cursor: pointer;
        } */
        div.pos{
            position:relative;
            width: 100%;
            height: 100%;
            z-index:150;
            color: goldenrod;


        }
        /*footer*/
        div.footer {
            position: absolute;
            bottom: 0px;
            right: auto;

            width: 100%;
            min-height: 50px;
            padding-top: 15px;
            color: whitesmoke;
            /* background: rgba(110,100,200,0.8); */
            text-align: center;
            z-index: 100;
        }

        .icp {
            color: white;
            text-decoration: none;
        }

        .icp:hover {
            text-decoration: none;
            color: wheat;
        }

    </style>

</head>
<body>

<div class="img-back">

</div>
<div class="pos">
    <div class="container">
        <div style="margin-top: 50px;">

        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h3 style="margin-bottom: 40px; text-align: center;"><span>注册管理员</span></h3>
<%--                <form>--%>
                    <div class="form-group"><label for="email">输入邮箱</label>
                        <input id="email" type="email" class="form-control" name="name" placeholder="请输入用户邮箱 @dm.net 或者@alike.com"
                               required autofocus onblur="validateName()"><span style="color: red;" id="emailInfo"></span></div>

                    <div class="form-group"><label for="email">输入电话号码</label>
                        <input id="telephone" type="tel" class="form-control" name="telephone" placeholder="请输入电话号码" required autofocus></div>
                    <div class="form-group"><label for="password">输入密码</label>
                        <input id="password" type="password" class="form-control" name="password" placeholder="请输入六位以上由数字,密码,符号组成的密码"
                               required data-eye></div>
                    <div class="form-group"><label for="checkPassword">确认密码</label>
                        <input id="checkPassword" type="password" class="form-control" name="password" placeholder="确认输入的密码" onkeyup="checkPassword()" required
                               data-eye><span id="hint"></span></div>
                    <!-- <div class="form-group">
                            <label for="checkCode">验证码

                            </label>
                            <span class="float-right" style="float: right;">看不清，点击图片</span>
                            <input id="checkCode" type="text" class="form-control" name="checkCode" style="width: 50%;" placeholder="请输入验证码">
                        </div> -->
                    <div class="form-group no-margin"><button id="register-btn" type="button" style="width: 15%; margin: 0 auto;"
                                                              data-toggle="modal" data-target="#exampleModal" class="btn btn-primary btn-block">注册 </button></div>
                    <div class="margin-top20 text-center">我有账号返回? <a href="${pageContext.request.contextPath}/admin/toLogin">登录</a></div>
<%--                </form>--%>

            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
    <div class="footer">
        <p>
            <span>© 2020 xhmy.cloud . &nbsp;</span>
            <span>赣ICP证</span> &nbsp;
            <a class="icp" href="https://beian.miit.gov.cn" title="" target="_blank">赣ICP备</a>
            <span>2020012707号</span> &nbsp;
        </p>
    </div>
</div>


<div style="z-index: 1000;">
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document" style="position: fixed;left: 50%;top: 50%; width: 350px; height: 304px; margin-top: -175px; margin-left: -152px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span id="clo" aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">拖动图片验证</h4>
                </div>
                <div class="modal-body">
                    <div id="captcha"></div>
                </div>
            </div>
        </div>
    </div>

</div>



<script src="${pageContext.request.contextPath}/static/js/admin/admin-jigsaw.min.js"></script>
<script>
    jigsaw.init({
        el: document.getElementById('captcha'),
        onSuccess: function() {
            // alert("success")
            // $("#register-btn").click(function() {
                //alert(html);
                $.ajax({
                    type: 'post',
                    url: '${pageContext.request.contextPath}/admin/register',
                    data: {
                        'email': $("#email").val(),
                        'password': $("#password").val(),
                        'telephone':$("#telephone").val()
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.code == "1") {
                            window.location.href = "${pageContext.request.contextPath}/admin/toLogin";
                        } else {
                            alert(data.msg);
                        }
                    }
                })
            // });
        },
        onFail: cleanMsg,
        onRefresh: cleanMsg
    });

    function cleanMsg() {
        alert("failure：验证失败")
    };
</script>
<script type="text/javascript">
    //验证两次输入密码是否一致
    function checkPassword() {
        var password = document.getElementById("password").value;
        var chkPassword = document.getElementById("checkPassword").value;

        if(password == chkPassword) {
            document.getElementById("hint").innerHTML="<br><font color='green'>两次密码输入一致</font>";
            // document.getElementById("button").disabled = false;

        }else {
            document.getElementById("hint").innerHTML="<br><font color='red'>两次输入密码不一致!</font>";
            // document.getElementById("button").disabled = true;
        }
    }
</script>
<script>
    // 原生ajax检测用户的唯一性

    var xmlHttp;

    //创建Ajax对象
    function createXMLHttpRequest() {
        if (window.ActiveXObject) {
            //IE6 and IE5
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            //W3C浏览器和IE7及其以上版本
            xmlHttp = new XMLHttpRequest();
        }
    }

    function validateName() {
        //调用创建Ajax方法
        createXMLHttpRequest();
        //获取用户名的值
        var email = document.getElementById("email");
        //将内容发送给哪个url处理
        var url = "${pageContext.request.contextPath}/admin/judgeEmail?email=" + escape(email.value);
        //创建HTTP请求（请求方式，请求URL，是否异步）
        xmlHttp.open("GET", url, true);
        //状态改变时
        xmlHttp.onreadystatechange = function () {
            //如果都ok
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {

                // var nameReg = /^[a-zA-Z][\w]{5,19}$/;//以字母开头，后面内容可以是字母，数字，下划线，且6~20位
                //必须由 大小写字母 或 数字 或下划线开头
                var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

                // var emailReg = /^[a-z0-9A-Z]+[- | a-z0-9A-Z . _]+@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-z]{2,}$/;
                if (emailReg.test(document.getElementById('email').value) == false) {
                    document.getElementById('emailInfo').innerText = "邮箱地址格式不正确";
                } else {
                    var color = "red";
                    var message = xmlHttp.responseXML.getElementsByTagName("message")[0].firstChild.data;
                    var passed = xmlHttp.responseXML.getElementsByTagName("passed")[0].firstChild.data;
                    if (passed == "true") {
                        color = "green";
                    }
                    document.getElementById("emailInfo").innerHTML = "<span style=color:" + color + ">" + message + "</span>";
                }
            }
        };
        xmlHttp.send();

    }
</script>

</body>
</html>
