function goback() {
    window.history.back();
};

function byLanguage(language) {
    switch (language) {
        case 0:
            window.location.href = "/browse/eBooksLanguage?clq=chinese";
            break;
        case 1:
            window.location.href = "/browse/eBooksLanguage?clq=english";
            break;
    }
}

function byClassify(claNum) {
    switch (claNum) {
        case 0:
            window.location.href = "/browse/eBooksClassify?clq=";
            break;
        case 1:
            window.location.href = "/browse/eBooksClassify?clq=艺术";
            break;
        case 2:
            window.location.href = "/browse/eBooksClassify?clq=商业";
            break;
        case 2:
            window.location.href = "/browse/eBooksClassify?clq=化学";
            break;
        case 4:
            window.location.href = "/browse/eBooksClassify?clq=经济";
            break;
        case 5:
            window.location.href = "/browse/eBooksClassify?clq=教育";
            break;
        case 6:
            window.location.href = "/browse/eBooksClassify?clq=地理";
            break;
        case 7:
            window.location.href = "/browse/eBooksClassify?clq=地质";
            break;
        case 8:
            window.location.href = "/browse/eBooksClassify?clq=历史";
            break;
        case 9:
            window.location.href = "/browse/eBooksClassify?clq=休闲";
            break;
        case 10:
            window.location.href = "/browse/eBooksClassify?clq=法学";
            break;
        case 11:
            window.location.href = "/browse/eBooksClassify?clq=宗教";
            break;
        case 12:
            window.location.href = "/browse/eBooksClassify?clq=技术";
            break;
        case 13:
            window.location.href = "/browse/eBooksClassify?clq=工艺";
            break;
        case 14:
            window.location.href = "/browse/eBooksClassify?clq=文学";
            break;
        case 15:
            window.location.href = "/browse/eBooksClassify?clq=数学";
            break;
        case 16:
            window.location.href = "/browse/eBooksClassify?clq=医学";
            break;
        case 17:
            window.location.href = "/browse/eBooksClassify?clq=物理";
            break;
        case 18:
            window.location.href = "/browse/eBooksClassify?clq=心理学";
            break;
        case 19:
            window.location.href = "/browse/eBooksClassify?clq=计算机";
            break;
        case 20:
            window.location.href = "/browse/eBooksClassify?clq=生物学";
            break;
        case 21:
            window.location.href = "/browse/eBooksClassify?clq=语言学";
            break;
        case 22:
            window.location.href = "/browse/eBooksClassify?clq=社会科学";
            break;
        case 23:
            window.location.href = "/browse/eBooksClassify?clq=体育与运动";
            break;
        case 24:
            window.location.href = "/browse/eBooksClassify?clq=科学(通用)";
            break;

    }
}