package cloud.xhmy.controller;

import cloud.xhmy.pojo.Feedback;
import cloud.xhmy.pojo.LostFeed;
import cloud.xhmy.pojo.Notes;
import cloud.xhmy.service.FeedbackService;
import cloud.xhmy.service.LostFeedService;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author aliketh.xhmy
 */
@Controller
public class OperationController {

    @Autowired
    @Qualifier("feedbackServiceImpl")
    private FeedbackService feedbackService;
    @Autowired
    @Qualifier("lostFeedServiceImpl")
    private LostFeedService lostFeedService;

    /**
     *跳转到反馈页面
     * @return
     */
    @RequestMapping("/toFeedback")
    public String toFeedback(){
        return "operation/feedback";
    }

    /**
     *跳转到遗失申报页面
     * @return
     */
    @RequestMapping("/toLostFeed")
    public String toLostFeed(){
        return "operation/lostFeed";
    }

    /**
     *提交反馈
     * @param feedback
     * @param request
     * @return
     */
    @RequestMapping("/pushFeedback")
    @ResponseBody
    public JSONObject pushFeedback(@ModelAttribute Feedback feedback, HttpServletRequest request){
        JSONObject jsonObject = new JSONObject();
        feedback.setFeedbackTime(new Date());
        feedbackService.addFeedback(feedback);
        jsonObject.put("msg","提交成功！");
        return jsonObject;
    }

    /**
     *提交遗失申报
     * @param feedback
     * @param request
     * @return
     */
    @RequestMapping("/pushLostFeed")
    @ResponseBody
    public JSONObject pushLostFeed(@ModelAttribute LostFeed feedback, HttpServletRequest request){
        JSONObject jsonObject = new JSONObject();
        lostFeedService.add(feedback);
        jsonObject.put("msg","提交成功！");
        return jsonObject;
    }

    /**
     * 列出个人所有反馈记录
     * @param author
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/listFeedBack/{user.email}")
    public ModelAndView listFeedBack(@PathVariable("user.email") String author,
                                     @RequestParam(defaultValue = "1") int page,
                                     @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<Feedback> list = feedbackService.listAll(author,page, size);
        PageInfo<Feedback> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos", pageInfos);
        mv.setViewName("operation/listFeedback");
        return mv;
    }

    /**
     * 列出个人所有遗失申报
     * @param author
     * @param page
     * @param size
     * @return
     */

    @ResponseBody
    @RequestMapping("/listLostFeed/{user.email}")
    public ModelAndView listLostFeed(@PathVariable("user.email") String author,
                                     @RequestParam(defaultValue = "1") int page,
                                     @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<LostFeed> list = lostFeedService.listAll(author,page, size);
        PageInfo<LostFeed> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos", pageInfos);
        mv.setViewName("operation/listLostFeed");
        return mv;
    }




    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */

}
