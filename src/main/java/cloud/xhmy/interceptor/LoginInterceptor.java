package cloud.xhmy.interceptor;

import cloud.xhmy.pojo.LoginLog;
import cloud.xhmy.service.LoginLogService;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author aliketh.xhmy
 * 登录日志记录
 */
public class LoginInterceptor implements HandlerInterceptor {

    @Autowired
    @Qualifier("loginLogServiceImpl")
    private LoginLogService loginLogService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

//        String uri = request.getRequestURI();
//
//        ModelAndView modelAndView = new ModelAndView();
//
//        if (uri.contains("Login") || uri.contains("login")) {
//            //登录请求，直接放行
//            return true;
//        } else {
//
//            //判断用户是否登录
//            if (request.getSession().getAttribute("user") != null) {
//                //说明已经登录，放行
//                return true;
//            } else {
//                //没有登录，跳转到登录界面
//                response.sendRedirect(request.getContextPath() + "/toLogin");
//            }
//
//        }
//        }catch (){
//
//        }
        //默认拦截
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {


        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        Browser browser = userAgent.getBrowser();
        OperatingSystem osVersion = userAgent.getOperatingSystem();
        String address = request.getRemoteAddr();
        String relCode = (String) request.getSession().getAttribute("code");
        String avatarEmail = (String) request.getSession().getAttribute("avatarEmail");
        LoginLog log = new LoginLog();
        log.setLoginSystem(osVersion.toString().toLowerCase());
        log.setLoginIp(address);
        log.setLoginDevice(browser.toString().toLowerCase());
        log.setLoginUser(avatarEmail);
        log.setLoginWay("password");

        log.setLoginState(relCode);
        // 向数据库写入数据
        loginLogService.addLog(log);

        System.out.println("=========================================log.toString():"+log.toString());

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
