package cloud.xhmy.service;

import cloud.xhmy.pojo.BlogRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface BlogService {
    /**
     * 增加
     * @param blog
     * @return
     */
    int addBlog(BlogRecord blog);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<BlogRecord> queryByName(String name, int page, int size);

    /**
     * 搜索个人记录
     * @param name
     * @param user
     * @param page
     * @param size
     * @return
     */
    List<BlogRecord> queryByMyself(String name,String user, int page, int size);

    /**
     * 列出所有
     * @param page
     * @param size
     * @return
     */
    List<BlogRecord> listAll(String author,int page, int size);

    /**
     * 修改
     * @param id
     * @return
     */
    BlogRecord modifyById(int id);

    /**
     * 用于修改获取信息
     * @param id
     * @return
     */
    BlogRecord queryById(int id);

    /**
     *主页显示
     */
    List<BlogRecord> homeQuery();


    /*------------------------------------------------------------------------------------------------------------*/
    /**
     * 增加
     * @param blog
     * @return
     */
    int addBlogDraft(BlogRecord blog);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteDraftById(int id);

    /**
     * 查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<BlogRecord> queryDraftByName(String name, int page, int size);

    /**
     * 列出所有
     * @param page
     * @param size
     * @return
     */
    List<BlogRecord> listAllDraft(String author,int page, int size);

    /**
     * 修改
     * @param id
     * @return
     */
    BlogRecord modifyDraftById(int id);

    /**
     * 用于修改获取信息
     * @param id
     * @return
     */
    BlogRecord queryDraftById(int id);


    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */

    /**
     * 查询所有
     * @param page
     * @param size
     * @return
     */
    List<BlogRecord> listAllBlog(int page, int size);

    /**
     * 查询所有等待审核
     * @param state
     * @param page
     * @param size
     * @return
     */
    List<BlogRecord> listAllAudit(String state, int page, int size);

    /**
     * 通过审核
     * @param id
     * @return
     */
    int push(int id,String state);

    /**
     * 统计
     * @return
     */
    BlogRecord countNumber();

    /**
     * 审核列表
     * @param state
     * @param page
     * @param size
     * @return
     */
    List<BlogRecord> listAuditArticle(String state,int page,int size);

    /**
     *
     * @param state
     * @param page
     * @param size
     * @return
     */
    List<BlogRecord> listQueryAuditArticle(String state,String name,int page,int size);
}

