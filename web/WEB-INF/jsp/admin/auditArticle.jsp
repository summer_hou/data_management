<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/23
  Time: 20:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="" style="width: 90%;margin: 5px auto;">
    <div class="row">
        <div>
            <h3><span>博客</span></h3>
        </div>
        <div class="col-md-12" style="overflow-x: scroll;">
            <div style="margin-top: 10px;">
                <%--
                <button class="btn btn-danger"
                        style="width: 10%; height: 38px;margin-left: 0px;margin-top: -2px;padding: unset;" type="button"
                        onclick="delAllProduct()">删除
                </button>
                --%>
                <form style="display: inline;">
                    <input id="search-audit-article" style="width: 80%;height: 40px; padding: 5px; border-radius: 2px; border: 1px solid #666666;margin-left: -4px;"
                           name="search"
                           value="${search}" placeholder="输入要搜索的书名" type="search">
                    <button style="width: 10%; height: 40px;margin-left: -4px;" type="button" onclick="searchAuditArticle()">搜索</button>
                </form>
            </div>

            <table class="table table-hover table-striped" style="overflow-x: scroll;color: gold;">
                <thead>
                <tr>
                    <th><input type="checkbox" name="CheckAll" id="CheckAll"/></th>
                    <th>博客作者</th>
                    <th>博客名称</th>
                    <th>博客标题</th>
                    <th>博客标签</th>
                    <th>博客状态</th>
                    <th>博客主题</th>
                    <th>博客内容</th>
                    <th>创建时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="auditArticles-list">
                <c:forEach var="list" items="${pageInfo.list}">
                    <tr>
                        <td><input name="ids" type="checkbox"> <input type="hidden" value=" + blog[i].id} "></td>
                        <td>${list.blogAuthor}</td>
                        <td>${list.blogName}</td>
                        <td>${list.blogTitle}</td>
                        <td>${list.blogTag}</td>
                        <td>${list.blogState}</td>
                        <td>${list.blogTheme}</td>
                        <td>${list.blogContent}</td>
                        <td>${list.blogTime}</td>
                        <td class="text-center">
                            <a href="javascript:browseArticle(${list.id})"
                               class="btn bg-olive btn-xs">查看</a>
                            ||
                            <a href="javascript:articleAuditResultOn(${list.id})"
                               class="btn bg-olive btn-xs">通过</a>
                            ||
                            <a href="javascript:articleAuditResultOff(${list.id})"
                               class="btn bg-olive btn-xs">驳回</a><%----%>
                    </tr>
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>
    </div>
    <div class="xhmy_page_div"></div>

</div>

<script type="text/javascript">
    $(".xhmy_page_div").createPage({

        pageNum: ${pageInfo.pages},
        current: 1,
        backfun: function (e) {
        }
    });
    function pageNumber(params) {
        $("#auditArticles-list").load("${pageContext.request.contextPath}/admin/auditArticle #auditArticles-list >*",{page:params,size:"10"});
    };
</script>

<script type="text/javascript">

    function articleAuditResultOn(on) {
        $.ajax({
            type: 'post',
            url: '${pageContext.request.contextPath}/admin/articleAuditResult',
            data: {
                'articleId':on,
                'result':'1'
            },
            dataType: 'json',
            success:function (data) {
                if (data.code == "1"){
                    alert("成功！")
                }else {
                    alert("失败！")
                }
            }
        })
    };
    function articleAuditResultOff(off) {
        $.ajax({
            type: 'post',
            url: '${pageContext.request.contextPath}/admin/articleAuditResult',
            data: {
                'articleId':off,
                'result':'2'
            },
            dataType: 'json',
            success:function (data) {
                if (data.code == "1"){
                    alert("成功！")
                }else {
                    alert("失败！")
                }

            }
        })
    }
</script>

<script type="text/javascript">
    //全选/全不选
    $("#CheckAll").bind("click", function () {
        $("input[name='Check[]']").prop("checked", this.checked);
    });

    //批量删除
    function delAllProduct() {
        if (!confirm("确定要删除吗？")) {
            return;
        }
        var cks = $("input[name='Check[]']:checked");
        var str = "";
        //拼接所有的id
        for (var i = 0; i < cks.length; i++) {
            if (cks[i].checked) {
                str += cks[i].value + ",";
            }
        }
        //去掉字符串末尾的‘&'
        str = str.substring(0, str.length - 1);
        // var a = str.split('&');//分割成数组
        // console.log(str);return false;
        $.ajax({
            type: 'post',
            url: "${pageContext.request.contextPath}/delete/article",
            dataType: "json",
            data: {
                filenames: str
            },
            success: function (data) {
                console.log(data);
                return false;
                if (data.code == "1") {
                    alert('删除成功！');
                    // location.href = data['url'];
                } else {
                    alert('删除失败！');
                    // return false;
                }
            }
        });
    };
</script>
<%----%>