package cloud.xhmy.controller;

import cloud.xhmy.pojo.*;
import cloud.xhmy.service.*;
import cloud.xhmy.util.files.FilesDelete;
import cloud.xhmy.util.files.FilesUpload;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.code.kaptcha.Constants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.UnknownHostException;
import java.util.*;

@Controller
public class AdminsController {

    @Autowired
    @Qualifier("adminsServiceImpl")
    private AdminsService adminsService;
    @Autowired
    @Qualifier("blogServiceImpl")
    private BlogService blogService;
    @Autowired
    @Qualifier("booksServiceImpl")
    private BooksService booksService;
    @Autowired
    @Qualifier("eBooksServiceImpl")
    private EBooksService eBooksService;
    @Autowired
    @Qualifier("usersServiceImpl")
    private UsersService usersService;
    @Autowired
    @Qualifier("loginLogServiceImpl")
    private LoginLogService loginLogService;
    @Autowired
    @Qualifier("notesServiceImpl")
    private NotesService notesService;
    @Autowired
    @Qualifier("mediaServiceImpl")
    private MediaService mediaService;
    @Autowired
    @Qualifier("feedbackServiceImpl")
    private FeedbackService feedbackService;
    @Autowired
    @Qualifier("lostFeedServiceImpl")
    private LostFeedService lostFeedService;
    @Autowired
    @Qualifier("subscribeServiceImpl")
    private SubscribeService subscribeService;
    @Autowired
    @Qualifier("borrowServiceImpl")
    private BorrowService borrowService;
    @Autowired
    @Qualifier("uploadServiceImpl")
    private UploadService uploadService;
    @Autowired
    @Qualifier("classifyServiceImpl")
    private ClassifyService classifyService;
    @Autowired
    @Qualifier("eFormatServiceImpl")
    private EFormatService eFormatService;
    @Autowired
    @Qualifier("downloadServiceImpl")
    private DownloadService downloadService;
    @Autowired
    @Qualifier("blacklistServiceImpl")
    private BlacklistService blacklistService;

//    @Autowired
//    @Qualifier("notesServiceImpl")
//    private NotesService notesService;

    /**
     * 跳转到登录页面
     *
     * @return
     */
    @RequestMapping("/admin/toLogin")
    public String adminLogin() {
        return "admin/login";
    }

    /**
     * 跳转到注册页面
     *
     * @return
     */
    @RequestMapping("/admin/toRegister")
    public String adminRegister() {
        return "admin/register";
    }

    /**
     * 跳转到找回密码页面
     *
     * @return
     */
    @RequestMapping("/admin/toForgot")
    public String adminForgot() {
        return "admin/forgot";
    }

    /**
     * 跳转到模板
     *
     * @return
     */
    @RequestMapping("/admin/home")
    public String adminHome() {
        return "admin/template";
    }

    /**
     * 用户登录
     *
     * @param user
     * @param request
     * @param model   用于传递参数
     * @param session 用于登录状态保持也可用token（建议）
     * @return
     * @throws UnknownHostException
     */
    @ResponseBody
    @RequestMapping("/admin/login")
    public JSONObject login(@ModelAttribute Admins user, HttpServletRequest request, Model model,
                            HttpSession session, HttpServletResponse response) throws IOException, ServletException {
//        ModelAndView mv = new ModelAndView();
        JSONObject jsonObject = new JSONObject();
        if (adminsService.login(user) != null) {
            Admins loginMsg = adminsService.login(user);
            // 用于上传头像，以及用户邮箱传递
            String proMsg = loginMsg.getEmail();
            session.setAttribute("avatarEmail", proMsg);
            // 用于页面传递信息
            session.setAttribute("user", loginMsg);
            // 用于传递登录状态1为成功0为失败
            session.setAttribute("code", "1");
            jsonObject.put("code", "1");
            return jsonObject;
        }


        // 用于传递登录状态1为成功0为失败
        session.setAttribute("code", "0");
        System.out.println("=============================================" + "\n" +
                "=====================登入失败：用户名，密码不能为空");
        jsonObject.put("code", "0");
        jsonObject.put("msg", "用户名或密码错误");
        return jsonObject;
    }

    /**
     * 用户注册
     *
     * @param user
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/admin/register", produces = "text/plain;charset=utf-8")
    public JSONObject register(@ModelAttribute Admins user, HttpServletRequest request, Model model) {
        user.setCreateTime(new Date());
        user.setPermissions("0");
        adminsService.register(user);
        System.out.println("=================================新注册用户" + user.getEmail());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", "1");
        jsonObject.put("msg", "成功");
//        return "admin/register";
        return jsonObject;
    }

    /**
     * 用户找回密码
     *
     * @param user
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/forgot")
    public JSONObject forgot(@ModelAttribute Admins user, HttpServletRequest request, Model model) {
        JsonReturnMsg jsonReturnMsg = new JsonReturnMsg();
        JSONObject jsonObject = new JSONObject();
        adminsService.forgot(user);
        System.out.println("=================================用户" + user.getEmail() + "修改密码成功！");
        jsonReturnMsg.setCodes("200");
        jsonReturnMsg.setMessages("修改密码成功！");
//        return jsonReturnMsg;
        jsonObject.put("code", "1");
        jsonObject.put("msg", "失败");
        return jsonObject;
    }

    /**
     * 用户退出登录
     *
     * @param session
     * @param request
     * @return
     */
    @RequestMapping("/admin/exit")
    public String exit(HttpSession session, HttpServletRequest request) {
//        Admins msg = (Admins) request.getSession().getAttribute("user");
//        System.out.println("=================================用户"+msg.getEmail()+"退出登录！");
        session.invalidate();
        return "admin/login";
    }

    /**
     * 修改信息后刷新
     *
     * @param user
     * @param session
     */
    @RequestMapping("/admin/refresh")
    public void refresh(Admins user, HttpSession session, Model model) {
        Admins admins = adminsService.refresh(user);
        model.addAttribute("user", admins);
    }

    /**
     * 注销账户（删除用户信息）
     *
     * @param request
     * @return
     */
    @RequestMapping("/admin/unsubscribe")
    public ModelAndView unsubscribe(Admins uid, HttpServletRequest request) {
        Admins msg = (Admins) request.getSession().getAttribute("user");
        ModelAndView mv = new ModelAndView();
        adminsService.logout(uid);
        String success = msg.getEmail() + "用户注销成功！";
        mv.addObject("success", success);
        mv.setViewName("../../index");
        return mv;
    }

    /**
     * @return
     */
    @RequestMapping("/admin/oneself")
    public String oneself() {
        return "admin/selfPage";
    }

    /**
     * @param newTel
     * @param request
     * @param tel
     * @return
     */
    @RequestMapping("/admin/modifyTel")
    public ModelAndView modifyTel(@PathVariable Admins admins,
                                  @RequestParam("newTelephone") BigInteger newTel,
                                  HttpServletRequest request,
                                  @RequestParam("oldTelephone") BigInteger tel) {
        Admins msg = (Admins) request.getSession().getAttribute("user");
        ModelAndView mv = new ModelAndView();
        if (msg.getTelephone() == null && msg.getTelephone().equals(tel)) {
            adminsService.modifyTel(admins);
            mv.addObject("telMsg", "成功");
            mv.setViewName("admin/selfPage");
            return mv;
        }
        mv.addObject("telMsg", "失败");
        mv.setViewName("admin/selfPage");
        return mv;
    }

    /**
     * 修改密码
     *
     * @param password
     * @param request
     * @param newPassword
     * @return
     */
    @RequestMapping("/admin/modifyPwd")
    public ModelAndView modifyPwd(@PathVariable Admins admins,
                                  @RequestParam("oldPassword") String password,
                                  HttpServletRequest request,
                                  @RequestParam("newPassword") String newPassword) {
        Admins msg = (Admins) request.getSession().getAttribute("user");
        ModelAndView mv = new ModelAndView();

        if (msg.getPassword().equals(password)) {
            admins.setPassword(newPassword);
            adminsService.modifyPwd(admins);
            mv.addObject("msg", "成功");
            mv.setViewName("admin/selfPage");
            return mv;
        }
        mv.addObject("msg", "失败");
        mv.setViewName("admin/selfPage");
        return mv;
    }

    /**
     * 实名认证
     *
     * @param admins
     * @return
     */
    @RequestMapping("/admin/realUser")
    public ModelAndView realUser(@PathVariable Admins admins) {
        ModelAndView mv = new ModelAndView();
        adminsService.realUser(admins);
        mv.addObject("success", "成功");
        mv.setViewName("admin/selfPage");
        return mv;
    }


    /**
     * 注册验证(更改验证)
     * <p>
     * 根据用户email查询
     *
     * @throws IOException
     */
//    @ResponseBody
    @RequestMapping("/admin/judgeEmail")
    public void findBrandByEmail(String email, HttpServletResponse response) throws IOException {
        //初始化
        String message = "";
        boolean judge = true;
        JSONObject jsonObject = new JSONObject();
        //查询是否有输入的用户名
        List<Admins> rel = adminsService.judge(email);
        //如果为null（没有该用户email）即可用
        if (rel == null) {
            message = "用户名可用";
            jsonObject.put("code", "1");
            jsonObject.put("msg", "用户名可用");
        } else {
            judge = false;
            message = "用户名已经存在，请使用其他用户名";
            jsonObject.put("code", "0");
            jsonObject.put("msg", "用户名已经存在，请使用其他用户名");
        }

        //设置返回数据为utf-8
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<response>");
        out.println("<passed>" + Boolean.toString(judge) + "</passed>");
        out.println("<message>" + message + "</message>");
        out.println("</response>");
//        return jsonObject;
    }


    /*---------------------------------------------------------------------------------------------------------*/

    /**
     * 上传头像图片
     *
     * @param request
     * @param
     * @param file
     * @throws IOException
     */
    @RequestMapping("/admin/avatar")
    @ResponseBody
    public Map<String, String> uploadAvatar(@RequestParam(value = "avatar") MultipartFile file,
                                            HttpServletRequest request, HttpSession session)
            throws IllegalStateException, IOException {

        Map<String, String> map = new HashMap<String, String>();
        //从session中获得用户用户邮箱
        String uid = String.valueOf(request.getSession().getAttribute("avatarEmail"));
        String contextPath = "/images/avatar/user/";
//        //确定保存的文件夹
//        String rootPath = request.getSession().getServletContext().getRealPath("/static");
//        String contextPath = "/images/avatar/user/";
//        String dirPath = rootPath+contextPath;
//        System.out.println("==========================================dirPath"+dirPath);
//        //会在 web 下面创建此文件夹
//        System.out.println("dirPath="+dirPath);
//
//        File dir = new File(dirPath);
//        if(!dir.exists()) {
//            dir.mkdirs();
//        }
//
//        //2.确定保存的文件名
//        String orginalFilename = file.getOriginalFilename();
//        int beginIndex = orginalFilename.lastIndexOf(".");
//        String suffix ="";
//        if(beginIndex!=-1) {
//            suffix = orginalFilename.substring(beginIndex);
//        }
//        String filename = UUID.randomUUID().toString().replaceAll("-","")+suffix;
//        //创建文件对象，表示要保存的头像文件,第一个参数表示存储的文件夹，第二个参数表示存储的文件
//        File dest = new File(dir,filename);
//        //执行保存?
//        file.transferTo(dest);
//
//        //更新数据表
//        String avatar = contextPath+filename;
//
//
//        System.out.println("==========================================avatar"+avatar);

        //-----------------------------------------------------------------------------------

        String avatar = FilesUpload.setWithUuidSuffixPath(contextPath, request, file);
        //-----------------------------------------------------------------------------------

        //初始化 Users实体类 并添加Email和avatar的地址位置
        Admins pro = new Admins();
        //（头像不存储在数据库）删除旧头像
        Admins admins = adminsService.queryByEmail(uid);
        if (!admins.getAvatar().isEmpty()) {
            FilesDelete.delete(admins.getAvatar(), request);
        }

        //设置新头像
        pro.setEmail(uid);
        pro.setAvatar(avatar);
        adminsService.avatar(pro);

        map.put("sta", "ok");
        map.put("msg", avatar);
        return map;

    }


    /**
     * 首页 导航
     * --------------------------------------------------------------------------------------------------------
     */
    /**
     * 跳转到欢迎首页
     *
     * @return
     */
    @RequestMapping("/admin/welcome")
    public String welcome() {
        return "admin/welcome";
    }

    /**
     * 跳转到书籍首页
     *
     * @return
     */
    @RequestMapping("/admin/homeBook")
    public String homeBook() {
        return "admin/homeBook";
    }

    /**
     * 跳转到博客首页
     *
     * @return
     */
    @RequestMapping("/admin/homeArticle")
    public String homeArticle() {
        return "admin/homeArticle";
    }

    /**
     * 用户管理 导航
     * --------------------------------------------------------------------------------------------------------
     */
    /**
     * 跳转到上传操作页面
     *
     * @return
     */
    @RequestMapping("/admin/uploadOperation")
    public String uploadOperation() {
        return "admin/uploadOperation";
    }

    //-----------------------------------------------------------------------------------------------------

    /**
     * 跳转到用户处罚列表
     *
     * @return
     */
    @RequestMapping("/admin/toUserPunish")
    public String toUserPublish() {
        return "admin/userPunish";
    }

    //----------------------------------------------------------------------------------------------------

    /**
     * 跳转到用户管理
     *
     * @return
     */
    @RequestMapping("/admin/userManage")
    public String userManage() {
        return "admin/userManage";
    }

    /**
     * 博客管理 导航
     * ---------------------------------------------------------------------------------------------------------
     */
    /**
     * 跳转到editormd在线编辑器
     *
     * @return
     */
    @RequestMapping("/admin/toEditorArticle")
    public String toEditorArticle() {
        return "admin/editorMd";
    }
    //---------------------------------------------------------------------------------------------------------

    /**
     * 跳转到博客审核列表
     *
     * @return
     */
    @RequestMapping("/admin/toAuditArticle")
    public String toArticleAudit() {
        return "admin/auditArticle";
    }

    /**
     * 跳转到书籍审核列表
     *
     * @return
     */
    @RequestMapping("/admin/toAuditBook")
    public String toAuditBook() {
        return "admin/auditBook";
    }

    /**
     * 跳转到媒体文件审核列表
     *
     * @return
     */
    @RequestMapping("/admin/toAuditMedia")
    public String toAuditMedia() {
        return "admin/auditMedia";
    }

    /**
     * 跳转到电子书审核列表
     *
     * @return
     */
    @RequestMapping("/admin/toAuditEBook")
    public String toAuditEBook() {
        return "admin/auditEBook";
    }

    //---------------------------------------------------------------------------------------------------------

    /**
     * 跳转到图书借阅处理列表
     *
     * @return
     */
    @RequestMapping("/admin/toBorrowDispose")
    public String toBorrowDispose() {
        return "admin/borrowDispose";
    }

    /**
     * 跳转到图书预约处理列表
     *
     * @return
     */
    @RequestMapping("/admin/toSubscribeDispose")
    public String toSubscribeDispose() {
        return "admin/subscribeDispose";
    }

    /**
     * 跳转到遗失反馈处理列表
     *
     * @return
     */
    @RequestMapping("/admin/toLostFeedDispose")
    public String toLostFeedDispose() {
        return "admin/lostFeedDispose";
    }

    /**
     * 跳转到反馈处理列表
     *
     * @return
     */
    @RequestMapping("/admin/toFeedbackDispose")
    public String toFeedbackDispose() {
        return "admin/feedbackDispose";
    }

    //---------------------------------------------------------------------------------------------------------

    /**
     * 跳转到数据统计控制台
     *
     * @return
     */
    @RequestMapping("/admin/toController")
    public String toController() {
        return "admin/dataController";
    }

    /**
     * 跳转到博客数据列表
     *
     * @return
     */
    @RequestMapping("/admin/toArticle")
    public String toArticle() {
        return "admin/articles";
    }

    /**
     * 跳转到用户黑名单列表
     *
     * @return
     */
    @RequestMapping("/admin/toBlacklist")
    public String toBlacklist() {
        return "admin/blacklists";
    }

    /**
     * 跳转到电子书数据列表
     *
     * @return
     */
    @RequestMapping("/admin/toEBook")
    public String toEBook() {
        return "admin/eBooks";
    }

    /**
     * 跳转到图书数据列表
     *
     * @return
     */
    @RequestMapping("/admin/toBook")
    public String toBook() {
        return "admin/books";
    }

    /**
     * 跳转到借阅数据列表
     *
     * @return
     */
    @RequestMapping("/admin/toBorrow")
    public String toBorrow() {
        return "admin/borrows";
    }

    /**
     * 跳转到下载数据列表
     *
     * @return
     */
    @RequestMapping("/admin/toDownload")
    public String toDownload() {
        return "admin/downloads";
    }

    /**
     * 跳转到反馈数据列表
     *
     * @return
     */
    @RequestMapping("/admin/toFeedback")
    public String toFeedback() {
        return "admin/feedbacks";
    }

    /**
     * 跳转到登录日志数据列表
     *
     * @return
     */
    @RequestMapping("/admin/toLoginLog")
    public String toLoginLog() {
        return "admin/loginLogs";
    }

    /**
     * 跳转到遗失申报数据列表
     *
     * @return
     */
    @RequestMapping("/admin/toLostFeed")
    public String toLostFeed() {
        return "admin/lostFeeds";
    }

    /**
     * 跳转到媒体文件数据列表
     *
     * @return
     */
    @RequestMapping("/admin/toMedia")
    public String toMedia() {
        return "admin/medias";
    }

    /**
     * 跳转到预约数据列表
     *
     * @return
     */
    @RequestMapping("/admin/toSubscribe")
    public String toSubscribe() {
        return "admin/subscribes";
    }

    /**
     * 跳转到上传数据列表
     *
     * @return
     */
    @RequestMapping("/admin/toUpload")
    public String toUpload() {
        return "admin/uploads";
    }

    /**
     * 跳转到用户列表
     *
     * @return
     */
    @RequestMapping("/admin/toUser")
    public String toUser() {
        return "admin/users";
    }

    /**
     * 审核处理
     * --------------------------------------------------------------------------------------------------------
     */
    /**
     * 博客审核列表
     *
     * @param state
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/auditArticle")
    public ModelAndView listAuditArticle(String state,
                                         @RequestParam(defaultValue = "1") int page,
                                         @RequestParam(defaultValue = "10") int size) {
        state = "0";
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<BlogRecord> list = blogService.listAllAudit(state, page, size);
        PageInfo<BlogRecord> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/auditArticle");
        return mv;
    }

    /**
     * 媒体文件审核列表
     *
     * @param state
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/auditMedia")
    public ModelAndView listAuditMedia(String state,
                                       @RequestParam(defaultValue = "1") int page,
                                       @RequestParam(defaultValue = "10") int size) {
        state = "0";
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Media> list = mediaService.listAllAudit(state, page, size);
        PageInfo<Media> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/auditMedia");
        return mv;
    }

    /**
     * 电子书审核列表
     *
     * @param state
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/auditEBook")
    public ModelAndView listAuditEBook(String state,
                                       @RequestParam(defaultValue = "1") int page,
                                       @RequestParam(defaultValue = "10") int size) {
        state = "0";
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<EBooks> list = eBooksService.listAllAudit(state, page, size);
        PageInfo<EBooks> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/auditEBook");
        return mv;
    }

    /**
     * 书籍审核列表
     * @param state
     * @param page
     * @param size
     * @return
     */
//    @ResponseBody
//    @RequestMapping("/admin/auditBook")
//    public ModelAndView listAuditBook(String state,
//                                         @RequestParam(defaultValue = "1") int page,
//                                         @RequestParam(defaultValue = "10") int size){
//        state = "0";
//        PageHelper.startPage(page, size);
//        ModelAndView mv = new ModelAndView();
//        List<Books> list = bookService.listAuditBook(state,page.size);
//        PageInfo<Books> pageInfo = new PageInfo<>(list);
//        mv.addObject("pageInfo",pageInfo);
//
//        return mv;
//    }
    //---------------------------------------------------------------------------------------------------------

    /**
     * 借阅处理列表
     * @param page
     * @param size
     * @return
     */
//    @ResponseBody
//    @RequestMapping("/admin/")
//    public ModelAndView listBorrowDispose(String result,
//                                          @RequestParam(defaultValue = "1") int page,
//                                          @RequestParam(defaultValue = "10") int size){
//        result = "0";
//        PageHelper.startPage(page, size);
//        ModelAndView mv = new ModelAndView();
//        List<Borrow> list = borrowService.listAllDispose(result,page,size);
//        PageInfo<Borrow> pageInfo = new PageInfo<>(list);
//        mv.addObject("pageInfo",pageInfo);
//
//        return mv;
//    }

    /**
     * 预约处理列表
     * @param page
     * @param size
     * @return
     */
    //@ResponseBody
//    @RequestMapping("/admin/")
//    public ModelAndView listSubscribeDispose(String result,
//                                             @RequestParam(defaultValue = "1") int page,
//                                             @RequestParam(defaultValue = "10") int size){
//        result = "0";
//        PageHelper.startPage(page, size);
//        ModelAndView mv = new ModelAndView();
//        List<Subscribe> list = subscribeService.listAllDispose(result,page,size);
//        PageInfo<Subscribe> pageInfo = new PageInfo<>(list);
//        mv.addObject("pageInfo",pageInfo);
//
//        return mv;
//    }

    /**
     * 反馈处理列表
     * @param page
     * @param size
     * @return
     */
//    @RequestMapping("/admin/")
//    public ModelAndView listFeedbackDispose(String result,
//                                            @RequestParam(defaultValue = "1") int page,
//                                            @RequestParam(defaultValue = "10") int size){
//        result = "0";
//        PageHelper.startPage(page, size);
//        ModelAndView mv = new ModelAndView();
//        List<Feedback> list = feedbackService.listAllDispose(result,page,size);
//        PageInfo<Feedback> pageInfo = new PageInfo<>(list);
//        mv.addObject("pageInfo",pageInfo);
//
//        return mv;
//    }

    /**
     * 遗失处理列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/lostFeedDispose")
    public ModelAndView listLostFeedDispose(String result,
                                            @RequestParam(defaultValue = "1") int page,
                                            @RequestParam(defaultValue = "10") int size) {
        result = "0";
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<LostFeed> list = lostFeedService.listAllDispose(result, page, size);
        PageInfo<LostFeed> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/lostFeedDispose");
        return mv;
    }

    /**
     * 用户管理
     * --------------------------------------------------------------------------------------------------------
     */

    /**
     * 用户黑名单列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/blacklist")
    public ModelAndView listBlacklist(@RequestParam(defaultValue = "1") int page,
                                      @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Blacklist> list = blacklistService.listAll(page, size);
        PageInfo<Blacklist> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/blacklists");
        return mv;
    }

    //--------------------------------------------------------------------------------------------------------

    /**
     * 反馈数据列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/feedback")
    public ModelAndView listFeedback(@RequestParam(defaultValue = "1") int page,
                                     @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Feedback> list = feedbackService.listAllFeedback(page, size);
        PageInfo<Feedback> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/feedbacks");
        return mv;
    }

    /**
     * 遗失申报列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/lostFeed")
    public ModelAndView listLostFeed(@RequestParam(defaultValue = "1") int page,
                                     @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<LostFeed> list = lostFeedService.listAllLostFeed(page, size);
        PageInfo<LostFeed> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/lostFeeds");
        return mv;
    }

    /**
     * 图书预约数据列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/subscribe")
    public ModelAndView listSubscribe(@RequestParam(defaultValue = "1") int page,
                                      @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Subscribe> list = subscribeService.listAllSubscribe(page, size);
        PageInfo<Subscribe> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/subscribes");
        return mv;
    }

    //--------------------------------------------------------------------------------------------------------

    /**
     * 博客数据列表
     *
     * @param page
     * @param size
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/article")
    public ModelAndView listArticle(@RequestParam(defaultValue = "1") int page,
                                    @RequestParam(defaultValue = "10") int size,
                                    HttpSession session, Model model) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<BlogRecord> list = blogService.listAllBlog(page, size);
        PageInfo<BlogRecord> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/articles");
        model.addAttribute("blogPages", pageInfo.getPages());
        System.out.println("blogPages=======================================p=" + pageInfo.getPages());
        return mv;
    }

    /**
     * 电子书数据列表
     *
     * @param page
     * @param size
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/eBook")
    public ModelAndView listEBook(@RequestParam(defaultValue = "1") int page,
                                  @RequestParam(defaultValue = "10") int size,
                                  HttpSession session, Model model) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<EBooks> list = eBooksService.listAllEBook(page, size);
        PageInfo<EBooks> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/eBooks");
        model.addAttribute("pages", pageInfo.getPages());

        return mv;
    }

    /**
     * 图书数据列表
     *
     * @param page
     * @param size
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/book")
    public ModelAndView listBook(@RequestParam(defaultValue = "1") int page,
                                 @RequestParam(defaultValue = "10") int size,
                                 HttpSession session, Model model) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Books> list = booksService.listAllBooks(page, size);
        PageInfo<Books> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/books");
        model.addAttribute("pages", pageInfo.getPages());
        System.out.println("pages=======================================p=" + pageInfo.getPages());
        return mv;
    }

    /**
     * 博客数据列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/borrow")
    public ModelAndView listBorrow(@RequestParam(defaultValue = "1") int page,
                                   @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Borrow> list = borrowService.listAllBorrow(page, size);
        PageInfo<Borrow> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/borrows");
        return mv;
    }

    /**
     * 下载数据列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/download")
    public ModelAndView listDownload(@RequestParam(defaultValue = "1") int page,
                                     @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<DownloadRecord> list = downloadService.listAll(page, size);
        PageInfo<DownloadRecord> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/downloads");
        return mv;
    }

    /**
     * 登入日志列表
     *
     * @param page
     * @param size
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/loginLog")
    public ModelAndView listLoginLog(@RequestParam(defaultValue = "1") int page,
                                     @RequestParam(defaultValue = "10") int size,
                                     HttpSession session, Model model) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<LoginLog> list = loginLogService.listAllLoginLog(page, size);
        PageInfo<LoginLog> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/loginLogs");
        model.addAttribute("pages", pageInfo.getPages());
        return mv;
    }

    /**
     * 媒体文件数据列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/media")
    public ModelAndView listMedia(@RequestParam(defaultValue = "1") int page,
                                  @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Media> list = mediaService.listAllMedia(page, size);
        PageInfo<Media> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/medias");
        return mv;
    }

    /**
     * 上传数据列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/upload")
    public ModelAndView listUpload(@RequestParam(defaultValue = "1") int page,
                                   @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<UploadRecord> list = uploadService.listAll(page, size);
        PageInfo<UploadRecord> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/uploads");
        return mv;
    }

    //-------------------------------------------------------------------------------------------------------

    /**
     * 用户列表
     *
     * @param page
     * @param size
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/user")
    public ModelAndView listUser(@RequestParam(defaultValue = "1") int page,
                                 @RequestParam(defaultValue = "10") int size,
                                 HttpSession session, Model model) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Users> list = usersService.listAllUser(page, size);
        PageInfo<Users> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/users");
        model.addAttribute("pages", pageInfo.getPages());
        return mv;
    }

    /**
     * 管理员列表
     * @param page
     * @param size
     * @param session
     * @return
     */
//    @ResponseBody
//    @RequestMapping("admin/admin")
//    public ModelAndView<> listAdmin(@RequestParam(defaultValue = "1") int page,
//                                  @RequestParam(defaultValue = "10") int size){
//        PageHelper.startPage(page, size);
//        ModelAndView mv = new ModelAndView();
//        List<Admins> list ="";
//        PageInfo<Admins> pageInfo = new PageInfo<>(list);
//        mv.addObject("pageInfo",pageInfo);
//        mv.setViewName("admin/");
//        return mv;
//    }

    /**
     *--------------------------------------------------------------------------------------------------------
     */
    /**
     * 配置文档
     *
     * @return
     */
    @RequestMapping("/admin/doc")
    public String doc() {
        return "admin/document";
    }

    /**
     * 阅读文档
     *
     * @return
     */
    @RequestMapping("/admin/readMe")
    public String readMe() {
        return "admin/readMe";
    }

    //--------------------------------------------------------------------------------------------------------

    /**
     * 设置
     *
     * @return
     */
    @RequestMapping("/admin/setting")
    public String setting() {
        return "admin/setting";
    }

    //--------------------------------------------------------------------------------------------------------

    /**
     * 关于
     *
     * @return
     */
    @RequestMapping("/admin/about")
    public String about() {
        return "admin/about";
    }

    /**
     * browse
     * -------------------------------------------------------------------------------------------------------
     */

    /**
     * 查看博客
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/admin/browseArticle/{id}")
    public String browseArticle(@PathVariable int id, Model model, HttpSession session) {
        BlogRecord article = blogService.queryById(id);
        model.addAttribute("article", article);
        session.setAttribute("articles", article);
        return "admin/browseArticle";
    }

    /**
     * 查看电子书
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/admin/browseEBook/{id}")
    public String browseEBook(@PathVariable int id, Model model) {
        EBooks eBooks = eBooksService.queryById(id);
        model.addAttribute("eBook", eBooks);
        return "admin/browseEBook";
    }

    /**
     * 查看图书
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/admin/browseBook/{id}")
    public String browseBook(@PathVariable int id, Model model) {
        Books eBooks = booksService.queryById(id);
        model.addAttribute("book", eBooks);
        return "admin/browseBook";
    }

    /**
     * 查看遗失反馈
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/admin/browseLostFeed/{id}")
    public String browseLostFeed(@PathVariable int id, Model model) {
        LostFeed lostFeed = lostFeedService.queryById(id);
        model.addAttribute("article", lostFeed);
        return "admin/browseLostFeed";
    }

    /**
     * 查看媒体文件
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/admin/browseMedia/{id}")
    public String browseMedia(@PathVariable int id, Model model) {
        Media media = mediaService.queryById(id);
        model.addAttribute("article", media);
        return "admin/browseArticle";
    }

    /**
     * audit result
     * -------------------------------------------------------------------------------------------------------
     */

    /**
     * 博客审核结果
     *
     * @param result
     * @param articleId
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/articleAuditResult")
    public JSONObject articleAuditResult(String result, int articleId) {
        JSONObject jsonObject = new JSONObject();
        System.out.println("=================================result=:" + result);
        System.out.println("=================================articleId=:" + articleId);
        if ("1".equals(result)) {
            blogService.push(articleId, result);
            jsonObject.put("code", "1");
            return jsonObject;
        } else {
            if ("2".equals(result)) {
                blogService.push(articleId, result);
            } else {
                jsonObject.put("msg", "失败！");
            }
        }
        jsonObject.put("code", "1");
        return jsonObject;
    }

    /**
     * 电子书审核结果
     *
     * @param result
     * @param eBookId
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/eBookAuditResult")
    public JSONObject eBookAuditResult(String result, int eBookId) {
        JSONObject jsonObject = new JSONObject();
        System.out.println("=================================result=:" + result);
        System.out.println("=================================eBookId=:" + eBookId);

        if ("1".equals(result)) {
            eBooksService.push(eBookId, result);
            jsonObject.put("code", "1");
            return jsonObject;
        } else {
            if ("2".equals(result))
                eBooksService.push(eBookId, result);
            else {
                jsonObject.put("msg", "失败！");
            }
        }

        return jsonObject;
    }

    /**
     * 媒体文件审核结果
     *
     * @param result
     * @param mediaId
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/mediaAuditResult")
    public JSONObject mediaAuditResult(String result, int mediaId) {
        JSONObject jsonObject = new JSONObject();
        if ("1".equals(result)) {
            mediaService.push(mediaId, result);
            jsonObject.put("code", "1");
            return jsonObject;
        } else {
            if ("2".equals(result)) {
                mediaService.push(mediaId, result);
            } else {
                jsonObject.put("msg", "失败！");
            }
        }

        return jsonObject;
    }

    /**
     * modify
     * -------------------------------------------------------------------------------------------------------
     */

    /**
     * 处理遗失申报
     *
     * @param lostId
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/modifyLostFeedResult")
    public JSONObject modifyLostFeedResult(int lostId) {
        JSONObject jsonObject = new JSONObject();
        LostFeed lostFeed = lostFeedService.queryById(lostId);

        jsonObject.put("code", "1");
        return jsonObject;
    }

    /**
     * 查询
     * -------------------------------------------------------------------------------------------------------
     */

    /**
     * 博客审核列表
     *
     * @param state
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/auditArticle/{state}")
    public ModelAndView listQueryAuditArticle(@PathVariable String state,
                                              @RequestParam("search") String name,
                                              @RequestParam(defaultValue = "1") int page,
                                              @RequestParam(defaultValue = "10") int size) {

        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<BlogRecord> list = blogService.listQueryAuditArticle(state, name, page, size);
        PageInfo<BlogRecord> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/auditArticle");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 媒体文件审核列表
     *
     * @param state
     * @param page
     * @param size
     * @param name
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/auditMedia/{state}")
    public ModelAndView listQueryAuditMedia(@PathVariable String state,
                                            @RequestParam("search") String name,
                                            @RequestParam(defaultValue = "1") int page,
                                            @RequestParam(defaultValue = "10") int size) {

        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Media> list = mediaService.listQueryAllAudit(state, name, page, size);
        PageInfo<Media> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/auditMedia");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 电子书审核列表
     *
     * @param state
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/auditEBook/{state}")
    public ModelAndView listQueryAuditEBook(@PathVariable String state,
                                            @RequestParam("search") String name,
                                            @RequestParam(defaultValue = "1") int page,
                                            @RequestParam(defaultValue = "10") int size) {

        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<EBooks> list = eBooksService.listAuditEBook(state, name, page, size);
        PageInfo<EBooks> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/auditEBook");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 用户黑名单列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/query/blacklist")
    public ModelAndView listQueryBlacklist(@RequestParam("search") String name,
                                           @RequestParam(defaultValue = "1") int page,
                                           @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Blacklist> list = blacklistService.queryByName(name, page, size);
        PageInfo<Blacklist> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/blacklists");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 遗失处理列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/lostFeedDispose/{result}")
    public ModelAndView listQueryLostFeedDispose(@PathVariable String result,
                                                 @RequestParam("search") String name,
                                                 @RequestParam(defaultValue = "1") int page,
                                                 @RequestParam(defaultValue = "10") int size) {

        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<LostFeed> list = lostFeedService.listQueryAllDispose(result, name, page, size);
        PageInfo<LostFeed> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/lostFeedDispose");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 博客数据列表
     *
     * @param page
     * @param size
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/query/article")
    public ModelAndView listQueryArticle(@RequestParam(defaultValue = "1") int page,
                                         @RequestParam("search") String name,
                                         @RequestParam(defaultValue = "10") int size,
                                         HttpSession session, Model model) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<BlogRecord> list = blogService.queryByName(name, page, size);
        PageInfo<BlogRecord> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/articles");
        model.addAttribute("blogPages", pageInfo.getPages());
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 电子书数据列表
     *
     * @param page
     * @param size
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/query/eBook")
    public ModelAndView listQueryEBook(@RequestParam(defaultValue = "1") int page,
                                       @RequestParam("search") String name,
                                       @RequestParam(defaultValue = "10") int size,
                                       HttpSession session, Model model) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<EBooks> list = eBooksService.queryByName(name, page, size);
        PageInfo<EBooks> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/eBooks");
        model.addAttribute("pages", pageInfo.getPages());
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 图书预约数据列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/query/subscribe")
    public ModelAndView listQuerySubscribe(@RequestParam(defaultValue = "1") int page,
                                           @RequestParam("search") String name,
                                           @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Subscribe> list = subscribeService.queryByName(name, page, size);
        PageInfo<Subscribe> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/subscribes");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 反馈数据列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/query/feedback")
    public ModelAndView listQueryFeedback(@RequestParam(defaultValue = "1") int page,
                                          @RequestParam("search") String name,
                                          @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Feedback> list = feedbackService.queryByName(name, page, size);
        PageInfo<Feedback> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/feedbacks");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 遗失申报列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/query/lostFeed")
    public ModelAndView listQueryLostFeed(@RequestParam(defaultValue = "1") int page,
                                          @RequestParam("search") String name,
                                          @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<LostFeed> list = lostFeedService.queryByName(name, page, size);
        PageInfo<LostFeed> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/lostFeeds");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 图书数据列表
     *
     * @param page
     * @param size
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/query/book")
    public ModelAndView listQueryBook(@RequestParam(defaultValue = "1") int page,
                                      @RequestParam("search") String name,
                                      @RequestParam(defaultValue = "10") int size,
                                      HttpSession session, Model model) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Books> list = booksService.queryByName(name, page, size);
        PageInfo<Books> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/books");
        model.addAttribute("pages", pageInfo.getPages());
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 媒体文件数据列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/query/media")
    public ModelAndView listQueryMedia(@RequestParam(defaultValue = "1") int page,
                                       @RequestParam("search") String name,
                                       @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Media> list = mediaService.queryByName(name, page, size);
        PageInfo<Media> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/medias");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 下载数据列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/query/download")
    public ModelAndView listQueryDownload(@RequestParam(defaultValue = "1") int page,
                                          @RequestParam("search") String name,
                                          @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<DownloadRecord> list = downloadService.queryByName(name, page, size);
        PageInfo<DownloadRecord> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/downloads");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 上传数据列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/query/upload")
    public ModelAndView listQueryUpload(@RequestParam(defaultValue = "1") int page,
                                        @RequestParam("search") String name,
                                        @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<UploadRecord> list = uploadService.queryByName(name, page, size);
        PageInfo<UploadRecord> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/uploads");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 博客数据列表
     *
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/query/borrow")
    public ModelAndView listQueryBorrow(@RequestParam(defaultValue = "1") int page,
                                        @RequestParam("search") String name,
                                        @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Borrow> list = borrowService.queryByName(name, page, size);
        PageInfo<Borrow> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/borrows");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * 登入日志列表
     *
     * @param page
     * @param size
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/query/loginLog")
    public ModelAndView listQueryLoginLog(@RequestParam(defaultValue = "1") int page,
                                          @RequestParam("search") String name,
                                          @RequestParam(defaultValue = "10") int size,
                                          HttpSession session, Model model) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<LoginLog> list = loginLogService.listAllLoginLog(page, size);
        PageInfo<LoginLog> pageInfo = new PageInfo<>(list);
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("admin/loginLogs");
        mv.addObject("search", name);
        return mv;
    }

    /**
     * userManage
     * -------------------------------------------------------------------------------------------------------
     */

    /**
     * 修改密码
     *
     * @param users
     * @return
     */
    @ResponseBody
    @RequestMapping("/user/modifyPwd")
    public ModelAndView modifyUserPwd(@PathVariable Users users) {
        ModelAndView mv = new ModelAndView();
        JSONObject jsonObject = new JSONObject();
        Users msg = usersService.queryByEmail(users.getEmail());
        if (!users.getPassword().equals(msg.getPassword())) {
            usersService.adminModifyPwd(users);
            mv.addObject("msg", "成功");
            mv.setViewName("admin/userManage");
            return mv;
        }
        mv.addObject("msg", "失败");
        mv.setViewName("admin/userManage");
        return mv;
    }

    /**
     * 修改联系方式
     *
     * @param users
     * @return
     */
    @RequestMapping("/user/modifyTel")
    public ModelAndView modifyTel(@PathVariable Users users) {
        ModelAndView mv = new ModelAndView();
        Users msg = usersService.queryByEmail(users.getEmail());
        if (msg.getTelephone() == null || users.getTelephone().equals(msg.getTelephone())) {
            usersService.adminModifyTel(users);
            mv.addObject("telMsg", "成功");
            mv.setViewName("admin/userManage");
            return mv;
        }
        mv.addObject("telMsg", "失败");
        mv.setViewName("admin/userManage");
        return mv;
    }


    /**
     * 实名认证
     *
     * @param users
     * @return
     */
    @RequestMapping("/user/realUser")
    public ModelAndView realUser(@PathVariable Users users) {
        ModelAndView mv = new ModelAndView();
        usersService.adminRealUser(users);
        mv.addObject("success", "成功");
        mv.setViewName("admin/userManage");
        return mv;
    }

    /**
     * 用户注册
     *
     * @param user
     * @param request
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/user/register", produces = "text/plain;charset=utf-8")
    public JSONObject register(@ModelAttribute Users user, HttpServletRequest request, Model model) {
        user.setCreateTime(new Date());
        JSONObject jsonObject = new JSONObject();
        // 获取用户传递进来的验证码
        String code = request.getParameter("checkCode");


        String sessionCode = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        System.out.println("------------------------------sessionCode---------" + sessionCode);
        if (StringUtils.isNotEmpty(code)) {
            if (code.equalsIgnoreCase(sessionCode)) {
                usersService.register(user);
                System.out.println("=================================新注册用户" + user.getEmail());
                jsonObject.put("code","1");
                return jsonObject;
            } else {
                model.addAttribute("error", "验证码有误！");
                jsonObject.put("code","0");
                return jsonObject;
            }
        }
        model.addAttribute("error", "验证码为空！");
        jsonObject.put("code","0");
        return jsonObject;
    }

    /**
     * 注销账户（删除用户信息）
     *
     * @param request
     * @return
     */
    @RequestMapping("/user/unsubscribe")
    public ModelAndView unsubscribe(Users uid, HttpServletRequest request) {
        Users msg = usersService.queryByEmail(uid.getEmail());
        ModelAndView mv = new ModelAndView();
        usersService.logout(msg);
        String success = msg.getEmail() + "用户注销成功！";
        mv.addObject("success", success);
        mv.setViewName("../../index");
        return mv;
    }


}
