package cloud.xhmy.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;
import java.util.Date;
/**
 * 遗失申报
 * lostFeed
 * @author aliketh.xhmy
 */
public class LostFeed {

    private int id;
    private String lostBook;
    private String lostUser;
    private String lostTime;
    private String processResults;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Timestamp feedTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLostBook() {
        return lostBook;
    }

    public void setLostBook(String lostBook) {
        this.lostBook = lostBook;
    }

    public String getLostUser() {
        return lostUser;
    }

    public void setLostUser(String lostUser) {
        this.lostUser = lostUser;
    }

    public String getLostTime() {
        return lostTime;
    }

    public void setLostTime(String lostTime) {
        this.lostTime = lostTime;
    }

    public String getProcessResults() {
        return processResults;
    }

    public void setProcessResults(String processResults) {
        this.processResults = processResults;
    }

    public Timestamp getFeedTime() {
        return feedTime;
    }

    public void setFeedTime(Timestamp feedTime) {
        this.feedTime = feedTime;
    }
}
