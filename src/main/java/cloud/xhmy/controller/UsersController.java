package cloud.xhmy.controller;

import cloud.xhmy.pojo.JsonReturnMsg;
import cloud.xhmy.pojo.Users;
import cloud.xhmy.service.UsersService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.UnknownHostException;
import java.util.*;

/**
 * @author aliketh.xhmy
 * http://www.manongjc.com/article/5310.html
 * https://www.cnblogs.com/zhaoyingjie/p/8503154.html
 */
@Controller
public class UsersController {

    @Autowired
    @Qualifier("usersServiceImpl")
    private UsersService usersService;


    /**
     * 用户登录
     *
     * @param user
     * @param request
     * @param model   用于传递参数
     * @param session 用于登录状态保持也可用token（建议）
     * @return
     * @throws UnknownHostException
     */
    @ResponseBody
    @RequestMapping("/login")
    public JSONObject login(@ModelAttribute Users user, HttpServletRequest request, Model model,
                            HttpSession session) throws UnknownHostException {
        JSONObject jsonObject = new JSONObject();
        if (usersService.login(user) != null) {
            Users loginMsg = usersService.login(user);
            // 用于上传头像，以及用户邮箱传递
            String proMsg = loginMsg.getEmail();
            session.setAttribute("avatarEmail", proMsg);
            // 用于页面传递信息
            session.setAttribute("user", loginMsg);
            // 用于传递登录状态1为成功0为失败
            session.setAttribute("code", "1");
            jsonObject.put("code", "1");
            return jsonObject;
        } else {

            // 用于传递登录状态1为成功0为失败
            session.setAttribute("code", "0");
            model.addAttribute("err", "用户名，密码不能为空");
            jsonObject.put("code", "0");
            jsonObject.put("msg", "用户名，密码不能为空");
            return jsonObject;
        }
        /*
        // 获取用户传递进来的验证码
        String code = request.getParameter("checkCode");

        System.out.println("------------------------------codeMsg---------" + code);

        String sessionCode = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        System.out.println("------------------------------sessionCode---------" + sessionCode);
        if (StringUtils.isNotEmpty(code) && usersService.login(user) != null) {
            Users loginMsg = usersService.login(user);
            // 用于上传头像，以及用户邮箱传递
            String proMsg = loginMsg.getEmail();
            session.setAttribute("avatarEmail", proMsg);
            // 用于页面传递信息
            session.setAttribute("user", loginMsg);
            // 用于传递登录状态1为成功0为失败
            session.setAttribute("code", "1");


            if (code.equalsIgnoreCase(sessionCode)) {

                System.out.println("=============================================" + "\n" +
                        "=====================登入成功");
                jsonObject.put("code", "1");
            } else {
                System.out.println("验证码不正确");
                // 用于传递登录状态1为成功0为失败
                session.setAttribute("code", "0");
                model.addAttribute("msg", "验证码不正确");

                System.out.println("=============================================" + "\n" +
                        "=====================登入失败：验证码不正确");
                jsonObject.put("code", "0");
                jsonObject.put("msg", "验证码不正确");
                return jsonObject;
            }

        } else {

            // 用于传递登录状态1为成功0为失败
            session.setAttribute("code", "0");
            model.addAttribute("err", "用户名，密码，验证码不能为空");

            System.out.println("=============================================" + "\n" +
                    "=====================登入失败：用户名，密码，验证码不能为空");
            jsonObject.put("code", "0");
            jsonObject.put("msg", "用户名，密码，验证码不能为空");
            return jsonObject;
        }
        return jsonObject;
         */

    }

    /**
     * 用户注册
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/register", produces = "text/plain;charset=utf-8")
    public String register(@ModelAttribute Users user, HttpServletRequest request, Model model) {
        user.setCreateTime(new Date());
        // 获取用户传递进来的验证码
        String code = request.getParameter("checkCode");

        System.out.println("------------------------------codeMsg---------" + code);

        String sessionCode = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        System.out.println("------------------------------sessionCode---------" + sessionCode);
        if (StringUtils.isNotEmpty(code)) {
            if (code.equalsIgnoreCase(sessionCode)) {
                usersService.register(user);
                System.out.println("=================================新注册用户" + user.getEmail());
                return "user/login";
            } else {
                model.addAttribute("error", "验证码有误！");
                System.out.println("=================================注册用户" + user.getEmail() + "失败！" + "\n" +
                        "原因：验证码有误！");
                return "user/register";
            }
        }
        model.addAttribute("error", "验证码为空！");
        System.out.println("=================================注册用户" + user.getEmail() + "失败！" + "\n" +
                "原因：验证码为空！");
        return "user/register";
    }

    /**
     * 用户找回密码
     *
     * @param user
     * @return
     */
    @ResponseBody
    @RequestMapping("/forgot")
    public JsonReturnMsg forgot(@ModelAttribute Users user, HttpServletRequest request, Model model) {
        JsonReturnMsg jsonReturnMsg = new JsonReturnMsg();
        // 获取用户传递进来的验证码
        String code = request.getParameter("checkCode");

        System.out.println("------------------------------codeMsg---------" + code);

        String sessionCode = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        if (StringUtils.isEmpty(code)) {
            if (code.equalsIgnoreCase(sessionCode)) {
                usersService.forgot(user);
                System.out.println("=================================用户" + user.getEmail() + "修改密码成功！");
                jsonReturnMsg.setCodes("200");
                jsonReturnMsg.setMessages("修改密码成功！");
                return jsonReturnMsg;
            } else {
                model.addAttribute("error", "验证码有误！");
                System.out.println("=================================用户" + user.getEmail() + "修改密码失败！" + "\n" +
                        "原因：验证码有误！");
                jsonReturnMsg.setCodes("400");
                jsonReturnMsg.setMessages("修改密码失败！原因：验证码有误！");
            }
        }
        model.addAttribute("error", "验证码为空！");
        System.out.println("=================================用户" + user.getEmail() + "修改密码失败！" + "\n" +
                "原因：验证码为空！");


        jsonReturnMsg.setCodes("400");
        jsonReturnMsg.setMessages("修改密码成功！原因：验证码为空！");
//        System.out.println("=================================用户"+user.getEmail()+"修改密码成功！");
        return jsonReturnMsg;
    }

    /**
     * 用户退出登录
     *
     * @param session
     * @param request
     * @return
     */
    @RequestMapping("/exit")
    public String exit(HttpSession session, HttpServletRequest request) {
        String msg = request.getSession().getAttribute("avatarEmail").toString();
        System.out.println("=================================用户" + msg + "退出登录！");
        session.invalidate();
        return "user/login";
    }

    /**
     * 修改信息后刷新
     *
     * @param user
     * @param session
     */
    @RequestMapping("/refresh")
    public void refresh(Users user, HttpSession session) {

    }

    /**
     * 注销账户（删除用户信息）
     *
     * @param request
     * @return
     */
    @RequestMapping("/unsubscribe")
    public ModelAndView unsubscribe(Users uid, HttpServletRequest request) {
        Users msg = (Users) request.getSession().getAttribute("user");
        ModelAndView mv = new ModelAndView();
        usersService.logout(uid);
        String success = msg.getEmail() + "用户注销成功！";
        mv.addObject("success", success);
        mv.setViewName("../../index");
        return mv;
    }

    /**
     * 删除
     *
     * @param logId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete/user")
    public JSONObject notes(@RequestParam("id") List<Integer> logId) {
        List<Integer> list = logId;
        Users users = new Users();
        for (int i = 0; i < list.size(); i++) {
            int id = list.get(i);
            users.setUid(id);
            usersService.logout(users);
        }
//        return "user/listNotes";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", "1");
        return jsonObject;
    }

    /**
     * @return
     */
    @RequestMapping("/oneself")
    public String oneself() {
        return "user/selfPage";
    }

    /**
     * 修改联系方式
     *
     * @param newTel
     * @param request
     * @param uid
     * @param tel
     * @return
     */
    @ResponseBody
    @RequestMapping("/modifyTel")
    public JSONObject modifyTel(@RequestParam("newTelephone") BigInteger newTel,
                                HttpServletRequest request,
                                @RequestParam String uid,
                                @RequestParam("oldTelephone") BigInteger tel) {
        Users msg = (Users) request.getSession().getAttribute("user");
        JSONObject jsonObject = new JSONObject();
        ModelAndView mv = new ModelAndView();
        if (msg.getTelephone() == null || tel.equals(msg.getTelephone())) {
            usersService.modifyTel(newTel, uid);
            mv.addObject("telMsg", "成功");
            mv.setViewName("user/selfPage");
//            return mv;
            jsonObject.put("code", "1");
            return jsonObject;
        }
        mv.addObject("telMsg", "失败");
        mv.setViewName("user/selfPage");
//        return mv;
        jsonObject.put("code", "0");
        return jsonObject;
    }

    /**
     * 修改密码
     *
     * @param password
     * @param request
     * @param uid
     * @param newPassword
     * @return
     */
    @ResponseBody
    @RequestMapping("/modifyPwd")
    public JSONObject modifyPwd(@RequestParam("oldPassword") String password,
                                HttpServletRequest request,
                                String uid,
                                @RequestParam("newPassword") String newPassword) {
        Users msg = (Users) request.getSession().getAttribute("user");
        JSONObject jsonObject = new JSONObject();
        ModelAndView mv = new ModelAndView();
        if (msg.getPassword().equals(password)) {
            usersService.modifyPwd(newPassword, uid);
            mv.addObject("msg", "成功");
            mv.setViewName("user/selfPage");
//            return mv;
            jsonObject.put("code", "1");
            return jsonObject;
        }
        mv.addObject("msg", "失败");
        mv.setViewName("user/selfPage");
//        return mv;
        jsonObject.put("code", "0");
        return jsonObject;
    }

    /**
     * 实名认证
     *
     * @param realName
     * @param idNumber
     * @param uid
     * @return
     */
    @ResponseBody
    @RequestMapping("/realUser")
    public JSONObject realUser(String realName, String idNumber, String uid) {
        JSONObject jsonObject = new JSONObject();
        ModelAndView mv = new ModelAndView();
        usersService.realUser(realName, idNumber, uid);
        mv.addObject("success", "成功");
        mv.setViewName("user/selfPage");
//        return mv;
        jsonObject.put("code", "1");
        return jsonObject;
    }


    /**
     * 注册验证(更改验证)
     * <p>
     * 根据用户email查询
     *
     * @throws IOException
     */
    @RequestMapping("/judgeEmail")
    public void findBrandByEmail(String email, HttpServletResponse response) throws IOException {
        //初始化
        String message = "";
        boolean judge = true;
        //查询是否有输入的用户名
        List<Users> rel = usersService.judge(email);
        //如果为null（没有该用户email）即可用
        if (rel.isEmpty()) {
            message = "用户名可用";
        } else {
            judge = false;
            message = "用户名已经存在，请使用其他用户名";
        }

        //设置返回数据为utf-8
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<response>");
        out.println("<passed>" + Boolean.toString(judge) + "</passed>");
        out.println("<message>" + message + "</message>");
        out.println("</response>");
    }


    /*-----------------------------------------------------------------------------------------------*/
    @Autowired
    private Producer captchaProducer = null;

    /**
     * 验证码
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping("/code")
    public ModelAndView getKaptchaImage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        // 获取验证码
        //    String code = (String) session.getAttribute(Constants.KAPTCHA_SESSION_KEY);
        //    String code = (String) session.getAttribute("Kaptcha_Code");
        // 清除浏览器的缓存
        response.setDateHeader("Expires", 0);
        // Set standard HTTP/1.1 no-cache headers.
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        // Set standard HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache");
        // return a jpeg
        response.setContentType("image/jpeg");
        // 浏览器记忆功能-----当前过浏览器和服务器交互成功以后下载的图片和资源会进行缓存一次。下次刷新的时候就不会在到服务器去下载。
        // 获取KAPTCHA验证的随机文本


        String capText = captchaProducer.createText();
        // 将生成好的图片放入会话中
        session.setAttribute(Constants.KAPTCHA_SESSION_KEY, capText);
        // create the image with the text
        BufferedImage bi = captchaProducer.createImage(capText);
        ServletOutputStream out = response.getOutputStream();
        // write the data out
        ImageIO.write(bi, "jpg", out);
        try {
            out.flush();
        } finally {
            out.close();//关闭
        }
        return null;
    }


}
