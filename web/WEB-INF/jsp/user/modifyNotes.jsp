<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/31
  Time: 23:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>modifyNotes</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>


    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div style="width: 90%;margin: 5px auto;">
    <div class="row">
        <div><h3><span>修改笔记</span></h3></div>
        <div></div>

    </div>
    <form action="">
        <input type="hidden" id="id" name="id" value="${notes.id}">
        <span>笔记名称：</span><input class="form-control" id="notesName" name="notesName" value="${notes.notesName}" type="text"><br>
        <span>笔记作者：</span><input class="form-control" id="notesAuthor" name="notesAuthor" value="${notes.notesAuthor}" readonly type="text">
    </form>
</div>

<div style="margin: 10px auto ; text-align: center;">
    <h3><span>笔记内容</span></h3>
</div>
<div id="notes-modify">

    <textarea style="display:none;" class="form-control" id="mdDate" name="editormd"></textarea>

</div>


<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>

<script src="${pageContext.request.contextPath}/plugins/editor-md/editormd.min.js"></script>
<script type="text/javascript">

    $("#mdDate").load("${pageContext.request.contextPath}/static${notes.notesContent}");

    var draftArticle;
    $(function () {

        //$.get('${pageContext.request.contextPath}/static/upload/document.md', function(md){
        draftArticle = editormd("notes-modify", {
            width: "90%",
            height: 700,
            path: "${pageContext.request.contextPath}/plugins/editor-md/lib/",
            imageUpload: true,
            imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL: "${pageContext.request.contextPath}/upload/mdImages",
            saveHTMLToTextarea : true
        });
    });
    $("#saveDraft").click(function () {
        var data = $("#mdData").val();
        //alert(html);
        $.ajax({

            url:'${pageContext.request.contextPath}/',
            data:{
                'data':data,
                'notesName':$("#notesName").val(),
                'notesAuthor':$("#notesAuthor").val()

            },
            dataType:'text',
            type:'post',

            success:function (msg) {
                alert(msg);
            }
        })
    });
</script>
</body>
</html>
