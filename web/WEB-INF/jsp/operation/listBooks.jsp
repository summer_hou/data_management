<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/31
  Time: 21:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>listBooks</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>


    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/page-helper.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div class="" style="width: 95%; margin: 5px auto;">

    <div class="row">
        <div class="col-md-12 ">
            <div>
                <h3><span>书籍</span></h3>
            </div>
            <div style="margin-top: 10px;">
<%--                <button class="btn btn-danger" style="width: 10%; height: 38px;margin-left: 0px;margin-top: -2px;padding: unset;" type="button" onclick="delAllProduct()">删除</button>

                <form action="" style="display: inline;width: 90%;float: right;">
                    <input style="width: 90%;height: 38px; padding: 5px; border-radius: 2px; border: 1px solid #666666;" name="search"
                           value="" placeholder="输入要搜索的书名" type="search">
                    <button style="width: 10%; height: 40px;margin-left: -5px;" type="button">搜索</button>
                </form>
    --%>
            </div>
            <table class="table table-hover table-striped table-o">
                <thead>
                <tr>
                    <th><input type="checkbox" name="CheckAll" id="CheckAll"/></th>
                    <th>bookName</th>
                    <th>author</th>
                    <th>publishTime</th>
                    <th>language</th>
                    <th>publishHouse</th>
                    <th>classify</th>
                    <!-- <th>shelves</th> -->
                    <!-- <th>coverPicture</th> -->
                    <!-- <th>introduction</th> -->
                    <!-- <th>price</th> -->
                    <!-- <th>shelvesTime</th> -->
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="books" items="${pageInfos.list}">
                    <tr>
                        <td class="chk"><input type="checkbox" name="Check[]" value="${books.id}" id="Check[]"/></td>
                        <td>${books.bookName}</td>
                        <td>${books.author}</td>
                        <td>${books.publishTime}</td>
                        <td>${books.language}</td>
                        <td>${books.publishHouse}</td>
                        <td>${books.classify}</td>
                        <%--
                        <td>${books.shelves}</td>
                        <td>${books.coverPicture}</td>
                        <td>${books.introduction}</td>
                        <td>${books.price}</td>
                        <td>${books.shelvesTime}</td>
                        --%>
                        <td class="text-center">
                            <a href="${pageContext.request.contextPath}/borrow/${books.id}"
                               class="btn bg-olive btn-xs">借阅</a>
                            ||
                            <a href="${pageContext.request.contextPath}/browseBook/${books.id}"
                               class="btn bg-olive btn-xs">查看</a>

                        </td>
                    </tr>
                </c:forEach>

                </tbody>
                <tfoot>

                </tfoot>

            </table>

        </div>
    </div>
    <div class="xhmy_page_div"></div>
    <script type="text/javascript">
        //翻页
        $(".xhmy_page_div").createPage({
            pageNum: ${pageInfos.pages},
            current: 1,
            backfun: function(e) {
                //console.log(e);//回调
            }
        });
        function pageNumber(params) {
            $("#eBook").load("${pageContext.request.contextPath}/allBooks/${user.email} #eBook >*",{page:params,size:"10"});

        };
    </script>
</div>


<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>


</body>
</html>
