package cloud.xhmy.controller;

import cloud.xhmy.pojo.BlogRecord;
import cloud.xhmy.pojo.EBooks;
import cloud.xhmy.service.BlogService;
import cloud.xhmy.service.EBooksService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author aliketh.xhmy
 *
 */
@Controller
public class HomeController {

    @Autowired
    @Qualifier("eBooksServiceImpl")
    private EBooksService eBooksService;
    @Autowired
    @Qualifier("blogServiceImpl")
    private BlogService blogService;

    /**
     * 跳转到初始页面
     * @return
     */
    @RequestMapping("/index")
    public String index(){
        return "forward:../../index";
    }

    /**
     * 跳转到用户登录页面
     * @return
     */
    @RequestMapping("/toLogin")
    public String toLogin(){
        return "user/login";
    }

    /**
     * 跳转到用户注册页面
     * @return
     */
    @RequestMapping("/toRegister")
    public String toRegister(){
        return "user/register";
    }

    /**
     * 跳转到用户找回密码页面
     * @return
     */
    @RequestMapping("/toForgot")
    public String toForgot(){
        return "user/forgot";
    }

    /**
     * 关于
     * @return
     */
    @RequestMapping("/about")
    public String about(){
        return "doc/about";
    }

    /**
     * 设置
     * @return
     */
    @RequestMapping("/setting")
    public String setting(){
        return "user/setting";
    }

    /**
     * 阅读文档
     * @return
     */
    @RequestMapping("/reanme")
    public String readme(){
        return "reanMe";
    }

    /**
     * 配置文档
     * @return
     */
    @RequestMapping("/docx")
    public String docx(){
        return "doc/document";
    }

    /**
     * 控制台
     * @return
     */
    @RequestMapping("/controller")
    public String controller(){
        return "user/controller";
    }

    /*-------------------------------------------------------------------------------------------------------*/

    /**
     * 400
     * @return
     */
    @RequestMapping("/x400")
    public String x400(){
        return "static/x400";
    }

    /**
     * 404
     * @return
     */
    @RequestMapping("/x401")
    public String x401(){
        return "static/x401";
    }

    /**
     * 500
     * @return
     */
    @RequestMapping("/x500")
    public String x500(){
        return "static/x500";
    }

    /**
     * 404
     * @return
     */
    @RequestMapping("/x404")
    public String x404(){
        return "static/x404";
    }

    /**
     * exception
     * @return
     */
    @RequestMapping("/exception")
    public String exception(){
        return "static/exception";
    }





    /*--------------------------------------------------------------------------------------------------------*/
    /**
     * 跳转到 浏览页面
     * @return
     */
    @RequestMapping("/browse")
    public String browse(){
        return "browse/browseHome";
    }

    /**
     * ebooks
     * 语言分类
     * @param language
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/browse/eBooksLanguage")
    public ModelAndView eBooksLanguage(@RequestParam("clq")String language ,
                                       @RequestParam(defaultValue = "1" ) int page,
                                       @RequestParam(defaultValue = "10") int size){
        ModelAndView mv = new ModelAndView();
        PageHelper.startPage(page,size);
        List<EBooks> list = eBooksService.queryLanguage(language ,page,size);
        PageInfo<EBooks> pageInfo = new PageInfo<>(list);

        mv.addObject("pageInfoEBook" ,pageInfo);
        mv.addObject("search",language);
        mv.setViewName("browse/LQuery");
        return mv;
    }

    /**
     * ebooks
     * 语言分类
     * @param classify
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/browse/eBooksClassify")
    public ModelAndView eBooksClassify(@RequestParam("clq")String classify ,
                                       @RequestParam(defaultValue = "1" ) int page,
                                       @RequestParam(defaultValue = "10") int size){
        ModelAndView mv = new ModelAndView();
        PageHelper.startPage(page,size);
        List<EBooks> list = eBooksService.queryClassify(classify ,page,size);
        PageInfo<EBooks> pageInfo = new PageInfo<>(list);

        mv.addObject("pageInfoEBook" ,pageInfo);
        mv.addObject("search",classify);
        mv.setViewName("browse/CQuery");
        return mv;
    }

    /**
     * 查询页面
     * @param retrieve
     * @param model
     * @return
     */
    @RequestMapping("/browse/retrieve")
    public String retrieve(@RequestParam("search") String retrieve,Model model){

        model.addAttribute("search",retrieve);
        return "browse/query";
    }
    /**
     * 查询电子书
     * @param name
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/browse/queryEBook")
    public ModelAndView queryEBook(@RequestParam("search") String name,
                                   @RequestParam(defaultValue = "1") int page,
                                   @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<EBooks> list = eBooksService.queryByName(name, page, size) ;
        PageInfo<EBooks> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfoEBook",pageInfos);
        mv.addObject("search",name);
        mv.setViewName("browse/query-eBook");
        return mv;

    }

    /**
     * 查询blog
     * @param name
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/browse/queryArticle")
    public ModelAndView query(@RequestParam("search") String name,
                              @RequestParam(defaultValue = "1") int page,
                              @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<BlogRecord> list = blogService.queryByName(name, page, size) ;
        PageInfo<BlogRecord> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfoArticle",pageInfos);
        mv.setViewName("browse/query-article");
        return mv;
    }

    /**
     * 跳转到博客预览页面
     * @param id
     * @return
     */
    @RequestMapping("/browse/toBrowseBlog/{id}")
    public String toBrowseBlog(@PathVariable int id, Model model){


        BlogRecord blogRecord = blogService.queryById(id);
        model.addAttribute("blog",blogRecord);
        return "browse/browseBlog";
    }

    /**
     * 查看书籍
     * @param eBookId
     * @param model
     * @return
     */
    @RequestMapping("/browse/browseEBook/{id}")
    public String browseEBook(@PathVariable("id") int eBookId, Model model){
        EBooks eBooks = eBooksService.queryById(eBookId);
        model.addAttribute("books",eBooks);
        return "browse/browseEBook";
    }
}
