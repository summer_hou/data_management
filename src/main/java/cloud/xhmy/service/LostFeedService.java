package cloud.xhmy.service;

import cloud.xhmy.pojo.LostFeed;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface LostFeedService {

    /**
     * 添加
     * @param lost
     * @return
     */
    int add(LostFeed lost);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     *
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<LostFeed> queryByName(String name, int page, int size);


    /**
     * 查询用户所有
     * @param author
     * @param page
     * @param size
     * @return
     */
    List<LostFeed> listAll(String author,int page, int size);


    /**
     * 修改
     * @param id
     * @return
     */
    int modifyById(int id);


    /**
     * 通过id查询
     * @param id
     * @return
     */
    LostFeed queryById(int id);


    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */

    /**
     * 查询所有
     * @param page
     * @param size
     * @return
     */
    List<LostFeed> listAllLostFeed(int page, int size);

    /**
     * 统计
     * @return
     */
    LostFeed countNumber();

    /**
     * 遗失处理
     * @param result
     * @param page
     * @param size
     * @return
     */
    List<LostFeed> listAllDispose(String result ,int page,int size);

    /**
     *
     * @param result
     * @param page
     * @param size
     * @return
     */
    List<LostFeed> listQueryAllDispose(String result ,String name,int page,int size);


}
