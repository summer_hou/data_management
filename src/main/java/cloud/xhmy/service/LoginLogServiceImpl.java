package cloud.xhmy.service;

import cloud.xhmy.dao.LoginLogMapper;
import cloud.xhmy.pojo.LoginLog;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public class LoginLogServiceImpl  implements LoginLogService{

    private LoginLogMapper loginLogMapper;

    public void setLoginLogMapper(LoginLogMapper loginLogMapper) {
        this.loginLogMapper = loginLogMapper;
    }

    public int addLog(LoginLog log) {
        return this.loginLogMapper.addLog(log);
    }

    public int deleteLogById(int id) {
        return this.loginLogMapper.deleteLogById(id);
    }

    public LoginLog queryLogById(int id) {
        return this.loginLogMapper.queryLogById(id);
    }

    public List<LoginLog> queryAllLog(String user,int page, int size) {
        return this.loginLogMapper.queryAllLog(user,page,size);
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public LoginLog queryById(int id) {
        return this.loginLogMapper.queryLogById(id);
    }

    public List<LoginLog> listAllLoginLog(int page, int size) {
        return this.loginLogMapper.listAllLoginLog(page, size);
    }

    public LoginLog countNumber() {
        return this.loginLogMapper.countNumber();
    }

    public List<LoginLog> queryByName(String name, int page, int size) {
        return this.loginLogMapper.queryByName(name, page, size);
    }


}
