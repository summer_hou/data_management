<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/11
  Time: 20:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>lostFeed</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>

</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h3 style="margin: 15px auto;text-align: center;"><span>遗失报备</span></h3>
            <form id="lostFeed" action="" style="line-height: 2em;font-size: 16px;">
                <div><span>遗失书名：</span><input id="lostBook" name="lostBook" class="form-control" type="text"></div>
                <div><span>遗失用户：</span><input id="lostUser" type="text" class="form-control" name="lostUser" value="${user.email}"></div>
                <div><span>遗失时间：</span><input id="lostTime" type="text" class="form-control" name="lostTime"></div>
                <div style="margin-top: 20px;"><input id="save" type="button" class="btn btn-primary" value="提交"></div>
            </form>
        </div>
        <div id="">
            <img src="" alt="">
        </div>

    </div>
</div>



<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>


<script type="text/javascript">
    $("#save").click(function () {
        var formData = new FormData(document.getElementById("lostFeed"));
        //alert(html);
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/pushLostFeed',
            data:formData,
            processData: false,
            contentType: false,
            dataType:'json',


            success:function (data) {
                alert(data.msg);
            }
        })
    });
</script>
</body>
</html>

