<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/24
  Time: 17:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>upload</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>


<div style="width: 90%; margin: 10px auto;">
    <%--
        <div class="tabbable" id="tabs-736228">
        <%--
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a href="#panel-364990" data-toggle="tab">书籍上架</a>
                </li>
                <li>
                    <a href="#panel-230698" data-toggle="tab">电子书上传</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="panel-364990"  style="width: 100%;m-right: 0px;line-height: 2em;font-size: 16px;">

                    <form id="form-book" enctype="multipart/form-data">
                        <div>
                            <span>封面图片：</span><input id="coverPicture"  name="cp" type="file">
                            <span></span>
                        </div>
                        <div>
                            <span>书名：</span><input class="form-control" id="bookName" name="bookName" value="${bookName}" type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>作者：</span><input class="form-control" id="author" name="author" value="${author}" type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>出版时间：</span><input class="form-control" id="publishTime" name="publishTime" value="${publishTime}" type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>语言：</span><input class="form-control" id="language" name="language" value="${language}" type="text">
                            <span></span>
                        </div>

                        <div>
                            <span>出版社：</span><input class="form-control" id="publishHouse" name="publishHouse" value="${publishHouse}" type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>上架者：</span><input class="form-control" id="shelves" name="shelves" value="${user.email}" readonly type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>简介：</span><input class="form-control" id="introduction" name="introduction" value="${introduction}" type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>价格：</span><input class="form-control" id="price" name="price" value="${price}" type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>分类：</span><input class="form-control" id="classify" name="classify" value="${classify}" type="text">
                            <span></span>
                        </div>

                        <div style="text-align: center; margin: 20px auto;">
                            <input id="saveBook" type="button" class="btn btn-primary" value="上传">
                        </div>
                    </form>
                    <span>${success}</span>
                </div>
                --%>
    <div class="tab-pane" id="panel-230698" style="width: 100%;m-right: 0px;line-height: 2em;font-size: 16px;">

        <form id="form-eBook" enctype="multipart/form-data">
            <div>
                <span>封面图片：</span><input name="files" type="file">
                <span></span>
            </div>
            <div>
                <span>书名：</span><input class="form-control" name="bookName" type="text"><br>
                <span></span><input name="files" type="file">
            </div>
            <div>
                <span>作者：</span><input class="form-control" name="author" type="text">
                <span></span>
            </div>
            <div>
                <span>出版时间：</span><input class="form-control" name="publishTime" type="text">
                <span></span>
            </div>
            <div>
                <span>语言：</span><input class="form-control" name="language" type="text">
                <span></span>
            </div>

            <div>
                <span>出版社：</span><input class="form-control" name="publishHouse" type="text">
                <span></span>
            </div>
            <div>
                <span>上架者：</span><input class="form-control" name="shelves" value="${user.email}" readonly type="text">
                <span></span>
            </div>
            <div>
                <span>简介：</span><input class="form-control" name="introduction" type="text">
                <span></span>
            </div>
            <div>
                <span>简述：</span><input class="form-control" name="description" type="text">
                <span></span>
            </div>
            <div>
                <span>分类：</span><input class="form-control" name="classify" type="text">
                <span></span>
            </div>
            <%--
            <div>
                <span>上传状态：</span><input class="form-control" name="auditStatus" type="text">
                <span></span>
            </div>
            <div>
                <span>格式：</span><input class="form-control"  name="format" type="text">
                <span></span>
            </div>
            --%>
            <div style="text-align: center; margin: 20px auto;">
                <input id="saveEBook" type="button" class="btn btn-primary" value="上传">
            </div>
        </form>
    </div>

    <%--        </div>
        </div>
        --%>
</div>


<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>


<script type="text/javascript">
    $("#saveBook").click(function () {
        var formData = new FormData(document.getElementById("form-eBook"));
        $.ajax({
            type: 'post',
            url: '${pageContext.request.contextPath}/upload/book',

            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',

            success: function (data) {
                alert(data);
            }
        })
    });

    // $("#coverPicture").change(function () {
    //     // 1. 创建一个读取文件的对象
    //     var fileReader = new FileReader();
    //     // 取到当前选中的头像文件
    //     // console.log(this.files[0]);
    //     // 读取你选中的那个文件
    //     fileReader.readAsDataURL(this.files[0]);  // 读取文件是需要时间的
    //     fileReader.onload = function () {
    //         // 2. 等上一步读完文件之后才 把图片加载到img标签中
    //         $("#avatar-img").attr("src", fileReader.result);
    //     };
    // });

    $("#saveEBook").click(function () {
        var formData = new FormData(document.getElementById("form-eBook"));
        $.ajax({
            type: 'post',
            url: '${pageContext.request.contextPath}/upload/eBook',
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',

            success: function (data) {
                alert(data.msg);
            }
        })
    });
</script>
</body>
</html>
