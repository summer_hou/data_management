<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/20
  Time: 20:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="" style="width: 90%; margin: 5px auto;">

    <div class="row">
        <div class="col-md-12 " style="overflow-x: scroll;">
            <div style="color: gold;">
                <h3><span>登录日志</span></h3>
            </div>
            <div style="margin-top: 10px;">
                <%--
                <button class="btn btn-danger"
                        style="width: 10%; height: 38px;margin-left: 0px;margin-top: -2px;padding: unset;" type="button"
                        onclick="delAllProduct()">删除
                </button>
--%>
                <form action="" style="display: inline;">
                    <input id="search-loginLog" style="width: 80%;height: 40px; padding: 5px; border-radius: 2px; border: 1px solid #666666;margin-left: -4px;" name="search"
                           value="${search}" placeholder="输入要搜索的书名" type="search">
                    <button style="width: 10%; height: 40px;margin-left: -4px;" type="button" onclick="searchLoginLog()">搜索</button>
                </form>

            </div>
            <table class="table table-hover table-striped" style="overflow-x: scroll;color: gold;">
                <thead>
                <tr>
                    <th><input type="checkbox" name="CheckAll" id="CheckAll"/></th>
                    <th>登录用户</th>
                    <th>登录方式</th>
                    <th>登录设备</th>
                    <th>登录状态</th>
                    <th>登录ip</th>
                    <th>登录系统</th>
                    <th>登入时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="loginLogs-list">
                <c:forEach var="list" items="${pageInfo.list}">
                    <tr>
                        <td><input type=checkbox name=Check[] value="${list.id}" id=Check[]/></td>
                        <td>${list.loginUser }</td>
                        <td>${list.loginWay }</td>
                        <td>${list.loginDevice }</td>
                        <td>${list.loginState }</td>
                        <td>${list.loginIp }</td>
                        <td>${list.loginSystem }</td>
                        <td>${list.loginTime }</td>
                        <td class=text-center>
                            <a href=${pageContext.request.contextPath}/?id=${list.id}" class=btn bg-olive btn-xs>更新</a>
                            ||
                            <a href=${pageContext.request.contextPath}/${list.id}" class=btn bg-olive btn-xs>删除</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
                <tfoot>

                </tfoot>

            </table>

        </div>
    </div>
    <div class="xhmy_page_div"></div>

</div>
<script type="text/javascript">
    <%--
    var p;
    var list;
    var i;
    var t;
    var html;
    //
    // $("#content-list").empty();
    $.ajax({
        type: "get",
        url: "${pageContext.request.contextPath}/admin/loginLog",
        dataType: "json",
        success: function (data) {

            list = data.list;
            t = data.total;
            // document.getElementById("blog-total").innerText=t;
            p = data.pages;
            // document.getElementById("blog-pages").innerText=p;
            for (i = 0; list.length > i; i++) {
                html += "<tr>" +
                    "<td><input type=\"checkbox\" name=\"Check[]\" value=\"" + list[i].id + "\" id=\"Check[]\"/></td>" +
                    "<td>" + list[i].loginUser + "</td>" +
                    "<td>" + list[i].loginWay + "</td>" +
                    "<td>" + list[i].loginDevice + "</td>" +
                    "<td>" + list[i].loginState + "</td>" +
                    "<td>" + list[i].loginIp + "</td>" +
                    "<td>" + list[i].loginSystem + "</td>" +
                    "<td>" + list[i].loginTime + "</td>" +
                    "<td class=\"text-center\">" +
                    "<a href=\"${pageContext.request.contextPath}/?id=" + list[i].id +
                    "\" class=\"btn bg-olive btn-xs\">更新</a>" +
                    "||" +
                    "<a href=\"${pageContext.request.contextPath}/" + list[i].id +
                    "\" class=\"btn bg-olive btn-xs\">删除</a>" +
                    "</td>" +
                    "</tr>"

            };
            // $("#content-list").empty();
            // $("#loginLogs-list").empty();
            $("#loginLogs-list").html(html);
            $(".xhmy_page_div").createPage({
                pageNum: p,
                current: 1,
                backfun: function (e) {
                }
            });
        }
    });
    --%>
    $(".xhmy_page_div").createPage({

        pageNum: ${pageInfo.pages},
        current: 1,
        backfun: function (e) {
        }
    });
    function pageNumber(params) {
        $("#loginLogs-list").load("${pageContext.request.contextPath}/admin/loginLog #loginLogs-list >*", {
            page: params,
            size: "10"
        });
    };
</script>

<script type="text/javascript">
    //全选/全不选
    $("#CheckAll").bind("click", function () {
        $("input[name='Check[]']").prop("checked", this.checked);
    });

    //批量删除
    function delAllProduct() {
        if (!confirm("确定要删除吗？")) {
            return;
        }
        var cks = $("input[name='Check[]']:checked");
        var str = "";
//拼接所有的id
        for (var i = 0; i < cks.length; i++) {
            if (cks[i].checked) {
                str += cks[i].value + ",";
            }
        }
//去掉字符串末尾的‘&'
        str = str.substring(0, str.length - 1);
// var a = str.split('&');//分割成数组
// console.log(str);return false;
        $.ajax({
            type: 'post',
            url: "${pageContext.request.contextPath}/delete/loginLog/?id=",
            dataType: "json",
            data: {
                filenames: str
            },
            success: function (data) {
                console.log(data);
                return false;
                if (data['status'] == 1) {
                    alert('删除成功！');
                    location.href = data['url'];
                } else {
                    alert('删除失败！');
                    return false;
                }
            }
        });
    };
</script>