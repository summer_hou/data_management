<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/29
  Time: 14:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>browseHome</title>
    <meta charset="utf-8">


    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/browse-cql.js"></script>

    <style>
        /*===========================================================*/
        /*footer*/
        div.footer {
            position: relative;
            bottom: 0px;
            right: auto;

            color: snow;
            width: 100%;
            min-height: 50px;
            padding-top: 15px;
            background: #081325;
            text-align: center;
            z-index: 50;
        }

        .icp {
            color: white;
        }

        .icp:hover {
            text-decoration: none;
            color: wheat;
        }

    </style>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    综合图书管理系统<small>&nbsp; &nbsp;浏览页面</small>
                </h1>
                <span style="float: right;margin-top: 10px; font-size: 16px; margin-right: 5px;"><a
                        href="${pageContext.request.contextPath}/toLogin">登录</a> || <a
                        href="${pageContext.request.contextPath}/toRegister">注册</a> || <a
                        href="javascript:goback()">返回</a></span>
            </div>
            <div style="height: 20px"></div>
            <div style=" width: 100% ;margin:0 auto; z-index: 50;">
                <!-- 搜索框 -->
                <div style="margin: 10px auto 30px auto;">
                    <form action="${pageContext.request.contextPath}/browse/retrieve">
                        <input style="width: 90%;height: 38px; padding: 5px; border-radius: 2px; border: 1px solid #666666;"
                               name="search"
                               value="${search}" placeholder="全站搜索（输入要搜索的名称）" type="search">
                        <button style="width: 10%; height: 40px;margin-left: -5px;" type="submit">搜索</button>
                    </form>

                </div>
                <div style="margin-right: 0px; width: auto ;margin-top: -30px; float: right;background: unset;">
                    <div class="btn-group" style="margin-right: -5px;">
                        <button class="btn btn-default" style="border: unset; background: unset;">语言</button>
                        <button style="border: unset; background: unset;" data-toggle="dropdown"
                                class="btn btn-default dropdown-toggle"><span
                                class="caret"></span></button>
                        <ul class="dropdown-menu" style=" left: unset;right: 0;">
                            <li><a href="#">所有语言</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:byLanguage(0)">Chinese</a></li>
                            <li><a href="javascript:byLanguage(1)">English</a></li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <button style="border: unset; background: unset;" class="btn btn-default">分类</button>
                        <button style="border: unset; background: unset;" data-toggle="dropdown"
                                class="btn btn-default dropdown-toggle"><span
                                class="caret"></span></button>
                        <ul class="dropdown-menu" style=" left: unset;right: 0;">
                            <li>
                                <a href="javascript:classify(0)">所有类型</a>
                            </li>
                            <li class="divider">
                            </li>
                            <li><a href="javascript:byClassify(1)">艺术</a></li>
                            <li><a href="javascript:byClassify(2)">商业</a></li>
                            <li><a href="javascript:byClassify(3)">化学</a></li>
                            <li><a href="javascript:byClassify(4)">经济</a></li>
                            <li><a href="javascript:byClassify(5)">教育</a></li>
                            <li><a href="javascript:byClassify(6)">地理</a></li>
                            <li><a href="javascript:byClassify(7)">地质</a></li>
                            <li><a href="javascript:byClassify(8)">历史</a></li>
                            <li><a href="javascript:byClassify(9)">休闲</a></li>
                            <li><a href="javascript:byClassify(10)">法学</a></li>
                            <li><a href="javascript:byClassify(11)">宗教</a></li>
                            <li><a href="javascript:byClassify(12)">技术</a></li>
                            <li><a href="javascript:byClassify(13)">工艺</a></li>
                            <li><a href="javascript:byClassify(14)">文学</a></li>
                            <li><a href="javascript:byClassify(15)">数学</a></li>
                            <li><a href="javascript:byClassify(16)">医学</a></li>
                            <li><a href="javascript:byClassify(17)">物理</a></li>
                            <li><a href="javascript:byClassify(18)">心理学</a></li>
                            <li><a href="javascript:byClassify(19)">计算机</a></li>
                            <li><a href="javascript:byClassify(20)">生物学</a></li>
                            <li><a href="javascript:byClassify(21)">语言学</a></li>
                            <li><a href="javascript:byClassify(22)">社会科学</a></li>
                            <li><a href="javascript:byClassify(23)">体育与运动</a></li>
                            <li><a href="javascript:byClassify(24)">科学(通用)</a></li>


                        </ul>
                    </div>
                </div>
            </div>
            <div style="margin-top: 50px;"></div>

            <div class="carousel slide" id="carousel-336080">
                <ol class="carousel-indicators">
                    <li data-slide-to="0" data-target="#carousel-336080">
                    </li>
                    <li data-slide-to="1" data-target="#carousel-336080">
                    </li>
                    <li data-slide-to="2" data-target="#carousel-336080" class="active">
                    </li>
                </ol>
                <div class="carousel-inner">
                    <div class="item">
                        <img src="${pageContext.request.contextPath}/static/images/235228-1549900348f5ed.jpg" alt="">
                        <div class="carousel-caption">
                            <h4>


                            </h4>
                            <p>

                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <img alt="" src="${pageContext.request.contextPath}/static/images/224813-16038964934a7d.jpg"/>
                        <div class="carousel-caption">
                            <h4>

                            </h4>
                            <p>

                            </p>
                            <span></span>
                        </div>
                    </div>
                    <div class="item active">
                        <img alt="" src="${pageContext.request.contextPath}/static/images/230718-159456643848d4.jpg"/>
                        <div class="carousel-caption">
                            <h4>

                            </h4>
                            <p>

                            </p>
                        </div>
                    </div>
                </div>
                <a class="left carousel-control" href="#carousel-336080" data-slide="prev"><span
                        class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control"
                                                                                href="#carousel-336080"
                                                                                data-slide="next"><span
                    class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <div style="margin: 20px auto;"></div>
            <div class="row">
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img alt="300x200"
                             src="${pageContext.request.contextPath}/static/upload/picture/b0e134d36447474bcb3a28793be13cd9.jpg"/>
                        <div class="caption">
                            <h3>
                                读客经典文库：三体全集 （套装共3册）
                            </h3>
                            <p>
                                Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta
                                gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                            </p>
                            <p>
                                <a class="btn btn-primary" href="#">查看</a> <a class="btn"
                                                                              href="https://b-ok.global/dl/4965802/abfb71">下载</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img alt="300x200"
                             src="${pageContext.request.contextPath}/static/upload/picture/c8df6633230111b6fa05e182b0348648.jpg"/>
                        <div class="caption">
                            <h3>
                                Manipulation Dark Psychology to Manipulate and Control People
                            </h3>
                            <p>
                                Step-by-step instructional guide to manipulate people using dark psychology
                                Dark Psychology can be an incredibly powerful method for mind control, brainwashing,
                                influencing, and manipulating those around you, but only if you know how to do it right!
                                Need to learn how to manipulate someone fast?
                            </p>
                            <p>
                                <a class="btn btn-primary" href="#">查看</a> <a class="btn"
                                                                              href="https://b-ok.global/dl/3722107/ebcda4">下载</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img alt="300x200"
                             src="${pageContext.request.contextPath}/static/upload/picture/8e83689665599548afb421470fac65ee.jpg"/>
                        <div class="caption">
                            <h3>
                                活着
                            </h3>
                            <p>
                                地主少爷福贵嗜赌成性，终于赌光了家业一贫如洗，穷困之中的福贵因为母亲生病前去求医，没想到半路上被国民党部队抓了壮丁，后被解放军所俘虏，回到家乡他才知道母亲已经去世，妻子家珍含辛茹苦带大了一双儿女，但女儿不幸变成了聋哑人，儿子机灵活泼……

                                然而，真正的悲剧从此才开始渐次上演，每读一页，都让我们止不住泪湿双眼，因为生命里难得的温情将被一次次死亡撕扯得粉碎，只剩得老了的福贵伴随着一头老牛在阳光下回忆。
                            </p>
                            <p>
                                <a class="btn btn-primary" href="#">查看</a> <a class="btn"
                                                                              href="https://b-ok.global/dl/5241668/305dc4">下载</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img alt="300x200"
                             src="${pageContext.request.contextPath}/static/upload/picture/458365e0a86de3b6392feb97c110647e.jpg"/>
                        <div class="caption">
                            <h3>
                                They Both Die At The End
                            </h3>
                            <p>
                                Adam Silvera reminds us that there’s no life without death and no love without loss in
                                this devastating yet uplifting story about two people whose lives change over the course
                                of one unforgettable day.

                                On September 5, a little after midnight, Death-Cast calls Mateo Torrez and Rufus
                                Emeterio to give them some bad news: They’re going to die today.

                                Mateo and Rufus are total strangers, but, for different reasons, they’re both looking to
                                make a new friend on their End Day. The good news: There’s an app for that.
                                It’s called the Last Friend, and through it, Rufus and Mateo are about to meet up for
                                one last great adventure—to live a lifetime in a single day.
                            </p>
                            <p>
                                <a class="btn btn-primary" href="#">查看</a> <a class="btn"
                                                                              href="https://b-ok.global/dl/4824673/e209ba?dsource=mostpopular">下载</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img alt="300x200"
                             src="${pageContext.request.contextPath}/static/upload/picture/f3b99fa0ce54b7d3eca94d47e884c807.jpg"/>
                        <div class="caption">
                            <h3>
                                Encyclopedia of Spirits: The Ultimate Guide to the Magic of Fairies, Genies, Demons,
                                Ghosts, Gods & Goddesses
                            </h3>
                            <p>
                                Enter the World of Spirits! The Encyclopedia of Spirits is a
                                comprehensive and entertaining A to Z of spirits from around this world
                                and the next. Within these pages meet love goddesses and disease demons,
                                guardians of children and guardians of cadavers. Discover Celtic
                                goddesses and goddesses of the Kabbalah, female Buddhas, African Powers,
                                Dragon Ladies, White Ladies, Black Madonnas, the Green Man, the Green
                                Fairy, lots and lots of ghosts, djinn, mermaids, fairies, and more. From
                                the beneficent to the mischievous, working with these spirits can bring
                                good fortune, lasting love, health, fertility, revenge, and relief.
                                Discover: The true identities of over one thousand spirits (as well as
                                their likes and dislikes) How to communicate with specific spirits for
                                your own benefit How to recognize these spirits when they manifest
                                themselves The mythological and historical even
                            </p>
                            <p>
                                <a class="btn btn-primary" href="#">查看</a> <a class="btn"
                                                                              href="https://b-ok.global/dl/2295929/7ff900?dsource=mostpopular">下载</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img alt="300x200"
                             src="${pageContext.request.contextPath}/static/upload/picture/0e9af24b960877b55da0a3d4105c8665.jpg"/>
                        <div class="caption">
                            <h3>
                                The Business Book (Big Ideas Simply Explained)
                            </h3>
                            <p>
                                Packed with innovative graphics and simple explanations of business concepts, from
                                managing risk and alternative business models to effective leadership and thinking
                                outside the box, The Business Book covers every facet of business management. Big ideas
                                make great business thinkers and leaders. From Adam Smith and Andrew Carnegie to Bill
                                Gates and Warren Buffett, The Business Book is perfect for college students, would-be
                                entrepreneurs, or anyone interested in how business works. The Business Book is the
                                perfect primer to key theories of business and management, covering inspirational
                                business ideas, business strategy and alternative business models. One hundred key
                                quotations introduce you to the work of great commercial thinkers, leaders,
                                and gurus from Henry Ford to Steve Jobs, and to topics spanning from start-ups to
                                ethics.
                            </p>
                            <p>
                                <a class="btn btn-primary" href="#">查看</a> <a class="btn"
                                                                              href="https://b-ok.global/dl/2363902/fb7c9b?dsource=mostpopular">下载</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img alt="300x200"
                             src="${pageContext.request.contextPath}/static/upload/picture/b0ac18af1a8030e4bf17a54b7e5c6de0.jpg"/>
                        <div class="caption">
                            <h3>
                                Use MySQL with Python
                            </h3>
                            <p>

                            </p>
                            <p>
                                <a class="btn btn-primary" href="#">查看</a> <a class="btn"
                                                                              href="https://b-ok.global/dl/2699024/ffc619">下载</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img alt="300x200"
                             src="${pageContext.request.contextPath}/static/upload/picture/7fb0e58e87aef95feae8c9d3a986a0a7.jpg"/>
                        <div class="caption">
                            <h3>
                                Java: The Complete Reference, Eleventh Edition
                            </h3>
                            <p>
                                Fully updated for Java SE 11, Java: The Complete Reference, Eleventh Edition explains
                                how to develop, compile, debug, and run Java programs. Best-selling programming author
                                Herb Schildt covers the entire Java language, including its syntax, keywords, and
                                fundamental programming principles. You’ll also find information on key portions of the
                                Java API library, such as I/O, the Collections Framework, the stream library, and the
                                concurrency utilities. Swing, JavaBeans, and servlets are examined and numerous examples
                                demonstrate Java in action. Of course, the very important module system is discussed in
                                detail. This Oracle Press resource also offers an introduction to JShell, Java’s
                                interactive programming tool. Best of all, the
                                book is written in the clear, crisp, uncompromising style that has made Schildt the
                                choice of millions worldwide.
                            </p>
                            <p>
                                <a class="btn btn-primary" href="#">查看</a> <a class="btn"
                                                                              href="https://b-ok.global/dl/3697965/054bba">下载</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img alt="300x200"
                             src="${pageContext.request.contextPath}/static/upload/picture/31c9cb6d2122299ac8d6a6d1142efa9e.jpg"/>
                        <div class="caption">
                            <h3>

                                C# 8.0 and .NET Core 3.0 – Modern Cross-Platform Development Fourth Edition
                            </h3>
                            <p>
                                地主少爷福贵嗜赌成性，终于赌光了家业一贫如洗，穷困之中的福贵因为母亲生病前去求医，没想到半路上被国民党部队抓了壮丁，后被解放军所俘虏，回到家乡他才知道母亲已经去世，妻子家珍含辛茹苦带大了一双儿女，但女儿不幸变成了聋哑人，儿子机灵活泼……

                                然而，真正的悲剧从此才开始渐次上演，每读一页，都让我们止不住泪湿双眼，因为生命里难得的温情将被一次次死亡撕扯得粉碎，只剩得老了的福贵伴随着一头老牛在阳光下回忆。
                            </p>
                            <p>
                                <a class="btn btn-primary" href="#">查看</a> <a class="btn"
                                                                              href="https://b-ok.global/dl/5285318/c72593">下载</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="footer">
    <p>
        <span>© 2020 xhmy.cloud . &nbsp;</span>
        <span>赣ICP证</span> &nbsp;
        <a class="icp"
           href="https://beian.miit.gov.cn"
           title=""
           target="_blank">赣ICP备</a>
        <span>2020012707号</span> &nbsp;
    </p>
</div>
<script type="text/javascript">

</script>
</body>
</html>
