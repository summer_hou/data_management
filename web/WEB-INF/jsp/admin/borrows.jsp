<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/20
  Time: 20:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="" style="width: 95%;margin: 5px auto;">
    <div class="row">
        <div class="col-md-12 " style="overflow-x: scroll;">
            <div style="color: gold;">
                <h3><span>借阅</span></h3>
            </div>
            <div style="margin-top: 10px;">
                <%--
                <button class="btn btn-danger"
                        style="width: 10%; height: 38px;margin-left: 0px;margin-top: -2px;padding: unset;" type="button"
                        onclick="delAllProduct()">删除
                </button>
--%>
                <form action="" style="display: inline;">
                    <input id="search" style="width: 80%;height: 40px; padding: 5px; border-radius: 2px; border: 1px solid #666666;margin-left: -4px;" name="search"
                           value="${search}" placeholder="输入要搜索的书名" type="search">
                    <button style="width: 10%; height: 40px;margin-left: -4px;" type="button" onclick="searchBorrow()">搜索</button>
                </form>

            </div>
            <table class="table table-hover table-striped" style="overflow-x: scroll;color: gold;">
                <thead>
                <tr>
                    <th><input type="checkbox" name="CheckAll" id="CheckAll"/></th>
                    <th>借阅书名</th>
                    <th>借阅用户</th>
                    <th>借阅数量</th>
                    <th>借阅天数</th>
                    <th>缴付押金</th>
                    <th>是否处罚</th>
                    <th>内容处罚</th>
                    <th>借阅时间</th>
                    <th>是否归还</th>
                    <th>归还时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="borrows-list">
                <c:forEach var="list" items="${pageInfo.list}">
                    <tr>
                    <td><input type="checkbox" name="Check[]" value="${list.id} " id="Check[]"/></td>
                    <td>${list.borrowName}</td>
                    <td>${list.borrowUser}</td>
                    <td>${list.borrowNumber}</td>
                    <td>${list.borrowDay}</td>
                    <td>${list.deposit}</td>
                    <td>${list.tfDefault}</td>
                    <td>${list.punish}</td>
                    <td>${list.borrowTime}</td>
                    <td>${list.tfReturn}</td>
                    <td>${list.returnTime}</td>
                    <td class="text-center">
                    <a href="${pageContext.request.contextPath}/${list.id}" class="btn bg-olive btn-xs">更新</a>
                    ||
                    <a href="${pageContext.request.contextPath}/delete/${list.id}" class="btn bg-olive btn-xs">删除</a>" +
                    </td>
                    </tr>
                </c:forEach>

                </tbody>

            </table>

        </div>
    </div>
    <div class="xhmy_page_div"></div>
</div>

<script type="text/javascript">
    <%--
    var p;
    var list;
    var i;
    var t;
    var html;
    //
    $.ajax({
        type:"get",
        url:"${pageContext.request.contextPath}/admin/borrow",
        dataType:"json",
        success:function (data) {

            list = data.list;
            t =data.total;
            // document.getElementById("blog-total").innerText=t;
            p =data.pages;
            // document.getElementById("blog-pages").innerText=p;
            for (i=0;list.length>i;i++){
                html += "<tr>" +
                    "<td><input type=\"checkbox\" name=\"Check[]\" value=\"" + list[i].id + "\" id=\"Check[]\"/></td>" +
                    "<td>" + list[i].borrowName + "</td>" +
                    "<td>" + list[i].borrowUser + "</td>" +
                    "<td>" + list[i].borrowNumber + "</td>" +
                    "<td>" + list[i].borrowDay + "</td>" +
                    "<td>" + list[i].deposit + "</td>" +
                    "<td>" + list[i].tfDefault + "</td>" +
                    "<td>" + list[i].punish + "</td>" +
                    "<td>" + list[i].borrowTime + "</td>" +
                    "<td>" + list[i].tfReturn + "</td>" +
                    "<td>" + list[i].returnTime + "</td>" +
                    "<td class=\"text-center\">" +
                    "<a href=\"${pageContext.request.contextPath}/" + list[i].id +
                    "\" class=\"btn bg-olive btn-xs\">更新</a>" +
                    "||" +
                    "<a href=\"${pageContext.request.contextPath}/delete/" + list[i].id +
                    "\" class=\"btn bg-olive btn-xs\">删除</a>" +
                    "</td>" +
                    "</tr>"
            };
            //$("#content-list").empty();
            $("#borrows-list").empty();
            $("#borrows-list").html(html);
            $(".xhmy_page_div").createPage({
                pageNum: p,
                current: 1,
                backfun: function(e) {
                }
            });
        }
    });
    --%>
    $(".xhmy_page_div").createPage({
        pageNum: ${pageInfo.pages},
        current: 1,
        backfun: function(e) {
        }
    });
    function pageNumber(params) {
        $("#borrows-list").load("${pageContext.request.contextPath}/admin/borrow #borrows-list >*", {
            page: params,
            size: "10"
        });
    };
</script>
<script type="text/javascript">
    //全选/全不选
    $("#CheckAll").bind("click", function () {
        $("input[name='Check[]']").prop("checked", this.checked);
    });

    //批量删除
    function delAllProduct() {
        if (!confirm("确定要删除吗？")) {
            return;
        }
        var cks = $("input[name='Check[]']:checked");
        var str = "";
        //拼接所有的id
        for (var i = 0; i < cks.length; i++) {
            if (cks[i].checked) {
                str += cks[i].value + ",";
            }
        }
        //去掉字符串末尾的‘&'
        str = str.substring(0, str.length - 1);
        // var a = str.split('&');//分割成数组
        // console.log(str);return false;
        $.ajax({
            type: 'post',
            url: "${pageContext.request.contextPath}/delete/borrow/?id=",
            dataType: "json",
            data: {
                filenames: str
            },
            success: function (data) {
                console.log(data);
                return false;
                if (data['status'] == 1) {
                    alert('删除成功！');
                    location.href = data['url'];
                } else {
                    alert('删除失败！');
                    return false;
                }
            }
        });
    };
</script>