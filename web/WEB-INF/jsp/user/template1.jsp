<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/25
  Time: 19:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>template1</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/user/user-template-framework.css">
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico">
    <script src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/user/user-template-framework.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/user/user-template-back.js"></script>
    <!--
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        /* body {
            background-image: linear-gradient(90deg, #5b4099, #fb83b7);
        } */

        ul {
            list-style: none;
        }

        #wrapper {
            width: auto;
            height: auto;
            position: fixed;
            top: 0;
            bottom: 0;
            z-index: 50;

            background-repeat: no-repeat;
            background-size: cover;
            transition: all 0.5s;

        }

        .sidebar {
            overflow: scroll;
        }

        #wrapper .sidebar {
            width: 220px;
            height: 100%;
            position: absolute;
            top: 0;
            left: -220px;
            bottom: 0;
            background-color: #BE04A7;

        }

        #wrapper button.kj {
            width: 40px;
            height: 40px;
            background-color: rgba(255, 255, 255, 0);
            background-image: url(/static/images/button.png);
            background-repeat: no-repeat;
            background-size: 100%;
            border: 0;
            outline: none;
            position: absolute;
            top: 20px;
            left: 10px;
            transition: all 0.5s;
            cursor: pointer;
        }

        .headSculpture {
            width: 100%;
            margin-top: 35px;
            text-align: center;
        }

        .headSculpture img {
            width: 100px;
            height: 100px;
            border-radius: 50px;
            margin: 0 auto;
            transform: translate(0, -135px);
            transition: all 1s;
        }

        .headSculpture .img {
            transform: translate(0, 0);
        }

        .headSculpture p {
            margin: 0 auto;
            margin-top: 15px;
            font-size: 15px;
            width: 160px;
            height: 30px;
            line-height: 30px;
            background-color: pink;
            border-radius: 15px;
            color: #fff;
            transform: translate(200px, 0);
            transition: all 1s;
        }

        .headSculpture .opacity {
            transform: translate(0, 0);
        }

        .option,
        .option ul li {
            width: 100%;
        }

        .option ul {
            padding: 15px 0;
        }

        .option ul li {
            display: flex;
            align-items: center;
            width: 195px;
            margin-top: 15px;
            padding-left: 25px;
            cursor: pointer;
            color: #D0D1D7;
            transform: translateZ(0);
            position: relative;
            transition-property: color;
            transition-duration: 0.4s;
            transition: all 1s;
        }

        .option ul li:nth-child(1),
        .option ul li:nth-child(3),
        .option ul li:nth-child(5) {
            transform: translate(-200px, 0);
        }

        .option ul li:nth-child(2),
        .option ul li:nth-child(4) {
            transform: translate(200px, 0);
        }

        .sidebar .option ul li.li {
            transform: translate(0, 0);
        }

        .option ul li:before {
            content: "";
            position: absolute;
            z-index: -1;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background: pink;
            transform-origin: 0 50%;
            transform: scaleX(0);
            transition: transform 0.3s ease-out;
        }

        .option ul li:hover,
        .option ul li:focus,
        .option ul li:active {
            color: #ffffff;
        }

        .option ul li:hover:before,
        .option ul li:focus:before,
        .option ul li:active:before {
            transform: scaleX(1);
        }

        .option ul li img {
            width: 25px;
            height: 25px;
        }

        .option ul li p {
            width: 145px;
            height: 30px;
            line-height: 30px;
            font-size: 15px;
            padding-left: 15px;
            margin-left: 10px;
            letter-spacing: 1px;
        }
        a.na{
            color: #222222;
            text-decoration: none;
        }
        .na:hover{
            color: orangered;
        }
        span.ply{
            position: absolute;
            top: 8px;
            left: 15px;
        }
        a.naCh{
            color: #333333;
            line-height: 2em;
            margin-left: 20px;
        }
        .naCh:hover{
            color: darkorange;
            text-decoration: none;

        }
        /*===========================================================*/
        /*footer*/
        div.footer{
            position:relative;
            bottom: 0px;
            right: auto;

            width: 100%;
            min-height: 50px;
            padding-top: 15px;
            color: white;
            background: #081325;
            text-align: center;
            z-index: 50;
        }
        .icp{
            color: whitesmoke;
        }
        .icp:hover{
            text-decoration: none;
            color: snow;
        }



        /*===========================================================*/
        /* home 描述*/
        .homeMs{
            text-align: center;
            margin: 20px auto;
        }

    </style>
    -->


</head>

<body>
<canvas id="canvas" width="100%" height="100%"></canvas>

<p id="offscreen-text" class="offscreen-text"></p>
<p id="text" class="text"></p>

<div style="position: absolute;top: 0;left: 0; width: 100%;height: 100%; background: rgba(100,100,200,0.5) ;">
    <!-- 作者：袁金 -->
    <!-- 请不要用于非法行为 -->
    <div id="wrapper" style="left:0;">
        <!-- 侧边栏 -->
        <div class="sidebar">
            <div class="headSculpture">
                <img src="${pageContext.request.contextPath}/static/images/avatar/headSculpture.jpg" alt=""
                     onerror="this.src='${pageContext.request.contextPath}/static/images/avatar/headSculpture.jpg'">
                <p style="color: #000000;">邮箱：<a class="na" href="">aliketh@dm.net</a></p>
            </div>
            <div class="option">
                <ul>
                    <li>
							<span class="ply">
								<span class="glyphicon glyphicon-home"></span>
							</span>
                        <!-- <p style=""><a class="na" href="home.html"></a></p> -->
                        <div class="panel-group" id="home" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default" style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                <div class="panel-heading" role="tab" id="headingHome"
                                     style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                    <p class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseHome" aria-expanded="true"
                                           aria-controls="collapseHome" class=" na ">
                                            首页
                                        </a>
                                    </p>
                                </div>
                                <div id="collapseHome" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingHome" aria-expanded="true">
                                    <div class="panel-body">

                                        <span><a class="naCh" href="">首页一</a></span><br>
                                        <span><a class="naCh" href="">首页二</a></span><br>
                                        <span><a class="naCh" href="">首页三</a></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
							<span class="ply">
								<span class="glyphicon glyphicon-pencil"></span>
							</span>
                        <!-- <p><a class="na" href=""></a></p> -->
                        <div class="panel-group" id="record" role="tablist" aria-multiselectable="false">
                            <div class="panel panel-default" style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                <div class="panel-heading" role="tab" id="headingRecord"
                                     style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                    <p class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseRecord" aria-expanded="flase"
                                           aria-controls="collapseRecord" class="collapsed na">
                                            个人笔记
                                        </a>
                                    </p>
                                </div>
                                <div id="collapseRecord" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingRecord" aria-expanded="flase">
                                    <div class="panel-body">

                                        <span><a class="naCh" href="">添加日志</a></span><br>
                                        <span><a class="naCh" href="">添加博客</a></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
							<span class="ply">
								<span class="glyphicon glyphicon-upload"></span>
							</span>
                        <!-- <p><a class="na" href=""></a></p> -->
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default" style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                <div class="panel-heading" role="tab" id="headingUpload"
                                     style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                    <p class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseUpload" aria-expanded="flase"
                                           aria-controls="collapseUpload" class="collapsed na">
                                            我的上传
                                        </a>
                                    </p>
                                </div>
                                <div id="collapseUpload" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingUpload" aria-expanded="flase">
                                    <div class="panel-body">

                                        <span><a class="naCh" href="">图片</a></span><br>
                                        <span><a class="naCh" href="">文档</a></span><br>
                                        <span><a class="naCh" href="">压缩包</a></span><br>
                                        <span><a class="naCh" href="">媒体文件</a></span><br>
                                        <span><a class="naCh" href="">可执行文件</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
							<span class="ply">
								<span class="glyphicon glyphicon-star"></span>
							</span>
                        <!-- <p><a class="na" href=""></a></p> -->
                        <div class="panel-group" id="star" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default" style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                <div class="panel-heading" role="tab" id="headingStar"
                                     style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                    <p class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseStar" aria-expanded="flase"
                                           aria-controls="collapseStar" class="collapsed na">
                                            我的收藏
                                        </a>
                                    </p>
                                </div>
                                <div id="collapseStar" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingStar" aria-expanded="flase">
                                    <div class="panel-body">

                                        <span><a class="naCh" href="">个人收藏</a></span><br>
                                        <span><a class="naCh" href="">引用收藏</a></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
							<span class="ply">
								<span class="glyphicon glyphicon-hdd"></span>
							</span>
                        <!-- <p><a class="na" href=""></a></p> -->
                        <div class="panel-group" id="dat" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default" style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                <div class="panel-heading" role="tab" id="headingData"
                                     style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                    <p class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseData" aria-expanded="flase"
                                           aria-controls="collapseData" class="collapsed na">
                                            个人数据
                                        </a>
                                    </p>
                                </div>
                                <div id="collapseData" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingData" aria-expanded="flase">
                                    <div class="panel-body">

                                        <span><a class="naCh" href="">总体预览</a></span><br>
                                        <span><a class="naCh" href="">上传数据</a></span><br>
                                        <span><a class="naCh" href="">收藏数据</a></span><br>
                                        <span><a class="naCh" href="">笔记数据</a></span><br>
                                        <span><a class="naCh" href="">下载数据</a></span><br>
                                        <span><a class="naCh" href="">用户数据</a></span><br>
                                        <span><a class="naCh" href="">操作数据</a></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li>
							<span class="ply">
								<span class="glyphicon glyphicon-download"></span>
							</span>
                        <!-- <p><a class="na" href=""></a></p> -->
                        <div class="panel-group" id="down" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default" style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                <div class="panel-heading" role="tab" id="headingDown"
                                     style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                    <p class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDown" aria-expanded="flase"
                                           aria-controls="collapseDown" class="collapsed na">
                                            本地下载
                                        </a>
                                    </p>
                                </div>
                                <div id="collapseDown" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDown" aria-expanded="flase">
                                    <div class="panel-body">

                                        <span><a class="naCh" href="">图片下载</a></span><br>
                                        <span><a class="naCh" href="">文档下载</a></span><br>
                                        <span><a class="naCh" href="">压缩包下载</a></span><br>
                                        <span><a class="naCh" href="">可执行文件下载</a></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
							<span class="ply">
								<span class="glyphicon glyphicon-th-list"></span>
							</span>
                        <!-- <p><a class="na" href=""></a></p> -->
                        <div class="panel-group" id="cla" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default" style="padding: 0;margin-left: 15px;border: none; background: unset;">
                                <div class="panel-heading" role="tab" id="headingCla"
                                     style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                    <p class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseCla" aria-expanded="flase"
                                           aria-controls="collapseCla" class="collapsed na">
                                            分类
                                        </a>
                                    </p>
                                </div>
                                <div id="collapseCla" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingCla" aria-expanded="flase">
                                    <div class="panel-body">

                                        <span><a class="naCh" href="">标签分类</a></span><br>
                                        <span><a class="naCh" href="">格式分类</a></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li>
							<span class="ply">
								<span class="glyphicon glyphicon-file"></span>
							</span>
                        <!-- <p><a class="na" href=""></a></p> -->
                        <div class="panel-group" id="doc" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default" style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                <div class="panel-heading" role="tab" id="headingDoc"
                                     style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                    <p class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDoc" aria-expanded="flase"
                                           aria-controls="collapseDoc" class="collapsed na">
                                            阅读文档
                                        </a>
                                    </p>
                                </div>
                                <div id="collapseDoc" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDoc" aria-expanded="flase">
                                    <div class="panel-body">

                                        <span><a class="naCh" href="">项目文档</a></span><br>
                                        <span><a class="naCh" href="">环境配置文档</a></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
							<span class="ply">
								<span class="glyphicon glyphicon-cog"></span>
							</span>
                        <p style="margin-left: 25px;"><a class="na" href="">设置</a></p>
                        <!-- <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default" style="padding: 0;margin-left: 15px;border: none;background: none;background-color: none;">
                                <div class="panel-heading" role="tab" id="headingSett"
                                style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                    <p class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSett" aria-expanded="flase"
                                         aria-controls="collapseSett" class="collapsed na">
                                            设置
                                        </a>
                                    </p>
                                </div>
                                <div id="collapseSett" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSett" aria-expanded="flase">
                                    <div class="panel-body">

                                            <span><a class="naCh" href="">背景设置</a></span><br>
                                            <span><a class="naCh" href="">1-2</a></span>

                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </li>
                    <li>
							<span class="ply">
								<span class="glyphicon glyphicon-road"></span>
							</span>
                        <p style="margin-left: 25px;"><a class="na" href="">关于</a></p>
                        <!-- <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default" style="padding: 0;margin-left: 15px;border: none;background: none;background-color: none;">
                                <div class="panel-heading" role="tab" id="headingAbout"
                                style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                    <p class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAbout" aria-expanded="flase"
                                         aria-controls="collapseAbout" class="collapsed na">
                                            关于
                                        </a>
                                    </p>
                                </div>
                                <div id="collapseAbout" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAbout" aria-expanded="flase">
                                    <div class="panel-body">

                                            <span><a class="naCh" href="">1-1</a></span><br>
                                            <span><a class="naCh" href="">1-2</a></span>

                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </li>
                </ul>
            </div>
        </div>
        <!-- 侧边栏按钮 -->
        <button class="kj"></button>
    </div>

    <!-- 内容区域 -->
    <div style="width: auto;height: 62px;z-index: 50;">

        <div style="margin-left: 280px;margin-top: 10px; z-index: 60; background: whitesmoke;">
            <nav class="navbar navbar-default nav-divider" style="width: 100%; margin-right:50px;background: #F5F5F5;color: #122B40;">
                <div class="navbar-header">
                    <button style="float: right;border-radius: 3px;" type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">DM</a>
                </div>
                <div class="container-fluid" style="">
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="#">主页</a></li>
                            <li><a href="#">关于</a></li>
                            <li><a href="#">控制台</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">

                            <li><a href="#">阅读 <span class="sr-only">(current)</span></a></li>
                            <li>
                                <a style=" margin: 0; padding: 0px;" href="#">
                                    <img style="width: 50px; height: 50px;" src="${pageContext.request.contextPath}/static/images/avatar/headSculpture.jpg" alt="avatar"
                                         onerror="this.src='${pageContext.request.contextPath}/static/images/avatar/headSculpture.jpg'" class="img-circle"></a>
                            </li>
                            <li><a href="#">退出</a></li>
                            <li><a href="#">返回</a></li>
                        </ul>
                    </div>
                </div>
            </nav>

        </div>


    </div>


    <div style="position: absolute;top: 72px; bottom: 0; text-align: center;overflow-y: scroll;color: wheat;">
        <h2>内容框</h2>
        <!-- footer-->
        <div class="footer">
            <p><span>© 2020 xhmy.cloud . &nbsp;</span><span>赣ICP证</span> &nbsp;<span> </span> &nbsp;<a class="icp" href="https://beian.miit.gov.cn"
                                                                                                       title="" target="_blank">赣ICP备</a></p>
        </div>
    </div>
</div>


<!--
			<script type="text/javascript">
				$(function() {
					// 侧边栏弹出
					$('button.kj').click(function() {
						var left = $('#wrapper')[0].offsetLeft;
						if (left == 0) {
							$('#wrapper').offset({
								'left': 220
							});
							$(this).css('transform', 'rotate(450deg)');
							$('.headSculpture img').addClass('img');
							$('.headSculpture p').addClass('opacity');
							setTimeout(function() {
								$('.option ul>li').addClass('li');
							}, 600)
						} else {
							$('#wrapper').offset({
								'left': 0
							});
							$(this).css('transform', 'rotate(0deg)');
							setTimeout(function() {
								$('.headSculpture img').removeClass('img');
								$('.headSculpture p').removeClass('opacity');
								$('.option ul>li').removeClass('li');
							}, 300)
						}
					})



				})
			</script>

-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
</body>
</html>
