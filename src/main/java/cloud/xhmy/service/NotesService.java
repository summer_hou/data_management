package cloud.xhmy.service;

import cloud.xhmy.pojo.Books;
import cloud.xhmy.pojo.Notes;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface NotesService {

    /**
     * 增加书籍
     * @param notes
     * @return
     */
    int addNotes(Notes notes);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<Notes> queryByName(String name, int page, int size);

    /**
     * 列出所有
     * @param page
     * @param size
     * @return
     */
    List<Notes> listAll(String author,int page, int size);

    /**
     * 修改
     * @param id
     * @return
     */
    int modifyById(int id);

    /**
     * 用于修改
     * @param id
     * @return
     */
    Notes queryById(int id);
    
}
