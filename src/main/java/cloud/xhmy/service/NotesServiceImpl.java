package cloud.xhmy.service;

import cloud.xhmy.dao.NotesMapper;
import cloud.xhmy.pojo.Notes;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public class NotesServiceImpl implements NotesService {

    private NotesMapper notesMapper;

    public void setNotesMapper(NotesMapper notesMapper) {
        this.notesMapper = notesMapper;
    }

    public int addNotes(Notes notes) {
        return this.notesMapper.addNotes(notes);
    }

    public int deleteById(int id) {
        return this.notesMapper.deleteById(id);
    }

    public List<Notes> queryByName(String name, int page, int size) {
        return this.notesMapper.queryByName(name,page,size);
    }

    public List<Notes> listAll(String author,int page, int size) {
        return this.notesMapper.listAll(author,page, size);
    }

    public int modifyById(int id) {
        return this.notesMapper.modifyById(id);
    }

    public Notes queryById(int id) {
        return this.notesMapper.queryById(id);
    }
}
