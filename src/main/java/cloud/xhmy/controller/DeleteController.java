package cloud.xhmy.controller;

import cloud.xhmy.service.*;
import cloud.xhmy.util.upload.FilesPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cloud.xhmy.util.upload.FilesPath.*;

/**
 * @author aliketh.xhmy
 */
@Controller
//@RequestMapping("/delete")
public class DeleteController {

    @Autowired
    @Qualifier("blogServiceImpl")
    private BlogService blogService;
    @Autowired
    @Qualifier("booksServiceImpl")
    private BooksService booksService;
    @Autowired
    @Qualifier("eBooksServiceImpl")
    private EBooksService eBooksService;
    @Autowired
    @Qualifier("usersServiceImpl")
    private UsersService usersService;
    @Autowired
    @Qualifier("loginLogServiceImpl")
    private LoginLogService loginLogService;
    @Autowired
    @Qualifier("notesServiceImpl")
    private NotesService notesService;


//    private static final Logger logger = new LogFactory(DeleteController.class);







//    @RequestMapping("/showDeleteFiles")
//    public String deleteFile(HttpServletRequest request, Model model) {
//        String realpath = request.getSession().getServletContext().getRealPath("/uploadFiles");
//        File dir = new File(realpath);
//        File files[] = dir.listFiles();
//        //获取该目录下的所有文件名
//        ArrayList<String> fileName = new ArrayList<String>();
//        for (int i = 0; i < files.length; i++) {
//            fileName.add(files[i].getName());
//        }
//        model.addAttribute("files", fileName);
//
//
//        return "showDeleteFiles";
//    }

    /**
     * 通用文件删除
     * @param filenames
     * @param request
     * @param response
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Map<String,String> delete(@RequestParam("delete") List<String> filenames, HttpServletRequest request,
                      HttpServletResponse response, Model model) {
        String aFilePath = null; //要下载的文件路径
//        ModelAndView mv = new ModelAndView();
////        mv.setViewName("showDeleteFiles");
        Map<String,String> map = new HashMap<>();

        try {
            //单文件删除
//            aFilePath = request.getSession().getServletContext().getRealPath("/static")
//            System.out.println("===================" + aFilePath);
//            String stringFile = aFilePath + "\\"+ filename;
//            System.out.println("==================="+stringFile);
//            //文件文件路径
//            File deleteFile = new File(stringFile);
//            if (deleteFile.delete()) {
//                System.out.println(deleteFile.getName() + "\n===================文件已被删除！");
//                model.addAttribute("success", "成功：文件已被删除！");
//            } else {
//                System.out.println("===================文件删除失败！");
//                model.addAttribute("failure", "文件删除失败！");
//            }
            List<String> strings = filenames;
            for (int i = 0; i < strings.size(); i++) {

                String filename = strings.get(i);
                aFilePath = request.getSession().getServletContext().getRealPath("/static") +"/"+ FilesPath.parsingPath(filename);
                String stringFile = aFilePath + FilesPath.parsingName(filename);
                System.out.println("=========================="+stringFile);
                //文件文件路径
                File deleteFile = new File(stringFile);
                if (deleteFile.delete()) {
                    System.out.println(deleteFile.getName() + "\n===================文件已被删除！");
//                    model.addAttribute("success", "成功：文件已被删除！");

                } else {
                    System.out.println(deleteFile.getName() + "===================文件删除失败！");
//                    model.addAttribute("failure", "文件删除失败！");
                }

//                map.put("states","1");
//                map.put("message","删除成功！");
//                return map;
            }
        } catch (Throwable e) {
            e.printStackTrace();
            map.put("sta","0");
            map.put("msg","删除失败！");
            return map;
        }
        map.put("sta","1");
        map.put("msg","删除成功！");
        return map;
    }

}
