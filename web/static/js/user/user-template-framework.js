$(function() {
    // 侧边栏弹出
    $('button.kj').click(function() {
        var left = $("#wrapper")[0].offsetLeft;
        if (left == 0) {
            $('#wrapper').offset({
                'left': 220
            });
            $(this).css('transform', 'rotate(450deg)');
            $('.headSculpture img').addClass('img');
            $('.headSculpture p').addClass('opacity');
            setTimeout(function() {
                $('.option ul>li').addClass('li');
            }, 600)
        } else {
            $('#wrapper').offset({
                'left': 0
            });
            $(this).css('transform', 'rotate(0deg)');
            setTimeout(function() {
                $('.headSculpture img').removeClass('img');
                $('.headSculpture p').removeClass('opacity');
                $('.option ul>li').removeClass('li');
            }, 300)
        }
    })

});



// 头像预览
// 找到头像的input标签绑定change事件
$("#avatar").change(function () {
    // 1. 创建一个读取文件的对象
    var fileReader = new FileReader();
    // 取到当前选中的头像文件
    // console.log(this.files[0]);
    // 读取你选中的那个文件
    fileReader.readAsDataURL(this.files[0]);  // 读取文件是需要时间的
    fileReader.onload = function () {
        // 2. 等上一步读完文件之后才 把图片加载到img标签中
        $("#avatar-img").attr("src", fileReader.result);
    };
});

$("#form-btn").click(function() {
    var formData = new FormData();
    formData.append('avatar', document.getElementById('avatar').files[0]);
    $.ajax({
        url:"${pageContext.request.contextPath}/upload/avatar",
        data:formData,
        processData:false,
        contentType:false,
        type:"POST",
        dataType:"json",
        success:function(data) {

            if (data.sta == "ok") {
                alert("修改成功："+data.msg)
            } else {
                alert("修改失败！");
            }
        },
        "error":function(xhr) {
            alert("您的登录信息已经过期，请重新登录！\n\nHTTP响应码：" + xhr.status);
        }
    });
});





// /*
// 部分引用
// www.jq22.com
// 作者：袁金
// 请不要用于非法行为
// */