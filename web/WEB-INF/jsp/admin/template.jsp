
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/9
  Time: 16:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>Comprehensive library management system</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico">
    <%--    模板背景--%>
    <link rel="stylesheet"  type="text/css"
          href="${pageContext.request.contextPath}/static/css/admin/admin-template-font.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/admin/admin-template-style.css">
    <%--    jquery--%>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>
    <%--    bootstrap--%>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css">
    <%--    模板--%>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/admin/admin-template-framework.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/admin/admin.js"></script>
    <%--    分页--%>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/page-helper.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>
    <%--    查询--%>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/admin/admin-query.js"></script>


    <%--    md--%>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">


</head>
<body>
<%--<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav1.jsp"/>--%>
<canvas id="canvas" width="100%" height="100%"></canvas>
<%--模板文字--%>
<script type="text/javascript"
        src="${pageContext.request.contentType}/static/js/admin/admin-template-back.js"></script>
<p id="offscreen-text" class="offscreen-text"></p>
<p id="text" class="text"></p>


<div style="position: absolute;top: 0;left: 0; width: 100%;height: 100%; background: rgba(100,100,200,0.5) ;overflow-y: scroll;">


    <div class="container">

        <span id="btnLeft" class="glyphicon glyphicon-th"
              style="width: 50px;height: 50px;padding: 18px;background-color: rgba(190,191,15,0.5); color: rgb(195,191,19); border-radius: 5px; margin-top: 10px;margin-left: 10px;"></span>
    </div>
    <div style="width: auto;height: 62px;z-index: 100; margin-top: -60px;">

        <div style="margin-left: 220px;width: auto;margin-top: 10px; z-index: 100;">
            <nav class="navbar navbar-inverse"
                 style="width: 100%; margin-right:50px;background: rgba(10,00,50,0.8);color: #122B40;">
                <div class="navbar-header">
                    <button style="width: 44px; height: 34px;border: unset;" type="button"
                            class="navbar-toggle collapsed"
                            data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">CLMS</a>
                </div>
                <div class="container-fluid" style="">
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="liTopNav"><a href="javascript:homeTopNav()" data-id="homeTopNav">主页</a></li>
                            <li class="liTopNav"><a href="javascript:aboutTopNav()" data-id="aboutTopNav">关于</a></li>
                            <li class="liTopNav"><a href="javascript:controllerTopNav()" data-id="controllerTopNav">控制台</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">

                            <li class="liTopNav"><a href="javascript:readMeTopNav()" data-id="readMeTopNav">阅读 <span class="sr-only">(current)</span></a></li>
                            <li>
                                <a style=" margin: 0; padding:0px;" href="javascript:myselfTopNav()">
                                    <img style="width: 50px; height:50px;"
                                         src="${pageContext.request.contextPath}/static${user.avatar}" alt="avatar"
                                         onerror="this.src='${pageContext.request.contextPath}/static/images/logo.jpg'"
                                         class="img-circle"></a>
                            </li>
                            <li><a href="${pageContext.request.contextPath}/admin/exit">退出</a></li>
                            <li class="liTopNav"><a href="javascript:adminBack()" data-id="adminBack">返回</a></li>
                        </ul>
                    </div>
                </div>
            </nav>

        </div>
    </div>

    <div class="wrap">
        <div class="layer-left" style="width: 220px; height: 100%;">
            <div id="wrapper" class="">
                <div class="sidebar">
                    <div class="headSculpture">
                        <a href="" data-toggle="modal" data-target="#exampleModal" data-whatever="upAvatar">
                            <img src="${pageContext.request.contextPath}/static${user.avatar}" alt=""
                                 onerror="this.src='${pageContext.request.contextPath}/static/images/logo.jpg'"></a>
                        <p style="color: gold;">邮箱：<a class="na" href="">${user.email}</a></p>
                    </div>
                    <div class="option">
                        <ul>
                            <li>
							<span class="ply">
								<span class="glyphicon glyphicon-home"></span>
							</span>
                                <!-- <p style=""><a class="na" href="#"></a></p> -->
                                <div class="panel-group" id="home" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default"
                                         style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                        <div class="panel-heading" role="tab" id="headingHome"
                                             style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                            <p class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseHome" aria-expanded="true"
                                                   aria-controls="collapseHome" class=" na ">
                                                    首页
                                                </a>
                                            </p>
                                        </div>
                                        <div id="collapseHome" class="panel-collapse collapse in" role="tabpanel"
                                             aria-labelledby="headingHome" aria-expanded="true">
                                            <div class="panel-body">

                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:welcomeNav()" data-id="welcomeNav">欢迎</a></span><br>
<%--                                                <span class="naSpan"><a class="naCh"--%>
<%--                                                         href="javascript:homeBookNav()" data-id="homeBookNav">书籍</a></span><br>--%>
<%--                                                <span class="naSpan"><a class="naCh"--%>
<%--                                                         href="javascript:homeArticleNav()" data-id="homeArticleNav">博客</a></span>--%>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
							<span class="ply">
								<span class="glyphicon glyphicon-pencil"></span>
							</span>
                                <!-- <p><a class="na" href=""></a></p> -->
                                <div class="panel-group" id="record" role="tablist" aria-multiselectable="false">
                                    <div class="panel panel-default"
                                         style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                        <div class="panel-heading" role="tab" id="headingRecord"
                                             style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                            <p class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseRecord" aria-expanded="flase"
                                                   aria-controls="collapseRecord" class="collapsed na">
                                                    博客管理
                                                </a>
                                            </p>
                                        </div>
                                        <div id="collapseRecord" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingRecord" aria-expanded="flase">
                                            <div class="panel-body">
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:blogNav()" data-id="blogNav">添加博客</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:articleAuditNav()" data-id="articleAuditNav">审核列表</a></span><br>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
							<span class="ply">
								<span class="glyphicon glyphicon-upload"></span>
							</span>
                                <!-- <p><a class="na" href=""></a></p> -->
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default"
                                         style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                        <div class="panel-heading" role="tab" id="headingUpload"
                                             style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                            <p class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseUpload" aria-expanded="flase"
                                                   aria-controls="collapseUpload" class="collapsed na">
                                                    上传处理
                                                </a>
                                            </p>
                                        </div>
                                        <div id="collapseUpload" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingUpload" aria-expanded="flase">
                                            <div class="panel-body">
                                                <%--<span class="naSpan"><a class="naCh" href="auditPictureNav()" data-id="auditPictureNav">图片</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:auditBookNav()" data-id="auditBookNav">图书</a></span><br>--%>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:auditMediaNav()" data-id="auditMediaNav">媒体文件</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:auditEBookNav()" data-id="auditEBookNav">电子书</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:uploadOperationNav()" data-id="uploadOperationNav">操作</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
							<span class="ply">
								<span class="glyphicon glyphicon-user"></span>
							</span>
                                <!-- <p><a class="na" href=""></a></p> -->
                                <div class="panel-group" id="star" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default"
                                         style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                        <div class="panel-heading" role="tab" id="headingStar"
                                             style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                            <p class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseStar" aria-expanded="flase"
                                                   aria-controls="collapseStar" class="collapsed na">
                                                    用户处理
                                                </a>
                                            </p>
                                        </div>
                                        <div id="collapseStar" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingStar" aria-expanded="flase">
                                            <div class="panel-body">
                                                <%--                                                <span class="naSpan"><a class="naCh" href="javascript:;" data-id=""></a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:userPunishNav()" data-id="userPunishNav">处罚</a></span><br>--%>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:blacklistNav()" data-id="blacklistNav">黑名单</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:userManageNav()" data-id="userManageNav">用户管理</a></span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="ply">
                                    <span class="glyphicon glyphicon-book"></span>
                                </span>
                                <!-- <p><a class="na" href=""></a></p> -->
                                <div class="panel-group" id="down" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default"
                                         style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                        <div class="panel-heading" role="tab" id="headingDown"
                                             style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                            <p class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseDown" aria-expanded="flase"
                                                   aria-controls="collapseDown" class="collapsed na">
                                                    图书操作处理
                                                </a>
                                            </p>
                                        </div>
                                        <div id="collapseDown" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingDown" aria-expanded="flase">
                                            <div class="panel-body">

                                                <%--<span class="naSpan"><a class="naCh"
                                                         href="javascript:borrowDisposeNav()" data-id="borrowDisposeNav">问题反馈处理</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:subscribeDisposeNav()" data-id="subscribeDisposeNav">预约处理</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:feedbackDisposeNav()" data-id="feedbackDisposeNav">问题反馈处理</a></span><br>--%>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:lostFeedDisposeNav()" data-id="lostFeedDisposeNav">申报处理</a></span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
							<span class="ply">
								<span class="glyphicon glyphicon-hdd"></span>
							</span>
                                <!-- <p><a class="na" href=""></a></p> -->
                                <div class="panel-group" id="dat" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default"
                                         style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                        <div class="panel-heading" role="tab" id="headingData"
                                             style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                            <p class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseData" aria-expanded="flase"
                                                   aria-controls="collapseData" class="collapsed na">
                                                    数据统计
                                                </a>
                                            </p>
                                        </div>
                                        <div id="collapseData" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingData" aria-expanded="flase">
                                            <div class="panel-body">

<%--                                                <span class="naSpan"><a class="naCh"--%>
<%--                                                         href="javascript:controllerNav()" data-id="controllerNav">总体预览</a></span><br>--%>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript: articleNav()" data-id="articleNav">博客数据</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:eBookNav()" data-id="eBookNav">电子书数据</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:subscribeNav()" data-id="subscribeNav">预约数据</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:feedbackNav()" data-id="feedbackNav">问题反馈数据</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:lostFeedNav()" data-id="lostFeedNav">申报数据</a></span><br>
                                                <%--<span class="naSpan"><a class="naCh"
                                                         href="javascript:notesNav()" data-id="notesNav">笔记数据</a></span><br>--%>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:bookNav()" data-id="bookNav">图书数据</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:mediaNav()" data-id="mediaNav">媒体数据</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:downloadNav()" data-id="downloadNav">下载数据</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:uploadNav()" data-id="uploadNav">上传数据</a></span><br>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:borrowNav()" data-id="borrowNav">借阅数据</a></span><br>
                                                <%--<span class="naSpan"><a class="naCh"
                                                         href="javascript:browseNav()" data-id="browseNav">浏览数据</a></span><br>--%>
                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:loginLogNav()" data-id="loginLogNav">日志数据</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                 <span class="ply">
                                     <span class="glyphicon glyphicon-th-list"></span>
                                 </span>
                                <!-- <p><a class="na" href=""></a></p> -->
                                <div class="panel-group" id="cla" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default"
                                         style="padding: 0;margin-left: 15px;border: none; background: unset;">
                                        <div class="panel-heading" role="tab" id="headingCla"
                                             style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                            <p class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseCla"
                                                   aria-expanded="flase"
                                                   aria-controls="collapseCla" class="collapsed na">
                                                    分类
                                                </a>
                                            </p>
                                        </div>
                                        <div id="collapseCla" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingCla" aria-expanded="flase">
                                            <div class="panel-body">

                                                <span class="naSpan"><a class="naCh"
                                                         href="javascript:classifyNav()" data-id="classifyNav">图书分类</a></span><br>
<%--                                                <span class="naSpan"><a class="naCh"--%>
<%--                                                         href="javascript:eFormatNav()" data-id="eFormatNav">电子书格式分类</a></span>--%>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <%--
                            <li>
							<span class="ply">
								<span class="glyphicon glyphicon-file"></span>
							</span>
                                <!-- <p><a class="na" href=""></a></p> -->
                                <div class="panel-group" id="doc" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default"
                                         style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                        <div class="panel-heading" role="tab" id="headingDoc"
                                             style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                            <p class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseDoc"
                                                   aria-expanded="flase"
                                                   aria-controls="collapseDoc" class="collapsed na">
                                                    阅读文档
                                                </a>
                                            </p>
                                        </div>
                                        <div id="collapseDoc" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingDoc" aria-expanded="flase">
                                            <div class="panel-body">

                                                <span class="naSpan"><a class="naCh" href="javascript:docNav()" data-id="docNav">项目文档</a></span><br>
                                                <span class="naSpan"><a class="naCh" href="javascript:readMeNav()" data-id="readMeNav">环境配置文档</a></span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
							<span class="ply">
								<span class="glyphicon glyphicon-cog"></span>
							</span>
                                <div class="panel-group" id="setting" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default"
                                         style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                        <div class="panel-heading" role="tab" id="headingSetting"
                                             style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                            <p class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseSetting"
                                                   aria-expanded="flase"
                                                   aria-controls="collapseSetting" class="collapsed na">
                                                    设置
                                                </a>
                                            </p>
                                        </div>
                                        <div id="collapseSetting" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingSetting" aria-expanded="flase">
                                            <div class="panel-body">
                                                <span class="naSpan"><a class="naCh" href="javascript:settingNav()" data-id="settingNav">设置</a></span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            --%>
                            <li>
							<span class="ply">
								<span class="glyphicon glyphicon-open-file"></span>
							</span>
                                <div class="panel-group" id="restful" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default"
                                         style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                        <div class="panel-heading" role="tab" id="headingRestFul"
                                             style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                            <p class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseRestFul"
                                                   aria-expanded="flase"
                                                   aria-controls="collapseRestFul" class="collapsed na">
                                                    控制台api
                                                </a>
                                            </p>
                                        </div>
                                        <div id="collapseRestFul" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingRestFul" aria-expanded="flase">
                                            <div class="panel-body">
                                                <span class="naSpan"><a class="naCh" href="${pageContext.request.contextPath}/swagger-ui.html" data-id="apiNav" target="_blank">swagger-api</a></span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
							<span class="ply">
								<span class="glyphicon glyphicon-link"></span>
							</span>
                                <div class="panel-group" id="about" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default"
                                         style="padding: 0;margin-left: 15px;border: none;background: unset;">
                                        <div class="panel-heading" role="tab" id="headingAbout"
                                             style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                            <p class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseAbout"
                                                   aria-expanded="flase"
                                                   aria-controls="collapseAbout" class="collapsed na">
                                                    关于
                                                </a>
                                            </p>
                                        </div>
                                        <div id="collapseAbout" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingAbout" aria-expanded="flase">
                                            <div class="panel-body">

<%--                                                <span class="naSpan"><a class="naCh" href="javascript:docNav()" data-id="docNav"></a></span><br>--%>
                                                <span class="naSpan"><a class="naCh" href="javascript:aboutNav()" data-id="aboutNav">关于</a></span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
<%--                                <p style="margin-left: 25px;"></p>--%>

                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <%--context--%>
    <div style="width: 100%;margin: 10px auto;color: gold;">
<%--        <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/admin/index.jsp"/>--%>



        <div id="content-list">

        </div>
            <%--
        <div id="article">
            <table>
                <tbody id="article-list"></tbody>
            </table>
        </div>
        <div id="blacklist">
            <table>
                <tbody id="blacklist-list"></tbody>
            </table>
        </div>
        <div id="book">
            <table>
                <tbody id="book-list"></tbody>
            </table>
        </div>
        <div id="borrow">
            <table>
                <tbody id="borrow-list"></tbody>
            </table>
        </div>
        <div id="download">
            <table>
                <tbody id="download-list"></tbody>
            </table>
        </div>
        <div id="feedback">
            <table>
                <tbody id="feedback-list"></tbody>
            </table>
        </div>
        <div id="loginLog">
            <table>
                <tbody id="loginLog-list"></tbody>
            </table>
        </div>
        <div id="lostFeed">
            <table>
                <tbody id="lostFeed-list"></tbody>
            </table>
        </div>
        <div id="media">
            <table>
                <tbody id="media-list"></tbody>
            </table>
        </div>
        <div id="subscribe">
            <table>
                <tbody id="subscribe-list"></tbody>
            </table>
        </div>
        <div id="upload">
            <table>
                <tbody id="upload-list"></tbody>
            </table>
        </div>
        <div id="user">
            <table>
                <tbody id="user-list"></tbody>
            </table>
        </div>
        <div>
            <table>
                <tbody></tbody>
            </table>
        </div>
        <div>
            <table>
                <tbody></tbody>
            </table>
        </div>
        <div>
            <table>
                <tbody></tbody>
            </table>
        </div>
        <div>
            <table>
                <tbody></tbody>
            </table>
        </div>
        <div>
            <table>
                <tbody></tbody>
            </table>
        </div>
        <div>
            <table>
                <tbody></tbody>
            </table>
        </div>
        <div>
            <table>
                <tbody></tbody>
            </table>
        </div>
        <div>
            <table>
                <tbody></tbody>
            </table>
        </div>
        --%>
    </div>


    <%--    footer--%>
    <div class="footer">
        <p>
            <span>© 2020 xhmy.cloud . &nbsp;</span>
            <span>赣ICP证</span> &nbsp;
            <a class="icp" href="https://beian.miit.gov.cn" title="" target="_blank">赣ICP备</a>
            <span>2020012707号</span> &nbsp;
        </p>
    </div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document"
         style="position: fixed;left: 50%;top: 50%; width: 340px; height: 340px; margin-top: -170px; margin-left: -170px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span id="clo" aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">跟换头像</h4>
            </div>
            <div class="modal-body">
                <form id="form-upload">
                    <section>
                        <input type="hidden" name="uId" value="${avatarEmail}">
                        <div class="row">

                            <div class="" style="">
                                <label for="avatar"><img id="avatar-img"
                                                         style="width: 200px; height: 200px ;margin-left: 70px;"
                                                         src="${pageContext.request.contextPath}/static${user.avatar}"
                                                         alt="" class="img-circle"
                                                         onerror="this.src='${pageContext.request.contextPath}/static/images/avatar/logo.jpg'"></label>
                                <input accept="image/*" type="file" name="avatar" id="avatar" value="${avatar}"
                                       style="display: none">
                                <span class="help-block"></span>
                            </div>

                        </div>

                        <div class="" style="text-align: center;">
                            <input style=" margin-right: 30px;" id="form-btn" type="button" class="btn btn-primary"
                                   value="上传"/>

                            <a href="${pageContext.request.contextPath}/refresh"><input style="" id="form-seb"
                                                                                        type="button"
                                                                                        class="btn btn-primary"
                                                                                        value="刷新"/></a>

                        </div>
                    </section>

                </form>

            </div>
        </div>
    </div>
    <script type="text/javascript">
        // 头像预览
        // 找到头像的input标签绑定change事件
        $("#avatar").change(function () {
            // 1. 创建一个读取文件的对象
            var fileReader = new FileReader();
            // 取到当前选中的头像文件
            // console.log(this.files[0]);
            // 读取你选中的那个文件
            fileReader.readAsDataURL(this.files[0]); // 读取文件是需要时间的
            fileReader.onload = function () {
                // 2. 等上一步读完文件之后才 把图片加载到img标签中
                $("#avatar-img").attr("src", fileReader.result);
            };
        });

        $("#form-btn").click(function () {
            var formData = new FormData();
            formData.append('avatar', document.getElementById('avatar').files[0]);
            $.ajax({
                url: "${pageContext.request.contextPath}/admin/avatar",
                data: formData,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {

                    if (data.sta == "ok") {
                        alert("修改成功：" + data.msg)
                    } else {
                        alert("修改失败！");
                    }
                },
                "error": function (xhr) {
                    alert("您的登录信息已经过期，请重新登录！\n\nHTTP响应码：" + xhr.status);
                }
            });
        });
    </script>
</div>

<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/admin/admin-template-framework.js"></script>
<%--<script src="${pageContext.request.contextPath}/static/js/admin/admin-jigsaw.min.js"></script>--%>
<script>
    window.onload = function () {

        //移动端使用touchend
        var event = navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i) ? 'touchend' : 'click';

        // 选择器
        var Q = function (id) {
            return document.getElementById(id)
        };

        //方向控制
        //左
        var _left = new mSlider({
            dom: ".layer-left"
        });

        Q('btnLeft').addEventListener(event, function () {
            _left.open();
        })
    }
</script>
    <%--
<script type="text/javascript">
    //----------------------------------------------------------------------------------------------------
    function welcomeNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/welcome");
    };
    function homeBookNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/homeBook");
    };
    function homeArticleNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/homeArticle");
    };
    //----------------------------------------------------------------------------------------------------
    function blogs() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toEditorArticle");
    };
    function articleAuditNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toArticleAudit");
    };
    //----------------------------------------------------------------------------------------------------
    function auditBookNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toAuditBook");
    };

    function auditMediaNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toAuditMedia");
    };

    function auditEBookNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toAuditEBook");
    };

    function uploadOperationNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/uploadOperation");
    };
    //----------------------------------------------------------------------------------------------------
    function userPunlishNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toUserPunlish");
    };
    function blacklistNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toBlacklist");
    };
    function userManageNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/userManage");
    };
    //----------------------------------------------------------------------------------------------------

    function borrowDisposeNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toBorrowDispose");
    };function subscribeDisposeNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toSubscribeDispose");
    };
    function lostFeedDisposeNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toLostFeedDispose");
    };
    function feedbackDisposeNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toFeedbackDispose");
    };
    //----------------------------------------------------------------------------------------------------
    function controllerNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toController");
    };
    function subscribeNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toSubscribe");
    };
    function eBookNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toEBook");
    };
    function lostFeedNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toLostFeed");
    };
    function feedbackNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toFeedback");
    };
    function articleNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toArticle");
    };
    function bookNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toBook");
    };
    function borrowNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toBorrow");
    };
    function mediaNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toMedia");
    };
    function downloadNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toDownload");
    };
    function uploadNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toUpload");
    };
    function browseNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toBrowse");
    };
    function loginLogNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toLoginLog");
    };
    //----------------------------------------------------------------------------------------------------
    function classifyNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/classify");
    };
    function eFormatNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/toEFoemat");
    };
    //----------------------------------------------------------------------------------------------------
    function docNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/doc");
    };
    function readMeNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/readMe");
    };
    //----------------------------------------------------------------------------------------------------
    function settingNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/setting");
    };
    //----------------------------------------------------------------------------------------------------
    function aboutNav() {
        $("#content-list").load("${pageContext.request.contextPath}/admin/about");
    };
    //----------------------------------------------------------------------------------------------------
</script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/codemirror/codemirror.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/marked.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/prettify.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/raphael.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/underscore.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/sequence-diagram.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/flowchart.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/jquery.flowchart.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/editormd.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>

    <script>
        function loadContent(){
             $("#article-content").load("/static${articles.blogContent}");
        };
    </script>
    --%>
</body>
</html>
