<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/31
  Time: 16:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>notes</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>


    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div  style="width: 90%; margin: 5px auto;">
    <div class="row" style="margin: 30px">
        <span><button><a href="#upNotes" class="btn btn-primary">本地上传</a></button></span>
        <span><button id="save"  class="btn btn-primary">保存</button></span>
        <span><a href="${pageContext.request.contextPath}/allNotes/${user.email}" ><button  class="btn btn-primary">所有笔记</button></a></span>
    </div>
    <div class="">
        <div>
            <span>${success}</span>
            <span>${error}</span>
        </div>
        <div style="width: 100%;margin-right: 0px;font-size: 16px;">
            <form action="">
                <span>笔记名称：</span><input class="form-control" id="notesName" name="notesName" type="text"><br>
                <span>笔记作者：</span><input class="form-control" id="notesAuthor" name="notesAuthor" value="${user.email}" readonly type="text">
            </form>
        </div>



    </div>
</div>
<div style="margin: 10px auto ; text-align: center;">
    <h3><span>笔记内容</span></h3>
</div>
<div id="notes-editor">

    <textarea style="display:none;" class="editormd-markdown-textarea" id="notesDate" name="editormd"></textarea>
    <textarea class="editormd-html-textarea" name="text" id="editormdData"></textarea>

</div>

<div id="upNotes" style="width: 90%; margin: 20px auto;font-size: 16px;">
    <form  id="upload-notes" action="${pageContext.request.contextPath}/upload/notes" method="post" enctype="multipart/form-data">
        <span>文件：</span><input type="file" name="scan"><br>
        <span>名称：</span><input class="form-control" type="text" name="notesName"><br>
        <span>作者：</span><input class="form-control" type="text" name="notesAuthor" value="${user.email}" readonly><br>
        <span><input type="button" class="btn btn-primary" id="up-notes" value="提交"></span>

    </form>
</div>

<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>

<script src="${pageContext.request.contextPath}/plugins/editor-md/editormd.min.js"></script>
<script type="text/javascript">
    $(function () {
        var editor = editormd("notes-editor", {
            width: "90%",
            height: 700,
            path: "${pageContext.request.contextPath}/plugins/editor-md/lib/",
            imageUpload: true,
            imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL: "${pageContext.request.contextPath}/upload/mdImages",
            saveHTMLToTextarea : true
        });
    });
    $("#save").click(function () {
        var data = $("#notesDate").val();
        //alert(html);
        $.ajax({
            url:'${pageContext.request.contextPath}/saveNotes',
            data:{
                'data':data,
                'notesName':$("#notesName").val(),
                'notesAuthor':$("#notesAuthor").val()
            },
            dataType:'json',
            type:'post',

            success:function (data) {
                if (data.code =="1"){
                    alert(data.msg);
                }
               else {
                   alert("保存失败！");
                }
            }
        })
    });
    $("#up-notes").click(function () {
        var formData = new FormData(document.getElementById("upload-notes"));
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/upload/notes',

            data:formData,
            processData:false,
            contentType:false,
            dataType:'json',

            success:function (data) {
                alert(data.msg);
            }
        });

    });
</script>
</body>
</html>
