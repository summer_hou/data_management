<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/27
  Time: 0:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div style=" width: 90%;margin: auto;">
    <div class="container">
        <div class="row" style="margin: auto;">

            <div class="col-md-3" style=" ">
                <img src="${pageContext.request.contextPath}/static${book.coverPicture}"
                     alt="抱歉图片意外丢失，要图片显示请重新刷新页面"
                     style=" margin: auto; padding: 10px; width: 240px;height: 320px;"
                     onerror="this.src='${pageContext.request.contextPath}/static/upload/picture/CMLS-NO-Cover.png'">
            </div>
            <div class="col-md-9" style="line-height: 2em;">
                <h3>书名：${book.bookName}</h3>
                <smell>作者：${book.author}</smell><br>
                <span><p>简介：${book.introduction}</p></span>
                <span>出版社：${book.publishHouse}</span><br>
                <span>出版时间：${book.publishTime}</span><br>
                <span>语言：${book.language}</span><br>
                <span>定价：${book.price}</span><br>
                <span>类型：${book.classify}</span>
                <span>数量：${book.bookNumber}</span>
                <span>地址：${book.bookLocation}</span>

            </div>
        </div>
        <%--
        <div class="row" style=" width: 100%;margin: auto;">

            <div class="col-md-3" style=" ">
                <img src="${pageContext.request.contextPath}/static${eBook.coverPicture}" alt=""
                     style=" margin:0 auto;width: 240px;height: 320px;"
                >
            </div>
            <div class="col-md-9" style="line-height: 2em;">
                <span><h3>书名：${eBook.bookName}</h3></span>
                <span><smell>作者：${eBook.author}</smell></span><br>
                <span><p>简介：${eBook.introduction}</p></span><br>
                <span>出版社：${eBook.publishHouse}</span><br>
                <span>出版时间：${eBook.publishTime}</span><br>
                <span>语言：${eBook.language}</span><br>
                <span>类型：${eBook.classify}</span><br>
                <span>描述：${eBook.description}</span><br>
                <span>状态：${eBook.auditStatus}</span>

            </div>
        </div>
        --%>
    </div>
</div>