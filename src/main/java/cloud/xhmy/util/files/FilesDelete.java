package cloud.xhmy.util.files;

import cloud.xhmy.util.upload.FilesPath;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

/**
 * 删除文件静态工具
 * @author aliketh.xhmy
 */
public class FilesDelete {


    public static void delete(String name, HttpServletRequest request) {
        String aFilePath = null; //文件路径
        try {
            aFilePath = request.getSession().getServletContext().getRealPath("/static") + "/" + FilesPath.parsingPath(name);
            String stringFile = aFilePath + FilesPath.parsingName(name);
            //文件文件路径
            File deleteFile = new File(stringFile);
            if (deleteFile.delete()) {
                System.out.println(deleteFile.getName() + "\n===================文件已被删除！");
            } else {
                System.out.println(deleteFile.getName() + "===================文件删除失败！");
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


}
