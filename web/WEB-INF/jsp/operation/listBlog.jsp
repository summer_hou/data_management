<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/31
  Time: 16:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>listBlog</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>


    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/page-helper.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>




</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>


<div id="" style="" class="container">
    <div style="margin: 10px auto; width: 100%;" class="check-list">
        <span></span>
        <button style="margin-right: 15px;" class="btn btn-primary btn-default" data-id="pushed">已发布<a href="${pageContext.request.contextPath}/allBlog/${user.email}" style=" color: floralwhite;" ></a></button>

        <span></span>
        <button style="margin-right: 15px;" class="btn btn-primary" data-id="draft">草稿箱<a href="${pageContext.request.contextPath}/allDraftBlog/${user.email}" style=" color: floralwhite;"></a></button>

    </div>
    <div id="list-content-blog"></div>


</div>





<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>


<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/timeFormat.js"></script>

<script type="text/javascript">

    $(function(){
        $(".check-list").on("click", "button", function(){
            var checkId = $(this).data("id");  //获取data-id的值
            window.location.hash = checkId;  //设置锚点
            loadInner(checkId);
        });
        function loadInner(checkId){
            var checkId = window.location.hash;
            var pathn, i;
            switch(checkId){
                case "#pushed": pathn = "${pageContext.request.contextPath}/allBlog/${user.email}"; i = 0; break;
                case "#draft": pathn = "${pageContext.request.contextPath}/allDraftBlog/${user.email}"; i = 1; break;
                default: pathn = "${pageContext.request.contextPath}/allBlog/${user.email}"; i = 0; break;
            }
            $("#list-content-blog").load(pathn); //加载相对应的内容
            $(".userMenu button").eq(i).addClass("btn-default").siblings().removeClass("btn-default"); //当前列表高亮
        }
        var checkId = window.location.hash;
        loadInner(checkId);
    });


    function deleteBlog(did) {
        $.ajax({
            type: 'get',
            url: '${pageContext.request.contextPath}/delete/blog?id='+did,
            processData:false,
            contentType:false,
            dataType: 'json',
            success: function (data) {
                if (data.code == "1") {
                    alert("成功！")
                } else {
                    alert("失败！")
                }
            }

        })
    };
    function deleteDraftBlog(did) {
        $.ajax({
            type: 'get',
            url: '${pageContext.request.contextPath}/delete/blogDraft?id='+did,
            processData:false,
            contentType:false,
            dataType: 'json',
            success: function (data) {
                if (data.code == "1") {
                    alert("成功！")
                } else {
                    alert("失败！")
                }
            }

        })
    };
</script>
<%--
<script type="text/javascript">
    function delAllProduct() {
        var str = $("#del").val();
        $.ajax({
            type: 'get',
            url: "${pageContext.request.contextPath}/delete",
            dataType: "json",
            data: {
                delete: str
            },
            success: function (data) {
                if (data.sta=="1"){
                    alert(data.msg)
                }
                alert(data.msg)
            }
        });
    };
    function deleAllProduct() {
        var str = $("#dele").val();
        $.ajax({
            type: 'get',
            url: "${pageContext.request.contextPath}/delete",
            dataType: "json",
            data: {
                delete: str
            },
            success: function (data) {
                if (data.sta=="1"){
                    alert(data.msg)
                }
                alert(data.msg)
            }
        });
    };
</script>
  --%>


<script type="text/javascript">
    //全选/全不选
    $("#CheckAll").bind("click", function () {
        $("input[name='Check[]']").prop("checked", this.checked);
    });

    //批量删除
    function delAllProduct() {
        if (!confirm("确定要删除吗？")) {
            return;
        }
        var cks = $("input[name='Check[]']:checked");
        var str = "";
        //拼接所有的id
        for (var i = 0; i < cks.length; i++) {
            if (cks[i].checked) {
                str += cks[i].value + ",";
            }
        }
        //去掉字符串末尾的‘&'
        str = str.substring(0, str.length - 1);
        // var a = str.split('&');//分割成数组
        // console.log(str);return false;
        $.ajax({
            type: 'post',
            url: "${pageContext.request.contextPath}/blog/",
            dataType: "json",
            data: {
                id: str
            },
            success: function (data) {
                console.log(data);
                // return false;
                if (data.code == "1") {
                    alert('删除成功！');
                    // location.href = data['url'];
                } else {
                    alert('删除失败！');
                    // return false;
                }
            }
        });
    };
</script>
<script type="text/javascript">
    <%--
    function simpleDateFormat(gmtSimple) {
        if(null==gmtSimple || ""==gmtSimple){
            return "";
        }
        var dateStr=gmtSimple.trim().split(" ");
        var strGMT = dateStr[0]+" "+dateStr[1]+" "+dateStr[2]+" "+dateStr[5]+" "+dateStr[3]+" GMT+0800";
        var date = new Date(Date.parse(strGMT));
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        m = m < 10 ? ('0' + m) : m;
        var d = date.getDate();
        d = d < 10 ? ('0' + d) : d;
        var h = date.getHours();
        var minute = date.getMinutes();
        minute = minute < 10 ? ('0' + minute) : minute;
        var second = date.getSeconds();
        second = second < 10 ? ('0' + second) : second;

        var gmt_simple = y+"-"+m+"-"+d+" "+h+":"+minute+":"+second;
        return y+"-"+m+"-"+d+" "+h+":"+minute+":"+second;
        $(".gmt-simple").html(gmt_simple)
        // document.getElementsByClassName('gmt-simple').
    }
    --%>
</script>
</body>
</html>
