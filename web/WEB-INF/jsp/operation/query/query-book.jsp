<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/12/12
  Time: 18:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container " style=" width: 90%;margin: 0 auto;background: lightgoldenrodyellow;">
    <div class="book">
        <c:forEach var="books" items="${pageInfoBook.list}">
            <div class="row" style="margin:5px auto;">

                <div class="col-md-3" style=" ">
                    <img src="${pageContext.request.contextPath}/static${books.coverPicture}"
                         alt="抱歉图片加载意外，要图片显示请重新刷新页面"
                         style="width: 120px;height: 160px; margin: 0 auto;"
                         onerror="this.src='${pageContext.request.contextPath}/static/upload/picture/CMLS-NO-Cover.png'">
                </div>
                <div class="col-md-9" style="line-height: 2em;">
                    <span><h3><a href="${pageContext.request.contextPath}/browseBook/${books.id}">${books.bookName}</a></h3></span>
                    <span><smell>${books.author}</smell></span><br>
                    <span><p>${books.introduction}</p></span>
                    <span>出版社：${books.publishHouse}</span>
                    <span style="margin-left: 280px;">出版时间：${books.publishTime}</span><br>
                    <span>语言：${books.language}</span>
                    <span style="margin-left: 160px; margin-right: 100px;">类型：${books.classify}</span><span>当前图书剩余数量：${books.bookNumber}</span>

                </div>
            </div>
        </c:forEach>
    </div>
    <div class="xhmy_page_div"></div>
    <script type="text/javascript">
        //翻页
        $(".xhmy_page_div").createPage({
            pageNum: ${pageInfoBook.pages},
            current: 1,
            backfun: function (e) {
                //console.log(e);//回调
            }
        });

        function pageNumber(params) {
            $(".book").load("${pageContext.request.contextPath}/queryBook?search=${search} .book >*", {
                page: params,
                size: "10"
            });

        };

    </script>
</div>