package cloud.xhmy.dao;

import cloud.xhmy.pojo.UploadRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UploadMapper {
    /**
     * 增加
     * @param uploadRecord
     * @return
     */
    int addUploadRecord(UploadRecord uploadRecord);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 模糊查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<UploadRecord> queryByName(@Param("uploadUser") String name, int page, int size);

    /**
     * 列出个人所有
     * @param page
     * @param size
     * @return
     */
    List<UploadRecord> listAll(int page, int size);

    /**
     * 修改
     * @param id
     * @return
     */
    UploadRecord modifyById(int id);

    /**
     * 用于修改获取信息
     * @param id
     * @return
     */
    UploadRecord queryById(int id);
}
