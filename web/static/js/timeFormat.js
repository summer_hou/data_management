/**
 * 可以自定义是否显示 时间 星期 分割符
 *
 * @param {Object} inoutStr 输入的日期
 * @param {Object} showSplit 分割符
 * @param {Object} showWeek 星期
 * @param {Object} showtime 时间
 */
// Wed Mar 22 13:38:37 CST 2017 转换为 yyyy-MM-dd 
function todate(inoutStr, showSplit, showWeek, showtime) {
    //Wed Mar 22 13:38:37 CST 2017
    inoutStr = inoutStr + ""; //末尾加一个空格
    var date = "";
    var time = "";
    var month = new Array();
    var week = new Array();

    month["Jan"] = 1;
    month["Feb"] = 2;
    month["Mar"] = 3;
    month["Apr"] = 4;
    month["May"] = 5;
    month["Jan"] = 6;
    month["Jul"] = 7;
    month["Aug"] = 8;
    month["Sep"] = 9;
    month["Oct"] = 10;
    month["Nov"] = 11;
    month["Dec"] = 12;
    week["Mon"] = "一";
    week["Tue"] = "二";
    week["Wed"] = "三";
    week["Thu"] = "四";
    week["Fri"] = "五";
    week["Sat"] = "六";
    week["Sun"] = "日";

    str = inoutStr.split(" ");

    date = str[5];
    time = str[3];
    date += showSplit + month[str[1]] + showSplit + str[2];
    if (showWeek) {
        date += "    " + " 星期" + week[str[0]];
    }
    if (showtime) {
        date += " " + time;
    }
    return date;
};

/**
 * 可以自定义是否显示  星期 分割符
 *默认不带 时间
 * @param {Object} inoutStr 输入的日期
 * @param {Object} showSplit 分割符
 * @param {Object} showWeek 星期
 */
function toDate(inoutStr, showSplit, showWeek) {
    var showtime = false;
    return todate(inoutStr, showSplit, showWeek, showtime);
};

/**
 * 可以自定义是否显示 时间 分割符
 * 默认不带 星期
 * @param {Object} inoutStr 输入的日期
 * @param {Object} showSplit 分割符
 * @param {Object} showtime 时间
 */
function toDatetime(inoutStr, showSplit, showtime) {
    var showWeek = false;
    return todate(inoutStr, showSplit, showWeek, showtime);
};

/**
 * 可以自定义 分割符
 * 默认不带 时间 星期
 * @param {Object} inoutStr 输入的日期
 * @param {Object} showSplit 分割符
 */
function toDate(inoutStr, showSplit) {
    var showWeek = false;
    var showtime = false;
    return todate(inoutStr, showSplit, showWeek, showtime);
};

/**
 * 可以自定义 分割符
 * 默认不带 星期
 * @param {Object} inoutStr 输入的日期
 * @param {Object} showSplit 分割符
 */
function toDatetime(inoutStr, showSplit) {
    var showWeek = false;
    var showtime = true;
    return todate(inoutStr, showSplit, showWeek, showtime);
};

/**不可以自定义
 * 默认不带 时间 星期
 * @param {Object} inoutStr 输入的日期
 */
function defToDate(inoutStr) {
    var showSplit = "-";
    var showWeek = false;
    var showtime = false;
    return todate(inoutStr, showSplit, showWeek, showtime);
};

/**不可以自定义
 * 默认不带 时间
 * @param {Object} inoutStr 输入的日期
 */
function defToDatetime(inoutStr) {
    var showSplit = "-";
    var showWeek = false;
    var showtime = true;
    return todate(inoutStr, showSplit, showWeek, showtime);
};