<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/22
  Time: 10:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <meta charset="UTF-8">
    <title>CLMS//Comprehensive library management system || PC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 网页图标 ico格式 -->
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico">
    <!-- jquery -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>
    <!-- 引入 bootstrap js文件 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- 引入 bootstrap css文件 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css" type="text/css">
    <!-- index css文件 -->
<%--    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/dm-index.css">--%>

    <style type="text/css">
      /**/
      html, body {
        overflow: hidden;
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
        background:#000;
      }
      canvas {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background:#000;
        cursor: pointer;
      }
      /* mnue*/

      div.demo {
        padding: 2em 0;
        transform: translate3d(0, 0, 0);
        position:relative;
        width: 40%;
        height: 50%;
        margin: 7% auto;
        z-index:100;
        color: wheat;
      }

      .navbar {
        width: 240px;
        height: 240px;
        line-height: 240px;
        border-radius: 50%;
        background: #fff;
        margin: 70px auto;
        position: relative;
        cursor: pointer;
        text-align: center;
        font-size: 1.75em;
        font-weight: bold;
        color: #383838;
        transition: 0.24s 0.2s;
      }

      .navbar:hover {
        background: rgba(255, 255, 255, 0.75);
      }

      .navbar .menu {
        list-style: none;
        padding: 0;
        margin: 0;
        position: absolute;
        top: -120px;
        left: -120px;
        border: 240px solid transparent;
        cursor: default;
        border-radius: 50%;
        transform: scale(0);
        transition: transform 1.4s 0.07s;
        z-index: -1;
      }

      .navbar:hover .menu {
        transition: transform 0.4s 0.08s, z-index 0s 0.5s;
        transform: scale(1);
        z-index: 1;
      }

      .navbar .menu li {
        position: absolute;
        top: -180px;
        left: -180px;
        transform-origin: 180px 180px;
        transition: all 0.5s 0.1s;
      }

      .navbar:hover .menu li {
        transition: all 0.6s;
      }

      .navbar .menu li a {
        transition: all .4s ease 0s;
        width: 90px;
        height: 90px;
        line-height: 90px;
        border-radius: 50%;
        background: rgba(94,96,190,0.6);
        position: absolute;
        font-size: 10%;
        color: #FFCC00;
        transition: 0.6s;
        text-decoration: none;
      }

      .navbar .menu li a:hover {
        background-color: rgba(16,19,160,0.6);
        color: #fff;
      }

      .navbar:hover .menu li:nth-child(1) {
        transition-delay: 0.02s;
        transform: rotate(85deg);
      }

      .navbar:hover .menu li:nth-child(1) a {
        transition-delay: 0.04s;
        transform: rotate(635deg);
      }

      .navbar:hover .menu li:nth-child(2) {
        transition-delay: 0.04s;
        transform: rotate(125deg);
      }

      .navbar:hover .menu li:nth-child(2) a {
        transition-delay: 0.08s;
        transform: rotate(595deg);
      }

      .navbar:hover .menu li:nth-child(3) {
        transition-delay: 0.06s;
        transform: rotate(165deg);
      }

      .navbar:hover .menu li:nth-child(3) a {
        transition-delay: 0.12s;
        transform: rotate(555deg);
      }

      .navbar:hover .menu li:nth-child(4) {
        transition-delay: 0.08s;
        transform: rotate(205deg);
      }

      .navbar:hover .menu li:nth-child(4) a {
        transition-delay: 0.16s;
        transform: rotate(515deg);
      }

      .navbar:hover .menu li:nth-child(5) {
        transition-delay: 0.1s;
        transform: rotate(245deg);
      }

      .navbar:hover .menu li:nth-child(5) a {
        transition-delay: 0.2s;
        transform: rotate(475deg);
      }

      .navbar:hover .menu li:nth-child(6) {
        transition-delay: 0.12s;
        transform: rotate(285deg);
      }

      .navbar:hover .menu li:nth-child(6) a {
        transition-delay: 0.24s;
        transform: rotate(435deg);
      }

      .navbar:hover .menu li:nth-child(7) {
        transition-delay: 0.14s;
        transform: rotate(325deg);
      }

      .navbar:hover .menu li:nth-child(7) a {
        transition-delay: 0.28s;
        transform: rotate(395deg);
      }

      .navbar:hover .menu li:nth-child(8) {
        transition-delay: 0.16s;
        transform: rotate(365deg);
      }

      .navbar:hover .menu li:nth-child(8) a {
        transition-delay: 0.32s;
        transform: rotate(355deg);
      }

      .navbar:hover .menu li:nth-child(9) {
        transition-delay: 0.18s;
        transform: rotate(405deg);
      }

      .navbar:hover .menu li:nth-child(9) a {
        transition-delay: 0.36s;
        transform: rotate(315deg);
      }

      /*footer*/
      div.footer {
        position: absolute;
        bottom: 0px;
        right: auto;

        width: 100%;
        min-height: 50px;
        padding-top: 15px;
        color: whitesmoke;
        /* background: rgba(110,100,200,0.5); */
        text-align: center;
        z-index: 50;
      }

      .icp {
        color: white;
        text-decoration: none;
      }

      .icp:hover {
        text-decoration: none;
        color: wheat;
      }
    </style>


  </head>
  <body>

  <div style="margin-top: 60px;">

  </div>
  <div class="demo">
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <div class="navbar"><img src="${pageContext.request.contextPath}/static/images/logo.jpg" alt="" style="width: 240px;height: 240px; border-radius: 120px;">
            <ul class="menu">
              <!-- <li><a href="#" class="fa fa-facebook">login</a></li>
<li><a href="#" class="fa fa-google-plus">register</a></li>
<li><a href="#" class="fa fa-twitter">forgot</a></li>
<li><a href="#" class="fa fa-linkedin">browse</a></li>
<li><a href="#" class="fa fa-pinterest">home</a></li>
<li><a href="#" class="fa fa-rss">register</a></li>
<li><a href="#" class="fa fa-instagram">login</a></li>
<li><a href="#" class="fa fa-skype">forget</a></li>
<li><a href="#" class="fa fa-github">源码</a></li> -->
              <li><a href="${pageContext.request.contextPath}/toLogin" class="">用户登录</a></li>
              <li><a href="${pageContext.request.contextPath}/toRegister" class="">用户注册</a></li>
              <li><a href="${pageContext.request.contextPath}/toForgot" class="">用户找回密码</a></li>
              <li><a href="${pageContext.request.contextPath}/browse" class="">浏览</a></li>
              <li><a href="${pageContext.request.contextPath}/mobile.jsp" class="">首页hover</a></li>
              <li><a href="${pageContext.request.contextPath}/admin/toLogin" class="">管理员登录</a></li>
              <li><a href="${pageContext.request.contextPath}/admin/toRegister" class="">管理员注册</a></li>
              <li><a href="${pageContext.request.contextPath}/admin/toForgot" class="">管理员找回密码</a></li>
              <li><a href="https://gitee.com/summer_hou/data_management" target="_blank" class="fa fa-git">源码</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer">
    <p>
      <span>© 2020 xhmy.cloud . &nbsp;</span>
      <span>赣ICP证</span> &nbsp;
      <a class="icp" href="https://beian.miit.gov.cn" title="" target="_blank">赣ICP备</a>
      <span>2020012707号</span> &nbsp;
    </p>
  </div>


  <script>
    "use strict"; {
      const perlin = {
        init() {
          this.p = new Uint8Array(512);
          this.reset();
        },
        reset() {
          const p = new Uint8Array(256);
          for (let i = 0; i < 256; i++) p[i] = i;
          for (let i = 255; i > 0; i--) {
            const n = Math.floor((i + 1) * Math.random());
            [p[i], p[n]] = [p[n], p[i]];
          }
          for (let i = 0; i < 512; i++) this.p[i] = p[i & 255];
        },
        lerp(t, a, b) {
          return a + t * (b - a);
        },
        grad2d(i, x, y) {
          const v = (i & 1) === 0 ? x : y;
          return (i & 2) === 0 ? -v : v;
        },
        noise2d(x2d, y2d) {
          const X = Math.floor(x2d) & 255;
          const Y = Math.floor(y2d) & 255;
          const x = x2d - Math.floor(x2d);
          const y = y2d - Math.floor(y2d);
          const fx = (3 - 2 * x) * x * x;
          const fy = (3 - 2 * y) * y * y;
          const p0 = this.p[X] + Y;
          const p1 = this.p[X + 1] + Y;
          return this.lerp(
                  fy,
                  this.lerp(
                          fx,
                          this.grad2d(this.p[p0], x, y),
                          this.grad2d(this.p[p1], x - 1, y)
                  ),
                  this.lerp(
                          fx,
                          this.grad2d(this.p[p0 + 1], x, y - 1),
                          this.grad2d(this.p[p1 + 1], x - 1, y - 1)
                  )
          );
        }
      };
      /////////////////////////////////////////////////////////////////
      const canvas = {
        init() {
          this.elem = document.createElement("canvas");
          document.body.appendChild(this.elem);
          this.width = this.elem.width = this.elem.offsetWidth;
          this.height = this.elem.height = this.elem.offsetHeight;
          return this.elem.getContext("2d");
        }
      };
      /////////////////////////////////////////////////////////////////
      const webgl = {
        init(canvas, options) {
          this.elem = document.createElement("canvas");
          this.gl = (
                  this.elem.getContext("webgl", options) ||
                  this.elem.getContext("experimental-webgl", options)
          );
          if (!this.gl) return false;
          const vertexShader = this.gl.createShader(this.gl.VERTEX_SHADER);
          this.gl.shaderSource(vertexShader,
                  `
				precision highp float;
				attribute vec3 aPosition;
				uniform vec2 uResolution;
				void main() {
					gl_PointSize = 1.0;
					gl_Position = vec4(
						( aPosition.x / uResolution.x * 2.0) - 1.0,
						(-aPosition.y / uResolution.y * 2.0) + 1.0,
						0.0,
						1.0
					);
				}`
          );
          this.gl.compileShader(vertexShader);
          const fragmentShader = this.gl.createShader(this.gl.FRAGMENT_SHADER);
          this.gl.shaderSource(fragmentShader,
                  `
				precision highp float;
				void main() {
					gl_FragColor = vec4(0.2, 0.3, 1.0, 1.0);
				}`
          );
          this.gl.compileShader(fragmentShader);
          const program = this.gl.createProgram();
          this.gl.attachShader(program, vertexShader);
          this.gl.attachShader(program, fragmentShader);
          this.gl.linkProgram(program);
          this.gl.useProgram(program);
          this.aPosition = this.gl.getAttribLocation(program, "aPosition");
          this.gl.enableVertexAttribArray(this.aPosition);
          this.positionBuffer = this.gl.createBuffer();
          this.elem.width = canvas.width;
          this.elem.height = canvas.height;
          const uResolution = this.gl.getUniformLocation(program, "uResolution");
          this.gl.enableVertexAttribArray(uResolution);
          this.gl.uniform2f(uResolution, canvas.width, canvas.height);
          this.gl.viewport(
                  0,
                  0,
                  this.gl.drawingBufferWidth,
                  this.gl.drawingBufferHeight
          );
          return this.gl;
        },
        drawBuffer(data, num) {
          this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.positionBuffer);
          this.gl.vertexAttribPointer(this.aPosition, 2, this.gl.FLOAT, false, 0, 0);
          this.gl.bufferData(
                  this.gl.ARRAY_BUFFER,
                  data,
                  this.gl.DYNAMIC_DRAW
          );
          this.gl.drawArrays(this.gl.GL_POINTS, 0, num);
        }
      };
      /////////////////////////////////////////////////////////////////
      const ctx = canvas.init();
      const gl = webgl.init(canvas, {
        alpha: false,
        stencil: false,
        antialias: false,
        depth: false,
      });
      perlin.init();
      const nParticles = 3000;
      const velocities = new Float32Array(nParticles * 2);
      const particles = new Float32Array(nParticles * 2);
      let frame = 0;
      for (let i = 0; i < nParticles; i++) {
        const p = i * 2;
        particles[p + 0] = Math.random() * canvas.width;
        particles[p + 1] = Math.random() * canvas.height;
      }
      /////////////////////////////////////////////////////////////////
      const run = () => {
        requestAnimationFrame(run);
        frame++;
        gl.clear(gl.COLOR_BUFFER_BIT);
        ctx.globalCompositeOperation = "source-over";
        ctx.fillStyle = "rgba(0, 0, 0, 0.025)";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        ctx.globalCompositeOperation = "lighter";
        for (let i = 0; i < nParticles; i++) {
          const p = i * 2;
          let n = 80 * perlin.noise2d(particles[p + 0] * 0.001, particles[p + 1] * 0.001);
          velocities[p + 0] += 0.1 * Math.cos(n);
          velocities[p + 1] += 0.1 * Math.sin(n);
          particles[p + 0] += (velocities[p + 0] *= 0.99);
          particles[p + 1] += (velocities[p + 1] *= 0.99);
          particles[p + 0] = (canvas.width + particles[p + 0]) % canvas.width;
          particles[p + 1] = (canvas.height + particles[p + 1]) % canvas.height;
        }
        webgl.drawBuffer(particles, nParticles);
        if (frame > 30) ctx.drawImage(webgl.elem, 0, 0);
      };
      requestAnimationFrame(run);
      /////////////////////////////////////////////////////////////////
      ["click", "touchdown"].forEach(event => {
        document.addEventListener(event, e => perlin.reset(), false);
      });
    }
  </script>




  <%--
  <div class='grain'></div>
  <div class='intro'>
    <div class='center'>
      <div class='core'></div>
      <div class='outer_one'>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
        <div class='outer_one__piece'></div>
      </div>
      <div class='outer_two'>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
        <div class='outer_two__piece'></div>
      </div>
      <div class='outer_three'>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
        <div class='outer_three__piece'></div>
      </div>
      <div class='outer_four'>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
        <div class='outer_four__piece'></div>
      </div>
      <div class='outer_five'>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
        <div class='outer_five__piece'></div>
      </div>
      <div class='pieces'>
        <div class='future_ui__piece'>
          <span><a href="${pageContext.request.contextPath}/">首页</a></span>
          <div class='line'></div>
          <div class='tip'>
            Home
          </div>
        </div>
        <div class='future_ui__piece'>
          <span><a href="${pageContext.request.contextPath}/toLogin">登录</a></span>
          <div class='line'></div>
          <div class='tip'>
            Login
          </div>
        </div>
        <div class='future_ui__piece'>
          <span><a href="${pageContext.request.contextPath}/toRegister">注册</a></span>
          <div class='line'></div>
          <div class='tip'>
            Register
          </div>
        </div>
        <div class='future_ui__piece'>
          <span><a href="${pageContext.request.contextPath}/toForgot">忘记密码</a></span>
          <div class='line'></div>
          <div class='tip'>
            Forgot
          </div>
        </div>
        <div class='future_ui__piece'>
          <span><a href="${pageContext.request.contextPath}/browse">浏览</a></span>
          <div class='line'></div>
          <div class='tip'>
            Browse
          </div>
        </div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
        <div class='future_ui__piece blank'></div>
      </div>
    </div>
  </div>
--%>
  <script src="${pageContext.request.contextPath}/static/js/dm-index.js"></script>
  <%--
  <script type="text/javascript">
    if(/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {
      window.location.href = "${pageContext.request.contextPath}/mobile.jsp";
    } else {
      window.location.href = "${pageContext.request.contextPath}/index.jsp";
    }
  </script>
  --%>
  </body>
</html>
