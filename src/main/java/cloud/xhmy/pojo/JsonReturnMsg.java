package cloud.xhmy.pojo;

/**
 * 返回json字符串封装
 * @author aliketh.xhmy
 */
public class JsonReturnMsg {
    private String messages;
    private String codes;

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }
}
