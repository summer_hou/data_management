package cloud.xhmy.controller;

import cloud.xhmy.pojo.Books;
import cloud.xhmy.pojo.Borrow;
import cloud.xhmy.service.BooksService;
import cloud.xhmy.service.BorrowService;
import cloud.xhmy.util.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author aliketh.xhmy
 */
@Controller
public class BorrowController {

    @Autowired
    @Qualifier("borrowServiceImpl")
    private BorrowService borrowService;
    @Autowired
    @Qualifier("booksServiceImpl")
    private BooksService booksService;


    /**
     * 跳转到借阅页面
     * @return
     */
    @RequestMapping("/toBorrow")
    public String toBorrow(){
        return "operation/borrow";
    }

    /**
     * 通过书籍名称快速借阅
     * @param bookId
     * @param model
     * @return
     */
    @RequestMapping("/borrow/{bookId}")
    public String borrow( @PathVariable int bookId, Model model, String uuid,HttpServletRequest request){
        Books books = booksService.queryById(bookId);
        model.addAttribute("bookName", books.getBookName());
        return "operation/borrow";
    }

    /**
     * 提交借阅请求
     * @param borrow
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("/pushBorrow")
    public JSONObject pushBorrow(@ModelAttribute Borrow borrow, HttpServletRequest request) throws ParseException {
        ModelAndView mv = new ModelAndView();
//        Books books = new Books();
        JSONObject jsonObject = new JSONObject();
        Books books = booksService.query(borrow.getBorrowName());
        if (books==null){
            jsonObject.put("code","0");
            jsonObject.put("msg","失败:书籍不存在");
            return jsonObject;
        }
        if (borrow.getBorrowNumber()>books.getBookNumber()){
            jsonObject.put("code","0");
            jsonObject.put("msg","失败:书籍库存不足");
            return jsonObject;
        }
        if (borrow.getBorrowName().equals(books.getBookName())){

        borrow.setBorrowTime(new Date());
        borrow.setTakeLocation(books.getBookLocation());
        borrowService.addBorrow(borrow);
        //更新书籍数量
        booksService.updateNumber(books.getId(),books.getBookNumber()-borrow.getBorrowNumber());
        jsonObject.put("code","0");
        jsonObject.put("msg","成功");
        jsonObject.put("address",books.getBookLocation());
        return jsonObject;
        }

        return jsonObject;
//        mv.setViewName("operation/borrow");
//        return mv;
    }

    /**
     * 列出所有个人借阅记录
     * @param borrow
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/listBorrow/{user.email}")
    public ModelAndView listBorrow(@PathVariable("user.email") String borrow,
                                   @RequestParam(defaultValue = "1") int page,
                                   @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<Borrow> list = borrowService.listAll(borrow,page,size);

        PageInfo<Borrow> pageInfos = new PageInfo<>(list);


        mv.addObject("pageInfos",pageInfos);
        mv.setViewName("operation/listBorrow");
        return mv;
    }

    /**
     * 通过id删除借阅记录
     * @param borrowId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete/borrow")
    public Map<String,String> deleteBorrow(@RequestParam("id") List<Integer> borrowId,HttpServletRequest request){
        Map<String,String> map = new HashMap<>();
        List<Integer> list = borrowId;
        for (int i = 0; i < list.size(); i++) {
            int id = list.get(i);
            borrowService.deleteById(id);
        }
        map.put("sates","1");
        map.put("msg","成功");
        return map;
    }

    /**
     * 归还
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/backBorrow")
    public JSONObject listClass(int id) throws ParseException {
        JSONObject jsonObject = new JSONObject();
        Borrow borrows = new Borrow();
        Borrow borrow = borrowService.queryById(id);
        Books books = booksService.query(borrow.getBorrowName());
        if ("0".equals(borrow.getTfReturn())){
            borrows.setBorrowUser(borrow.getBorrowUser());
            borrows.setTfReturn("1");
            borrows.setReturnTime(new Date());
            borrowService.backBorrow(borrows);
            booksService.updateNumber(books.getId(),books.getBookNumber()+borrow.getBorrowNumber());
            jsonObject.put("code","1");
        }else {
            jsonObject.put("msg","");
        }

        return jsonObject;
    }

    /**
     *
     * @param user
     * @param option
     * @param page
     * @param size
     * @return
     */
//    @ResponseBody
//    @RequestMapping("/borrowClass")
//    public ModelAndView listClass(String user,String option,
//                                  @RequestParam(defaultValue = "1") int page,
//                                  @RequestParam(defaultValue = "10") int size){
//        ModelAndView mv = new ModelAndView();
//
//        PageHelper.startPage(page,size);
//        List<Borrow> list =borrowService.backBook(user, option, page, size);
//        PageInfo<Borrow> pageInfos = new PageInfo<>(list);
//        mv.addObject("pageInfos",pageInfos);
//        mv.setViewName("operation/listBorrow");
//        return mv;
//    }

}
