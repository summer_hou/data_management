<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/10
  Time: 16:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>401</title>
    <style>
        #inFor_message {
            text-align: center;
        }

        .error {
            font-size: 150px;
            margin-top: 20px;
            color: #9d261d;
        }

        /*footer*/
        div.footer {
            position: absolute;
            bottom: 0px;
            right: auto;

            width: 100%;
            min-height: 50px;
            padding-top: 15px;
            background: rgba(05, 03, 90, 0.3);
            text-align: center;
            z-index: 50;
        }

        .icp {
            color: white;
        }

        .icp:hover {
            text-decoration: none;
            color: wheat;
        }
    </style>
</head>
<body>
<div id="inFor_message">
    <p>401</p>
    <h1>您没有权限执行该操作</h1>
</div>
<div class="footer">
    <p>
        <span>© 2020 xhmy.cloud . &nbsp;</span>
        <span>赣ICP证</span> &nbsp;
        <a class="icp"
           href="https://beian.miit.gov.cn"
           title=""
           target="_blank">赣ICP备</a>
        <span>2020012707号</span> &nbsp;
    </p>
</div>
</body>
</html>
