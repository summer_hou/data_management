package cloud.xhmy.dao;

import cloud.xhmy.pojo.Books;
import cloud.xhmy.pojo.EFormat;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface EFormatMapper {
    /**
     * 增加
     * @param format
     * @return
     */
    int addFormat(EFormat format);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<EFormat> queryByName(@Param("formatSuffix") String name, int page, int size);

    /**
     * 列出所有
     * @param page
     * @param size
     * @return
     */
    List<EFormat> listAll(int page, int size);

    /**
     * 修改
     * @param id
     * @return
     */
    int modifyById(int id);

    /**
     * 用于修改
     * @param id
     * @return
     */
    EFormat queryById(int id);
}
