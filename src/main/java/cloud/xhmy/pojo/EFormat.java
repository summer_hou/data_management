package cloud.xhmy.pojo;

/**
 * 电子书格式
 * EFormat
 * @author aliketh.xhmy
 */
public class EFormat {

    private int id;
    private String format;
    private String formatSuffix;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getFormatSuffix() {
        return formatSuffix;
    }

    public void setFormatSuffix(String formatSuffix) {
        this.formatSuffix = formatSuffix;
    }
}
