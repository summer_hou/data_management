package cloud.xhmy.service;

import cloud.xhmy.dao.BooksMapper;
import cloud.xhmy.pojo.Books;
import org.apache.poi.openxml4j.util.ZipSecureFile;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public class BooksServiceImpl implements BooksService {

    private BooksMapper booksMapper;

    public void setBooksMapper(BooksMapper booksMapper) {
        this.booksMapper = booksMapper;
    }

    public int addBook(Books book) {
        return this.booksMapper.addBook(book);
    }

    public int deleteById(int id) {
        return this.booksMapper.deleteById(id);
    }

    public List<Books> queryByName(String name, int page, int size) {
        return this.booksMapper.queryByName(name, page, size);
    }

    public List<Books> listAll(String shelves,int page, int size) {
        return this.booksMapper.listAll( shelves,page, size);
    }

    public int modifyById(Books id) {
        return this.booksMapper.modifyById(id);
    }

    public Books queryById(int id) {
        return this.booksMapper.queryById(id);
    }

    public Books query(String name) {
        return this.booksMapper.query(name);
    }

    public List<Books> homeQuery() {
        return this.booksMapper.homeQuery();
    }

    public List<Books> listAllBooks(int page, int size) {
        return this.booksMapper.listAllBooks(page, size);
    }

    public int updateNumber(int id, int bookNumber) {
        return this.booksMapper.updateNumber(id, bookNumber);
    }

    public List<Books> queryLanguage(String language, int page, int size) {
        return this.booksMapper.queryLanguage(language, page, size);
    }

    public List<Books> queryClassify(String classify, int page, int size) {
        return this.booksMapper.queryClassify(classify, page, size);
    }

    public List<Books> queryGroup(Books books) {
        return this.booksMapper.queryGroup(books);
    }

    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/


}
