<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/12/12
  Time: 18:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>query</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>

    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/page-helper.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>
<div style="width: 90%; margin: 5px auto;">
    <div style="margin: 10px auto; width: 90%;" class="check-list">
        <span></span>
        <button style="margin-right: 15px;" class="btn btn-primary btn-default" data-id="books">图书<a href="" style=" color: floralwhite;"></a>
        </button>

        <span></span>
        <button style="margin-right: 15px;" class="btn btn-primary" data-id="eBooks">电子书<a href="" style=" color: floralwhite;"></a>
        </button>
    </div>
    <div id="cq-query-content">

    </div>
</div>
<script type="text/javascript">
    $(function () {
        $(".check-list").on("click", "button", function () {
            var checkId = $(this).data("id");  //获取data-id的值
            window.location.hash = checkId;  //设置锚点
            loadInner(checkId);
        });

        function loadInner(checkId) {
            var checkId = window.location.hash;
            var pathn, i;
            switch (checkId) {
                case "#books":
                    pathn = "${pageContext.request.contextPath}/booksClassify?clq=${search}";
                    i = 0;
                    break;
                case "#eBooks":
                    pathn = "${pageContext.request.contextPath}/eBooksClassify?clq=${search}";
                    i = 1;
                    break;
                default:
                    pathn = "${pageContext.request.contextPath}/booksClassify?clq=${search}";
                    i = 0;
                    break;
            }
            $("#cq-query-content").load(pathn); //加载相对应的内容
            // $(".userMenu button").eq(i).addClass("btn-default").siblings().removeClass("btn-default"); //当前列表高亮

        }

        var checkId = window.location.hash;
        loadInner(checkId);
    });

</script>

<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>
</body>
</html>
