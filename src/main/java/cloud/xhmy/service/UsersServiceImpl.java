package cloud.xhmy.service;

import cloud.xhmy.dao.UsersMapper;
import cloud.xhmy.pojo.Users;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.util.List;

/**
 * @author aliketh.xhmy
 */

public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersMapper usersMapper;

    public void setUsersMapper(UsersMapper usersMapper) {
    }

    public Users login(Users user) {
        return this.usersMapper.login(user);
    }

    public Integer register(Users user) {
        return this.usersMapper.register(user);
    }

    public int forgot(Users user) {
        return this.usersMapper.forgot(user);
    }

    public int logout(Users user) {
        return this.usersMapper.logout(user);
    }

    public int modify(Users user) {
        return this.usersMapper.modify(user);
    }

    public List<Users> judge(String email) {
        return this.usersMapper.judge(email);
    }

    public int avatar(Users user) {
        return this.usersMapper.avatar(user);
    }

    public int pro(Users pro) {
        return this.usersMapper.pro(pro);
    }

    public int modifyTel(BigInteger tel,String uid) {
        return this.usersMapper.modifyTel(tel,uid);
    }

    public int modifyPwd(String pwd,String uid) {
        return this.usersMapper.modifyPwd(pwd,uid);
    }

    public int realUser(String realName, String idNumber,String user) {
        return this.usersMapper.realUser(realName,idNumber,user);
    }

    public Users queryByEmail(String users) {
        return this.usersMapper.queryByEmail(users);
    }

    public Users refresh(Users user) {
        return this.usersMapper.refresh(user);
    }

    public List<Users> listAllUser(int page, int size) {
        return this.usersMapper.listAllUser(page, size);
    }

    public int adminModifyTel(Users users) {
        return this.usersMapper.adminModifyTel(users);
    }

    public int adminModifyPwd(Users users) {
        return this.usersMapper.adminModifyPwd(users);
    }

    public int adminRealUser(Users users) {
        return this.usersMapper.adminRealUser(users);
    }
}
