package cloud.xhmy.service;

import cloud.xhmy.dao.FeedbackMapper;
import cloud.xhmy.pojo.Feedback;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface FeedbackService {
    /**
     * 增加
     * @param feedback
     * @return
     */
    int addFeedback(Feedback feedback);

    /**
     * 根据id删除
     * @param id
     * @return
     */
    int deleteById(int id);


    /**
     * 根据id查询,返回
     * @param id
     * @return
     */
    Feedback queryById(int id);


    /**
     * 查询全部,返回list集合
     * @param page
     * @param size
     * @return
     */
    List<Feedback> listAll(String user, int page, int size);



    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */

    /**
     * 查询所有
     * @param page
     * @param size
     * @return
     */
    List<Feedback> listAllFeedback(int page, int size);

    /**
     * 通过审核
     * @param id
     * @return
     */
    int push(int id);

    /**
     * 统计
     * @return
     */
    Feedback countNumber();

    /**
     * 模糊查询
     * @param page
     * @param size
     * @return
     */
    List<Feedback> queryByName(String name, int page, int size);
}
