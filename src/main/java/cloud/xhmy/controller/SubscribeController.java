package cloud.xhmy.controller;


import cloud.xhmy.pojo.Books;
import cloud.xhmy.pojo.Borrow;
import cloud.xhmy.pojo.Subscribe;
import cloud.xhmy.service.BooksService;
import cloud.xhmy.service.BorrowService;
import cloud.xhmy.service.SubscribeService;
import cloud.xhmy.util.FileUtil;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author aliketh.xhmy
 */
@Controller
public class SubscribeController {


    @Autowired
    @Qualifier("subscribeServiceImpl")
    private SubscribeService subscribeService;
    @Autowired
    @Qualifier("booksServiceImpl")
    private BooksService booksService;
    @Autowired
    @Qualifier("borrowServiceImpl")
    private BorrowService borrowService;

    /**
     * 跳转到预约页面
     * @return
     */
    @RequestMapping("/toSubscribe")
    public String toSubscribe(){
        return "operation/subscribe";
    }

    /**
     * 通过预览图书快速预约
     * @param bookId
     * @param model
     * @return
     */
    @RequestMapping("/subscribe/{bookId}")
    public String Subscribe(@PathVariable int bookId, Model model,HttpServletRequest request,String uuid){
        Books books = booksService.queryById(bookId);
        model.addAttribute("bookName",books.getBookName());
        return "operation/subscribe";
    }

    /**
     * 提交预约请求
     * @param subscribe
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("/pushSubscribe")
    public JSONObject pushBorrow(@ModelAttribute Subscribe subscribe, HttpServletRequest request){
        JSONObject jsonObject = new JSONObject();
        Books books = booksService.query(subscribe.getSubscribeName());
        if(books==null){
            jsonObject.put("code","0");
            jsonObject.put("msg","失败:书籍不存在");
            return jsonObject;
        }
        if (subscribe.getSubscribeNumber()>books.getBookNumber()){
            jsonObject.put("code","0");
            jsonObject.put("msg","失败:书籍库存不足");
            return jsonObject;
        }
        if (subscribe.getSubscribeName().equals(books.getBookName())){

            String ref = subscribe.getTakeTime().replaceAll("T"," ");
            subscribe.setTakeTime(ref);
            subscribe.setTakeSubscribe(books.getBookLocation());
            subscribeService.addSubscribe(subscribe);
            booksService.updateNumber(books.getId(),books.getBookNumber()-subscribe.getSubscribeNumber());

            jsonObject.put("code","1");
            jsonObject.put("msg","成功");
            jsonObject.put("address",books.getBookLocation());
            return jsonObject;
        }
//        jsonObject.put("code","0");
//        jsonObject.put("msg","失败:书籍不存在");
        return jsonObject;
    }

    /**
     * 列出个人所有预约记录
     * @param borrow
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/listSubscribe/{user.email}")
    public ModelAndView listSubscribe(@PathVariable("user.email") String borrow,
                                      @RequestParam(defaultValue = "1") int page,
                                      @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<Subscribe> list = subscribeService.listAll(borrow,page,size);
        PageInfo<Subscribe> pageInfos = new PageInfo<>(list);


        mv.addObject("pageInfos",pageInfos);
        mv.setViewName("operation/listSubscribe");
        return mv;
    }

    /**
     * 通过id删除预约记录
     * @param subscribeId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete/subscribe")
    public Map<String,String> deleteSubscribe(@RequestParam("id") List<Integer> subscribeId,HttpServletRequest request){
        Map<String,String> map = new HashMap<>();
        List<Integer> list = subscribeId;
        for (int i = 0; i < list.size(); i++) {
            int id = list.get(i);
            subscribeService.deleteById(id);
        }
        map.put("sates","1");
        map.put("msg","成功");
        return map;
    }

    /**
     * took books确认取书
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/takeSubscribe")
    public JSONObject takeSubscribe(int id) throws ParseException {
        JSONObject jsonObject = new JSONObject();
        Borrow borrows = new Borrow();
        Subscribe subscribe = subscribeService.queryById(id);
        Books books = booksService.query(subscribe.getSubscribeName());

        borrows.setBorrowUser(subscribe.getSubscribeUser());
        borrows.setTfReturn("0");
        borrows.setDeposit(subscribe.getDeposit());
        borrows.setBorrowTime(new Date());
        borrows.setTakeLocation(subscribe.getTakeSubscribe());
        borrows.setBorrowNumber(subscribe.getSubscribeNumber());
        borrows.setBorrowName(subscribe.getSubscribeName());
        borrows.setBorrowDay(subscribe.getBorrowDay());


        borrowService.addBorrow(borrows);
        booksService.updateNumber(books.getId(), books.getBookNumber() + subscribe.getSubscribeNumber());
        jsonObject.put("code", "1");


        return jsonObject;
    }


//    @RequestMapping("/querySubscribe/{byName}")
//    public ModelAndView querySubscribe(@PathVariable("byName") String name,
//                                   @RequestParam(defaultValue = "1") int page,
//                                   @RequestParam(defaultValue = "10") int size){
//        PageHelper.startPage(page,size);
//        ModelAndView mv = new ModelAndView();
//        List<EBooks> list = subscribeService.quqery(name, page, size) ;
//        PageInfo<EBooks> pageInfos = new PageInfo<>(list);
//        mv.addObject("pageInfos",pageInfos);
//        mv.setViewName("operation/listEBooks");
//        return mv;
//
//    }


    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/

    /**
     * 管理员
     */
}
