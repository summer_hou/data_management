<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/11
  Time: 18:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>query</title>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/page-helper.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/browse-cql.js"></script>
    <style>
        /*===========================================================*/
        /*footer*/
        div.footer {
            position: relative;
            bottom: 0px;
            right: auto;

            color: snow;
            width: 100%;
            min-height: 50px;
            padding-top: 15px;
            background: #081325;
            text-align: center;
            z-index: 50;
        }

        .icp {
            color: white;
        }

        .icp:hover {
            text-decoration: none;
            color: wheat;
        }

    </style>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    综合图书管理系统<small>&nbsp; &nbsp;浏览页面</small>
                </h1>
                <span style="float: right;margin-top: 10px; font-size: 16px; margin-right: 5px;"><a
                        href="${pageContext.request.contextPath}/toLogin">登录</a> || <a
                        href="${pageContext.request.contextPath}/toRegister">注册</a> || <a
                        href="javascript:goback()">返回</a></span>
            </div>
            <div style="height: 20px"></div>
            <div style=" width: 100% ;margin:0 auto; z-index: 50;">
                <!-- 搜索框 -->
                <div style="margin: 10px auto 30px auto;">
                    <form action="${pageContext.request.contextPath}/browse/retrieve">
                        <input style="width: 90%;height: 38px; padding: 5px; border-radius: 2px; border: 1px solid #666666;"
                               name="search"
                               value="${search}" placeholder="全站搜索（输入要搜索的名称）" type="search">
                        <button style="width: 10%; height: 40px;margin-left: -5px;" type="submit">搜索</button>
                    </form>

                </div>
                <div style="margin-right: 0px; width: auto ;margin-top: -30px; float: right;background: unset;">
                    <div class="btn-group" style="margin-right: -5px;">
                        <button class="btn btn-default" style="border: unset; background: unset;">语言</button>
                        <button style="border: unset; background: unset;" data-toggle="dropdown"
                                class="btn btn-default dropdown-toggle"><span
                                class="caret"></span></button>
                        <ul class="dropdown-menu" style=" left: unset;right: 0;">
                            <li><a href="#">所有语言</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:byLanguage(0)">Chinese</a></li>
                            <li><a href="javascript:byLanguage(1)">English</a></li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <button style="border: unset; background: unset;" class="btn btn-default">分类</button>
                        <button style="border: unset; background: unset;" data-toggle="dropdown"
                                class="btn btn-default dropdown-toggle"><span
                                class="caret"></span></button>
                        <ul class="dropdown-menu" style=" left: unset;right: 0;">
                            <li>
                                <a href="javascript:classify(0)">所有类型</a>
                            </li>
                            <li class="divider">
                            </li>
                            <li><a href="javascript:byClassify(1)">艺术</a></li>
                            <li><a href="javascript:byClassify(2)">商业</a></li>
                            <li><a href="javascript:byClassify(3)">化学</a></li>
                            <li><a href="javascript:byClassify(4)">经济</a></li>
                            <li><a href="javascript:byClassify(5)">教育</a></li>
                            <li><a href="javascript:byClassify(6)">地理</a></li>
                            <li><a href="javascript:byClassify(7)">地质</a></li>
                            <li><a href="javascript:byClassify(8)">历史</a></li>
                            <li><a href="javascript:byClassify(9)">休闲</a></li>
                            <li><a href="javascript:byClassify(10)">法学</a></li>
                            <li><a href="javascript:byClassify(11)">宗教</a></li>
                            <li><a href="javascript:byClassify(12)">技术</a></li>
                            <li><a href="javascript:byClassify(13)">工艺</a></li>
                            <li><a href="javascript:byClassify(14)">文学</a></li>
                            <li><a href="javascript:byClassify(15)">数学</a></li>
                            <li><a href="javascript:byClassify(16)">医学</a></li>
                            <li><a href="javascript:byClassify(17)">物理</a></li>
                            <li><a href="javascript:byClassify(18)">心理学</a></li>
                            <li><a href="javascript:byClassify(19)">计算机</a></li>
                            <li><a href="javascript:byClassify(20)">生物学</a></li>
                            <li><a href="javascript:byClassify(21)">语言学</a></li>
                            <li><a href="javascript:byClassify(22)">社会科学</a></li>
                            <li><a href="javascript:byClassify(23)">体育与运动</a></li>
                            <li><a href="javascript:byClassify(24)">科学(通用)</a></li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="width: 90%; margin: 5px auto;">
    <div style="margin: 10px auto; width: 90%;" class="check-list">
        <span></span>
        <button style="margin-right: 15px;" class="btn btn-primary" data-id="eBooks">电子书<a
                href="javascript:checkModel(eBooks)" style=" color: floralwhite;"></a></button>

        <span></span>
        <button class="btn btn-primary" data-id="articles">文章<a
                href="javascript:checkModel(articles)" style=" color: floralwhite;"></a></button>

    </div>
    <div id="content-query" style=" width: 90%;margin: 0 auto;background: lightgoldenrodyellow;">


    </div>


</div>
<script type="text/javascript">
    $(function () {

        $(".check-list").on("click", "button", function () {
            var checkId = $(this).data("id");  //获取data-id的值
            window.location.hash = checkId;  //设置锚点
            loadInner(checkId);

        });

        function loadInner(checkId) {
            var checkId = window.location.hash;
            var pathn, i;
            switch (checkId) {
                case "#eBooks":
                    pathn = "${pageContext.request.contextPath}/browse/queryEBook?search=${search}";
                    i = 0;
                    break;
                case "#articles":
                    pathn = "${pageContext.request.contextPath}/browse/queryArticle?search=${search}";
                    i = 1;
                    break;
                default:
                    pathn = "${pageContext.request.contextPath}/browse/queryEBook?search=${search}";
                    i = 0;
                    break;
            }
            $("#content-query").load(pathn); //加载相对应的内容
            $(".userMenu button").eq(i).addClass("btn-default").siblings().removeClass("btn-default"); //当前列表高亮

        }

        var checkId = window.location.hash;
        loadInner(checkId);
    });

</script>

<div class="footer">
    <p>
        <span>© 2020 xhmy.cloud . &nbsp;</span>
        <span>赣ICP证</span> &nbsp;
        <a class="icp"
           href="https://beian.miit.gov.cn"
           title=""
           target="_blank">赣ICP备</a>
        <span>2020012707号</span> &nbsp;
    </p>
</div>

</body>
</html>
