package cloud.xhmy.service;

import cloud.xhmy.pojo.DownloadRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DownloadService {

    /**
     * 增加
     * @param downloadRecord
     * @return
     */
    int addDownloadRecord(DownloadRecord downloadRecord);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 模糊查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<DownloadRecord> queryByName(String name, int page, int size);

    /**
     * 列出所有
     * @param page
     * @param size
     * @return
     */
    List<DownloadRecord> listAll(int page, int size);

    /**
     * 修改
     * @param id
     * @return
     */
    int modifyById(int id);

    /**
     * 用于修改获取信息
     * @param id
     * @return
     */
    DownloadRecord queryById(int id);
}
