<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/12/13
  Time: 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/timeFormat.js"></script>
<div class="row">
    <div class="col-md-12">
        <div><h3><span>个人博客</span></h3></div>
        <div style="margin-top: 10px;">
            <%--
            <button class="btn btn-danger"
                    style="width: 10%; height: 38px;margin-left: 0px;margin-top: -2px;padding: unset;"
                    type="button">删除
            </button>

            <form id="search-push-blog" action="${pageContext.request.contextPath}/queryMyselfArticle/${user.email}" style="display: inline;">

                <input style="width: 80%;height: 40px; padding: 5px; border-radius: 2px; border: 1px solid #666666;margin-left: -4px;"
                       name="search"
                       value="${searchPushBlog}" placeholder="输入要搜索的博客名称" type="search">
                <button style="width: 10%; height: 40px;margin-left: -4px;" type="submit" >搜索</button>
            </form>
                --%>
        </div>
        <div>

            <div class="row">
                <div class="col-md-12 ">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="CheckAll" id="CheckAll"/></th>
                            <th>博客名称</th>
                            <th>博客标题</th>
                            <th>博客标签</th>
                            <th>博客状态</th>
                            <th>博客主题</th>
                            <th>博客内容</th>
                            <th>创建时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody id="blog">

                        <c:forEach var="blog" items="${pageInfos.list}">
                            <tr>
                                <td>
                                    <input type="checkbox" name="Check[]" value="${blog.id}" id="Check[]"/>
                                </td>

                                <td>${blog.blogName}</td>
                                <td>${blog.blogTitle}</td>
                                <td>${blog.blogTag}</td>
                                <td>${blog.blogState}</td>
                                <td>${blog.blogTheme}</td>
                                <td>${blog.blogContent}</td>
                                <td class="gmt-simple">
                                         <span onclick="simpleDateFormat('${blog.blogTime}')">${blog.blogTime}</span>
                                        <%-- <span id="blog${blog.id}"></span>--%>
<%--                                    <input type="text" id="blog${blog.id}" onfocus="simpleDateFormat($(this))" value=""--%>
<%--                                           style="border: unset;background-color: unset" readonly>--%>
                                </td>
<%--                                <script>--%>
<%--                                    function simpleDateFormat(time) {--%>
<%--                                        time.attr("value", defToDatetime('${blog.blogTime}'));--%>
<%--                                        &lt;%&ndash;document.getElementById("blog${blog.id}").innerText += defToDatetime(time);&ndash;%&gt;--%>
<%--                                    }--%>
<%--                                </script>--%>
                                <td class="text-center">
                                    <a href="${pageContext.request.contextPath}/toBrowseBlog/${blog.id}"
                                       class="btn bg-olive btn-xs">预览</a>
                                    ||
                                    <a href="${pageContext.request.contextPath}/toModifyBlog/${blog.id}"
                                       class="btn bg-olive btn-xs">更新</a>
                                    ||
                                    <a href="javascript:deleteBlog(${blog.id})"
                                       class="btn bg-olive btn-xs">删除</a>

                                </td>
                            </tr>
                        </c:forEach>
                        <%--    --%>
                        </tbody>
                        <tfoot>
                        <span><em><b>注意：博客状态已数字代表【0】表示等待审核，【1】表示通过，【2】表示不通过</b></em></span>
                        </tfoot>

                    </table>

                </div>
            </div>
            <div class="xhmy_page_div"></div>

            <script type="text/javascript">
                //翻页
                $(".xhmy_page_div").createPage({
                    pageNum: ${pageInfos.pages},
                    current: 1,
                    backfun: function (e) {
                        //console.log(e);//回调
                    }
                });

                function pageNumber(params) {
                    $("#blog").load("${pageContext.request.contextPath}/allBlog/${user.email} #blog >*", {
                        page: params,
                        size: "10"
                    });
                };

                // function simpleDateFormat(time){
                //     var datetime = new Date();
                //     datetime.setTime(time);
                //     var year = datetime.getFullYear();
                //     var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
                //     var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
                //     var hour = datetime.getHours()< 10 ? "0" + datetime.getHours() : datetime.getHours();
                //     var minute = datetime.getMinutes()< 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
                //     var second = datetime.getSeconds()< 10 ? "0" + datetime.getSeconds() : datetime.getSeconds();
                //     var sdt = year + "-" + month + "-" + date+" "+hour+":"+minute+":"+second;
                //     document.getElementById("#time").innerText = "<span>"+sdt+"</span>";
                // }
            </script>

        </div>
    </div>
</div>
