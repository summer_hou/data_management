# data_management
*特此声明*
*此项目将不再进行更新维护，需要的可以自行fork维护*

#### 介绍
ssm框架应用

#### 软件架构
软件架构说明
spring
springmvc
mybatis
mysql



#### 安装教程

1.  下载源码解压（或者从gitee获取）
2.  使用idea 导入项目
    打开idea 选择导入项目 选择maven
3.  修改数据连接
```xml
    <!-- 关联数据库配置文件 -->
<!--    <context:property-placeholder location="classpath:database.properties"/>-->
<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
        <!-- 关联数据库配置文件的连接池属性配置 -->
<!--        <property name="driverClass" value="${jdbc.driver}"/>-->
<!--        <property name="jdbcUrl" value="${jdbc.url}"/>-->
<!--        <property name="user" value="${jdbc.username}"/>-->
<!--        <property name="password" value="${jdbc.password}"/>-->
        <property name="driverClass" value="com.mysql.cj.jdbc.Driver"/>
        <!-- 数据库连接配置 注意mysql的版本 6.0以上要配置时区 -->
        <property name="jdbcUrl" value="jdbc:mysql://localhost:3306/datamanagement?useSSL=true&amp;useUnicode=true&amp;characterEncoding=utf8&amp;serverTimezone=Asia/Shanghai"/>
        <!-- 用户 -->
        <property name="user" value="root"/>
        <!-- 用户密码 -->
        <property name="password" value="aliketh.my"/>

        <!-- c3p0连接池的私有属性 -->
        <property name="maxPoolSize" value="30"/>
        <property name="minPoolSize" value="10"/>
        <!-- 关闭连接后不自动commit -->
        <property name="autoCommitOnClose" value="false"/>
        <!-- 获取连接超时时间 -->
        <property name="checkoutTimeout" value="10000"/>
        <!-- 当获取连接失败重试次数 -->
        <property name="acquireRetryAttempts" value="2"/>
    </bean>
```
    db.properties

```properties
jdbc.driver=com.mysql.cj.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/datamanagement?useSSL=true&useUnicode=true&characterEncoding=utf8&serverTimezone=Asia/Shanghai
jdbc.username=root
jdbc.password=aliketh.my
```

4.  创建数据库并执行数据库脚本
    创建一个数据库在数据库中执行datamanagement.sql

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 提交说明
1.  第一次提交
        基本完成用户端基础功能
2.  用户系统的第一次优化
3.  完成管理员用户登录
        优化用户界面


#### 后期优化

1.  采用模板+json+ajax实现局部刷新减少前端代码和后台压力
2.  优化页面以及框架

