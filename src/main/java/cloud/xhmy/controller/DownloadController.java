package cloud.xhmy.controller;

import cloud.xhmy.util.FileUtil;
import cloud.xhmy.util.upload.FilesPath;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;

/**
 * spring/spring-st td12
 * @author aliketh.xhmy
 */
@Controller
public class DownloadController {

    // 得到一个用来记录日志的对象，这样打印信息的时候能够标记打印的是那个类的信息
    private static final Log logger = LogFactory.getLog(DownloadController.class);
    /**
     * 显示要下载的文件
     */
//    @RequestMapping("/showDownloadFiles")
//    public String show(HttpServletRequest request, Model model){
//        //从workspace\.metadata\.plugins\org.eclipse.wst.server.core\tmp0\wtpwebapps\ch7\下载
//        String realpath = request.getSession().getServletContext().getRealPath("/uploadFiles");
//        File dir = new File(realpath);
//        File files[] = dir.listFiles();
//        //获取该目录下的所有文件名
//        ArrayList<String> fileName = new ArrayList<String>();
//        for (int i = 0; i < files.length; i++) {
//            fileName.add(files[i].getName());
//        }
//        model.addAttribute("files", fileName);
//        return "showDownloadFiles";
//    }
    /**
     * 重命名（非原名）
     * 执行下载
     */
    @RequestMapping("/download")
    public String down(@RequestParam("download") String filename, HttpServletRequest request, HttpServletResponse response){
        String aFilePath = null; //要下载的文件路径
        FileInputStream in = null; //输入流
        ServletOutputStream out = null; //输出流
        try {
            aFilePath = request.getSession().getServletContext().getRealPath("/static") +"/"+ FilesPath.parsingPath(filename);

            System.out.println("==================================="+aFilePath);
            //设置下载文件使用的报头
            response.setHeader("Content-Type", "application/x-msdownload" );
            response.setHeader("Content-Disposition", "attachment; filename="
                    + FileUtil.toUTF8String(FilesPath.parsingName(filename)));
            // 读入文件
            in = new FileInputStream(aFilePath + FilesPath.parsingName(filename));
            //得到响应对象的输出流，用于向客户端输出二进制数据
            out = response.getOutputStream();
            out.flush();
            int aRead = 0;
            byte b[] = new byte[1024];
            while ((aRead = in.read(b)) != -1 & in != null) {
                out.write(b,0,aRead);
            }
            out.flush();
            in.close();
            out.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
//        logger.info("下载成功");
        return null;
    }


    /**
     * 非重命名（原名）eBook
     * 执行下载
     */
    @RequestMapping("/youdownload")
    public String download(@RequestParam("download") String filename, HttpServletRequest request, HttpServletResponse response){
        String aFilePath = null; //要下载的文件路径
        FileInputStream in = null; //输入流
        ServletOutputStream out = null; //输出流
        try {
            aFilePath = request.getSession().getServletContext().getRealPath("/static") +"/"+ FilesPath.parsingPath(filename);

            System.out.println("==================================="+aFilePath);
            //设置下载文件使用的报头
            response.setHeader("Content-Type", "application/x-msdownload" );
            response.setHeader("Content-Disposition", "attachment; filename="
                    + FileUtil.toUTF8String(FilesPath.parsingName(filename)));
            // 读入文件
            in = new FileInputStream(aFilePath + FilesPath.parsingName(filename));
            //得到响应对象的输出流，用于向客户端输出二进制数据
            out = response.getOutputStream();
            out.flush();
            int aRead = 0;
            byte b[] = new byte[1024];
            while ((aRead = in.read(b)) != -1 & in != null) {
                out.write(b,0,aRead);
            }
            out.flush();
            in.close();
            out.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
//        logger.info("下载成功");
        return null;
    }
}
