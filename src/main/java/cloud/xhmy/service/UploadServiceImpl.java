package cloud.xhmy.service;

import cloud.xhmy.dao.UploadMapper;
import cloud.xhmy.pojo.UploadRecord;

import java.util.List;

public class UploadServiceImpl implements UploadService {

    private UploadMapper uploadMapper;

    public void setUploadMapper(UploadMapper uploadMapper) {
        this.uploadMapper = uploadMapper;
    }

    public int addUploadRecord(UploadRecord uploadRecord) {
        return this.uploadMapper.addUploadRecord(uploadRecord);
    }

    public int deleteById(int id) {
        return this.uploadMapper.deleteById(id);
    }

    public List<UploadRecord> queryByName(String name, int page, int size) {
        return this.uploadMapper.queryByName(name, page, size);
    }

    public List<UploadRecord> listAll(int page, int size) {
        return this.uploadMapper.listAll(page, size);
    }

    public UploadRecord modifyById(int id) {
        return this.uploadMapper.modifyById(id);
    }

    public UploadRecord queryById(int id) {
        return this.uploadMapper.queryById(id);
    }
}
