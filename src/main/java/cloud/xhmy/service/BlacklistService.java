package cloud.xhmy.service;

import cloud.xhmy.pojo.Blacklist;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author aliketh.xhmy
 */
public interface BlacklistService {

    /**
     * 增加
     * @param blacklist
     * @return
     */
    int addBlacklist(Blacklist blacklist);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 模糊查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<Blacklist> queryByName(String name, int page, int size);

    /**
     * 列出个人所有
     * @param page
     * @param size
     * @return
     */
    List<Blacklist> listAll(int page, int size);

    /**
     * 修改
     * @param id
     * @return
     */
    int modifyById(int id);

    /**
     * 用于修改获取信息
     * @param id
     * @return
     */
    Blacklist queryById(int id);
}
