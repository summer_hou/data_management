package cloud.xhmy.service;

import cloud.xhmy.pojo.Classify;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ClassifyService {
    /**
     * 增加
     * @param classify
     * @return
     */
    int addClassify(Classify classify);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 模糊查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<Classify> queryByName(String name, int page, int size);

    /**
     * 列出个人所有
     * @param page
     * @param size
     * @return
     */
    List<Classify> listAll(int page, int size);

    /**
     * 修改
     * @param id
     * @return
     */
    int modifyById(int id);

    /**
     * 用于修改获取信息
     * @param id
     * @return
     */
    Classify queryById(int id);
}
