package cloud.xhmy.controller;

import com.alibaba.fastjson.JSONObject;
import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;

/**
 * 验证码
 *
 * @author aliketh.xhmy
 *
 */
@Controller
public class CheckCodeController {

    @Autowired
    private Producer captchaProducer = null;

    /**
     * 验证码
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
//    @RequestMapping("/generateCode")
//    public ModelAndView getKaptchaImage(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        HttpSession session = request.getSession();
//        // 获取验证码
//        //    String code = (String) session.getAttribute(Constants.KAPTCHA_SESSION_KEY);
//        //    String code = (String) session.getAttribute("Kaptcha_Code");
//        // 清除浏览器的缓存
//        response.setDateHeader("Expires", 0);
//        // Set standard HTTP/1.1 no-cache headers.
//        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
//        // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
//        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
//        // Set standard HTTP/1.0 no-cache header.
//        response.setHeader("Pragma", "no-cache");
//        // return a jpeg
//        response.setContentType("image/jpeg");
//        // 浏览器记忆功能-----当前过浏览器和服务器交互成功以后下载的图片和资源会进行缓存一次。下次刷新的时候就不会在到服务器去下载。
//        // 获取KAPTCHA验证的随机文本
//
//
//        String capText = captchaProducer.createText();
//        // 将生成好的图片放入会话中
//        session.setAttribute(Constants.KAPTCHA_SESSION_KEY, capText);
//        // create the image with the text
//        BufferedImage bi = captchaProducer.createImage(capText);
//        ServletOutputStream out = response.getOutputStream();
//        // write the data out
//        ImageIO.write(bi, "jpg", out);
//        try {
//            out.flush();
//        } finally {
//            out.close();//关闭
//        }
//        return null;
//    }

    /**
     * 验证码验证
     *
     * @param code
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("/checkCode")
    public JSONObject checkCode(String code, HttpServletRequest request){
        JSONObject jsonObject = new JSONObject();
        System.out.println("==================================code:"+code);
        String sessionCode = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        if (StringUtils.isNotEmpty(code)) {
            if (sessionCode.equals(code)){
                jsonObject.put("code","1");
                jsonObject.put("msg","验证成功");
            }else {
                jsonObject.put("code","0");
                jsonObject.put("msg","验证码不正确");
            }
        }else {
            jsonObject.put("code", "0");
            jsonObject.put("msg", "验证码为空");
        }
        return jsonObject;
    }
}
