package cloud.xhmy.util.files;

import cloud.xhmy.util.upload.FilesPath;
import com.hazelcast.util.UuidUtil;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.util.UUID;

public class FilesDownload {
    /**
     * download
     * @param filename
     * @param request
     * @param response
     * @return
     */
    public static void download(String filename, HttpServletRequest request, HttpServletResponse response){
        String aFilePath = null; //要下载的文件路径
        FileInputStream in = null; //输入流
        ServletOutputStream out = null; //输出流
        String downloadUid =  UUID.randomUUID().toString().replaceAll("-","");//响应
        String newName=null;
        try {
            aFilePath = request.getSession().getServletContext().getRealPath("/static") +"/"+ FilesPath.parsingPath(filename);

            //设置下载文件使用的报头
            response.setHeader("Content-Type", "application/x-msdownload" );
            response.setHeader("Content-Disposition", "attachment; filename="
                    + downloadUid);
            //FileUtil.toUTF8String(FilesPath.parsingName(filename))
            String file = aFilePath + FilesPath.parsingName(filename);
            // 读入文件
            in = new FileInputStream(file);
            //得到响应对象的输出流，用于向客户端输出二进制数据
            out = response.getOutputStream();
            out.flush();
            int aRead = 0;
            byte b[] = new byte[1024];
            while ((aRead = in.read(b)) != -1 & in != null) {
                out.write(b,0,aRead);
            }
            out.flush();
            in.close();
            out.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
//        logger.info("下载成功");
//        return null;
    }
}
