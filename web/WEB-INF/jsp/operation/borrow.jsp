<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/3
  Time: 16:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>borrow</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div style="width: 90%;margin: auto;" class="row">

    <div id="" class="col-md-4" style="color: #ffff7f;line-height: 2em;">
        <h3 style="margin: 30px 0 40px;"><span><em>书籍借阅说明</em></span></h3>
        <ul><em style="font-size: 16px;margin-bottom: 15px;">借阅时务必注意</em></ul>
        <ol>
            <li>借阅前通过网络借阅时请确认是否有该图书,若不存在可能导致借阅失败</li>
            <li>借阅时确保借阅书名无误</li>
            <li>借阅数量不能为空</li>
            <li>借阅天数默认为五天（可自行修改），修改后务必记住，避免因错过时间，接受超时处罚。处罚过多可影响个人在本网站的个人体验</li>
            <li>#不确定书名（或不清楚是否存在）点击这里 &nbsp;<a href="#form-seb" style="color: #000000;">查询</a></li>
            <li>借阅成功时,返回取书地址（1HF/18#/5f/32或第一层第十八列第五行第三十二列）解释为图书馆层数/图书列数/图书所在列数的层数/图书所在列数的层数的第几列</li>
            <li>查看更多规则点击这里 <a href="${pageContext.request.contextPath}/about" style="color: #000000;">查看</a></li>
        </ol>
    </div>
    <div class="col-md-8">
        <div>
            <h3 style="margin: 20px auto; text-align: center;">
                <sapn>书籍借阅</sapn>
            </h3>
        </div>
        <div style="line-height: 2.5em;"class="form-group">
            <form id="form-borrow" action="">
                <div><span>借阅书名：<input class=" form-control" type="text" name="borrowName" value="${bookName}" placeholder="请输入要借阅的书名"></span></div>
                <div><span>借阅用户：<input class="form-control" type="text" name="borrowUser" value="${user.email}" readonly></span></div>
                <div><span>借阅数量：<input class="form-control" type="number" name="borrowNumber"  value="1"
                                       maxlength="10" onkeyup="value=value.replace(/[^\d]/g,'')" onblur="value=value.replace(/[^\d]/g,'')" placeholder="默认为1，请输入正整数"></span></div>
                <div><span>借阅天数：<input class="form-control" type="number" name="borrowDay" id="defaultNumber" value="5"
                                       onkeyup="value=value.replace(/[^\d]/g,'')" onblur="value=value.replace(/[^\d]/g,'')" placeholder="默认5天"></span></div>
                <div><span>默认押金：<input class="form-control" type="number" name="deposit" value="30"
                                       onkeyup="value=value.replace(/[^\d]/g,'')" onblur="value=value.replace(/[^\d]/g,'')"></span></div>
                <div style="margin: 20px auto; text-align: center;"><input class="btn btn-primary" type="button" id="borrow-btn" value="借阅"></div>
            </form>
        </div>
    </div>
</div>



<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>

<script type="text/javascript">
    $("#borrow-btn").click(function () {
        var formData = new FormData(document.getElementById("form-borrow"));
        // var df = document.getElementById("defaultNumber");
        // formData.set("borrowDay",df);
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/pushBorrow',

            data:formData,
            processData:false,
            contentType:false,
            dataType:'json',

            success:function (data) {
                alert("状态："+data.msg+"\n"+"取书地址："+data.address);
            }
        });

    });
</script>
</body>
</html>
