package cloud.xhmy.controller;

import cloud.xhmy.pojo.Books;
import cloud.xhmy.pojo.EBooks;
import cloud.xhmy.service.BooksService;
import cloud.xhmy.service.EBooksService;
import cloud.xhmy.util.files.FilesDelete;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author aliketh.xhmy
 */
@Controller
public class BooksController {

    @Autowired
    @Qualifier("booksServiceImpl")
    private BooksService booksService;
    @Autowired
    @Qualifier("eBooksServiceImpl")
    private EBooksService eBooksService;


    /**
     * 跳转到书籍上传页面
     * @return
     */
    @RequestMapping("/toUploadBook")
    public String toUpload(){
        return "operation/uploadBook";
    }
    @RequestMapping("/allBooks")
    public ModelAndView allBook(String shelves, @RequestParam(defaultValue = "1") int page,
                                @RequestParam(defaultValue = "10") int size) {
        ModelAndView mv = new ModelAndView();
        List<Books> list = booksService.listAll(shelves,page, size);
        PageInfo<Books> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos", pageInfos);
        mv.setViewName("operation/listBooks");
        return mv;
    }

    /**
     * 分页列出所有EBooks
     * @param shelves
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/allEBooks/{user.email}")
    public ModelAndView allEBook(@PathVariable("user.email") String shelves, @RequestParam(defaultValue = "1") int page,
                                @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<EBooks> list = eBooksService.listAll(shelves,page, size);
        PageInfo<EBooks> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos", pageInfos);
        mv.setViewName("operation/listEBooks");
        return mv;
    }

    /**
     * 分页列出所有Books
     * @param shelves
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/allBooks/{user.email}")
    public ModelAndView allBooks(@PathVariable("user.email") String shelves, @RequestParam(defaultValue = "1") int page,
                                @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<Books> list = booksService.listAll(shelves,page, size);
        PageInfo<Books> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos", pageInfos);
        mv.setViewName("operation/listBooks");
        return mv;
    }

    /**
     * 跳转到修改页面
     * @param id
     * @return
     */
    @RequestMapping("/toModifyBook/{bookId}")
    public String toModifyBook(@PathVariable("bookId") int id,Model model){
        Books book = booksService.queryById(id);
        model.addAttribute("book",book);
        return "operation/modifyBook";
    }
    /**
     * 跳转到修改页面
     * @param id
     * @return
     */
    @RequestMapping("/toModifyEBook/{eBookId}")
    public String toModifyEBook(@PathVariable("eBookId") int id,Model model){
        EBooks eBook = eBooksService.queryById(id);
        model.addAttribute("eBook",eBook);
        return "operation/modifyBook";
    }

    /**
     * 修改book
     * @param request
     * @param model
     * @param file
     * @return
     */
    @ResponseBody
    @RequestMapping("/modifyBook")
    public JSONObject oneFileUpload(HttpServletRequest request, @ModelAttribute Books books,
                                    Model model, @RequestParam("cp") MultipartFile file){

        JSONObject jsonObject = new JSONObject();
        String bookPicture = null;
        Books book = booksService.queryById(books.getId());

        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String contextPath = "/books/picture/";
        String realpath = rootPath+contextPath;
        System.out.println("================================================realpath:=="+realpath);
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        String savePicName =new Date().getTime()+suffix;

        File dirPath = new File(realpath);
        if(!dirPath.exists()){
            dirPath.mkdirs();
        }

        File savePath = new File(dirPath,savePicName);
        if (!file.isEmpty()){
            //删除旧图片
            if (!book.getCoverPicture().isEmpty()){
                FilesDelete.delete(books.getCoverPicture(),request);
                bookPicture = contextPath + savePicName;
                try {
                    file.transferTo(savePath);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else {
            bookPicture = books.getCoverPicture();
        }
        //上传
        books.setUuid(UUID.randomUUID().toString().replaceAll("-",""));
        books.setCoverPicture(bookPicture);
        booksService.modifyById(books);

        jsonObject.put("code","1");
        jsonObject.put("msg","成功！");
        return jsonObject;
    }


    /**
     * 修改eBook
     * @param model
     * @param request
     * @param files
     * @return
     */
    @ResponseBody
    @RequestMapping("/modifyEBook")
    public JSONObject multiFileUpload(Model model,@ModelAttribute EBooks eBooks,
                                      HttpServletRequest request, List<MultipartFile> files) {

        JSONObject jsonObject = new JSONObject();
        String bookContent = "";
        String bookPicture = "";
        EBooks books = eBooksService.queryById(eBooks.getId());

        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String contextPath = "/upload/eBooks/";
        String picturePath = "/upload/picture/";
        //------------------------------------------------------------------------

        String bookRealpath = rootPath+contextPath;
        String pictureRealpath= rootPath+picturePath;
        File picDir = new File(pictureRealpath);
        File booksDir = new File(bookRealpath);
        if(!booksDir.exists() || !picDir.exists()){
            booksDir.mkdirs();
            picDir.mkdir();
        }
        //------------------------------------------------------------------------

        files.get(0);
        String pictureNme = files.get(0).getOriginalFilename();
        String suffixPic = pictureNme.substring(pictureNme.lastIndexOf("."));
        String savePictureName = new Date().getTime()+suffixPic;
        File picFile = new File(pictureRealpath,savePictureName);
        //------------------------------------------------------------------------

        files.get(1);
        String bookName =  files.get(1).getOriginalFilename();
        String suffixBook = bookName.substring(bookName.lastIndexOf("."));
        String saveBookName = UUID.randomUUID().toString().replaceAll("-","")+suffixBook;
        File bookFile = new File(bookRealpath, saveBookName);
        //------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------


        if (!files.get(0).isEmpty()){
            //删除旧图片
            if (!books.getCoverPicture().isEmpty()){
                FilesDelete.delete(books.getCoverPicture(),request);
                bookPicture = picturePath + savePictureName;
                try {
                    files.get(0).transferTo(picFile);
                    bookPicture = picturePath + savePictureName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else {
            bookPicture = books.getCoverPicture();
        }

        //----------------------------------------------------------------------------------------
        if (!files.get(1).isEmpty()) {
            //删除旧文件
            if (!books.getUuid().isEmpty()) {
                FilesDelete.delete(books.getUuid(), request);
                bookContent = contextPath + saveBookName;
                try {
                    files.get(1).transferTo(bookFile);
                    bookContent = contextPath + saveBookName;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
           bookContent = books.getUuid();
        }
        //----------------------------------------------------------------------------------------


        //设置保存封面图片路劲
        eBooks.setCoverPicture(bookPicture);
        //设置保存电子书的路径，以uuid重命名
        eBooks.setUuid(bookContent);
        //保存
        eBooksService.modifyById(eBooks);

        model.addAttribute("failure","失败");
        jsonObject.put("code","1");
        jsonObject.put("msg","成功！");
        return jsonObject;
    }


    /**
     * 查询图书
     * @param name
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/queryBook")
    public ModelAndView queryBook(@RequestParam("search") String name,
                                  @RequestParam(defaultValue = "1") int page,
                                  @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<Books> list = booksService.queryByName(name, page, size);
        PageInfo<Books> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfoBook",pageInfos);
        mv.addObject("search",name);
        mv.setViewName("operation/query/query-book");
        return mv;

    }

    /**
     * 查询电子书
     * @param name
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/queryEBook")
    public ModelAndView queryEBook(@RequestParam("search") String name,
                                   @RequestParam(defaultValue = "1") int page,
                                   @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<EBooks> list = eBooksService.queryByName(name, page, size) ;
        PageInfo<EBooks> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfoEBook",pageInfos);
        mv.addObject("search",name);
        mv.setViewName("operation/query/query-eBook");
        return mv;

    }

    /**
     * 查询个人列表电子书数据
     * @param name
     * @param user
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/queryMyselfEBook/{user}")
    public ModelAndView queryMyself(@RequestParam("search") String name, @PathVariable String user,
                                    @RequestParam(defaultValue = "1") int page,
                                    @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<EBooks> list = eBooksService.queryByMyself(name, user, page, size) ;
        PageInfo<EBooks> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos",pageInfos);
        mv.addObject("searchEBook",name);
        mv.setViewName("operation/listEBooks");
        return mv;

    }
//    SELECT * FROM PTP_FPROJ_INF ORDER BY PROJ_NO deSC LIMIT 5;
    /**
     * 主页一
     * @return
     */
    @RequestMapping("/home1")
    public String toIndex( Model model ){

        List<Books> list = booksService.homeQuery();
        model.addAttribute("book",list);
        return "user/index";
    }

    /**
     * 主页二
     * @return
     */
    @RequestMapping("/home2")
    public String toIndex2(Model model){
        List<EBooks> list = eBooksService.homeQuery();
        model.addAttribute("eBook",list);
        return "user/index1";
    }

//    /**
//     * 主页展示图书
//     * @param name
//     * @return
//     */
//    @RequestMapping("/homeBook")
//    public ModelAndView homeBook(@RequestParam("search") String name){
//        ModelAndView mv = new ModelAndView();
//        mv.setViewName("operation/query");
//        return mv;
//
//    }
//
//    /**
//     * 主页展示电子书
//     * @param name
//     * @return
//     */
//    @RequestMapping("/homeEBook")
//    public ModelAndView homeEBook(@RequestParam("search") String name,
//                                   @RequestParam(defaultValue = "1") int page,
//                                   @RequestParam(defaultValue = "10") int size){
//        PageHelper.startPage(page,size);
//        ModelAndView mv = new ModelAndView();
//        List<EBooks> list = eBooksService.queryByName(name, page, size) ;
//        PageInfo<EBooks> pageInfos = new PageInfo<>(list);
//        mv.addObject("pageInfos",pageInfos);
//        mv.setViewName("operation/listEBooks");
//        return mv;
//
//    }

    /**
     * 查看书籍
     * @param bookId
     * @param model
     * @return
     */
    @RequestMapping("/browseBook/{id}")
    public String browse(@PathVariable("id") int bookId, Model model){
        Books books = booksService.queryById(bookId);
        model.addAttribute("books",books);
        return "operation/browseBook";
    }
    /**
     * 查看书籍
     * @param eBookId
     * @param model
     * @return
     */
    @RequestMapping("/browseEBook/{id}")
    public String browseEBook(@PathVariable("id") int eBookId, Model model){
        EBooks eBooks = eBooksService.queryById(eBookId);
        model.addAttribute("books",eBooks);
        return "operation/browseEBook";
    }

    /**
     * 删除书籍
     * @param bookId
     */
    @ResponseBody
    @RequestMapping("/delete/book")
    public JSONObject deleteBook(@RequestParam("id") List<Integer> bookId ,HttpServletRequest request){
        List<Integer> list = bookId;
        for (int i = 0; i < list.size(); i++) {
            int id = list.get(i);
            booksService.deleteById(id);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code","1");
        return jsonObject;

    }

    /**
     * 删除电子书
     * @param eBookId
     */
    @ResponseBody
    @RequestMapping("/delete/eBook")
    public JSONObject deleteEBook(@RequestParam("id") List<Integer> eBookId,HttpServletRequest request){
        List<Integer> list = eBookId;
        EBooks eBooks;
        for (int i = 0; i < list.size(); i++) {
            int id = list.get(i);
            eBooks = eBooksService.queryById(id);
            FilesDelete.delete(eBooks.getUuid(),request);
            eBooksService.deleteById(id);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code","1");
        return jsonObject;

    }



    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */

}
