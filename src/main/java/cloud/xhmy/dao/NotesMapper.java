package cloud.xhmy.dao;

import cloud.xhmy.pojo.Books;
import cloud.xhmy.pojo.Notes;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface NotesMapper {

    /**
     * 增加书籍
     * @param format
     * @return
     */
    int addNotes(Notes format);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<Notes> queryByName(@Param("notesName") String name, int page, int size);

    /**
     * 列出所有
     * @param page
     * @param size
     * @return
     */
    List<Notes> listAll(@Param("notesAuthor") String author,int page, int size);

    /**
     * 修改
     * @param id
     * @return
     */
    int modifyById(int id);

    /**
     * 用于修改
     * @param id
     * @return
     */
    Notes queryById(int id);


    
}
