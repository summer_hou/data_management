<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/22
  Time: 22:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Document</title>

<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
    <style>
        div.downloadNva{
            position: absolute;
            top: 0;
            width: 100%;
            height: 60px;
            background: #122b40;
            color: white;
        }
        .load{
            width: 30%;
            height: 40px;
            margin:10px auto;
            padding: 2px;
            text-align: center;
        }


    </style>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>
<div class="downloadNva">
    <span class="load"><a href="#"><button>下载</button></a>word</span>
    <span class="load"><a href="#"><button>下载</button></a>pdf</span>
    <span class="load"><a href="#"><button>下载</button></a>md</span>
</div>

<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/admin/document.jsp"/>

<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>
</body>
</html>
