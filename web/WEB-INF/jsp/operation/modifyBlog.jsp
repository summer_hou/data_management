<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/31
  Time: 23:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>modifyBlog</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>


    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div class="container">

    <div class="row">
        <div style="margin: 30px auto; text-align: center;">
<%--            <span><a href="#push"><button class="btn btn-primary">本地上传</button></a></span>--%>
            <span><button class="btn btn-primary" id="pushBlog">发布</button></span>
            <span><button class="btn btn-primary" id="saveDraft">保存草稿</button></span>
            <span><a href="${pageContext.request.contextPath}/listBlog/${user.email}#draft"><button
                    class="btn btn-primary">草稿箱</button></a></span>
            <span><a href="${pageContext.request.contextPath}/listBlog/${user.email}#pushed"><button class="btn btn-primary">所有博客</button></a></span>
        </div>

    </div>
    <div class="row">

        <div class="col-md-12">
            <form>
                <input type="hidden" id="id" name="blogId" value="${blog.id}">
                <input type="hidden" id="content" name="blogContent" value="${blog.blogContent}">
                <span>博客名称：</span><input class="form-control" id="blogName" name="blogName" value="${blog.blogName}"
                                         type="text"><br>
                <span>博客标题：</span><input class="form-control" id="blogTitle" name="blogTitle" value="${blog.blogTitle}"
                                         type="text"><br>
                <span>博客标签：</span><input class="form-control" id="blogTag" name="blogTag" value="${blog.blogTag}"
                                         type="text"><br>
                <span>博客主题：</span><input class="form-control" id="blogTheme" name="blogTheme" value="${blog.blogTheme}"
                                         type="text"><br>
                <span>博客作者：</span><input class="form-control" id="blogAuthor" name="blogAuthor"
                                         value="${blog.blogAuthor}" readonly type="text">

            </form>
        </div>

    </div>

</div>

<div id="editorMd"></div>

<div style="width: 80%; margin: 5px auto;text-align: center;">
    <h3><span>博客内容：</span></h3>

</div>
<div id="article-modify">

    <textarea style="display:none;" class="form-control" id="mdData" name="text"></textarea>
    <textarea class="editormd-html-textarea" name="text" id="editormdData"></textarea>

</div>


<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>

<script src="${pageContext.request.contextPath}/plugins/editor-md/editormd.min.js"></script>
<script type="text/javascript">

    $("#mdData").load("${pageContext.request.contextPath}/static${blog.blogContent}");

    var draftArticle;
    $(function () {

        //$.get('${pageContext.request.contextPath}/static/upload/document.md', function(md){
        draftArticle = editormd("article-modify", {
            width: "90%",
            height: 700,
            path: "${pageContext.request.contextPath}/plugins/editor-md/lib/",
            imageUpload: true,
            imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL: "${pageContext.request.contextPath}/upload/mdImages",
            saveHTMLToTextarea: true
        });
        // });
        // var editor = editormd("", {
        //
        // });
    });
    $("#pushBlog").click(function () {
        var data = $("#editormdData").val();
        //alert(html);
        $.ajax({

            url: '${pageContext.request.contextPath}/editorBlog',
            data: {
                'data': data,
                'id': $("#id").val(),
                'blogName': $("#blogName").val(),
                'blogTitle': $("#blogTitle").val(),
                'blogTag': $("#blogTag").val(),
                'blogTheme': $("#blogTheme").val(),
                'blogAuthor': $("#blogAuthor").val()
            },
            dataType: 'text',
            type: 'post',

            success: function (data) {
                alert(data);
            }
        })
    });
    $("#saveDraft").click(function () {
        var data = $("#mdData").val();
        //alert(html);
        $.ajax({

            url: '${pageContext.request.contextPath}/draftBlog',
            data: {
                'data': data,
                'blogName': $("#blogName").val(),
                'blogTitle': $("#blogTitle").val(),
                'blogTag': $("#blogTag").val(),
                'blogTheme': $("#blogTheme").val(),
                'blogAuthor': $("#blogAuthor").val()

            },
            dataType: 'text',
            type: 'post',

            success: function (data) {
                var json = JSON.parse(data);
                alert(json.code + ":" + json.msg);
            }
        })
    });




</script>
</body>
</html>
