package cloud.xhmy.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.util.Date;

/**
 * 预约
 * subscribe
 * @author aliketh.xhmy
 */
public class Subscribe {

    private int id;
    private String subscribeName;
    private String subscribeUser;
    private int subscribeNumber;
    private int borrowDay;
    private double deposit;
    private String punish;
    private String tfReturn;
    private String tfDefault;
    private String takeSubscribe;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Timestamp subscribeTime;
//    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone="GMT+8")
    private String takeTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date returnTime;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubscribeName() {
        return subscribeName;
    }

    public void setSubscribeName(String subscribeName) {
        this.subscribeName = subscribeName;
    }

    public String getSubscribeUser() {
        return subscribeUser;
    }

    public void setSubscribeUser(String subscribeUser) {
        this.subscribeUser = subscribeUser;
    }

    public int getSubscribeNumber() {
        return subscribeNumber;
    }

    public void setSubscribeNumber(int subscribeNumber) {
        this.subscribeNumber = subscribeNumber;
    }

    public int getBorrowDay() {
        return borrowDay;
    }

    public void setBorrowDay(int borrowDay) {
        this.borrowDay = borrowDay;
    }

    public double getDeposit() {
        return deposit;
    }

    public void setDeposit(double deposit) {
        this.deposit = deposit;
    }

    public String getPunish() {
        return punish;
    }

    public void setPunish(String punish) {
        this.punish = punish;
    }

    public String getTfReturn() {
        return tfReturn;
    }

    public void setTfReturn(String tfReturn) {
        this.tfReturn = tfReturn;
    }

    public String getTfDefault() {
        return tfDefault;
    }

    public void setTfDefault(String tfDefault) {
        this.tfDefault = tfDefault;
    }

    public String getTakeSubscribe() {
        return takeSubscribe;
    }

    public void setTakeSubscribe(String takeSubscribe) {
        this.takeSubscribe = takeSubscribe;
    }

    public Timestamp getSubscribeTime() {
        return subscribeTime;
    }

    public void setSubscribeTime(Timestamp subscribeTime) {
        this.subscribeTime = subscribeTime;
    }

    public String getTakeTime() {
        return takeTime;
    }

    public void setTakeTime(String takeTime) {
        this.takeTime = takeTime;
    }

    public Date getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Date returnTime) {
        this.returnTime = returnTime;
    }
}
