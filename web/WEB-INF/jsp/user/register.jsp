<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/22
  Time: 14:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 网页图标 ico格式 -->
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico">
    <!-- jquery -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>
    <!-- 引入 bootstrap js文件 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- 引入 bootstrap css文件 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css" type="text/css">
    <!-- 密码的显示与隐藏 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/password-show-hidden.js"></script>
    <!-- 动态背景 -->
<%--    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/user-lrf-page-back.js"></script>--%>




    <style>
        html,
        body {
            overflow: hidden;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            background: #000;
        }

        canvas {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background: #000;
            cursor: pointer;
        }

        div.pos {
            position: relative;
            width: 40%;
            height: 50%;
            margin: 10% auto;
            z-index: 100;
            color: wheat;
        }

        div.footer {
            position: relative;
            bottom: -30px;

            color: #888;
            text-align: center;
            z-index: 100;
        }


    </style>

</head>
<body>
<div class="pos">
    <h3 style="margin-bottom: 20px; text-align: center;"><span>注册用户</span></h3>
    <form method="POST" action="${pageContext.request.contextPath}/register">
        <div class="form-group"><label for="email">输入邮箱</label>
            <input id="email" type="email" class="form-control" name="email" value="${email}"
                   placeholder="请输入用户邮箱 @dm.net 或者@alike.com" required autofocus onblur="validateName()">
            <span style="color: red;" id="emailInfo"></span>
        </div>
        <div class="form-group"><label for="password">输入密码</label>
            <input id="password" type="password" class="form-control" value="${password}"
                   name="password" placeholder="请输入六位以上由数字,密码,符号组成的密码" required data-eye></div>
        <div class="form-group"><label for="checkPwd">确认密码</label>
            <input id="checkPwd" type="password" class="form-control"
                   name="checkPwd" placeholder="重新输入确认密码" onkeyup="checkPassword()" required data-eye>
            <span id="hint"></span>
        </div>
        <div class="form-group">
            <label for="checkCode">验证码</label>
            <span class="" style="">看不清，点击图片</span>
            <br>
            <img src="${pageContext.request.contextPath}/code" class="" onclick="changeNewOne($(this));"
                 style=" width: 100px ;height: 34px;display: inline;border-radius: 2px;margin-bottom: 2px;"/>
            <input id="checkCode" type="text" class="" name="checkCode" placeholder="请输入验证码"
                   style="width: 50% ;  height: 35px;padding: 6px 12px; font-size: 14px;
                   line-height: 1.42857143;color: #555;background-color: #fff; background-image: none;
                   border: 1px solid #ccc;border-radius: 4px; margin-left: -5px;" onblur="inputCheckCode()">
            <span style="margin-left: 20px;" id="info-message"></span>
        </div>
        <div class="form-group no-margin"><button type="submit" class="btn btn-primary btn-block">注册 </button></div>
        <div class="text-center">我有账号返回? <a href=${pageContext.request.contextPath}/toLogin>登录</a></div>
    </form>

</div>
<div class="footer">
    <p>
        <span>© 2020 xhmy.cloud . &nbsp;</span>
        <span>赣ICP证</span> &nbsp;
        <a class="icp"
           href="https://beian.miit.gov.cn"
           title=""
           target="_blank">赣ICP备</a>
        <span>2020012707号</span> &nbsp;
    </p>
</div>
<script type="text/javascript">
    // 验证码
    function changeNewOne(obj) {
        obj.attr("src", "${pageContext.request.contextPath}/code?d=" + new Date().getTime());
    };

    function inputCheckCode() {
        $.ajax({
            type: 'post',
            url: '${pageContext.request.contextPath}/checkCode',
            data: {
                'code': $("#checkCode").val()
            },
            // processData:false,
            // contentType:false,
            dataType: 'json',
            success: function (data) {
                if (data.code == "1") {
                    document.getElementById("info-message").innerHTML = "<font style='color: green'>" + data.msg + "</font>";
                } else {
                    document.getElementById("info-message").innerHTML = "<font style='color: red'>" + data.msg + "</font>";
                }
            }
        })
    };

</script>
<script type="text/javascript">
    //验证两次输入密码是否一致
    function checkPassword() {
        var password = document.getElementById("password").value;
        var chkPassword = document.getElementById("checkPwd").value;

        if(password == chkPassword) {
            document.getElementById("hint").innerHTML="<br><font color='green'>两次密码输入一致</font>";
            document.getElementById("submit").disabled = false;

        }else {
            document.getElementById("hint").innerHTML="<br><font color='red'>两次输入密码不一致!</font>";
            document.getElementById("submit").disabled = true;
        }
    }
</script>
<!-- -->
<script>
    // 原生ajax检测用户的唯一性

    var xmlHttp;

    //创建Ajax对象
    function createXMLHttpRequest() {
        if (window.ActiveXObject) {
            //IE6 and IE5
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            //W3C浏览器和IE7及其以上版本
            xmlHttp = new XMLHttpRequest();
        }
    }

    function validateName() {
        //调用创建Ajax方法
        createXMLHttpRequest();
        //获取用户名的值
        var email = document.getElementById("email");
        //将内容发送给哪个url处理
        var url = "${pageContext.request.contextPath}/judgeEmail?email=" + escape(email.value);
        //创建HTTP请求（请求方式，请求URL，是否异步）
        xmlHttp.open("GET", url, true);
        //状态改变时
        xmlHttp.onreadystatechange = function () {
            //如果都ok
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {

                // var nameReg = /^[a-zA-Z][\w]{5,19}$/;//以字母开头，后面内容可以是字母，数字，下划线，且6~20位
                //必须由 大小写字母 或 数字 或下划线开头
                var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

                // var emailReg = /^[a-z0-9A-Z]+[- | a-z0-9A-Z . _]+@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-z]{2,}$/;
                if (emailReg.test(document.getElementById('email').value) == false) {
                    document.getElementById('emailInfo').innerText = "邮箱地址格式不正确";
                } else {
                    var color = "red";
                    var message = xmlHttp.responseXML.getElementsByTagName("message")[0].firstChild.data;
                    var passed = xmlHttp.responseXML.getElementsByTagName("passed")[0].firstChild.data;
                    if (passed == "true") {
                        color = "green";
                    }
                    document.getElementById("emailInfo").innerHTML = "<span style=color:" + color + ">" + message + "</span>";
                }
            }
        };
        xmlHttp.send();

    }
</script>

<script>
    "use strict";

    {
        const perlin = {
                init() {
                    this.p = new Uint8Array(512);
                    this.reset();
                }

                ,
                reset() {
                    const p = new Uint8Array(256);
                    for (let i = 0; i < 256; i++) p[i] = i;
                    for (let i = 255; i > 0;

                         i--) {
                        const n = Math.floor((i + 1) * Math.random());
                        [p[i],
                            p[n]
                        ] = [p[n],
                            p[i]
                        ];
                    }

                    for (let i = 0; i < 512; i++) this.p[i] = p[i & 255];
                }

                ,
                lerp(t, a, b) {
                    return a + t * (b - a);
                }

                ,
                grad2d(i, x, y) {
                    const v = (i & 1) === 0 ? x : y;
                    return (i & 2) === 0 ? -v : v;
                }

                ,
                noise2d(x2d, y2d) {
                    const X = Math.floor(x2d) & 255;
                    const Y = Math.floor(y2d) & 255;
                    const x = x2d - Math.floor(x2d);
                    const y = y2d - Math.floor(y2d);
                    const fx = (3 - 2 * x) * x * x;
                    const fy = (3 - 2 * y) * y * y;
                    const p0 = this.p[X] + Y;
                    const p1 = this.p[X + 1] + Y;
                    return this.lerp(fy,
                        this.lerp(fx,
                            this.grad2d(this.p[p0], x, y),
                            this.grad2d(this.p[p1], x - 1, y)),
                        this.lerp(fx,
                            this.grad2d(this.p[p0 + 1], x, y - 1),
                            this.grad2d(this.p[p1 + 1], x - 1, y - 1)));
                }
            }

        ;

        /////////////////////////////////////////////////////////////////
        const canvas = {
                init() {
                    this.elem = document.createElement("canvas");
                    document.body.appendChild(this.elem);
                    this.width = this.elem.width = this.elem.offsetWidth;
                    this.height = this.elem.height = this.elem.offsetHeight;
                    return this.elem.getContext("2d");
                }
            }

        ;

        /////////////////////////////////////////////////////////////////
        const webgl = {
                init(canvas, options) {
                    this.elem = document.createElement("canvas");
                    this.gl = (this.elem.getContext("webgl", options) || this.elem.getContext("experimental-webgl", options));
                    if (!this.gl) return false;
                    const vertexShader = this.gl.createShader(this.gl.VERTEX_SHADER);
                    this.gl.shaderSource(vertexShader,
                        ` precision highp float;
						attribute vec3 aPosition;
						uniform vec2 uResolution;

						void main() {
							gl_PointSize=1.0;
							gl_Position=vec4((aPosition.x / uResolution.x * 2.0) - 1.0,
							(-aPosition.y / uResolution.y * 2.0) + 1.0,
							0.0,
							1.0);
						}

						`
                    );
                    this.gl.compileShader(vertexShader);
                    const fragmentShader = this.gl.createShader(this.gl.FRAGMENT_SHADER);
                    this.gl.shaderSource(fragmentShader,
                        ` precision highp float;

						void main() {
							gl_FragColor=vec4(0.2, 0.3, 1.0, 1.0);
						}

						`);
                    this.gl.compileShader(fragmentShader);
                    const program = this.gl.createProgram();
                    this.gl.attachShader(program, vertexShader);
                    this.gl.attachShader(program, fragmentShader);
                    this.gl.linkProgram(program);
                    this.gl.useProgram(program);
                    this.aPosition = this.gl.getAttribLocation(program, "aPosition");
                    this.gl.enableVertexAttribArray(this.aPosition);
                    this.positionBuffer = this.gl.createBuffer();
                    this.elem.width = canvas.width;
                    this.elem.height = canvas.height;
                    const uResolution = this.gl.getUniformLocation(program, "uResolution");
                    this.gl.enableVertexAttribArray(uResolution);
                    this.gl.uniform2f(uResolution, canvas.width, canvas.height);
                    this.gl.viewport(0,
                        0,
                        this.gl.drawingBufferWidth,
                        this.gl.drawingBufferHeight);
                    return this.gl;
                }

                ,
                drawBuffer(data, num) {
                    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.positionBuffer);
                    this.gl.vertexAttribPointer(this.aPosition, 2, this.gl.FLOAT, false, 0, 0);
                    this.gl.bufferData(this.gl.ARRAY_BUFFER,
                        data,
                        this.gl.DYNAMIC_DRAW);
                    this.gl.drawArrays(this.gl.GL_POINTS, 0, num);
                }
            }

        ;
        /////////////////////////////////////////////////////////////////
        const ctx = canvas.init();

        const gl = webgl.init(canvas, {
                alpha: false,
                stencil: false,
                antialias: false,
                depth: false,
            }

        );
        perlin.init();
        const nParticles = 3000;
        const velocities = new Float32Array(nParticles * 2);
        const particles = new Float32Array(nParticles * 2);
        let frame = 0;
        for (let i = 0; i < nParticles;

             i++) {
            const p = i * 2;
            particles[p + 0] = Math.random() * canvas.width;
            particles[p + 1] = Math.random() * canvas.height;
        }

        /////////////////////////////////////////////////////////////////
        const run = () => {
                requestAnimationFrame(run);
                frame++;
                gl.clear(gl.COLOR_BUFFER_BIT);
                ctx.globalCompositeOperation = "source-over";
                ctx.fillStyle = "rgba(0, 0, 0, 0.025)";
                ctx.fillRect(0, 0, canvas.width, canvas.height);
                ctx.globalCompositeOperation = "lighter";
                for (let i = 0; i < nParticles;

                     i++) {
                    const p = i * 2;
                    let n = 80 * perlin.noise2d(particles[p + 0] * 0.001, particles[p + 1] * 0.001);
                    velocities[p + 0] += 0.1 * Math.cos(n);
                    velocities[p + 1] += 0.1 * Math.sin(n);
                    particles[p + 0] += (velocities[p + 0] *= 0.99);
                    particles[p + 1] += (velocities[p + 1] *= 0.99);
                    particles[p + 0] = (canvas.width + particles[p + 0]) % canvas.width;
                    particles[p + 1] = (canvas.height + particles[p + 1]) % canvas.height;
                }

                webgl.drawBuffer(particles, nParticles);
                if (frame > 30) ctx.drawImage(webgl.elem, 0, 0);
            }

        ;
        requestAnimationFrame(run);

        /////////////////////////////////////////////////////////////////
        ["click",
            "touchdown"
        ].forEach(event => {
                document.addEventListener(event, e => perlin.reset(), false);
            }

        );
    }
</script>


</body>
</html>
