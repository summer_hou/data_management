<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/4
  Time: 14:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>listSubscribe</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/page-helper.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>


<div class="" style="width: 95%;margin: 5px auto">
    <div class="row">
        <div class="col-md-12 ">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
<%--                    <th>#</th>--%>
                    <th>预约书名</th>
                    <th>预约用户</th>
                    <th>预约数量</th>
                    <th>借阅天数</th>
                    <th>缴付押金</th>
<%--                    <th>是否处罚</th>--%>
<%--                    <th>处罚内容</th>--%>
                    <th>预约时间</th>
                    <th>取书时间</th>
                    <th>取书地址</th>
<%--                    <th>是否归还</th>--%>
<%--                    <th>归还时间</th>--%>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="subscribe">
                <c:forEach var="subscribe" items="${pageInfos.list}">
                    <tr>
                            <%--                        <td><input name="ids" type="checkbox"><input type="hidden" name="id" value="${subscribe.id}"></td>--%>
                        <td>${subscribe.subscribeName}</td>
                        <td>${subscribe.subscribeUser}</td>
                        <td>${subscribe.subscribeNumber}</td>
                        <td>${subscribe.borrowDay}</td>
                        <td>${subscribe.deposit}</td>
<%--                        <td>${subscribe.tfDefault}</td>--%>
<%--                        <td>${subscribe.punish}</td>--%>
                        <td>${subscribe.subscribeTime}</td>
                        <td>${subscribe.takeTime}</td>
                        <td>${subscribe.takeSubscribe}</td>
<%--                        <td>${subscribe.tfReturn}</td>--%>
<%--                        <td>${subscribe.returnTime}</td>--%>


                        <td class="text-center">
                            <a href="javascript:takeSubscribe(${subscribe.id})"
                               class="btn bg-olive btn-xs" id="takeSubscribe">取书完成</a>
<%--                            ||--%>
<%--                            <a href="${pageContext.request.contextPath}/delete/${subscribe.id}"--%>
<%--                               class="btn bg-olive btn-xs">取消</a>--%>

                        </td>
                    </tr>
                </c:forEach>

                </tbody>
                <tfoot>
<%--                    <span><em><b>注意：博客状态已数字代表【0】表示等待审核，【1】表示通过，【2】表示不通过</b></em></span>--%>

                </tfoot>

            </table>

        </div>
    </div>
    <div class="xhmy_page_div"></div>
    <script type="text/javascript">
        //翻页
        $(".xhmy_page_div").createPage({
            pageNum: ${pageInfos.pages},
            current: 1,
            backfun: function(e) {
                //console.log(e);//回调
            }
        });
        function pageNumber(params) {
            $("#subscribe").load("${pageContext.request.contextPath}/listSubscribe/${user.email} #subscribe >*",{page:params,size:"10"});

        };
    </script>
</div>



<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>


<script type="text/javascript">
    function takeSubscribe(claNum) {
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/takeSubscribe',
            data:{
                'id':claNum
            },
            success:function (data) {
                if (data.code =="1"){
                    alert("状态："+"提交成功！")
                }else {
                    alert("失败："+"重复取书！")
                }
            }
        })
    }
</script>
</body>
</html>
