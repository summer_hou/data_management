package cloud.xhmy.service;

import cloud.xhmy.pojo.Subscribe;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface SubscribeService {
    /**
     * 增加
     * @param subscribe
     * @return
     */
    int addSubscribe(Subscribe subscribe);

    /**
     * 根据id删除
     * @param id
     * @return
     */
    int deleteById(int id);


    /**
     * 根据id查询,返回
     * @param id
     * @return
     */
    Subscribe queryById(int id);


    /**
     * 查询全部,返回list集合
     * @param page
     * @param size
     * @return
     */
    List<Subscribe> listAll(String user, int page, int size);



    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */

    /**
     * 查询所有
     * @param page
     * @param size
     * @return
     */
    List<Subscribe> listAllSubscribe(int page, int size);

    /**
     * 通过审核
     * @param id
     * @return
     */
    int push(int id);

    /**
     * 统计
     * @return
     */
    Subscribe countNumber();

    /**
     * 通过名称查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<Subscribe> queryByName(@Param("subscribeName") String name,int page,int size);
}
