<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/24
  Time: 17:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>about</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div id="about-html"></div>

<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>
<script type="text/javascript">
    $("#about-html").load("${pageContext.request.contextPath}/static/about.html");
</script>
</body>
</html>
