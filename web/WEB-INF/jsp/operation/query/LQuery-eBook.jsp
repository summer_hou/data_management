<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/12/5
  Time: 22:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container " style=" width: 90%;margin: 0 auto;background: lightgoldenrodyellow;">
    <div class="eBook">
        <c:forEach var="eBooks" items="${pageInfoEBook.list}">
            <div class="row" style="margin:5px auto;">

                <div class="col-md-3" style=" ">
                    <img src="${pageContext.request.contextPath}/static${eBooks.coverPicture}"
                         alt="抱歉图片加载意外，要图片显示请重新刷新页面"
                         style="width: 120px;height: 160px; margin: 0 auto;"
                         onerror="this.src='${pageContext.request.contextPath}/static/upload/picture/CMLS-NO-Cover.png'">
                </div>
                <div class="col-md-9" style="line-height: 2em;">
                            <span><h3><a
                                    href="${pageContext.request.contextPath}/browseEBook/${eBooks.id}">${eBooks.bookName}</a></h3></span>
                    <span><smell>${eBooks.author}</smell></span><br>
                    <span><p>${eBooks.introduction}</p></span>
                    <span>出版社：${eBooks.publishHouse}</span>
                    <span style="margin-left: 280px;">出版时间：${eBooks.publishTime}</span><br>
                    <span>语言：${eBooks.language}</span>
                    <span style="margin-left: 280px;">类型：${eBooks.classify}</span>

                </div>
            </div>
        </c:forEach>
    </div>

    <div class="xhmy_page_div"></div>
    <script type="text/javascript">
        //翻页
        $(".xhmy_page_div").createPage({
            pageNum: ${pageInfoEBook.pages},
            current: 1,
            backfun: function (e) {
                //console.log(e);//回调
            }
        });

        function pageNumber(params) {
            $(".eBook").load("${pageContext.request.contextPath}/eBooksLanguage?clq=${search} .eBook >*", {

                page: params,
                size: "10"
            });

        };

    </script>

</div>
