package cloud.xhmy.dao;

import cloud.xhmy.pojo.Admins;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;
import java.util.List;

/**
 * @author aliketh.xhmy
 */
public interface AdminsMapper {

    /**
     * 登录
     * @param user
     * @return
     */
    Admins login(Admins user);

    /**
     * 注册
     * @param user
     * @return
     */
    Integer register(Admins user);

    /**
     * 忘记密码
     * @param user
     * @return
     */
    int forgot(Admins user);

    /**
     * 退出
     * @param user
     * @return
     */
    int logout(Admins user);

    /**
     * 修改个人信息
     * @param user
     * @return
     */
    int modify(Admins user);

    /**
     * 注册验证用户邮箱
     * @param email
     * @return
     */
    List<Admins> judge(String email);

    /**
     * 头像保存到数据库
     * @param user
     * @return
     */
    int avatar(Admins user);

    /**
     * 头像保存到服务器
     * @param pro
     * @return
     */
    int pro(Admins pro);

    /**
     * 修改联系电话
     * @param admins
     * @return
     */
    int modifyTel(Admins admins);

    /**
     * 修改密码
     * @param admins
     * @return
     */
    int modifyPwd(Admins admins);

    /**
     * 实名认证
     * @param admins
     * @return
     */
    int realUser(Admins admins);

    /**
     * 用户查询
     * @param admins
     * @return
     */
    Admins queryByEmail(String admins);

    /**
     * 刷新
     * @param user
     * @return
     */
    Admins refresh (Admins user);
}
