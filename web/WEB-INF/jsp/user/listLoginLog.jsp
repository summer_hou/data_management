<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/31
  Time: 22:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>listLoginLog</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>

<%--
    <style>
        div.zxf_pagediv{
            text-align: center;
            color: #999999;
            padding: 20px 20px 40px 0;
        }
        div.zxf_pagediv a{
            text-decoration: none;

        }
        div.zxf_pagediv span,div.zxf_pagediv a{
            display: inline-block;
            box-sizing: border-box;
        }
        .current{
            color: #ffffff;
            background: #1ABC9C;
            width: 40px;
            height: 40px;
            line-height: 40px;
            border-radius: 3px;
        }
        .zxfPagenum{
            color: #666;
            background: #fff;
            width: 40px;
            height: 40px;
            line-height: 40px;
            margin: 0 5px;
            border-radius: 3px;
        }
        .nextpage{

            margin: 0 5px;
        }
        .nextbtn,.prebtn,span.disabled{
            color: #666;
            background: #fff;
            width: 88px;
            height: 42px;
            line-height: 42px;
            border-radius: 3px;
        }
        .zxfinput{
            width: 50px;
            height: 42px;
            text-align: center;
            box-sizing: border-box;
            border: 1px solid #E6E6E6;
            margin: 0 12px;
            border-radius: 3px;
            color: #666;
        }
        .zxfokbtn{
            width: 48px;
            height: 32px;
            line-height: 32px;
            border: 1px solid #E6E6E6;
            margin-left: 10px;
            cursor:pointer;
            border-radius: 3px;
            background: #fff;
        }
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }
        input[type="number"]{
            -moz-appearance: textfield;
        }

    </style>
--%>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/page-helper.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>
<%--    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">--%>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div class="" style="width: 90%; margin: 5px auto;">

    <div class="row">
        <div class="col-md-12 ">
            <table class="table table-hover table-striped" >
                <thead>
                <tr>
                    <th><input type="checkbox" name="CheckAll" id="CheckAll"/></th>
                    <th>登录用户</th>
                    <th>登录方式</th>
                    <th>登录设备</th>
                    <th>登录状态</th>
                    <th>登录ip</th>
                    <th>登录系统</th>
                    <th>登入时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="log" class="log">

                <c:forEach var="loginLog" items="${pageInfos.list}" >
                    <tr>
                        <td><input type="checkbox" name="Check[]" value="${loginLog.id}" id="Check[]"/></td>
                        <td>${loginLog.loginUser}</td>
                        <td>${loginLog.loginWay}</td>
                        <td>${loginLog.loginDevice}</td>
                        <td>${loginLog.loginState}</td>
                        <td>${loginLog.loginIp}</td>
                        <td>${loginLog.loginSystem}</td>
                        <td>${loginLog.loginTime}</td>
                        <td class="text-center">
<%--                            <a href="${pageContext.request.contextPath}/?id=${loginLog.id}"--%>
<%--                               class="btn bg-olive btn-xs">更新</a>--%>
<%--                            ||--%>
                            <a href="javascript:deleteLoginLog(${loginLog.id})"
                               class="btn bg-olive btn-xs">删除</a>

                        </td>
                    </tr>
                </c:forEach>

                </tbody>
                <tfoot>

                </tfoot>

            </table>

        </div>
    </div>
    <div class="xhmy_page_div"></div>
    <script type="text/javascript">
        //翻页
        $(".xhmy_page_div").createPage({
            pageNum: ${pageInfos.pages},
            current: 1,
            backfun: function(e) {
                //console.log(e);//回调
            }
        });
        var html = "";
        var blog = "";
        var i = "";
        var t = "";

        function pageNumber(params) {
            $("#log").load("${pageContext.request.contextPath}/allLoginLog/${user.email} #log >*",{page:params,size:"10"});

        };
    </script>



<%--
    <div class="row form-inline" >
        <div class="col-md-6" style="margin-top: auto;margin-left: 0 ">
			            <span>分别为您列出（<small aria-placeholder="0">${pageInfos.pages}</small>）页，
			                工（${pageInfos.total}）条登录记录</span>
        </div>
        <div class="col-md-6">
            <ul class="pagination " style="float: right">
                <li><a href="${pageContext.request.contextPath}/allLoginLog/${user.email}?page=1&size=10" aria-label="Previous">首页</a>
                </li>
                <li><a href="${pageContext.request.contextPath}/allLoginLog/${user.email}?page=${pageInfos.pageNum-1}&size=10">上一页</a>
                </li>
                <c:forEach begin="1" end="${pageInfos.pages}" var="pageNumber">
                    <li>
                        <a href="${pageContext.request.contextPath}/allLoginLog/${user.email}?page=${pageNumber}&size=10">${pageNumber}</a>
                    </li>
                </c:forEach>
                <li><a href="${pageContext.request.contextPath}/allLoginLog/${user.email}?page=${pageInfos.pageNum+1}&size=10">下一页</a>
                </li>
                <li><a href="${pageContext.request.contextPath}/allLoginLog/${user.email}?page=${pageInfos.pages}&size=10"
                       aria-label="Next">尾页</a></li>
            </ul>
        </div>
    </div>
--%>
</div>



<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>


<script type="text/javascript">
    function deleteLoginLog(did) {
        $.ajax({
            type:'get',
            url:'${pageContext.request.contextPath}/delete/loginLog?id='+did,
            processData:false,
            contentType:false,
            dataType: 'json',
            success:function (data) {
                if (data.code == "1"){
                    alert("成功！");
                }else {
                    alert("失败:"+data.msg);
                }
            }
        })
    }
</script>

<script type="text/javascript">
    //全选/全不选
    $("#CheckAll").bind("click", function () {
        $("input[name='Check[]']").prop("checked", this.checked);
    });

<%--
    //批量删除
    function delAllProduct() {
        if (!confirm("确定要删除吗？")) {
            return;
        }
        var cks = $("input[name='Check[]']:checked");
        var str = "";
        //拼接所有的id
        for (var i = 0; i < cks.length; i++) {
            if (cks[i].checked) {
                str += cks[i].value + ",";
            }
        }
        //去掉字符串末尾的‘&'
        str = str.substring(0, str.length - 1);
        // var a = str.split('&');//分割成数组
        // console.log(str);return false;
        $.ajax({
            type: 'post',
            url: "${pageContext.request.contextPath}/eBook/{id}",
            dataType: "json",
            data: {
                filenames: str
            },
            success: function (data) {
                console.log(data);
                return false;
                if (data['status'] == 1) {
                    alert('删除成功！');
                    location.href = data['url'];
                } else {
                    alert('删除失败！');
                    return false;
                }
            }
        });
    };--%>
</script>

</body>
</html>
