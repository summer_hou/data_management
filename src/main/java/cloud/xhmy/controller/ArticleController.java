package cloud.xhmy.controller;

import cloud.xhmy.pojo.BlogRecord;
import cloud.xhmy.service.BlogService;
import cloud.xhmy.util.files.FilesDelete;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author aliketh.xhmy
 */
@Controller
public class ArticleController {

    @Autowired
    @Qualifier("blogServiceImpl")
    private BlogService blogService;


    /**
     * 删除博客
     * @param blogId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delete/blog")
    public JSONObject blog(@RequestParam("id") List<Integer> blogId, HttpServletRequest request, HttpServletResponse response) {
        JSONObject jsonObject = new JSONObject();
        List<Integer> list = blogId;
        BlogRecord blogRecord;
        for (int i = 0; i < list.size(); i++) {
            int id = list.get(i);
            blogRecord = blogService.queryById(id);
            FilesDelete.delete(blogRecord.getBlogContent(),request);
            blogService.deleteById(id);
        }
        jsonObject.put("code","1");
        return jsonObject;
    }
    /**
     * 删除博客草稿
     * @param blogId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delete/blogDraft")
    public JSONObject blogDraft(@RequestParam("id") List<Integer> blogId,HttpServletRequest request) {
        List<Integer> list = blogId;
        BlogRecord blogRecord;
        for (int i = 0; i < list.size(); i++) {
            int id = list.get(i);
            blogRecord = blogService.queryDraftById(id);
            FilesDelete.delete(blogRecord.getBlogContent(),request);
            blogService.deleteDraftById(id);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code","1");
        return jsonObject;
    }
    /**
     * 跳转到博客编辑页面
     * @return
     */
    @RequestMapping("/toEditorBlog")
    public String toEditorBlog(){
        return "operation/editorBlog";
    }



    /**
     * 跳转到博客修改页面
     * @return
     */
    @RequestMapping("/toModifyBlog/{id}")
    public String toModifyBlog(@PathVariable int id,Model model){
        System.out.println("===================================="+id);
        BlogRecord blogRecord = blogService.queryById(id);
        System.out.println("===================================="+blogRecord.getBlogContent());
        model.addAttribute("blog",blogRecord);
        return "operation/modifyBlog";
    }

    /**
     * 跳转到博客预览页面
     * @param id
     * @return
     */
    @RequestMapping("/toBrowseBlog/{id}")
    public String toBrowseBlog(@PathVariable int id,Model model){


        BlogRecord blogRecord = blogService.queryById(id);
        model.addAttribute("blog",blogRecord);
        return "operation/browseBlog";
    }

    /**
     * 跳转到博客列表页
     * @return
     */
    @RequestMapping("/allBlog")
    public String listAll(){

        return "operation/listBlog";
    }

    /**
     * 列表
     * @param blogAuthor
     * @return
     */
    @RequestMapping("/listBlog/{user.email}")
    public String listBlog(@PathVariable("user.email") String blogAuthor) {
        return "operation/listBlog";
    }

    /**
     * 列出个人所有
     * @param blogAuthor
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/allBlog/{user.email}")
    public ModelAndView allBlog(@PathVariable("user.email") String blogAuthor,
                                @RequestParam(defaultValue = "1") int page,
                                @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<BlogRecord> list = blogService.listAll(blogAuthor,page, size);
        PageInfo<BlogRecord> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos", pageInfos);
        mv.setViewName("operation/listBlog-push");
        return mv;
    }

    /**
     * 跳转到博客草稿箱
     * @param blogAuthor
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/allDraftBlog/{user.email}")
    public ModelAndView toDraftBlog(@PathVariable("user.email") String blogAuthor,
                                    @RequestParam(defaultValue = "1") int page,
                                    @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<BlogRecord> list = blogService.listAllDraft(blogAuthor,page, size);
        PageInfo<BlogRecord> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos", pageInfos);
        mv.setViewName("operation/listBlog-draft");
        return mv;
    }


    //    https://blog.csdn.net/lianghecai52171314/article/details/102930664

    /**
     * 编辑博客
     * @param blogRecord
     * @param model
     * @param request
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping("/editorBlog")
    public JSONObject editorBlog(@ModelAttribute BlogRecord blogRecord, Model model, HttpServletRequest request) throws IOException {


        ModelAndView mv = new ModelAndView();

        //("/")是表示文件上传后的目标文件
        String realPath = request.getSession().getServletContext().getRealPath("/static");
        String contentPath = "/article/htmlFile/";
        String path = realPath+contentPath;

        //创建File对象，传入目标路径参数和文件名称
        File dir = new File(path);

        if (!dir.exists()) {  //如果dir代表的文件不存在，则创建
            dir.mkdirs();
        }

        System.out.println("==============================="+path);

        String suffix = ".html";
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fileName = date.getTime()+suffix;
        String destFile = path+fileName;
        System.out.println("==============================="+destFile);

        //换编码格式保存
        OutputStreamWriter os=new OutputStreamWriter(new FileOutputStream(destFile),"UTF-8");
        os.write(request.getParameter("data"));
        os.close();
        //不转换编码格式保存
//        FileWriter art = new FileWriter(destFile);
//        art.write(request.getParameter("data"));
//        art.flush();
//        art.close();

        blogRecord.setBlogState("0");
        blogRecord.setBlogContent(contentPath+fileName);
        blogRecord.setBlogTime(new Date());
        blogService.addBlog(blogRecord);


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code","1");
        jsonObject.put("msg","成功！");
        return jsonObject;
//        model.addAttribute("success","发布成功，等待审核");
//        mv.addObject("success","发布成功，等待审核");
//        mv.setViewName("operation/editorBlog");
//        return mv;
    }



    /**
     * 修改博客
     * https://blog.csdn.net/qiushisoftware/article/details/96906804/
     * @param blogRecord
     * @param model
     * @param request
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping("/draftBlog")
    public JSONObject draft( @ModelAttribute BlogRecord blogRecord, Model model,
                         HttpServletRequest request) throws IOException {


        //("/")是表示文件上传后的目标文件
        String realPath = request.getSession().getServletContext().getRealPath("/static");
        String contentPath = "/article/mdFile/";
        String path = realPath + contentPath;
        //创建File对象，传入目标路径参数和文件名称
        File dir = new File(path);
        if (!dir.exists()) {  //如果dir代表的文件不存在，则创建
            dir.mkdirs();
        }
        System.out.println("===============================" + path);
        String suffix = ".md";
        Date date = new Date();
        String fileName = date.getTime() + suffix;
        String destFile = path + fileName;
        System.out.println("===============================" + destFile);
        FileWriter art = new FileWriter(destFile);
        art.write(request.getParameter("data"));
        art.flush();
        art.close();

        blogRecord.setBlogState("2");
        blogRecord.setBlogContent(contentPath + fileName);
        blogRecord.setBlogTime(new Date());
        blogService.addBlogDraft(blogRecord);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code","1");
        jsonObject.put("msg","成功！");
        return jsonObject;
//        model.addAttribute("success", "发布成功，等待审核");
//        return "operation/browseBlog";

//        File file = new File(destFile);  //创建文件对象
//        try {
//            if (!file.exists()) {               //如果文件不存在则新建文件
//                file.createNewFile();
//            }
//            FileOutputStream output = new FileOutputStream(file);
//            byte[] bytes = "Java数据交流管道——IO流".getBytes();
//            output.write(bytes);                //将数组的信息写入文件中
//            output.close();
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
    }
    /**
     * 全局查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/queryArticle")
    public ModelAndView query(@RequestParam("search") String name,
                              @RequestParam(defaultValue = "1") int page,
                              @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<BlogRecord> list = blogService.queryByName(name, page, size) ;
        PageInfo<BlogRecord> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfoArticle",pageInfos);
        mv.setViewName("operation/query/query-article");
        return mv;
    }

    /**
     * 主页三
     * @return
     */
    @RequestMapping("/home3")
    public String toIndex2(Model model){
        List<BlogRecord> list = blogService.homeQuery();
        model.addAttribute("article",list);
        return "user/index2";
    }

    /**
     * 查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/queryMyselfArticle/{user}")
    public ModelAndView queryList(@RequestParam("search") String name,@PathVariable String user,
                                  @RequestParam(defaultValue = "1") int page,
                                  @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<BlogRecord> list = blogService.queryByMyself(name, user, page, size) ;
        PageInfo<BlogRecord> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos",pageInfos);
        mv.addObject("searchBlog",name);
        mv.setViewName("operation/listBlog-push");
        return mv;
    }


    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */

    @RequestMapping("/admin/listBlog")
    public ModelAndView adminListBlog(@ModelAttribute BlogRecord blogRecord,HttpServletRequest request,
                                      @RequestParam(defaultValue = "1") int page,
                                      @RequestParam(defaultValue = "10") int size){

        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
//        List<BlogRecord> list = blogService.queryByName(name, page, size) ;
//        PageInfo<BlogRecord> pageInfos = new PageInfo<>(list);
//        mv.addObject("pageInfos",pageInfos);
        return mv;

    }

}
