package cloud.xhmy.interceptor;

import cloud.xhmy.pojo.Admins;
import cloud.xhmy.pojo.Users;
import cloud.xhmy.service.AdminsService;
import cloud.xhmy.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * @author aliketh.xhmy
 * 权限管理
 */
public class PermissionsInterceptor implements HandlerInterceptor {
    @Autowired
    @Qualifier("adminsServiceImpl")
    private AdminsService adminsService;
    @Autowired
    @Qualifier("usersServiceImpl")
    private UsersService usersService;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String uri = request.getRequestURI();
        String email = (String) request.getSession().getAttribute("avatarEmail");
        Admins admins = adminsService.queryByEmail(email);
        Users users = usersService.queryByEmail(email);
        if (request.getSession().getAttribute("user") == null){
            return true;
        }
        if (admins == null ){
            if ("0".equals(users.getPermissions())) {
                return true;
            } else {
                response.sendRedirect(request.getContextPath() + "/x401");
            }
        }else {
            if ("0".equals(admins.getPermissions())) {
                return true;
            } else {
                response.sendRedirect(request.getContextPath() + "/x401");
            }
        }



        return true;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
