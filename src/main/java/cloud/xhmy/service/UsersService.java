package cloud.xhmy.service;

import cloud.xhmy.pojo.Users;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;
import java.util.List;

/**
 * @author aliketh.xhmy
 */
public interface UsersService {

    Users login(Users user);
    Integer register(Users user);
    int forgot(Users user);

    int logout(Users user);
    int modify(Users user);
    List<Users> judge(String email);

    int avatar(Users user);
    int pro(Users pro);

    int modifyTel(BigInteger tel,String uid);
    int modifyPwd(String pwd, String uid);
    int realUser(String realName,String idNumber,String user);
    Users queryByEmail(String users);

    Users refresh (Users user);

    /**
     * 管理员
     * ----------------------------------------------------------------------------------------------------------
     * ----------------------------------------------------------------------------------------------------------
     * ----------------------------------------------------------------------------------------------------------
     * ----------------------------------------------------------------------------------------------------------
     */

    /**
     * 列出所有用户
     * @param page
     * @param size
     * @return
     */
    List<Users> listAllUser(int page,int size);

    /**
     * 修改联系方式
     * @param users
     * @return
     */
    int adminModifyTel(Users users);

    /**
     * 修改密码
     * @param users
     * @return
     */
    int adminModifyPwd(Users users);

    /**
     * 实名认证
     * @param users
     * @return
     */
    int adminRealUser(Users users);

}
