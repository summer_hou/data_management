<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/4
  Time: 14:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>listBorrow</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/page-helper.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div class="" style="width: 95%;margin: 5px auto">
    <div class="row">
        <div class="col-md-12 " style="overflow-x: scroll;">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
<%--                    <th>默认--%>
<%--                        <select style="background: unset;border: unset;" id="class-borrow">--%>
<%--                            <option selected value="0">所有</option>--%>
<%--                            <option value="1">在阅</option>--%>
<%--                            <option value="2">归还</option>--%>
<%--                            <option value="3">处罚</option>--%>
<%--                        </select>--%>
<%--                    </th>--%>
                    <th>借阅书名</th>
                    <th>借阅用户</th>
                    <th>借阅数量</th>
                    <th>借阅天数</th>
                    <th>取书地址</th>
                    <th>缴付押金</th>
                    <th>是否处罚</th>
                    <th>处罚内容</th>
                    <th>借阅时间</th>
                    <th>是否归还</th>
                    <th>归还时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="borrow">
                <c:forEach var="borrow" items="${pageInfos.list}">
                    <tr>
<%--                        <td><input type="checkbox" name="Check[]" value="${borrow.id}" id="Check[]"/></td>--%>
                        <td>${borrow.borrowName}</td>
                        <td>${borrow.borrowUser}</td>
                        <td>${borrow.borrowNumber}</td>
                        <td>${borrow.borrowDay}</td>
                        <td>${borrow.takeLocation}</td>
                        <td>${borrow.deposit}</td>
                        <td>${borrow.tfDefault}</td>
                        <td>${borrow.punish}</td>
                        <td>${borrow.borrowTime}</td>
                        <td>${borrow.tfReturn}</td>
                        <td>${borrow.returnTime}</td>

                        <td class="text-center">
                            <a href="javascript:backBorrow(${borrow.id})"
                               class="btn bg-olive btn-xs" id="backBorrow">归还</a>
                        </td>
                    </tr>
                </c:forEach>

                </tbody>
                <tfoot>
                <span><em><b>注意【1】： 是否处罚以数字代表【0】表示未处罚，【1】表示处罚，处罚内容中将填写处罚的内容</b></em></span><br>
                <span><em><b>注意【2】： 是否归还以数字代表【0】表示在阅读在阅读状态，【1】表示已归还（归还分情况），【2】表示未归还</b></em></span>

                </tfoot>

            </table>

        </div>
    </div>
    <div class="xhmy_page_div"></div>
    <script type="text/javascript">
        //翻页
        $(".xhmy_page_div").createPage({
            pageNum: ${pageInfos.pages},
            current: 1,
            backfun: function(e) {
                //console.log(e);//回调
            }
        });
        function pageNumber(params) {
            $("#borrow").load("${pageContext.request.contextPath}/listBorrow/${user.email} #borrow >*",{page:params,size:"10"});
        };
    </script>
</div>
<script type="text/javascript">
    <%--var opt = $("#class-borrow").val();--%>
    <%--$.ajax({--%>
    <%--    type:'post',--%>
    <%--    url:'${pageContext.request.contextPath}/borrowClass',--%>
    <%--    data:{--%>
    <%--        'option':opt,--%>
    <%--        'user':${user.email}--%>
    <%--    },--%>
    <%--    success:function (data) {--%>
    <%--        $("#browse").load("${pageContext.request.contextPath}/borrowClass #borrow >*");--%>

    <%--    }--%>
    <%--})--%>
    // {
    //     var x=document.getElementById("mySelect");
    //     var y="";
    //     for (i=0;i<x.length;i++)
    //     {
    //         y+=x.options[i].text;
    //         y+="<br />";
    //     }
    //     document.write(y);
    // }
    function backBorrow(num){
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/backBorrow',
            data:{
                'id':num
            },
            dataType:'json',
            success:function (data) {
                if (data.code == "1"){
                    alert("状态："+"提交成功！")
                }else {
                    alert("失败：" + "重复提交！")
                }
            }
        })
    };
<%--$("#backBorrow").click(function () {--%>

<%--    $.ajax({--%>
<%--        type:'post',--%>
<%--        url:'${pageContext.request.contextPath}/backBorrow',--%>
<%--        data:{--%>
<%--            'id':$("#backBorrow-id").val()--%>
<%--        },--%>
<%--        dataType:'json',--%>
<%--        success:function (data) {--%>
<%--            if (data.code == "1"){--%>
<%--                alert("状态："+"提交成功！")--%>
<%--            }--%>
<%--        }--%>
<%--    })--%>
<%--})--%>
</script>




<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>
</body>
</html>
