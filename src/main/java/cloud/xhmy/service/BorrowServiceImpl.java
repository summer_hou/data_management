package cloud.xhmy.service;

import cloud.xhmy.dao.BorrowMapper;
import cloud.xhmy.pojo.Borrow;
import org.apache.poi.sl.draw.binding.CTHyperlink;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public class BorrowServiceImpl implements BorrowService {

    private BorrowMapper borrowMapper;

    public void setBorrowMapper(BorrowMapper borrowMapper) {
        this.borrowMapper = borrowMapper;
    }

    public int addBorrow(Borrow borrow) {
        return this.borrowMapper.addBorrow(borrow);
    }

    public int deleteById(int id) {
        return this.borrowMapper.deleteById(id);
    }

    public Borrow queryById(int id) {
        return this.borrowMapper.queryById(id);
    }

    public List<Borrow> listAll(String user, int page, int size) {
        return this.borrowMapper.listAll(user,page,size);
    }

    public int backBorrow(Borrow borrow) {
        return this.borrowMapper.backBorrow(borrow);
    }

    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/

    public List<Borrow> listAllBorrow(int page, int size) {
        return this.borrowMapper.listAllBorrow(page, size);
    }

    public List<Borrow> listAllTReturn(int page, int size) {
        return this.borrowMapper.listAllTReturn(page, size);
    }

    public List<Borrow> listAllFReturn(int page, int size) {
        return this.borrowMapper.listAllFReturn(page, size);
    }

    public List<Borrow> listAllOn(int page, int size) {
        return this.borrowMapper.listAllOn(page, size);
    }

    public Borrow countNumber() {
        return this.borrowMapper.countNumber();
    }

    public List<Borrow> queryByName(String name, int page, int size) {
        return this.borrowMapper.queryByName(name,page,size);
    }
}
