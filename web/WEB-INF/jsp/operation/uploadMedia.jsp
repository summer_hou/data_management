<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/1
  Time: 22:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>uploadMedia</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>


<div style=" width: 90%; margin: 5px auto;">

    <div class="tabbable" id="tabs-736228">
        <ul class="nav nav-tabs ">
            <li class="active">
                <a href="#panel-364990" data-toggle="tab">音频</a>
            </li>
            <li>
                <a href="#panel-230698" data-toggle="tab">视频</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="panel-364990"  style="width: 100%;m-right: 0px;line-height: 2em;font-size: 16px;">

                <form id="form-audio" enctype="multipart/form-data">
                    <div>
                        <span>文件名：</span><input class="form-control" name="mediaName" type="text"><br>
                        <span></span><input name="audio" type="file">
                    </div>
                    <div>
                        <span>文件类型：</span><input class="form-control" name="mediaType" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>上传者：</span><input class="form-control" name="mediaUpAuthor" value="${user.email}" readonly type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>语言：</span><input class="form-control" name="mediaSource" type="text">
                        <span></span>
                    </div>


                    <div style="margin: 20px auto; text-align: center;">
                        <input id="saveAudio" type="button" class="btn btn-primary" value="上传">
                    </div>
                </form>
                <span>${success}</span>
            </div>
            <div class="tab-pane" id="panel-230698" style="width: 100%;m-right: 0px;line-height: 2em;font-size: 16px;">

                <form id="from-video" enctype="multipart/form-data">
                    <div>
                        <span>文件名：</span><input class="form-control" name="mediaName" type="text"><br>
                        <span></span><input name="video" type="file">
                    </div>
                    <div>
                        <span>文件类型：</span><input class="form-control" name="mediaType" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>上传者：</span><input class="form-control" name="mediaUpAuthor" value="${user.email}" readonly type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>语言：</span><input class="form-control" name="mediaSource" type="text">
                        <span></span>
                    </div>


                    <div style="margin: 20px auto;text-align: center;">
                        <input id="saveVideo" type="button" class="btn btn-primary" value="上传">
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>


<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>


<script type="text/javascript">
    $("#saveAudio").click(function () {
        var formData = new FormData(document.getElementById("form-audio"));
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/upload/audio',

            data:formData,
            processData:false,
            contentType:false,
            dataType:'json',

            success:function (data) {
                alert(data.msg);
            }
        })
    });
    $("#saveVideo").click(function () {
        var formData = new FormData(document.getElementById("form-video"));
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/upload/audio',
            data:formData,
            dataType:'json',

            success:function (data) {
                alert(data.msg);
            }
        })
    });
</script>
</body>
</html>

