<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/25
  Time: 20:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>index2</title>

    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>


</head>
<body>

<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div>
    <div></div>
    <div class="container">
        <div class="row" style="width: 80%;margin: 5px auto;"><h3><span>最新发布文章</span></h3></div>
        <div class="row" style=" width: 80%;margin:0 auto;background: #c7c7c7;">
            <c:forEach var="articles" items="${requestScope.get('article')}">
                <div class="row" style=" width: 80%;margin:5px auto;">
                    <h3><span><a
                            href="${pageContext.request.contextPath}/toBrowseBlog/${articles.id}">${articles.blogName}</a></span>
                    </h3>
                    <span>${articles.blogTitle}</span><br>
                    <span>发布者：${articles.blogAuthor}</span><span style="margin-left: 30px;">${articles.blogTag}</span>
                    <br>
                    <em><span>来源：CLMS (Comprehensive library management system)</span></em><span style="margin-left: 30px;">time：${articles.blogTime}</span>
                </div>
            </c:forEach>
        </div>
    </div>
</div>

<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>
</body>
</html>
