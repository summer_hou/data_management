package cloud.xhmy.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 笔记
 * notes
 * @author aliketh.xhmy
 */
public class Notes {

    private int id;
    private String notesName;
    private String notesContent;
    private String notesAuthor;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date notesTime;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNotesName() {
        return notesName;
    }

    public void setNotesName(String notesName) {
        this.notesName = notesName;
    }

    public String getNotesContent() {
        return notesContent;
    }

    public void setNotesContent(String notesContent) {
        this.notesContent = notesContent;
    }

    public String getNotesAuthor() {
        return notesAuthor;
    }

    public void setNotesAuthor(String notesAuthor) {
        this.notesAuthor = notesAuthor;
    }

    public Date getNotesTime() {
        return notesTime;
    }

    public void setNotesTime(Date notesTime) {
        this.notesTime = notesTime;
    }
}
