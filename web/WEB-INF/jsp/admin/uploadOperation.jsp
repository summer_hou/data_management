<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/22
  Time: 15:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div style="width: 90%; margin: 10px auto;" >

    <div class="tabbable" id="tabs-736228">
        <ul class="nav nav-tabs ">
            <li class="active">
                <a href="#panel-364990" data-toggle="tab">书籍上架</a>
            </li>
            <li>
                <a href="#panel-230698" data-toggle="tab">电子书上传</a>
            </li>
            <li>
                <a href="#panel-230799" data-toggle="tab">媒体文件上传</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="panel-364990"  style="width: 100%;m-right: 0px;line-height: 2em;font-size: 16px;">

                <form id="form-book" enctype="multipart/form-data">
                    <div>
                        <span>封面图片：</span><input id="coverPicture"  name="cp" type="file">
                        <span></span>
                    </div>
                    <div>
                        <span>书名：</span><input class="form-control" id="bookName" name="bookName" value="${bookName}" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>作者：</span><input class="form-control" id="author" name="author" value="${author}" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>出版时间：</span><input class="form-control" id="publishTime" name="publishTime" value="${publishTime}" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>语言：</span><input class="form-control" id="language" name="language" value="${language}" type="text">
                        <span></span>
                    </div>

                    <div>
                        <span>出版社：</span><input class="form-control" id="publishHouse" name="publishHouse" value="${publishHouse}" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>上架者：</span><input class="form-control" id="shelves" name="shelves" value="${user.email}" readonly type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>简介：</span><input class="form-control" id="introduction" name="introduction" value="${introduction}" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>价格：</span><input class="form-control" id="price" name="price" value="${price}" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>分类：</span><input class="form-control" id="classify" name="classify" value="${classify}" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>分类：</span><input class="form-control" id="bookNumber" name="bookNumber" value="${bookNumber}" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>分类：</span><input class="form-control" id="bookLocation" name="bookLocation" value="${bookLocation}" type="text">
                        <span></span>
                    </div>

                    <div style="text-align: center; margin: 20px auto;">
                        <input id="saveBook" type="button" class="btn btn-primary" value="上传">
                    </div>
                </form>
            </div>
            <div class="tab-pane" id="panel-230698" style="width: 100%;m-right: 0px;line-height: 2em;font-size: 16px;">

                <form id="from-eBook"  enctype="multipart/form-data">
                    <div>
                        <span>封面图片：</span><input name="files" type="file">
                        <span></span>
                    </div>
                    <div>
                        <span>书名：</span><input class="form-control" name="bookName" type="text"><br>
                        <span></span><input name="files" type="file">
                    </div>
                    <div>
                        <span>作者：</span><input class="form-control" name="author" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>出版时间：</span><input class="form-control" name="publishTime" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>语言：</span><input class="form-control" name="language" type="text">
                        <span></span>
                    </div>

                    <div>
                        <span>出版社：</span><input class="form-control" name="publishHouse" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>上架者：</span><input class="form-control" name="shelves" value="${user.email}" readonly type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>简介：</span><input class="form-control" name="introduction" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>简述：</span><input class="form-control" name="description" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>分类：</span><input class="form-control" name="classify" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>上传状态：</span><input class="form-control" name="auditStatus" type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>格式：</span><input class="form-control"  name="format" type="text">
                        <span></span>
                    </div>
                    <div style="text-align: center; margin: 20px auto;">
                        <input id="saveEBook" type="button" class="btn btn-primary" value="上传">
                    </div>
                </form>
            </div>
            <div class="tab-pane" id="panel-230799" style="width: 100%;m-right: 0px;line-height: 2em;font-size: 16px;">

                <form id="from-media"  enctype="multipart/form-data">
                    <div>
                        <span>文件名：</span><input class="form-control" name="mediaName" type="text"><br>
                        <span></span><input name="audio" type="file">
                    </div>
                    <div style="margin-top: 15px;margin-bottom: 15px;">
                        <span style="margin-right: 100px;">文件类型：</span>
                        <input class="" name="mediaType" type="radio" value="audio"><span style="margin-right: 100px;">音频</span>
                        <input class="" name="mediaType" type="radio" value="video"><span style="margin-right: 100px;">视频</span>
                        <input class="" name="mediaType" type="radio" value="compress" checked="checked"><span style="margin-right: 100px">压缩包</span>
                        <span></span>
                    </div>
                    <div>
                        <span>上传者：</span><input class="form-control" name="mediaUpAuthor" value="${user.email}" readonly type="text">
                        <span></span>
                    </div>
                    <div>
                        <span>文件来源：</span><input class="form-control" name="mediaSource" type="text">
                        <span></span>
                    </div>
                    <div style="text-align: center; margin: 20px auto;">
                        <input id="saveMedia" type="button" class="btn btn-primary" value="上传">
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>
<script type="text/javascript">
    $("#saveAudio").click(function () {
        var formData = new FormData(document.getElementById("form-audio"));
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/upload/audio',

            data:formData,
            processData:false,
            contentType:false,
            dataType:'json',

            success:function (data) {
                alert(data);
            }
        })
    });
    $("#saveVideo").click(function () {
        var formData = new FormData(document.getElementById("form-video"));
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/upload/video',
            data:formData,
            dataType:'json',

            success:function (data) {
                alert(data);
            }
        })
    });

    $("#saveBook").click(function () {
        var formData = new FormData(document.getElementById("form-eBook"));
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/upload/book',

            data:formData,
            processData:false,
            contentType:false,
            dataType:'json',

            success:function (data) {
                alert(data);
            }
        })
    });
    $("#saveEBook").click(function () {
        var formData = new FormData(document.getElementById("form-eBook"));
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/upload/eBook',
            data:formData,
            dataType:'json',

            success:function (data) {
                alert(data);
            }
        })
    });
</script>
