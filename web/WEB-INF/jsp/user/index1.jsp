<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/22
  Time: 15:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>home1</title>

    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>
<div style="">

    <div>
<%--        <h2 style="text-align: center;margin: 30px auto;">欢迎[<span style="color: dodgerblue;">${user.email}</span>]</h2>--%>

    </div>

    <div class="container">

        <div class="row" style="width: 80%;margin: 5px auto;"><h3><span>最新上传电子书</span></h3></div>
        <c:forEach var="bro" items="${requestScope.get('eBook')}" >
            <div class="row" style=" width: 80%;margin:0 auto;background: #c7c7c7;">
                <div class="col-md-3" style=" ">
                    <img src="${pageContext.request.contextPath}/static${bro.coverPicture}"
                         alt="抱歉图片意外丢失，要图片显示请重新刷新页面"
                         style=" margin: auto; padding: 10px;width: 160px;height: 240px;"
                         onerror="this.src='${pageContext.request.contextPath}/static/upload/picture/CMLS-NO-Cover.png'">
                </div>
                <div class="col-md-9"  style="line-height: 2em;">
                    <h3><span>书名：<a href="${pageContext.request.contextPath}/browseEBook/${bro.id}">${bro.bookName}</a></span></h3>
                    <span><smell>作者：${bro.author}</smell></span><br>
                    <span>出版社：${bro.publishHouse}</span><br>
                    <span>出版时间：${bro.publishTime}</span><br>
                    <span>语言：${bro.language}</span><br>
                    <span>格式：${bro.format}</span><br>
                    <span>类型：${bro.classify}</span>

                </div>
            </div>
        </c:forEach>




        <%--
        <div class="row">
            <h3 class="homeMs"><span>描述</span></h3>
            <div class="col-md-5" style=" ">
                <img src="${pageContext.request.contextPath}/static${books.coverPicture}" alt="" style=" margin: auto; padding: 10px;">
            </div>
            <div class="col-md-7"  style="width: auto; line-height: 2em;">
                <h3>书名：${books.bookName}</h3>
                <smell>作者：${books.author}</smell><br>
                <p>简介：${books.bookName}</p>
                <span>出版社：${books.publishHouse}</span><br>
                <span>出版时间：${books.publishTime}</span><br>
                <span>语言：${books.language}</span><br>
                <span>定价：${books.price}</span><br>
                <span>类型：${books.classify}</span>

            </div>
        </div>
        <div class="row">
            <h3 class="homeMs"><span>描述</span></h3>
            <div class="col-md-5" style=" ">
                <img src="${pageContext.request.contextPath}/static${books.coverPicture}" alt="" style=" margin: auto; padding: 10px;">
            </div>
            <div class="col-md-7"  style="width: auto; line-height: 2em;">
                <h3>书名：${books.bookName}</h3>
                <smell>作者：${books.author}</smell><br>
                <p>简介：${books.bookName}</p>
                <span>出版社：${books.publishHouse}</span><br>
                <span>出版时间：${books.publishTime}</span><br>
                <span>语言：${books.language}</span><br>
                <span>定价：${books.price}</span><br>
                <span>类型：${books.classify}</span>

            </div>
        </div>
        <div class="row">
            <h3 class="homeMs"><span>描述</span></h3>
            <div class="col-md-5" style=" ">
                <img src="${pageContext.request.contextPath}/static${books.coverPicture}" alt="" style=" margin: auto; padding: 10px;">
            </div>
            <div class="col-md-7"  style="width: auto; line-height: 2em;">
                <h3>书名：${books.bookName}</h3>
                <smell>作者：${books.author}</smell><br>
                <p>简介：${books.bookName}</p>
                <span>出版社：${books.publishHouse}</span><br>
                <span>出版时间：${books.publishTime}</span><br>
                <span>语言：${books.language}</span><br>
                <span>定价：${books.price}</span><br>
                <span>类型：${books.classify}</span>

            </div>
        </div>
        <div class="row">
            <h3 class="homeMs"><span>描述</span></h3>
            <div class="col-md-5" style=" ">
                <img src="${pageContext.request.contextPath}/static${books.coverPicture}" alt="" style=" margin: auto; padding: 10px;">
            </div>
            <div class="col-md-7"  style="width: auto; line-height: 2em;">
                <h3>书名：${books.bookName}</h3>
                <smell>作者：${books.author}</smell><br>
                <p>简介：${books.bookName}</p>
                <span>出版社：${books.publishHouse}</span><br>
                <span>出版时间：${books.publishTime}</span><br>
                <span>语言：${books.language}</span><br>
                <span>定价：${books.price}</span><br>
                <span>类型：${books.classify}</span>

            </div>
        </div>
        <div class="row">
            <h3 class="homeMs"><span>描述</span></h3>
            <div class="col-md-5" style=" ">
                <img src="${pageContext.request.contextPath}/static${books.coverPicture}" alt="" style=" margin: auto; padding: 10px;">
            </div>
            <div class="col-md-7"  style="width: auto; line-height: 2em;">
                <h3>书名：${books.bookName}</h3>
                <smell>作者：${books.author}</smell><br>
                <p>简介：${books.bookName}</p>
                <span>出版社：${books.publishHouse}</span><br>
                <span>出版时间：${books.publishTime}</span><br>
                <span>语言：${books.language}</span><br>
                <span>定价：${books.price}</span><br>
                <span>类型：${books.classify}</span>

            </div>
        </div>
        <div class="row">
            <h3 class="homeMs"><span>描述</span></h3>
            <div class="col-md-5" style=" ">
                <img src="${pageContext.request.contextPath}/static${books.coverPicture}" alt="" style=" margin: auto; padding: 10px;">
            </div>
            <div class="col-md-7"  style="width: auto; line-height: 2em;">
                <h3>书名：${books.bookName}</h3>
                <smell>作者：${books.author}</smell><br>
                <p>简介：${books.bookName}</p>
                <span>出版社：${books.publishHouse}</span><br>
                <span>出版时间：${books.publishTime}</span><br>
                <span>语言：${books.language}</span><br>
                <span>定价：${books.price}</span><br>
                <span>类型：${books.classify}</span>

            </div>
        </div>
        <div class="row">
            <h3 class="homeMs"><span>描述</span></h3>
            <div class="col-md-5" style=" ">
                <img src="${pageContext.request.contextPath}/static${books.coverPicture}" alt="" style=" margin: auto; padding: 10px;">
            </div>
            <div class="col-md-7"  style="width: auto; line-height: 2em;">
                <h3>书名：${books.bookName}</h3>
                <smell>作者：${books.author}</smell><br>
                <p>简介：${books.bookName}</p>
                <span>出版社：${books.publishHouse}</span><br>
                <span>出版时间：${books.publishTime}</span><br>
                <span>语言：${books.language}</span><br>
                <span>定价：${books.price}</span><br>
                <span>类型：${books.classify}</span>

            </div>
        </div>
        --%>
    </div>

</div>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>
</body>
</html>
