package cloud.xhmy.util.upload;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * 文件地址，名称的解析
 * @author aliketh.xhmy
 */
public class FilesPath {

    /**
     * 通用排除路径前的(/)
     * @param input
     * @param delim
     * @return
     */
    public static String[] mySplit(final String input, final String delim) {
        return input.replaceFirst("^" + delim, "").split(delim);
    }

    /**
     * 文件存储静态工具
     * @param path
     * @param request
     * @param file
     * @return
     */
//    public static String setWithSuffixPath(String path,HttpServletRequest request, MultipartFile file){
//
//        Date date = new Date();
//        DateFormat dateFormat = DateFormat.getDateInstance();
//        String time = dateFormat.format(date);
//        // 工作空间的相对地址
//        String rootPath = request.getSession().getServletContext().getRealPath("/");
//        String[] str = time.split("-");
//
//        String realpath = rootPath+path+str[0]+"/"+str[1]+"/"+str[2];
////        String defaultPath = rootPath+"/static/upload/default/"+str[0]+"/"+str[1]+"/"+str[2];
//        File mkdir = new File(realpath);
//        if (!mkdir.exists()){
//            mkdir.mkdirs();
//        }
//        String fileName = file.getOriginalFilename();
//        String suffix = fileName.substring(fileName.lastIndexOf("."));
//        String saveFilename = date.getTime()+suffix;
//        File targetFile = new File(realpath, saveFilename);
//        try {
//            file.transferTo(targetFile);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        String savePath = realpath+date.getTime()+suffix;
//        return savePath;
//    }


    /**
     * 文件相对路径拆解排除文件名
     * @param path
     * @return
     */
    public static String parsingPath( String path){

        String[] str1 = mySplit(path,"/");
        String name = "";
        String newPath = "";
        for (int i=0;str1.length>i;i++){
            if (i==str1.length-1){
                name += str1[i];
            }else {
                newPath += str1[i];
                newPath += "/";
            }
        }
        return newPath;
    }

    /**
     * 获得相对路径下的文件名
     * @param filename
     * @return
     */
    public static String parsingName(String filename){
        String[] str1 = mySplit(filename,"/");
        return str1[str1.length-1];
    }
}
