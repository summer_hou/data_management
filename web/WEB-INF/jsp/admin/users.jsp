<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/20
  Time: 20:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div style="width: 90%;margin: 5px auto;">
    <div class="row">
        <div class="col-md-12" style="overflow-x: scroll;">
            <div style="color: gold;"><h3><span>用户</span></h3></div>
            <div style="margin-top: 10px;">
                <button class="btn btn-danger"
                        style="width: 10%; height: 38px;margin-left: 0px;margin-top: -2px;padding: unset;" type="button"
                        onclick="delAllProduct()">删除
                </button>

                <form action="" style="display: inline;">
                    <input style="width: 80%;height: 40px; padding: 5px; border-radius: 2px; border: 1px solid #666666;margin-left: -4px;" name="search"
                           value="" placeholder="输入要搜索的书名" type="search">
                    <button style="width: 10%; height: 40px;margin-left: -4px;" type="button">搜索</button>
                </form>

            </div>
            <table class="table table-hover table-striped" style="overflow-x: scroll;color: gold;">
                <thead>
                <tr>
                    <th><input type="checkbox" name="CheckAll" id="CheckAll"/></th>
                    <th>邮箱</th>
                    <th>密码</th>
                    <th>电话</th>
                    <th>性别</th>
                    <th>年龄</th>
                    <th>备用联系</th>
                    <th>用户名</th>
                    <th>真实姓名</th>
                    <th>证件号</th>
                    <th>地址</th>
                    <th>头像</th>
                    <th>权限</th>
                    <th>注册时间</th>
                    <th>操作</th>
                </tr>

                </thead>
                <tbody id="users-list">
                <c:forEach var="list" items="${pageInfo.list}">
                    <tr>
                        <td><input type="checkbox" name="Check[]" value="${list.id}" id="Check[]"/></td>
                        <td>${list.email}</td>
                        <td>${list.password}</td>
                        <td>${list.telephone}</td>
                        <td>${list.sex}</td>
                        <td>${list.age}</td>
                        <td>${list.altContact}</td>
                        <td>${list.usName}</td>
                        <td>${list.realName}</td>
                        <td>${list.idNumber}</td>
                        <td>${list.loaction}</td>
                        <td>${list.avatar}</td>
                        <td>${list.permissions}</td>
                        <td>${list.createTime}</td>
                        <td class="text-center">
                            <a href="${pageContext.request.contextPath}/${list.id}" class="btn bg-olive btn-xs">更新</a>
                            ||
                            <a href="${pageContext.request.contextPath}/${list.id}" class="btn bg-olive btn-xs">删除</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <div class="xhmy_page_div"></div>
</div>



<script type="text/javascript">
    <%--
    var p;
    var list;
    var i;
    var t;
    var html;
    //
    $.ajax({
        type: "get",
        url: "${pageContext.request.contextPath}/admin/user",
        dataType: "json",
        success: function (data) {

            list = data.list;
            t = data.total;
            // document.getElementById("blog-total").innerText=t;
            p = data.pages;
            // document.getElementById("blog-pages").innerText=p;
            for (i = 0; list.length > i; i++) {
                html += "<tr>" +
                    "<td><input type=\"checkbox\" name=\"Check[]\" value=\"" + list[i].id + "\" id=\"Check[]\"/></td>" +
                    "<td>" + list[i].email + "</td>" +
                    "<td>" + list[i].password + "</td>" +
                    "<td>" + list[i].telephone + "</td>" +
                    "<td>" + list[i].sex + "</td>" +
                    "<td>" + list[i].age + "</td>" +
                    "<td>" + list[i].altContact + "</td>" +
                    "<td>" + list[i].usName + "</td>" +
                    "<td>" + list[i].realName + "</td>" +
                    "<td>" + list[i].idNumber + "</td>" +
                    "<td>" + list[i].loaction + "</td>" +
                    "<td>" + list[i].avatar + "</td>" +
                    "<td>" + list[i].permissions + "</td>" +
                    "<td>" + list[i].createTime + "</td>" +
                    "<td class=\"text-center\">" +
                    "<a href=\"${pageContext.request.contextPath}/" + list[i].id +
                    "\" class=\"btn bg-olive btn-xs\">更新</a>" +
                    "||" +
                    "<a href=\"${pageContext.request.contextPath}/" + list[i].id +
                    "\" class=\"btn bg-olive btn-xs\">删除</a>" +
                    "</td>" +
                    "</tr>"

            };
            // $("#content-list").empty();
            $("#users-list").empty();
            $("#users-list").html(html);
            $(".xhmy_page_div").createPage({
                pageNum: p,
                current: 1,
                backfun: function (e) {
                }
            });
        }
    });

    --%>
    $(".xhmy_page_div").createPage({
        pageNum: ${pageInfo.pages},
        current: 1,
        backfun: function (e) {
        }
    });
    function pageNumber(params) {
        $("#users-list").load("${pageContext.request.contextPath}/admin/user #users-list >*", {
            page: params,
            size: "10"
        });
    };
</script>

<script type="text/javascript">
    //全选/全不选
    $("#CheckAll").bind("click", function () {
        $("input[name='Check[]']").prop("checked", this.checked);
    });

    //批量删除
    function delAllProduct() {
        if (!confirm("确定要删除吗？")) {
            return;
        }
        var cks = $("input[name='Check[]']:checked");
        var str = "";
//拼接所有的id
        for (var i = 0; i < cks.length; i++) {
            if (cks[i].checked) {
                str += cks[i].value + ",";
            }
        }
//去掉字符串末尾的‘&'
        str = str.substring(0, str.length - 1);
// var a = str.split('&');//分割成数组
// console.log(str);return false;
        $.ajax({
            type: 'post',
            url: "${pageContext.request.contextPath}/delete/user/?id=",
            dataType: "json",
            data: {
                filenames: str
            },
            success: function (data) {
                console.log(data);
                return false;
                if (data['status'] == 1) {
                    alert('删除成功！');
                    location.href = data['url'];
                } else {
                    alert('删除失败！');
                    return false;
                }
            }
        });
    };
</script>