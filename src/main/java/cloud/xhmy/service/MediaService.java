package cloud.xhmy.service;

import cloud.xhmy.pojo.Media;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface MediaService {
    /**
     * 增加
     * @param media
     * @return
     */
    int addMedia(Media media);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<Media> queryByName( String name, int page, int size);

    /**
     * 列出所有
     * @param page
     * @param size
     * @return
     */
    List<Media> listAll( String author,int page, int size);

    /**
     * 修改
     * @param id
     * @return
     */
    Media modifyById(int id);

    /**
     * 用于修改
     * @param id
     * @return
     */
    Media queryById(int id);


    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */

    /**
     * 查询所有
     * @param page
     * @param size
     * @return
     */
    List<Media> listAllMedia(int page, int size);

    /**
     * 通过审核
     * @param id
     * @return
     */
    int push(int id,String state);

    /**
     * 统计
     * @return
     */
    Media countNumber();

    /**
     * 审核列表
     * @param state
     * @param page
     * @param size
     * @return
     */
    List<Media> listAllAudit(String state,int page,int size);

    /**
     *
     * @param state
     * @param page
     * @param size
     * @return
     */
    List<Media> listQueryAllAudit(String state,String name,int page,int size);
}
