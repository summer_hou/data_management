<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/24
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>editor</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>


    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div class="container">

    <div class="row">
        <span><a href="${pageContext.request.contextPath}/listBlog/${user.email}#draft"><button class="btn btn-primary">草稿箱</button></a></span>
        <span><a href="${pageContext.request.contextPath}/listBlog/${user.email}"><button class="btn btn-primary">所有博客</button></a></span>
        <span><a href="#up-blog"><button class="btn btn-primary">本地上传</button></a></span>

    </div>
    <div class="row">

        <div class="col-md-12" style="" >
            <div class="row" style="margin: 30px auto 10px; text-align: center;">
                <h3>在线编辑博客</h3>
                <span><button class="btn btn-primary" id="pushBlog">发布</button></span>
                <span><button class="btn btn-primary" id="saveDraft">保存草稿</button></span>

            </div>
            <form style="line-height: 2em;font-size: 16px;">
                <div> <span>博客名称：</span><input class="form-control" id="blogName" name="blogName" value="${blogName}" type="text"></div>
                <div> <span>博客标题：</span><input class="form-control" id="blogTitle" name="blogTitle" value="${blogTitle}" type="text"></div>
                <div> <span>博客标签：</span><input class="form-control" id="blogTag" name="blogTag" value="${blogTag}" type="text"></div>
                <div> <span>博客主题：</span><input class="form-control" id="blogTheme" name="blogTheme" value="${blogTheme}" type="text"></div>
                <div> <span>博客作者：</span><input class="form-control" id="blogAuthor" name="blogAuthor" value="${user.email}" readonly type="text"></div>

            </form>
        </div>


    </div>

</div>

<div id="editorMd"></div>

<div style="width: 90%;text-align: center;margin: 5px auto;">
    <h3>在线编辑博客内容</h3>
</div>
<div id="article-editor">

    <textarea style="display:none;" class="form-control" id="mdData" name="editormd"></textarea>
    <textarea class="editormd-html-textarea" name="text" id="editormdData"></textarea>

</div>
<div class="row">
    <div style="margin: 20px auto"><span style="color: green">${success}</span></div>
    <div style="margin: 20px auto"><span style="color: red">${error}</span></div>
</div>
<div class="container">
    <div class="row" id="up-blog">
        <div class="col-md-12">
            <div style="margin: 30px auto; text-align: center;"><h3>本地上传</h3></div>
            <form id="upload-blog" enctype="multipart/form-data" style="line-height: 1.5em; font-size: 16px;">
                <section>

                    <div> <span>博客名称：</span><input class="form-control" name="blogName" value="" type="text"></div>
                    <div> <span>博客内容：</span><input type="file" id="file" name="scan" style="display:inline-block"></div>
                    <div> <span>博客标题：</span><input class="form-control" name="blogTitle" value="" type="text"></div>
                    <div> <span>博客标签：</span><input class="form-control" name="blogTag" value="" type="text"></div>
                    <div> <span>博客主题：</span><input class="form-control" name="blogTheme" value="" type="text"></div>
                    <div> <span>博客作者：</span><input class="form-control" name="blogAuthor" value="${user.email}" readonly type="text"></div>
                </section>
                <div class="" style="text-align: center;margin-top: 25px;">
                    <input style="" id="upBlog" type="button" class="btn btn-primary" value="上传" />
                </div>
            </form>
        </div>
    </div>
</div>




<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>

<script src="${pageContext.request.contextPath}/plugins/editor-md/editormd.min.js"></script>
<script type="text/javascript">
    $(function () {
        var editor = editormd("article-editor", {
            width: "90%",
            height: 700,
            path: "${pageContext.request.contextPath}/plugins/editor-md/lib/",
            imageUpload: true,
            imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL: "${pageContext.request.contextPath}/upload/mdImages",
            saveHTMLToTextarea : true
        });
    });
    $("#pushBlog").click(function () {
        var data = $("#editormdData").val();
        //alert(html);
        $.ajax({
            data:{
                'data':data,
                'blogName':$("#blogName").val(),
                'blogTitle':$("#blogTitle").val(),
                'blogTag':$("#blogTag").val(),
                'blogTheme':$("#blogTheme").val(),
                'blogAuthor':$("#blogAuthor").val()
            },
            dataType:'json',
            type:'post',
            url:'${pageContext.request.contextPath}/editorBlog',
            success:function (data) {
                alert(data.msg);
            }
        })
    });
    $("#saveDraft").click(function () {
        var data = $("#mdData").val();
        //alert(html);
        $.ajax({
            data:{
                'data':data,
                'blogName':$("#blogName").val(),
                'blogTitle':$("#blogTitle").val(),
                'blogTag':$("#blogTag").val(),
                'blogTheme':$("#blogTheme").val(),
                'blogAuthor':$("#blogAuthor").val()

            },
            dataType:'json',
            type:'post',
            url:'${pageContext.request.contextPath}/draftBlog',
            success:function (data) {
                alert(data.msg);
            }
        })
    });
    $("#upBlog").click(function () {
        var formData = new FormData(document.getElementById("upload-blog"));
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/upload/blog',

            data:formData,
            processData:false,
            contentType:false,
            dataType:'json',

            success:function (data) {
                alert(data.msg);
            }
        });

    });
    <%--
    var testEditor;

    $(function() {

        $.get('${pageContext.request.contextPath}/static/mdEditor/examples/test.md', function(md){
            testEditor = editormd("editorMd", {
                width: "90%",
                // height: 740,
                path : '${pageContext.request.contextPath}/static/mdEditor/lib/',
                theme : "dark",
                previewTheme : "dark",
                editorTheme : "pastel-on-dark",
                markdown : md,
                codeFold : true,
                //syncScrolling : false,
                saveHTMLToTextarea : true,    // 保存 HTML 到 Textarea
                searchReplace : true,
                //watch : false,                // 关闭实时预览
                htmlDecode : "style,script,iframe|on*",            // 开启 HTML 标签解析，为了安全性，默认不开启
                //toolbar  : false,             //关闭工具栏
                //previewCodeHighlight : false, // 关闭预览 HTML 的代码块高亮，默认开启
                emoji : true,
                taskList : true,
                tocm            : true,         // Using [TOCM]
                tex : true,                   // 开启科学公式TeX语言支持，默认关闭
                flowChart : true,             // 开启流程图支持，默认关闭
                sequenceDiagram : true,       // 开启时序/序列图支持，默认关闭,
                //dialogLockScreen : false,   // 设置弹出层对话框不锁屏，全局通用，默认为true
                //dialogShowMask : false,     // 设置弹出层对话框显示透明遮罩层，全局通用，默认为true
                //dialogDraggable : false,    // 设置弹出层对话框不可拖动，全局通用，默认为true
                //dialogMaskOpacity : 0.4,    // 设置透明遮罩层的透明度，全局通用，默认值为0.1
                //dialogMaskBgColor : "#000", // 设置透明遮罩层的背景颜色，全局通用，默认为#fff
                imageUpload : true,
                imageFormats : ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
                imageUploadURL : ${pageContext.request.contextPath}"/static/upload/image/",
                onload : function() {
                    console.log('onload', this);
                    //this.fullscreen();
                    //this.unwatch();
                    //this.watch().fullscreen();

                    //this.setMarkdown("#PHP");
                    //this.width("100%");
                    //this.height(480);
                    //this.resize("100%", 640);
                }
            });
        });

        $("#goto-line-btn").bind("click", function(){
            testEditor.gotoLine(90);
        });

        $("#show-btn").bind('click', function(){
            testEditor.show();
        });

        $("#hide-btn").bind('click', function(){
            testEditor.hide();
        });

        $("#get-md-btn").bind('click', function(){
            alert(testEditor.getMarkdown());
        });

        $("#get-html-btn").bind('click', function() {
            alert(testEditor.getHTML());
        });

        $("#watch-btn").bind('click', function() {
            testEditor.watch();
        });

        $("#unwatch-btn").bind('click', function() {
            testEditor.unwatch();
        });

        $("#preview-btn").bind('click', function() {
            testEditor.previewing();
        });

        $("#fullscreen-btn").bind('click', function() {
            testEditor.fullscreen();
        });

        $("#show-toolbar-btn").bind('click', function() {
            testEditor.showToolbar();
        });

        $("#close-toolbar-btn").bind('click', function() {
            testEditor.hideToolbar();
        });

        $("#toc-menu-btn").click(function(){
            testEditor.config({
                tocDropdown   : true,
                tocTitle      : "目录 Table of Contents",
            });
        });

        $("#toc-default-btn").click(function() {
            testEditor.config("tocDropdown", false);
        });
    });--%>

</script>
</body>
</html>