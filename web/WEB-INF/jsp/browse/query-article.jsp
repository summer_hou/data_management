<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/12/12
  Time: 23:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container article" style=" width: 90%;margin: 0 auto;background: lightgoldenrodyellow;">
    <c:forEach var="articles" items="${pageInfoArticle.list}">
        <div class="row" style=" width: 80%;margin:5px auto;">
            <h3><span><a
                    href="${pageContext.request.contextPath}/browse/toBrowseBlog/${articles.id}">${articles.blogName}</a></span>
            </h3>
            <span>${articles.blogTitle}</span><br>
            <span>${articles.blogTag}</span><span style="ma-left: 30px;">${articles.blogAuthor}</span><span
                style="ma-left: 30px;">${articles.blogTime}</span>
        </div>
    </c:forEach>
    <div class="xhmy_page_div" data-id="articles"></div>
    <script type="text/javascript">
        //翻页
        $(".xhmy_page_div").createPage({
            pageNum: ${pageInfoArticle.pages},
            current: 1,
            backfun: function (e) {
                //console.log(e);//回调
            }
        });

        function pageNumber(params) {
            $(".article").load("${pageContext.request.contextPath}/browse/queryArticle?search=${search} .article >*", {
                page: params,
                size: "10"
            });

        };

    </script>
</div>