<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/1
  Time: 22:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>listMedia</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/page-helper.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>
<div style="width: 90%;margin: 5px auto">


        <div class="row">
            <div class="col-md-12" id="panel-364990" style="width: 100%;m-right: 0px;line-height: 2em">
                <%--<p>
                    audio
                </p>--%>

                <div class="row"> <div >
                    <h3><span>媒体文件</span></h3>
                </div>
                    <div style="margin-top: 10px;">
                        <%--
                        <button class="btn btn-danger" style="width: 10%; height: 38px;margin-left: 0px;margin-top: -2px;padding: unset;" type="button" onclick="delAllProduct()">删除</button>

                        <form action="" style="display: inline;width: 90%; float: right;">
                            <input style="width: 90%;height: 38px; padding: 5px; border-radius: 2px; border: 1px solid #666666;" name="search"
                                   value="" placeholder="输入要搜索的媒体文件名" type="search">
                            <button style="width: 10%; height: 40px;margin-left: -5px;" type="button">搜索</button>
                        </form>
                            --%>
                    </div>
                    <div class="col-md-12 ">


                        <table class="table table-hover table-striped">
                            <thead>
                            <tr>
                                <th><input type="checkbox" name="CheckAll" id="CheckAll"/></th>
                                <th>文件名</th>
                                <th>文件类型</th>
                                <th>上传者</th>
                                <th>来源</th>
                                <th>保存名称</th>
                                <th>审核状态</th>
                                <th>上传时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody id="media">
                            <c:forEach var="media" items="${pageInfos.list}">
                                <tr>
                                    <td><input type="checkbox" name="Check[]" value="${media.id}" id="Check[]"/></td>

                                    <td>${media.mediaName}</td>
                                    <td>${media.mediaType}</td>
                                    <td>${media.mediaUpAuthor}</td>
                                    <td>${media.mediaSource}</td>
                                    <td>${media.mediaContent}</td>
                                    <td>${media.mediaState}</td>
                                    <td>${media.createTime}</td>
                                    <td class="text-center">
                                        <a href="${pageContext.request.contextPath}/download?download${media.mediaContent}"
                                           class="btn bg-olive btn-xs">下载</a>
                                        ||
                                        <a href="javascript:deleteMedia(${media.id})"
                                           class="btn bg-olive btn-xs">删除</a>

                                    </td>
                                </tr>
                            </c:forEach>

                            </tbody>
                            <tfoot>
                                <span><em><b>注意：审核状态以数字代表【0】表示等待审核，【1】表示通过，【2】表示不通过</b></em></span>

                            </tfoot>

                        </table>

                    </div>
                </div>


                <div class="xhmy_page_div"></div>
                <script type="text/javascript">
                    //翻页
                    $(".xhmy_page_div").createPage({
                        pageNum: ${pageInfos.pages},
                        current: 1,
                        backfun: function(e) {
                            //console.log(e);//回调
                        }
                    });
                    function pageNumber(params) {
                        $("#media").load("${pageContext.request.contextPath}/listMedia/${user.email} #media >*",{page:params,size:"10"});

                    };
                </script>
            </div>
            <div class="tab-pane" id="panel-230698" style="width: 90%;m-right: 0px;line-height: 2em">

            </div>

        </div>

</div>



<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>

<script type="text/javascript">
    function deleteMedia(did) {
        $.ajax({
            type: 'get',
            url: '${pageContext.request.contextPath}/delete/media?id='+did,
            processData:false,
            contentType:false,
            dataType: 'json',
            success:function (data) {
                if (data.code == "1"){
                    alert("成功！");
                }else {
                    alert("失败！");
                }
            }
        })
    };
</script>

<%--
<script type="text/javascript">
    //全选/全不选
    $("#CheckAll").bind("click", function () {
        $("input[name='Check[]']").prop("checked", this.checked);
    });

    //批量删除
    function delAllProduct() {
        if (!confirm("确定要删除吗？")) {
            return;
        }
        var cks = $("input[name='Check[]']:checked");
        var str = "";
        //拼接所有的id
        for (var i = 0; i < cks.length; i++) {
            if (cks[i].checked) {
                str += cks[i].value + ",";
            }
        }
        //去掉字符串末尾的‘&'
        str = str.substring(0, str.length - 1);
        // var a = str.split('&');//分割成数组
        // console.log(str);return false;
        $.ajax({
            type: 'post',
            url: "${pageContext.request.contextPath}/delete",
            dataType: "json",
            data: {
                filenames: str
            },
            success: function (data) {
                console.log(data);
                return false;
                if (data['status'] == 1) {
                    alert('删除成功！');
                    location.href = data['url'];
                } else {
                    alert('删除失败！');
                    return false;
                }
            }
        });
    };
</script>--%>
</body>
</html>