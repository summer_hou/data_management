package cloud.xhmy.service;

import cloud.xhmy.dao.AdminsMapper;
import cloud.xhmy.pojo.Admins;

import java.math.BigInteger;
import java.util.List;
/**
 * @author aliketh.xhmy
 */
public class AdminsServiceImpl implements AdminsService {

    private AdminsMapper adminsMapper;

    public void setAdminsMapper(AdminsMapper adminsMapper) {
        this.adminsMapper = adminsMapper;
    }


    public Admins login(Admins user) {
        return this.adminsMapper.login(user);
    }

    public Integer register(Admins user) {
        return this.adminsMapper.register(user);
    }

    public int forgot(Admins user) {
        return this.adminsMapper.forgot(user);
    }

    public int logout(Admins user) {
        return this.adminsMapper.logout(user);
    }

    public int modify(Admins user) {
        return this.adminsMapper.modify(user);
    }

    public List<Admins> judge(String email) {
        return this.adminsMapper.judge(email);
    }

    public int avatar(Admins user) {
        return this.adminsMapper.avatar(user);
    }

    public int pro(Admins pro) {
        return this.adminsMapper.pro(pro);
    }

    public int modifyTel(Admins admins) {
        return this.adminsMapper.modifyTel(admins);
    }

    public int modifyPwd(Admins admins) {
        return this.adminsMapper.modifyPwd(admins);
    }

    public int realUser(Admins admins) {
        return this.adminsMapper.realUser(admins);
    }

    public Admins queryByEmail(String admins) {
        return this.adminsMapper.queryByEmail(admins);
    }

    public Admins refresh(Admins user) {
        return this.adminsMapper.refresh(user);
    }
}
