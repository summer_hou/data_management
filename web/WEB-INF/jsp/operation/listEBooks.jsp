<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/31
  Time: 23:04
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>listEBooks</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>


    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/page-helper.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div style="width: 95%; margin: 5px auto">
    <div>
        <h3><span>书籍</span></h3>
    </div>
    <div style="margin-top: 10px;">
        <%--
        <button class="btn btn-danger" style="width: 10%; height: 38px;margin-left: 0px;margin-top: -2px;padding: unset;" type="button" onclick="delAllProduct()">删除</button>

        <form action="${pageContext.request.contextPath}/queryMyselfEBook/${user.email}" style="display: inline;width: 90%; float: right;">

            <input style="width: 90%;height: 38px; padding: 5px; border-radius: 2px; border: 1px solid #666666;" name="search"
                   value="${searchEBook}" placeholder="输入要搜索的书名" type="search">
            <button style="width: 10%; height: 40px;margin-left: -5px;" type="button">搜索</button>
        </form>
            --%>
    </div>
    <div class="row">

        <div class="col-md-12 " style="overflow-x: scroll;">

            <table class="table table-hover table-striped" style="overflow-x: scroll;">
                <thead>
                <tr>
                    <th><input type="checkbox" name="CheckAll" id="CheckAll"/></th>
                    <th>书名</th>
                    <th>作者</th>
                    <th>出版时间</th>
                    <th>语言</th>
                    <th>出版社</th>
<%--                    <th>上传者</th>--%>
<%--                    <th>封面图片</th>--%>
                    <th>简介</th>
                    <th>格式</th>
                    <th>分类</th>
                    <th>审核状态</th>
                    <th>描述</th>
                    <th>上传时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="eBook">
                <c:forEach var="eBooks" items="${pageInfos.list}">
                    <tr>
                        <td><input type="checkbox" name="Check[]" value="${eBooks.id}" id="Check[]"/></td>
                        <td>${eBooks.bookName}</td>
                        <td>${eBooks.author}</td>
                        <td>${eBooks.publishTime}</td>
                        <td>${eBooks.language}</td>
                        <td>${eBooks.publishHouse}</td>
<%--                        <td>${eBooks.shelves}</td>--%>
<%--                        <td>${eBooks.coverPicture}</td>--%>
                        <td>${eBooks.introduction}</td>
                        <td>${eBooks.format}</td>
                        <td>${eBooks.classify}</td>
                        <td>${eBooks.auditStatus}</td>
                        <td>${eBooks.description}</td>
                        <td>${eBooks.uploadTime}</td>
                        <td class="text-center">
                            <a href="${pageContext.request.contextPath}/toModifyEBook/${eBooks.id}"
                               class="btn bg-olive btn-xs">更新</a>
                            ||
                            <a id="delete" href="javascript:deleteEBook(${eBooks.id})"
                               class="btn bg-olive btn-xs">删除</a>

                        </td>
                    </tr>
                </c:forEach>

                </tbody>
                <tfoot>
                <span><em><b>注意：审核状态已数字代表【0】表示等待审核，【1】表示通过，【2】表示不通过</b></em></span>

                </tfoot>

            </table>

        </div>
    </div>
    <div class="xhmy_page_div"></div>
    <script type="text/javascript">
        //翻页
        $(".xhmy_page_div").createPage({
            pageNum: ${pageInfos.pages},
            current: 1,
            backfun: function(e) {
                //console.log(e);//回调
            }
        });
        function pageNumber(params) {
            $("#eBook").load("${pageContext.request.contextPath}/allEBooks/${user.email} #eBook >*",{page:params,size:"10"});

        };
    </script>
</div>


<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>

<script type="text/javascript">
    function deleteEBook(did) {
        $.ajax({
            type: 'get',
            url: '${pageContext.request.contextPath}/delete/eBook?id='+did,
            processData:false,
            contentType:false,
            dataType: 'json',
            success:function (data) {
                if (data.code == "1"){
                    alert("成功！");
                }else {
                    alert("失败！");
                }
            }
        })
    };
</script>
<script type="text/javascript">
    //全选/全不选
    $("#CheckAll").bind("click", function () {
        $("input[name='Check[]']").prop("checked", this.checked);
    });

    //批量删除
    function delAllProduct() {
        if (!confirm("确定要删除吗？")) {
            return;
        }
        var cks = $("input[name='Check[]']:checked");
        var str = "";
        //拼接所有的id
        for (var i = 0; i < cks.length; i++) {
            if (cks[i].checked) {
                str += cks[i].value + ",";
            }
        }
        //去掉字符串末尾的‘&'
        str = str.substring(0, str.length - 1);
        // var a = str.split('&');//分割成数组
        // console.log(str);return false;
        $.ajax({
            type: 'post',
            url: "${pageContext.request.contextPath}/eBook/{id}",
            dataType: "json",
            data: {
                filenames: str
            },
            success: function (data) {
                console.log(data);
                return false;
                if (data['status'] == 1) {
                    alert('删除成功！');
                    location.href = data['url'];
                } else {
                    alert('删除失败！');
                    return false;
                }
            }
        });
    };
</script>
</body>
</html>