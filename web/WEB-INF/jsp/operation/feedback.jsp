<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/5
  Time: 15:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>feedback</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>

</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-md-6" style="padding: 20px;">
            <div style="color: #ffff7f;line-height: 2em;margin-left: 20px;">
                <h3 style="margin: 15px auto;"><span>感谢各位用户的积极反馈</span></h3>
                <sapn style="margin: 10px auto 10px">我们目的是致力于完善系统，为用户提供更优质的用户体验，欢迎各位踊跃提供意见和见解</sapn>
                <ul>反馈说明</ul>
                <ol>
                    <li>反馈时可以尽量，以简洁明了的语句描述要点</li>
                    <li>反馈的内容不限（仅限本系统）</li>
                    <li>反馈用于增加新功能时这注明这是本系统不存在的功能</li>
                    <li>查看更多请点击，<a href="${pageContext.request.contextPath}/about" style="color: #0a001f">查看</a></li>
                    <%--                    <li></li>--%>
                </ol>
            </div>
            <img src="" alt="">
        </div>
        <div class="col-md-6">
            <h3 style="margin: 10px auto;text-align: center;"><span>问题反馈</span></h3>
            <form id="feedback" action="" style="line-height: 2em;font-size: 16px;">
                <div><span>反馈用户：</span><input name="feedbackUser" class="form-control" type="text" value="${user.email}"></div>
                <div><span>反馈内容：</span><br>
                    <textarea name="feedbackContent" class="form-control" rows="14"></textarea></div>
                <div style="margin-top: 20px;"><input id="save" type="button" class="btn btn-primary" value="提交"></div>
            </form>
        </div>


    </div>
</div>



<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>


<script type="text/javascript">
    $("#save").click(function () {
        var formData = new FormData(document.getElementById("feedback"));
        //alert(html);
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/pushFeedback',
            data:formData,
            processData: false,
            contentType: false,
            dataType:'json',


            success:function (data) {
                alert(data.msg);
            }
        })
    });
</script>
</body>
</html>
