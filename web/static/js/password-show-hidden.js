$(function() {
    $("input[type='password'][data-eye]").each(function(i) {
        let $this = $(this);
        /*在div下添加位置样式 【$("<div/>"】*/
        $this.wrap($("<div/>", {
            style: 'position:relative'
        }));
        /*设置input的padding样式*/
        $this.css({
            paddingRight: 60
        });
        /*添加div标签并设置 【$("<div/>"】*/
        $this.after($("<div/>", {
            html: 'Show',
            class: 'btn btn-primary btn-sm',
            id: 'passeye-toggle-'+i,
            style: 'position:absolute;right:10px;top:50%;transform:translate(0,-50%);padding: 2px 7px;font-size:12px;cursor:pointer;'
        }));
        /*添加隐藏 input 用于接收值存入value=""*/
        $this.after($("<input/>", {
            type: 'hidden',
            id: 'passeye-' + i
        }));
        /*接收键盘输入的值用于存入value=""*/
        $this.on("keyup paste", function() {
            $("#passeye-"+i).val($(this).val());
        });
        /*点击时间事件*/
        $("#passeye-toggle-"+i).on("click", function() {
            if($this.hasClass("show")) {
                $this.attr('type', 'password');
                $this.removeClass("show");
                $(this).removeClass("btn-outline-primary");
            }else{
                $this.attr('type', 'text');
                $this.val($("#passeye-"+i).val());
                $this.addClass("show");
                $(this).addClass("btn-outline-primary");
            }
        });
    });

});