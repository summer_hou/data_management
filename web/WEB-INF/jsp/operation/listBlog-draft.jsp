<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/25
  Time: 2:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<div class="row">
    <div class="col-md-12">
        <div><h3><span>博客草稿箱</span></h3></div>
        <div style="margin-top: 10px;">
            <%--
            <button class="btn btn-danger"
                    style="width: 10%; height: 38px;margin-left: 0px;margin-top: -2px;padding: unset;"
                    type="button" onclick="delAllProduct()">删除
            </button>

            <form  id="search-draft-blog" style="display: inline;">
                <input style="width: 80%;height: 40px; padding: 5px; border-radius: 2px; border: 1px solid #666666;margin-left: -4px;"
                       name="search"
                       value="" placeholder="输入要搜索的书名" type="search">
                <button style="width: 10%; height: 40px;margin-left: -4px;" type="button">搜索</button>
            </form>
                --%>
        </div>
        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th><input type="checkbox" name="CheckAll" id="CheckAll"/></th>
                <th>博客名称</th>
                <th>博客标题</th>
                <th>博客标签</th>
                <th>博客状态</th>
                <th>博客主题</th>
                <th>博客内容</th>
                <th>发布时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody id="draft">

            <c:forEach var="blog" items="${pageInfos.list}">

                <tr>
                    <td><input type="checkbox" name="Check[]" value="${blog.id}" id="Check[]"/></td>

                    <td>${blog.blogName}</td>
                    <td>${blog.blogTitle}</td>
                    <td>${blog.blogTag}</td>
                    <td>${blog.blogState}</td>
                    <td>${blog.blogTheme}</td>
                    <td>${blog.blogContent}</td>
                    <td>${blog.blogTime}</td>
                    <td class="text-center">
                        <a href="${pageContext.request.contextPath}/toBrowseBlog/${blog.id}"
                           class="btn bg-olive btn-xs">预览</a>
                        ||
                        <a href="${pageContext.request.contextPath}/toModifyBlog/${blog.id}"
                           class="btn bg-olive btn-xs">更新</a>
                        ||
                        <a href="${pageContext.request.contextPath}/delete/blogDraft?id=${blog.id}"
                           class="btn bg-olive btn-xs">删除</a>

                    </td>
                </tr>
            </c:forEach>

            </tbody>

        </table>

        <div class="xhmy_page_div"></div>
        <script type="text/javascript">

            $(".xhmy_page_div").createPage({
                pageNum: ${pageInfos.pages},
                current: 1,
                backfun: function (e) {
                    //console.log(e);//回调
                }
            });

            function pageNumber(params) {
                $("#draft").load("${pageContext.request.contextPath}/allDraftBlog/${user.email} #draft >*", {
                    page: params,
                    size: "10"
                });
            };
        </script>

    </div>

</div>


