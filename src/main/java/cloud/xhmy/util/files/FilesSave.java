package cloud.xhmy.util.files;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 自定义文件保存格式
 * @author aliketh.xhmy
 */
public class FilesSave {
    /**
     * 文件存储静态工具
     * (日期年月日目录加时间戳名称命名文件)
     * @param path 格式化（/xx/xx/）
     * @param request
     * @return
     */
    public static String setWithDateFormatPath(String path, HttpServletRequest request, String suffix,String content) throws IOException {

        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateInstance();
        String time = dateFormat.format(date);
        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String[] str = time.split("-");

        String datePath = str[0]+"/"+str[1]+"/"+str[2]+"/";
        String realpath = rootPath+path+datePath;
        File mkdir = new File(realpath);
        if (!mkdir.exists()){
            mkdir.mkdirs();
        }
        String saveFilename = date.getTime()+suffix;
        File destFile = new File(realpath, saveFilename);
        //换编码格式保存
        OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(destFile), "UTF-8");
        os.write(content);
        os.close();
        //不转换编码格式保存
//        FileWriter art = new FileWriter(destFile);
//        art.write(request.getParameter("data"));
//        art.flush();
//        art.close();
        String savePath = path+datePath+date.getTime()+suffix;
        return savePath;
    }

    /**
     * 文件存储静态工具
     * (日期年月日目录加时间戳名称命名文件)
     * @param path 格式化（/xx/xx/）
     * @param request
     * @return
     */
    public static String setWithDatePath(String path, HttpServletRequest request, String suffix,String content) throws IOException {

        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateInstance();
        String time = dateFormat.format(date);
        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String[] str = time.split("-");
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        String datePath = str[0]+"/"+str[1]+"/"+str[2]+"/";
        String realpath = rootPath+path+datePath;
        File mkdir = new File(realpath);
        if (!mkdir.exists()){
            mkdir.mkdirs();
        }
        String saveFilename = uuid+suffix;
        File destFile = new File(realpath, saveFilename);
        //换编码格式保存
        OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(destFile), "UTF-8");
        os.write(content);
        os.close();
        //不转换编码格式保存
//        FileWriter art = new FileWriter(destFile);
//        art.write(request.getParameter("data"));
//        art.flush();
//        art.close();
        String savePath = path+datePath+uuid+suffix;
        return savePath;
    }

    /**
     * 文件存储静态工具
     * (uuid名称命名文件)
     * @param path 格式化（/xx/xx/）
     * @param request
     * @param suffix
     * @return
     */
    public static String setWithUuidPath(String path, HttpServletRequest request, String suffix,String content) throws IOException {


        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        String realpath = rootPath+path;
        File mkdir = new File(realpath);
        if (!mkdir.exists()){
            mkdir.mkdirs();
        }
        String saveFilename = uuid+suffix;
        File destFile = new File(realpath, saveFilename);
        //换编码格式保存
        OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(destFile), "UTF-8");
        os.write(content);
        os.close();
        //不转换编码格式保存
//        FileWriter art = new FileWriter(destFile);
//        art.write(request.getParameter("data"));
//        art.flush();
//        art.close();
        String savePath = path+uuid+suffix;
        return savePath;
    }

    /**
     * 默认存储存储
     * 文件存储静态工具
     * (日期年月日目录加时间戳名称命名文件)
     * @param request
     * @param suffix
     * @return （/xx/xx/xx.xx）
     */
    public static String setWithDefaultPath(HttpServletRequest request, String suffix ,String content) throws IOException {

        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateInstance();
        String time = dateFormat.format(date);
        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String[] str = time.split("-");

        String path = "/upload/default/"+str[0]+"/"+str[1]+"/"+str[2]+"/";
        String defaultPath = rootPath+path;
        File mkdir = new File(defaultPath);
        if (!mkdir.exists()){
            mkdir.mkdirs();
        }
        String saveFilename = date.getTime()+suffix;
        File destFile = new File(defaultPath, saveFilename);
        //换编码格式保存
        OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(destFile), "UTF-8");
        os.write(content);
        os.close();
        //不转换编码格式保存
//        FileWriter art = new FileWriter(destFile);
//        art.write(request.getParameter("data"));
//        art.flush();
//        art.close();
        String savePath = path+date.getTime()+suffix;
        return savePath;
    }
}
