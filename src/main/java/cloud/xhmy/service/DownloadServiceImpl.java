package cloud.xhmy.service;

import cloud.xhmy.dao.DownloadMapper;
import cloud.xhmy.pojo.DownloadRecord;

import java.util.List;

public class DownloadServiceImpl implements DownloadService {

    private DownloadMapper downloadMapper;

    public void setDownloadMapper(DownloadMapper downloadMapper) {
        this.downloadMapper = downloadMapper;
    }

    public int addDownloadRecord(DownloadRecord downloadRecord) {
        return this.downloadMapper.addDownloadRecord(downloadRecord);
    }

    public int deleteById(int id) {
        return this.downloadMapper.deleteById(id);
    }

    public List<DownloadRecord> queryByName(String name, int page, int size) {
        return this.downloadMapper.queryByName(name, page, size);
    }

    public List<DownloadRecord> listAll(int page, int size) {
        return this.downloadMapper.listAll(page,size);
    }

    public int modifyById(int id) {
        return this.downloadMapper.modifyById(id);
    }

    public DownloadRecord queryById(int id) {
        return this.downloadMapper.queryById(id);
    }
}
