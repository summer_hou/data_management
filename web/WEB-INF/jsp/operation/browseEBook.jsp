<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/1
  Time: 22:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>showBook</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div style=" width: 90%;margin: auto;">
    <div class="container">
        <div class="row" style=" width: 100%;margin: auto;">

            <div class="col-md-3" style=" ">
                <img src="${pageContext.request.contextPath}/static${books.coverPicture}" alt=""
                     style=" margin:0 auto;width: 240px;height: 320px;"
                     onerror="this.src='${pageContext.request.contextPath}/static/upload/picture/CMLS-NO-Cover.png'">
            </div>
            <div class="col-md-9" id="" style="line-height: 2em;">
                <span><h3>书名：${books.bookName}</h3></span>
                <span><smell>作者：${books.author}</smell></span><br>
                <span><p>简介：${books.introduction}</p></span><br>
                <span>出版社：${books.publishHouse}</span><br>
                <span>出版时间：${books.publishTime}</span><br>
                <span>语言：${books.language}</span><br>
                <span>类型：${books.classify}</span>

            </div>
        </div>
        <div class="row" style="width: 90%;margin: auto;">
            <div style="margin: 20px auto;text-align: center;">
                <span><a class="btn btn-primary" href="${pageContext.request.contextPath}/download?download=${books.uuid}">下载</a></span>
<%--                <span><a class="btn btn-primary" href="">加入收藏</a></span>--%>
            </div>
        </div>

    </div>
</div>



<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>

</body>
</html>
