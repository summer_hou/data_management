package cloud.xhmy.dao;

import cloud.xhmy.pojo.Books;
import org.apache.ibatis.annotations.Param;

import java.awt.*;
import java.util.List;

/**
 * @author aliketh.xhmy
 */
public interface BooksMapper {

    /**
     * 增加书籍
     *
     * @param book
     * @return
     */
    int addBook(Books book);

    /**
     * 删除书籍
     *
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 模糊查询书籍
     *
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<Books> queryByName(@Param("bookName") String name, int page, int size);

    /**
     * 列出所有书籍
     *
     * @param page
     * @param size
     * @return
     */
    List<Books> listAll(@Param("shelves") String shelves, int page, int size);

    /**
     * 修改书籍
     *
     * @param id
     * @return
     */
    int modifyById(Books id);

    /**
     * 用于修改
     *
     * @param id
     * @return
     */
    Books queryById(int id);

    /**
     * 单独查询
     *
     * @param name
     * @return
     */
    Books query(String name);

    /**
     * 主页显示
     */
    List<Books> homeQuery();

    /**
     * 修改书籍数量
     *
     * @return
     */
    int updateNumber(@Param("id") int id,@Param("bookNumber") int bookNumber);

    /**
     * 列出所有
     * @param page
     * @param size
     * @return
     */
    List<Books> listAllBooks(int page,int size);

    /**
     * 语言查询
     * @param language
     * @param page
     * @param size
     * @return
     */
    List<Books> queryLanguage(@Param("language")String language,int page,int size);


    /**
     * 分类查询
     * @param classify
     * @param page
     * @param size
     * @return
     */
    List<Books> queryClassify(@Param("classify") String classify,int page,int size);

    /**
     * 组合查询
     *
     * @param books
     * @return
     */
    List<Books> queryGroup(Books books);
}
