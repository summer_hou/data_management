package cloud.xhmy.service;

import cloud.xhmy.dao.BlogMapper;
import cloud.xhmy.pojo.BlogRecord;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public class BlogServiceImpl implements BlogService {

    private BlogMapper blogMapper;

    public void setBlogMapper(BlogMapper blogMapper) {
        this.blogMapper = blogMapper;
    }

    public int addBlog(BlogRecord blog) {
        return this.blogMapper.addBlog(blog);
    }

    public int deleteById(int id) {
        return this.blogMapper.deleteById(id);
    }

    public List<BlogRecord> queryByName(String name, int page, int size) {
        return this.blogMapper.queryByName(name, page, size);
    }

    public List<BlogRecord> queryByMyself(String name, String user, int page, int size) {
        return this.blogMapper.queryByMyself(name, user, page, size);
    }

    public List<BlogRecord> listAll(String author,int page, int size) {
        return this.blogMapper.listAll(author,page,size);
    }

    public BlogRecord modifyById(int id) {
        return this.blogMapper.modifyById(id);
    }

    public BlogRecord queryById(int id) {
        return this.blogMapper.queryById(id);
    }

    @Override
    public List<BlogRecord> homeQuery() {
        return this.blogMapper.homeQuery();
    }

    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/


    public int addBlogDraft(BlogRecord blog) {
        return this.blogMapper.addBlogDraft(blog);
    }

    public int deleteDraftById(int id) {
        return this.blogMapper.deleteDraftById(id);
    }

    public List<BlogRecord> queryDraftByName(String name, int page, int size) {
        return this.blogMapper.queryDraftByName(name, page, size);
    }

    public List<BlogRecord> listAllDraft(String author,int page, int size) {
        return this.blogMapper.listAllDraft(author,page,size);
    }

    public BlogRecord modifyDraftById(int id) {
        return this.blogMapper.modifyDraftById(id);
    }

    public BlogRecord queryDraftById(int id) {
        return this.blogMapper.queryDraftById(id);
    }

    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/

    public List<BlogRecord> listAllBlog(int page, int size) {
        return this.blogMapper.listAllBlog(page, size);
    }

    public List<BlogRecord> listAllAudit(String state, int page, int size) {
        return this.blogMapper.listAllAudit(state, page, size);
    }

    public int push(int id,String state) {
        return this.blogMapper.push(id,state);
    }

    public BlogRecord countNumber() {
        return this.blogMapper.countNumber();
    }

    public List<BlogRecord> listAuditArticle(String state, int page, int size) {
        return this.blogMapper.listAuditArticle(state, page, size);
    }

    public List<BlogRecord> listQueryAuditArticle(String state, String name, int page, int size) {
        return this.blogMapper.listQueryAuditArticle(state, name, page, size);
    }
}
