<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/26
  Time: 19:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<div class="img-back" style="">

</div>
<!-- 作者：袁金 -->
<!-- 请不要用于非法行为 -->
<div id="wrapper" style="left:0;background: rgba(6, 251, 255, 0.5);">
    <!-- 侧边栏 -->
    <div class="sidebar" style="background: rgba(6, 251, 255, 0.5);">
        <div class="headSculpture">
            <a href="" data-toggle="modal" data-target="#exampleModal" data-whatever="upAvatar">
                <img src="${pageContext.request.contextPath}/static${user.avatar}" alt=""
                     onerror="this.src='${pageContext.request.contextPath}/static/images/logo.png'"></a>
            <p style="color: #000000;background-color: #63d1cb;">邮箱：<a class="na" href="" onerror="">${user.email}</a></p>
        </div>


        <div class="option">
            <ul>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-home"></span>
							</span>
                    <!-- <p style=""><a class="na" href="home.html"></a></p> -->
                    <div class="panel-group" id="home" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingHome"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseHome" aria-expanded="true"
                                       aria-controls="collapseHome" class=" na ">
                                        首页
                                    </a>
                                </p>
                            </div>
                            <div id="collapseHome" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingHome" aria-expanded="true">
                                <div class="panel-body">

                                    <span><a class="naCh" href="${pageContext.request.contextPath}/home1">图书</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/home2">电子书</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/home3">博客</a></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-pencil"></span>
							</span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="record" role="tablist" aria-multiselectable="false">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingRecord"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseRecord" aria-expanded="flase"
                                       aria-controls="collapseRecord" class="collapsed na">
                                        个人笔记
                                    </a>
                                </p>
                            </div>
                            <div id="collapseRecord" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingRecord" aria-expanded="flase">
                                <div class="panel-body">

                                    <span><a class="naCh"
                                             href="${pageContext.request.contextPath}/toNotes">添加笔记</a></span><br>
                                    <span><a class="naCh"
                                             href="${pageContext.request.contextPath}/toEditorBlog">添加博客</a></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-upload"></span>
							</span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingUpload"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseUpload" aria-expanded="flase"
                                       aria-controls="collapseUpload" class="collapsed na">
                                        我的上传
                                    </a>
                                </p>
                            </div>
                            <div id="collapseUpload" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingUpload" aria-expanded="flase">
                                <div class="panel-body">

                                    <%--                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">图片</a></span><br>--%>
                                    <%--                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">文档</a></span><br>--%>
                                    <%--                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">压缩包</a></span><br>--%>
                                    <span><a class="naCh"
                                             href="${pageContext.request.contextPath}/toUploadMedia">媒体文件</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/toUploadBook">电子书</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <%--
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-star"></span>
							</span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="star" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingStar"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseStar" aria-expanded="flase"
                                       aria-controls="collapseStar" class="collapsed na">
                                        我的收藏
                                    </a>
                                </p>
                            </div>
                            <div id="collapseStar" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingStar" aria-expanded="flase">
                                <div class="panel-body">

                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">个人收藏</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">引用收藏</a></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                --%>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-hdd"></span>
							</span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="dat" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingData"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseData" aria-expanded="flase"
                                       aria-controls="collapseData" class="collapsed na">
                                        个人数据
                                    </a>
                                </p>
                            </div>
                            <div id="collapseData" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingData" aria-expanded="flase">
                                <div class="panel-body">

<%--                                    <span><a class="naCh"--%>
<%--                                             href="${pageContext.request.contextPath}/controller/${user.email}">总体预览</a></span><br>--%>
                                    <span><a class="naCh"
                                             href="${pageContext.request.contextPath}/listBlog/${user.email}">BLOG数据</a></span><br>
<%--                                    <span><a class="naCh"--%>
<%--                                             href="${pageContext.request.contextPath}/star/${user.email}">STAR数据</a></span><br>--%>
                                    <span><a class="naCh"
                                             href="${pageContext.request.contextPath}/allNotes/${user.email}">NOTES数据</a></span><br>
                                    <span><a class="naCh"
                                             href="${pageContext.request.contextPath}/allEBooks/${user.email}">EBOOKS数据</a></span><br>
<%--                                    <span><a class="naCh"--%>
<%--                                             href="${pageContext.request.contextPath}/listMedia/${user.email}">MEDIA数据</a></span><br>--%>
<%--                                    <span><a class="naCh"--%>
<%--                                             href="${pageContext.request.contextPath}/listDownload/${user.email}">下载数据</a></span><br>--%>
                                    <span><a class="naCh"
                                             href="${pageContext.request.contextPath}/allLoginLog/${user.email}">登录记录</a></span><br>
                                    <span><a class="naCh"
                                             href="${pageContext.request.contextPath}/listBorrow/${user.email}">借阅记录</a></span><br>
                                    <span><a class="naCh"
                                             href="${pageContext.request.contextPath}/listSubscribe/${user.email}">预约记录</a></span><br>
                                    <span><a class="naCh"
                                             href="${pageContext.request.contextPath}/listFeedBack/${user.email}">反馈记录</a></span><br>
                                    <span><a class="naCh"
                                             href="${pageContext.request.contextPath}/listLostFeed/${user.email}">申报记录</a></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li>
                     <span class="ply">
                          <span class="glyphicon glyphicon-book"></span>
                     </span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="down" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingDown"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseDown" aria-expanded="flase"
                                       aria-controls="collapseDown" class="collapsed na">
                                        图书操作
                                    </a>
                                </p>
                            </div>
                            <div id="collapseDown" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingDown" aria-expanded="flase">
                                <div class="panel-body">

                                    <span><a class="naCh" href="${pageContext.request.contextPath}/toBorrow">借阅</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/toSubscribe">预约</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/toFeedback">问题反馈</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/toLostFeed">遗失申报</a></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                 <%--               <li>
                                            <span class="ply">
                                                <span class="glyphicon glyphicon-th-list"></span>
                                            </span>
                                    <!-- <p><a class="na" href=""></a></p> -->
                                    <div class="panel-group" id="cla" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default"
                                             style="padding: 0;margin-left: 15px;border: none; background: unset;">
                                            <div class="panel-heading" role="tab" id="headingCla"
                                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                                <p class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseCla"
                                                       aria-expanded="flase"
                                                       aria-controls="collapseCla" class="collapsed na">
                                                        分类
                                                    </a>
                                                </p>
                                            </div>
                                            <div id="collapseCla" class="panel-collapse collapse" role="tabpanel"
                                                 aria-labelledby="headingCla" aria-expanded="flase">
                                                <div class="panel-body">

                                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">标签分类</a></span><br>
                                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">格式分类</a></span>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                --%>
                <%--
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-file"></span>
							</span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="doc" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingDoc"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDoc"
                                       aria-expanded="flase"
                                       aria-controls="collapseDoc" class="collapsed na">
                                        源码下载
                                    </a>
                                </p>
                            </div>
                            <div id="collapseDoc" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingDoc" aria-expanded="flase">
                                <div class="panel-body">

                                    <span><a class="naCh" href="${pageContext.request.contextPath}/docx">项目文档</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/reanme">环境配置文档</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/download?download=/source/DataManagement.7z">下载源码</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                --%>
                <%--<li>
					<span class="ply">
						<span class="glyphicon glyphicon-download"></span>
					</span>
                    <p style="margin-left: 25px;"><a class="na" href="${pageContext.request.contextPath}/about">下载</a>
                    </p>

                </li>--%>

                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-download"></span>
							</span>
                    <p style="margin-left: 25px;"><a class="na" href="${pageContext.request.contextPath}/download?download=/source/DataManagement.7z">下载源码</a></p>

                </li>
                <%----%>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-link"></span>
							</span>
                    <p style="margin-left: 25px;"><a class="na" href="${pageContext.request.contextPath}/about">关于</a>
                    </p>

                </li>

            </ul>
        </div>
    </div>
    <!-- 侧边栏按钮 -->
    <button class="kj"></button>
</div>
<%--        头像弹框--%>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document"
         style="position: fixed;left: 50%;top: 50%; width: 340px; height: 340px; margin-top: -170px; margin-left: -170px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span id="clo" aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">跟换头像</h4>
            </div>
            <div class="modal-body">
                <form id="form-upload">
                    <section>
                        <input type="hidden" name="uId" value="${avatarEmail}">
                        <div class="row">

                            <div class="" style="">
                                <label for="avatar"><img id="avatar-img"
                                                         style="width: 200px; height: 200px ;margin-left: 70px;"
                                                         src="${pageContext.request.contextPath}/static${user.avatar}"
                                                         alt="" class="img-circle"
                                                         onerror="this.src='${pageContext.request.contextPath}/static/images/logo.png'"></label>
                                <input accept="image/*" type="file" name="avatar" id="avatar" value="${avatar}"
                                       style="display: none">
                                <span class="help-block"></span>
                            </div>

                        </div>

                        <div class="" style="text-align: center;">
                            <input style=" margin-right: 30px;" id="form-btn" type="button" class="btn btn-primary"
                                   value="上传"/>
<%--
                            <a href="${pageContext.request.contextPath}/refresh"><input style="" id="form-seb"
                                                                                        type="button"
                                                                                        class="btn btn-primary"
                                                                                        value="刷新"/></a>
--%>
                        </div>
                    </section>

                </form>

            </div>
        </div>
    </div>
    <script type="text/javascript">
        // 头像预览
        // 找到头像的input标签绑定change事件
        $("#avatar").change(function () {
            // 1. 创建一个读取文件的对象
            var fileReader = new FileReader();
            // 取到当前选中的头像文件
            // console.log(this.files[0]);
            // 读取你选中的那个文件
            fileReader.readAsDataURL(this.files[0]);  // 读取文件是需要时间的
            fileReader.onload = function () {
                // 2. 等上一步读完文件之后才 把图片加载到img标签中
                $("#avatar-img").attr("src", fileReader.result);
            };
        });

        $("#form-btn").click(function() {
            var formData = new FormData();
            formData.append('avatar', document.getElementById('avatar').files[0]);
            $.ajax({
                url:"${pageContext.request.contextPath}/upload/avatar",
                data:formData,
                processData:false,
                contentType:false,
                type:"POST",
                dataType:"json",
                success:function(data) {

                    if (data.sta == "ok") {
                        alert("修改成功："+"下次登录后将生效")
                    } else {
                        alert("修改失败！");
                    }
                },
                "error":function(xhr) {
                    alert("您的登录信息已经过期，请重新登录！\n\nHTTP响应码：" + xhr.status);
                }
            });
        });
    </script>
</div>


<!-- 内容区域 -->
<div style="width: auto;height: 62px;z-index: 50;">

    <div style="margin-left: 280px;margin-top: 10px; z-index: 60; background: whitesmoke;">
        <nav class="navbar navbar-inverse "
             style="width: 100%; margin-right:50px;z-index: 50; background: rgba(10,00,50,0.8);border-radius: 1px">
            <div class="navbar-header">
                <button style="width: 44px; height: 34px;float: right;border-radius: 3px;" type="button"
                        class="navbar-toggle collapsed"
                        data-toggle="collapse"
                        data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${pageContext.request.contextPath}/home1">CLMS</a>
            </div>
            <div class="container-fluid" style="">
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.request.contextPath}/home1">BOOK</a></li>
                        <li><a href="${pageContext.request.contextPath}/home2">EBOOK</a></li>
                        <li><a href="${pageContext.request.contextPath}/home3">BLOG</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">

                        <li><a href="${pageContext.request.contextPath}/about">关于 <span class="sr-only">(current)</span></a>
                        </li>
                        <li>
                            <a style=" margin: 0; padding: 0px;" href="${pageContext.request.contextPath}/oneself">
                                <img style="width: 50px; height: 50px;"
                                     src="${pageContext.request.contextPath}/static${user.avatar}"
                                     alt="avatar"
                                     onerror="this.src='${pageContext.request.contextPath}/static/images/logo.png'"
                                     class="img-circle"></a>
                        </li>
                        <li><a href="${pageContext.request.contextPath}/exit">退出</a></li>
                        <li><a href="javascript:goBack()">返回</a></li>
                    </ul>
                </div>
            </div>
        </nav>

    </div>


</div>

<div style=" width: 90% ;margin:0 auto; z-index: 50;">
    <!-- 搜索框 -->
    <div style="margin: 10px auto 30px auto;">
        <form action="${pageContext.request.contextPath}/retrieve">
            <input style="width: 70%;height: 38px; padding: 5px; border-radius: 2px; border: 1px solid #666666;margin-right: 0px;"
                   name="search"
                   value="${search}" placeholder="全站搜索（输入要搜索的名称）" type="search">
<%--            <span style="height: 38px;margin-left: 0px;width:10%;display: inline-block; border-radius: 2px;">--%>
                <select style="background: unset;border: unset;border-radius: 2px;display: inline-block;margin-right: -2px;margin-left: -2px;height: 36px;padding: 5px; background: white;" id="class-borrow">
                    <option selected value="0">语言</option>

                    <option value="chinese">汉语</option>
                    <option value="english">英语</option>
                </select>
<%--            </span>--%>
<%--            <span style="height: 38px;margin-left: 0px;width:10%;display: inline-block;background:white;  border-radius: 2px;">--%>
                <select style="background: unset;border: unset;border-radius: 2px;display: inline-block;margin-right: -2px;height: 36px;padding: 5px; background: white;" id="language-borrow">
                    <option selected value="0">分类</option>

                    <option value="艺术">艺术</option>
                    <option value="商业">商业</option>
                    <option value="化学">化学</option>
                    <option value="经济">经济</option>
                    <option value="教育">教育</option>
                    <option value="地理">地理</option>
                    <option value="地质">地质</option>
                    <option value="历史">历史</option>
                    <option value="休闲">休闲</option>
                    <option value="法学">法学</option>
                    <option value="宗教">宗教</option>
                    <option value="技术">技术</option>
                    <option value="工艺">工艺</option>
                    <option value="文学">文学</option>
                    <option value="数学">数学</option>
                    <option value="医学">医学</option>
                    <option value="物理">物理</option>
                    <option value="心理学">心理学</option>
                    <option value="计算机">计算机</option>
                    <option value="生物学">生物学</option>
                    <option value="语言学">语言学</option>
                    <option value="社会科学">社会科学</option>
                    <option value="体育与运动">体育与运动</option>
                    <option value="科学(通用)">科学(通用)</option>
                </select>
<%--            </span>--%>

            <button style="width: 120px; height: 40px;margin-left: 0px;" type="submit">搜索</button>
        </form>

    </div>
    <div style="margin-right: 0px; width: auto ;margin-top: -30px; float: right;background: unset;">
        <div class="btn-group" style="margin-right: -5px;">
            <button class="btn btn-default" style="border: unset; background: unset;">语言</button>
            <button style="border: unset; background: unset;" data-toggle="dropdown"
                    class="btn btn-default dropdown-toggle"><span
                    class="caret"></span></button>
            <ul class="dropdown-menu" style=" left: unset;right: 0;">
                <li><a href="#">所有语言</a></li>
                <li class="divider"></li>
                <%--<li><a href="javascript:byLanguage()">English (UK)</a></li>
                <li><a href="javascript:byLanguage()">English (US)</a></li>
                <li><a href="javascript:byLanguage()">Español</a></li>
                <li><a href="javascript:byLanguage()">Español (Latinoamérica)</a></li>
                <li><a href="javascript:byLanguage()">Русский</a></li>
                <li><a href="javascript:byLanguage()">Português</a></li>
                <li><a href="javascript:byLanguage()">Deutsch</a></li>
                <li><a href="javascript:byLanguage()">Français</a></li>
                <li><a href="javascript:byLanguage()">Italiano</a></li>--%>
                <li><a href="javascript:byLanguage(0)">Chinese</a></li>
                <li><a href="javascript:byLanguage(1)">English</a></li>
<%--                <li><a href="javascript:byLanguage()">正體中文 (繁體)</a></li>--%>
<%--                <li><a href="javascript:byLanguage()">Polski</a></li>--%>
<%--                <li><a href="javascript:byLanguage()">한국어</a></li>--%>
<%--                <li><a href="javascript:byLanguage()">Türkçe</a></li>--%>
<%--                <li><a href="javascript:byLanguage()">日本語</a></li>--%>
<%--                <li><a href="javascript:byLanguage()">Tiếng Việt</a></li>--%>
            </ul>
        </div>
        <div class="btn-group">
            <button style="border: unset; background: unset;" class="btn btn-default">分类</button>
            <button style="border: unset; background: unset;" data-toggle="dropdown"
                    class="btn btn-default dropdown-toggle"><span
                    class="caret"></span></button>
            <ul class="dropdown-menu" style=" left: unset;right: 0;">
                <li>
                    <a href="javascript:classify(0)">所有类型</a>
                </li>
                <li class="divider">
                </li>
                <li><a href="javascript:byClassify(1)">艺术</a></li>
                <li><a href="javascript:byClassify(2)">商业</a></li>
                <li><a href="javascript:byClassify(3)">化学</a></li>
                <li><a href="javascript:byClassify(4)">经济</a></li>
                <li><a href="javascript:byClassify(5)">教育</a></li>
                <li><a href="javascript:byClassify(6)">地理</a></li>
                <li><a href="javascript:byClassify(7)">地质</a></li>
                <li><a href="javascript:byClassify(8)">历史</a></li>
                <li><a href="javascript:byClassify(9)">休闲</a></li>
                <li><a href="javascript:byClassify(10)">法学</a></li>
                <li><a href="javascript:byClassify(11)">宗教</a></li>
                <li><a href="javascript:byClassify(12)">技术</a></li>
                <li><a href="javascript:byClassify(13)">工艺</a></li>
                <li><a href="javascript:byClassify(14)">文学</a></li>
                <li><a href="javascript:byClassify(15)">数学</a></li>
                <li><a href="javascript:byClassify(16)">医学</a></li>
                <li><a href="javascript:byClassify(17)">物理</a></li>
                <li><a href="javascript:byClassify(18)">心理学</a></li>
                <li><a href="javascript:byClassify(19)">计算机</a></li>
                <li><a href="javascript:byClassify(20)">生物学</a></li>
                <li><a href="javascript:byClassify(21)">语言学</a></li>
                <li><a href="javascript:byClassify(22)">社会科学</a></li>
                <li><a href="javascript:byClassify(23)">体育与运动</a></li>
                <li><a href="javascript:byClassify(24)">科学(通用)</a></li>


            </ul>
        </div>
    </div>
</div>
<div class="classify-list-content">

</div>
<script>
    function goBack() {
        window.history.back();
    };
    function byLanguage(language) {
        switch (language) {
            case 0:
                <%--$.ajax({--%>
                <%--    type: 'post',--%>
                <%--    url: '${pageContext.request.contextPath}/booksLanguage',--%>
                <%--    data: {--%>
                <%--        'clq':'chinese'--%>
                <%--    },--%>
                <%--    dataType: 'post',--%>
                <%--    success: function (data) {--%>
                <%--        &lt;%&ndash;if (data == null){&ndash;%&gt;--%>
                <%--        &lt;%&ndash;    alert("失效")&ndash;%&gt;--%>
                <%--        &lt;%&ndash;}else {&ndash;%&gt;--%>
                <%--        &lt;%&ndash;    $(".classify-list-content").load("${pageContext.request.contextPath}/booksLanguage #lq-content-query >*");&ndash;%&gt;--%>
                <%--        &lt;%&ndash;}&ndash;%&gt;--%>
                <%--        window.location.href = "${pageContext.request.contextPath}/booksLanguage";--%>
                <%--    }--%>
                <%--});--%>
                window.location.href = "${pageContext.request.contextPath}/language?clq=chinese";
                break;
            case 1:
                <%--$.ajax({--%>
                <%--    type: 'post',--%>
                <%--    url: '${pageContext.request.contextPath}/booksLanguage',--%>
                <%--    data: {--%>
                <%--        'clq':'english'--%>
                <%--    },--%>
                <%--    dataType: 'post',--%>
                <%--    success: function (data) {--%>
                <%--        if (data == null){--%>
                <%--            alert("失效")--%>
                <%--        }else {--%>
                <%--            alert("成功")--%>
                <%--        }--%>
                <%--    }--%>
                <%--});--%>
                <%--$("lq-content-query").load("'${pageContext.request.contextPath}/booksLanguage");--%>
                window.location.href = "${pageContext.request.contextPath}/language?clq=english";
                break;
        }
    }
    function byClassify(claNum) {
        switch (claNum) {
            case 0:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=";
                break;
            case 1:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Art";
                break;
            case 2:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Business";
                break;
            case 2:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Chemistry";
                break;
            case 4:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Economy";
                break;
            case 5:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Education";
                break;
            case 6:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Geography";
                break;
            case 7:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Geology";
                break;
            case 8:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=History";
                break;
            case 9:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Housekeeping, Leisure";
                break;
            case 10:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Jurisprudence";
                break;
            case 11:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Religion";
                break;
            case 12:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Technology";
                break;
            case 13:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=process";
                break;
            case 14:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Literature";
                break;
            case 15:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Mathematics";
                break;
            case 16:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Medicine";
                break;
            case 17:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Physics";
                break;
            case 18:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Psychology";
                break;
            case 19:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Computers";
                break;
            case 20:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Biology";
                break;
            case 21:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Linguistics";
                break;
            case 22:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Other Social Sciences";
                break;
            case 23:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Physical Education And Sport";
                break;
            case 24:
                window.location.href = "${pageContext.request.contextPath}/classify?clq=Science (General)";
                break;

        }
    }
</script>
