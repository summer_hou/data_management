import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class DateFor {
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date d;

    public Date getD() {
        return d;
    }

    public void setD(Date d) {
        this.d = d;
    }
}
