package cloud.xhmy.service;

import cloud.xhmy.pojo.Books;
import cloud.xhmy.pojo.LoginLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface LoginLogService {
    /**
     * 增加
     * @param log
     * @return
     */
    int addLog(LoginLog log);

    /**
     * 根据id删除
     * @param id
     * @return
     */
    int deleteLogById(int id);


    /**
     * 根据id查询,返回
     * @param id
     * @return
     */
    LoginLog queryLogById(int id);


    /**
     * 查询全部,返回list集合
     * @param page
     * @param size
     * @return
     */
    List<LoginLog> queryAllLog(String user,int page, int size);

    /**
     * 用于修改
     * @param id
     * @return
     */
    LoginLog queryById(int id);


    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */

    /**
     * 查询所有
     * @param page
     * @param size
     * @return
     */
    List<LoginLog> listAllLoginLog(int page, int size);

    /**
     * 统计
     * @return
     */
    LoginLog countNumber();

    /**
     * 模糊查询
     * @param page
     * @param size
     * @param name
     * @return
     */
    List<LoginLog> queryByName(String name, int page, int size);
}
