<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/13
  Time: 15:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>listFeedback</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/page-helper.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div class=""  style="width: 90%; margin: 5px auto;">
    <div class="row">
        <div class="col-md-12 ">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <%--                    <th>#</th>--%>
                    <th>反馈内容</th>
                    <th>反馈用户</th>
                    <th>反馈时间</th>
                    <%--                    <th>操作</th>--%>
                </tr>
                </thead>
                <tbody id="feedback">
                <c:forEach var="feedBack" items="${pageInfos.list}">
                    <tr>
                            <%--                        <td><input type="checkbox" name="Check[]" value="${books.id}" id="Check[]"/></td>--%>
                        <td>${feedBack.feedbackContent}</td>
                        <td>${feedBack.feedbackUser}</td>
                        <td>${feedBack.feedbackTime}</td>

                            <%--                        <td class="text-center">--%>
                            <%--                            <a href="${pageContext.request.contextPath}/draftBlog?id=${feedBack.id}"--%>
                            <%--                               class="btn bg-olive btn-xs">更新</a>--%>
                            <%--                            ||--%>
                            <%--                            <a href="${pageContext.request.contextPath}/delete/${feedBack.id}"--%>
                            <%--                               class="btn bg-olive btn-xs">删除</a>--%>

                            <%--                        </td>--%>
                    </tr>
                </c:forEach>

                </tbody>
                <tfoot>

                </tfoot>

            </table>

        </div>
    </div>
    <div class="xhmy_page_div"></div>
    <script type="text/javascript">
        //翻页
        $(".xhmy_page_div").createPage({
            pageNum: ${pageInfos.pages},
            current: 1,
            backfun: function(e) {
                //console.log(e);//回调
            }
        });
        function pageNumber(params) {
            $("#feedback").load("${pageContext.request.contextPath}/listFeedback/${user.email} #feedback >*",{page:params,size:"10"});

        };
    </script>
</div>




<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>
</body>
</html>
