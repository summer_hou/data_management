<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/20
  Time: 20:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="" style="width: 90%;margin: 5px auto;">
    <div class="row">
        <div>
            <h3><span>博客</span></h3>
        </div>
        <div class="col-md-12" style="overflow-x: scroll;">
            <div style="margin-top: 10px;">
                <%--
                <button class="btn btn-danger"
                        style="width: 10%; height: 38px;margin-left: 0px;margin-top: -2px;padding: unset;" type="button"
                        onclick="delAllProduct()">删除
                </button>
                --%>
                <form action="" style="display: inline;">
                    <input id="search-article" style="width: 80%;height: 40px; padding: 5px; border-radius: 2px; border: 1px solid #666666;margin-left: -4px;"
                           name="search"
                           value="${search}" placeholder="输入要搜索的书名" type="search">
                    <button style="width: 10%; height: 40px;margin-left: -4px;" type="button" onclick="searchArticle()">搜索</button>
                </form>
            </div>

            <table class="table table-hover table-striped" style="overflow-x: scroll;color: gold;">
                <thead>
                <tr>
                    <th><input type="checkbox" name="CheckAll" id="CheckAll"/></th>
                    <th>博客作者</th>
                    <th>博客名称</th>
                    <th>博客标题</th>
                    <th>博客标签</th>
                    <th>博客状态</th>
                    <th>博客主题</th>
                    <th>博客内容</th>
                    <th>创建时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="articles-list">
                <c:forEach var="list" items="${pageInfo.list}">
                    <tr>
                        <td><input name="ids" type="checkbox"> <input type="hidden" value=" + blog[i].id} "></td>
                        <td>${list.blogAuthor}</td>
                        <td>${list.blogName}</td>
                        <td>${list.blogTitle}</td>
                        <td>${list.blogTag}</td>
                        <td>${list.blogState}</td>
                        <td>${list.blogTheme}</td>
                        <td>${list.blogContent}</td>
                        <td>${list.blogTime}</td>
                        <td class="text-center">
                            <a href="javascript:browseArticle(${list.id})"
                               class="btn bg-olive btn-xs">预览</a>
                            ||
                            <%--<a href="${pageContext.request.contextPath}//${list.id}"
                               class="btn bg-olive btn-xs">更新</a>
                            ||--%>
                            <a href="${pageContext.request.contextPath}/delete/blog?id=${list.id}"
                               class="btn bg-olive btn-xs">删除</a>
                                <%--                        <input type="hidden" id="del" value="${list.blogContent}"></td>--%>
                    </tr>
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>
    </div>
    <div class="xhmy_page_div"></div>

</div>

<%--
<script type="text/javascript">

    var p;
    var html;
    var i;
    var t;
    var blog;
    //
    // if (window.name == "") {
    //
    //     window.name = "1";  // 在首次进入页面时我们可以给window.name设置一个固定值
    // };
    //
    $(function () {
        $.ajax({
            type: "get",
            url: "${pageContext.request.contextPath}/admin/article",
            dataType: "json",
            success: function (data) {

                blog = data.list;
                t = data.total;
                // document.getElementById("blog-total").innerText=t;
                // p = data.pages;
                // document.getElementById("blog-pages").innerText=p;
                for (i = 0; blog.length > i; i++) {
                    html += "<tr>" +
                        "<td><input name=\"ids\" type=\"checkbox\"> <input type=\"hidden\"value=\"" + blog[i].id + "\"></td>" +
                        "<td>" + blog[i].blogAuthor + "</td>" +
                        "<td>" + blog[i].blogName + "</td>" +
                        "<td>" + blog[i].blogTitle + "</td>" +
                        "<td>" + blog[i].blogTag + "</td>" +
                        "<td>" + blog[i].blogState + "</td>" +
                        "<td>" + blog[i].blogTheme + "</td>" +
                        "<td>" + blog[i].blogContent + "</td>" +
                        "<td>" + blog[i].blogTime + "</td>" +
                        "<td class=\"text-center\">" +
                        "<a href=\"${pageContext.request.contextPath}/toBrowseBlog/" + blog[i].id + "\"class=\"btn bg-olive btn-xs\">预览</a> " + " || " +
                        " <a href=\"${pageContext.request.contextPath}/toModifyBlog/" + blog[i].id + "\" class=\"btn bg-olive btn-xs\">更新</a> " + " || " +
                        " <a href=\"${pageContext.request.contextPath}/blogDraft/" + blog[i].id + "\" class=\"btn bg-olive btn-xs\" onclick=\"delAllProduct()\">删除</a>" +
                        "<input type=\"hidden\" id=\"del\" value=\"" + blog[i].blogContent + "\"></td>" +
                        "</tr>"
                };
                // $("#content-list").empty();
                $("#articles-list").empty();
                $("#articles-list").html(html);
                // $(".xhmy_page_div").createPage({
                //
                //     pageNum: p,
                //     current: 1,
                //     backfun: function (e) {
                //         //console.log(e);//回调
                //     }
                // });
            }
        });
    });


    //
    function pageNumber(params) {
        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/admin/article",
            data: {
                page: params,
                size: '10'
            },
            dataType: "json",
            success: function (data) {

                blog = data.list;
                t = data.total;
                // document.getElementById("blog-total").innerText=t;
                // p = data.pages;
                // document.getElementById("blog-pages").innerText=p;
                for (i = 0; blog.length > i; i++) {
                    html += "<tr>" +
                        "<td><input name=\"ids\" type=\"checkbox\"> <input type=\"hidden\"value=\"" + blog[i].id + "\"></td>" +
                        "<td>" + blog[i].blogAuthor + "</td>" +
                        "<td>" + blog[i].blogName + "</td>" +
                        "<td>" + blog[i].blogTitle + "</td>" +
                        "<td>" + blog[i].blogTag + "</td>" +
                        "<td>" + blog[i].blogState + "</td>" +
                        "<td>" + blog[i].blogTheme + "</td>" +
                        "<td>" + blog[i].blogContent + "</td>" +
                        "<td>" + blog[i].blogTime + "</td>" +
                        "<td class=\"text-center\">" +
                        "<a href=\"${pageContext.request.contextPath}/toBrowseBlog/" + blog[i].id + "\"class=\"btn bg-olive btn-xs\">预览</a> " + " || " +
                        " <a href=\"${pageContext.request.contextPath}/toModifyBlog/" + blog[i].id + "\" class=\"btn bg-olive btn-xs\">更新</a> " + " || " +
                        " <a href=\"${pageContext.request.contextPath}/blogDraft/" + blog[i].id + "\" class=\"btn bg-olive btn-xs\" onclick=\"delAllProduct()\">删除</a>" +
                        "<input type=\"hidden\" id=\"del\" value=\"" + blog[i].blogContent + "\"></td>" +
                        "</tr>"

                }
                ;
                $("#articles-list").empty();
                $("#articles-list").html(html);
                // $(".xhmy_page_div").createPage({
                //
                //     pageNum: p,
                //     current: 1,
                //     backfun: function (e) {
                //     }
                // });
            }
        })
    };
    
</script>
--%>

<script type="text/javascript">
    $(".xhmy_page_div").createPage({

        pageNum: ${pageInfo.pages},
        current: 1,
        backfun: function (e) {
        }
    });
    function pageNumber(params) {
        $("#articles-list").load("${pageContext.request.contextPath}/admin/article #articles-list >*",{page:params,size:"10"});
    };
</script>
<script type="text/javascript">
    function delAllProduct() {
        var str = $("#del").val();
        $.ajax({
            type: 'get',
            url: "${pageContext.request.contextPath}/delete",
            dataType: "json",
            data: {
                delete: str
            },
            success: function (data) {
                if (data.sta == "1") {
                    alert(data.msg)
                }
                alert(data.msg)
            }
        });
    };

    function deleAllProduct() {
        var str = $("#dele").val();
        $.ajax({
            type: 'get',
            url: "${pageContext.request.contextPath}/delete",
            dataType: "json",
            data: {
                delete: str
            },
            success: function (data) {
                if (data.sta == "1") {
                    alert(data.msg)
                }
                alert(data.msg)
            }
        });
    };
</script>

<script type="text/javascript">
    //全选/全不选
    $("#CheckAll").bind("click", function () {
        $("input[name='Check[]']").prop("checked", this.checked);
    });

    //批量删除
    function delAllProduct() {
        if (!confirm("确定要删除吗？")) {
            return;
        }
        var cks = $("input[name='Check[]']:checked");
        var str = "";
        //拼接所有的id
        for (var i = 0; i < cks.length; i++) {
            if (cks[i].checked) {
                str += cks[i].value + ",";
            }
        }
        //去掉字符串末尾的‘&'
        str = str.substring(0, str.length - 1);
        // var a = str.split('&');//分割成数组
        // console.log(str);return false;
        $.ajax({
            type: 'post',
            url: "${pageContext.request.contextPath}/delete/article",
            dataType: "json",
            data: {
                filenames: str
            },
            success: function (data) {
                console.log(data);
                return false;
                if (data.code == "1") {
                    alert('删除成功！');
                    // location.href = data['url'];
                } else {
                    alert('删除失败！');
                    // return false;
                }
            }
        });
    };
</script>