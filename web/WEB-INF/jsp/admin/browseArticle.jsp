<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/22
  Time: 14:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
<html>
<head>

    <title>Comprehensive library management system</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico">
--%>
<%--    &lt;%&ndash;    jquery&ndash;%&gt;--%>
<%--    <script type="text/javascript"--%>
<%--            src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>--%>
<%--    &lt;%&ndash;    bootstrap&ndash;%&gt;--%>
<%--    <script type="text/javascript"--%>
<%--            src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>--%>
<%--    <link rel="stylesheet" type="text/css"--%>
<%--          href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css">--%>
<%--    &lt;%&ndash;    分页&ndash;%&gt;--%>
<%--    <link rel="stylesheet" type="text/css"--%>
<%--          href="${pageContext.request.contextPath}/static/css/page-helper.css">--%>
<%--    <script type="text/javascript"--%>
<%--            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>--%>
<%--    &lt;%&ndash;    md&ndash;%&gt;--%>
<%--    <link rel="stylesheet" type="text/css"--%>
<%--          href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">--%>


<%--</head>--%>
<%--<body>--%>
<div class="browse-article-content" style="width: 90%;margin: 5px auto;">
    <div style="margin: 15px auto;">
        <a href="javascript:articleAuditResult(1)" class="btn btn-primary">通过</a><input id="on-in" name="blogState"
                                                                                        type="hidden" value="1">
        <a href="javascript:articleAuditResult(2)" class="btn btn-primary">不通过</a><input id="on-out" name="blogState"
                                                                                         type="hidden" value="2">
        <a href="javascript:loadContent()" class="btn btn-primary">加载内容</a>
    </div>
    <div style="text-align: center"><span>作者：${article.blogAuthor}</span> <span style="margin-left: 100px;margin-right: 100px">名称：${article.blogName}</span> <span>时间：${article.blogTime}</span></div>
    <div id="article-content">
        <textarea style="display:none;" id="articleContent"></textarea>
    </div>

</div>

<%--

<script type="text/javascript"
        src="${pageContext.request.contextPath}/plugins/editor-md/lib/codemirror/codemirror.min.js"></script>--%>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/plugins/editor-md/lib/marked.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/plugins/editor-md/lib/prettify.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/plugins/editor-md/lib/raphael.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/plugins/editor-md/lib/underscore.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/plugins/editor-md/lib/sequence-diagram.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/plugins/editor-md/lib/flowchart.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/plugins/editor-md/lib/jquery.flowchart.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/plugins/editor-md/editormd.min.js"></script>


<script type="text/javascript">
    <%--
    $(document).ready(function(){

       $("#article-Content").load("${pageContext.request.contextPath}/static${article.blogContent}");
    });

    window.onload = function () {
        alert("2");


        $("#article-Content").load("${pageContext.request.contextPath}/static${article.blogContent}");
    };
    --%>

    function loadContent() {
        $("#article-content").load("${pageContext.request.contextPath}/static${article.blogContent}");
    };

    $("#article-content").load("${pageContext.request.contextPath}/static${article.blogContent}");
    var testEditor;
    $(function () {
        testEditor = editormd.markdownToHTML("article-content", {//注意：这里是上面DIV的id
            htmlDecode: "style,script,iframe",
            emoji: true,
            taskList: true,
            tex: true, // 默认不解析
            flowChart: true, // 默认不解析
            sequenceDiagram: true, // 默认不解析
            codeFold: true,
        });
    });

    function articleAuditResult(re) {
        <%--
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/admin/articleAuditResult',
            data:{
                'result':re,
                'articleId':${article.id}
            },
            dataType:'json',
            success:function (data) {
                if (data.code == "1"){
                    alert("成功！")
                }else {
                    alert("失败！")
                }
            }
        });
        --%>


        switch (re) {
            case 1:
                $.ajax({
                    type: 'post',
                    url: '${pageContext.request.contextPath}/admin/articleAuditResult',
                    data: {
                        'result': 1,
                        'articleId':${article.id}
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.code == "1") {
                            alert("成功1");
                        } else {
                            alert("失败！");
                        }
                    }
                });
                break;
            case 2:
                $.ajax({
                    type: 'post',
                    url: '${pageContext.request.contextPath}/admin/articleAuditResult',
                    data: {
                        'result': 2,
                        'articleId':${article.id}
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.code == "1") {
                            alert("成功1");
                        } else {
                            alert("失败！");
                        }
                    }
                });
                break;
        }

    }
</script>
<%--</body>--%>
<%--</html>--%>
