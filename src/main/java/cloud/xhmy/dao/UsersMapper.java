package cloud.xhmy.dao;

import cloud.xhmy.pojo.Admins;
import cloud.xhmy.pojo.Users;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;
import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface UsersMapper {


//    public String checkLogin(String uEmail);

    /**
     * 用户登录
     * @param user
     * @return
     */
    Users login(Users user);

    /**
     * 用户注册
     * @param user
     * @return
     */
    Integer register(Users user);

    /**
     * 用户找回密码
     * @param user
     * @return
     */
    int forgot(Users user);

    /**
     * 用户注销
     * @param user
     * @return
     */
    int logout(Users user);

    /**
     * 修改个人信息
     * @param user
     * @return
     */
    int modify(Users user);

    /**
     * 注册验证邮箱是否可用
     * @param email
     * @return
     */
    List<Users> judge(String email);

    /**
     * 头像保存到数据库
     * @param user
     * @return
     */
    int avatar(Users user);

    /**
     * 头像保存到服务器
     * @param pro
     * @return
     */
    int pro(Users pro);

    /**
     * 修改联系方式
     * @param tel
     * @param uid
     * @return
     */
    int modifyTel(@Param("telephone") BigInteger tel,@Param("email") String uid);

    /**
     * 修改密码
     * @param pwd
     * @param uid
     * @return
     */
    int modifyPwd(@Param("password") String pwd,@Param("email") String uid);

    /**
     * 实名认证
     * @param realName
     * @param idNumber
     * @return
     */
    int realUser(@Param("realName") String realName, @Param("idNumber") String idNumber,@Param("email") String user);

    /**
     * 查找用户
     * @param users
     * @return
     */
    Users queryByEmail(String users);

    /**
     * 刷新
     * @param user
     * @return
     */
    Users refresh (Users user);


    /**
     * 管理员
     * ----------------------------------------------------------------------------------------------------------
     * ----------------------------------------------------------------------------------------------------------
     * ----------------------------------------------------------------------------------------------------------
     * ----------------------------------------------------------------------------------------------------------
     */

    /**
     * 列出所有用户
     * @param page
     * @param size
     * @return
     */
    List<Users> listAllUser(int page,int size);

    /**
     * 统计
     * @return
     */
    Users countNumber();

    /**
     * 修改联系方式
     * @param users
     * @return
     */
    int adminModifyTel(Users users);

    /**
     * 修改密码
     * @param users
     * @return
     */
    int adminModifyPwd(Users users);

    /**
     * 实名认证
     * @param users
     * @return
     */
    int adminRealUser(Users users);

}
