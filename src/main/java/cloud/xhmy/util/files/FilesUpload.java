package cloud.xhmy.util.files;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

public class FilesUpload {

    /**
     * 文件存储静态工具
     * (日期年月日目录加时间戳名称命名文件)
     * @param path 格式化（/xx/xx/）
     * @param request
     * @param file
     * @return
     */
    public static String setWithDateFormatSuffixPath(String path, HttpServletRequest request, MultipartFile file){

        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateInstance();
        String time = dateFormat.format(date);
        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String[] str = time.split("-");

        String datePath = str[0]+"/"+str[1]+"/"+str[2]+"/";
        String realpath = rootPath+path+datePath;
        File mkdir = new File(realpath);
        if (!mkdir.exists()){
            mkdir.mkdirs();
        }
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        String saveFilename = date.getTime()+suffix;
        File targetFile = new File(realpath, saveFilename);
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String savePath = path+datePath+date.getTime()+suffix;
        return savePath;
    }

    /**
     * 文件存储静态工具
     * @param path 格式化（/xx/xx/）
     * @param request
     * @param file
     * @return
     */
    public static String setWithDateSuffixPath(String path, HttpServletRequest request, MultipartFile file){

        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateInstance();
        String time = dateFormat.format(date);
        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String[] str = time.split("-");
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        String datePath = str[0]+"/"+str[1]+"/"+str[2]+"/";
        String realpath = rootPath+path+datePath;
        File mkdir = new File(realpath);
        if (!mkdir.exists()){
            mkdir.mkdirs();
        }
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        String saveFilename = uuid+suffix;
        File targetFile = new File(realpath, saveFilename);
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String savePath = path+datePath+uuid+suffix;
        return savePath;
    }

    /**
     * 文件存储静态工具
     * @param path 格式化（/xx/xx/）
     * @param request
     * @param file
     * @return
     */
    public static String setWithUuidSuffixPath(String path, HttpServletRequest request, MultipartFile file){


        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        System.out.println("=============================path++++========:"+rootPath+path+file);
        String realpath = rootPath+path;
        File mkdir = new File(realpath);
        if (!mkdir.exists()){
            mkdir.mkdirs();
        }
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        String saveFilename = uuid+suffix;
        File targetFile = new File(realpath, saveFilename);
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String savePath = path+uuid+suffix;
        return savePath;
    }

    /**
     * 默认存储存储
     * 文件存储静态工具
     * (日期年月日目录加时间戳名称命名文件)
     * @param request
     * @param file
     * @return （/xx/xx/xx.xx）
     */
    public static String setWithDefaultSuffixPath(HttpServletRequest request, MultipartFile file){

        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateInstance();
        String time = dateFormat.format(date);
        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String[] str = time.split("-");

        String path = "/upload/default/"+str[0]+"/"+str[1]+"/"+str[2]+"/";
        String defaultPath = rootPath+path;
        File mkdir = new File(defaultPath);
        if (!mkdir.exists()){
            mkdir.mkdirs();
        }
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        String saveFilename = date.getTime()+suffix;
        File targetFile = new File(defaultPath, saveFilename);
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String savePath = path+date.getTime()+suffix;
        return savePath;
    }

}
