package cloud.xhmy.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;

/**
 * 浏览用户
 * browseUser
 * @author aliketh.xhmy
 */
public class BrowseUser {

    private int id;
    private String  uuid;
    private String browseDevice;
    private String browseIp;
    private String browseSystem;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Timestamp browseTime;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getBrowseDevice() {
        return browseDevice;
    }

    public void setBrowseDevice(String browseDevice) {
        this.browseDevice = browseDevice;
    }

    public String getBrowseIp() {
        return browseIp;
    }

    public void setBrowseIp(String browseIp) {
        this.browseIp = browseIp;
    }

    public String getBrowseSystem() {
        return browseSystem;
    }

    public void setBrowseSystem(String browseSystem) {
        this.browseSystem = browseSystem;
    }

    public Timestamp getBrowseTime() {
        return browseTime;
    }

    public void setBrowseTime(Timestamp browseTime) {
        this.browseTime = browseTime;
    }
}
