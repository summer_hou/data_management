package cloud.xhmy.controller;

import cloud.xhmy.pojo.Media;
import cloud.xhmy.service.MediaService;
import cloud.xhmy.util.files.FilesDelete;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * @author aliketh.xhmy
 */
@Controller
public class MediaController {


    @Autowired
    @Qualifier("mediaServiceImpl")
    private MediaService mediaService;

    /**
     * 跳转到上传媒体文件页面
     * @return
     */
    @RequestMapping("/toUploadMedia")
    public String toUploadMedia(){
        return "operation/uploadMedia";
    }

//    /**
//     * 跳转到修改
//     * @return
//     */
//    @RequestMapping("/toModifyMedia/{mediaId}")
//    public String toModifyMedia(@PathVariable("mediaId") int id ,Model model){
//        Media media = mediaService.queryById(id);
//        model.addAttribute("media",media);
//        return "operation/uploadMedia";
//    }

    /**
     * 列出所有个人上传媒体文件
     * @param author
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/listMedia/{user.email}")
    public ModelAndView toModifyMedia(@PathVariable("user.email") String author ,@RequestParam(defaultValue = "1") int page,
                                @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<Media> list = mediaService.listAll(author,page, size);
        PageInfo<Media> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos", pageInfos);
        mv.setViewName("operation/listMedia");
        return mv;
    }



    /**
     * 增加
     * @param request
     * @param audio
     * @param model
     * @param file
     * @return
     */
    @ResponseBody
    @RequestMapping("/audio")
    public JSONObject audioUpload(HttpServletRequest request, @ModelAttribute Media audio,
                                  Model model, @RequestParam("audio") MultipartFile file){

        JSONObject jsonObject = new JSONObject();

        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String contextPath = "/media/";
        String realpath = rootPath+contextPath;
        System.out.println("================================================realpath:=="+realpath);
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        String saveMediaName =new Date().getTime()+suffix;

        File dirPath = new File(realpath);
        if(!dirPath.exists()){
            dirPath.mkdirs();
        }

        File savePath = new File(dirPath,saveMediaName);
        //上传
        try {

            file.transferTo(savePath);
            audio.setMediaState("0");
            audio.setMediaContent(contextPath+saveMediaName);
            mediaService.addMedia(audio);

//            model.addAttribute("name",fileName);

        } catch (Exception e) {
            e.printStackTrace();
        }
        jsonObject.put("code","1");
        jsonObject.put("msg","成功！");
        return jsonObject;
    }
    /**
     * 查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/queryMedia/{byName}")
    public ModelAndView query(@PathVariable("byName") String name,
                                   @RequestParam(defaultValue = "1") int page,
                                   @RequestParam(defaultValue = "10") int size){
        PageHelper.startPage(page,size);
        ModelAndView mv = new ModelAndView();
        List<Media> list = mediaService.queryByName(name, page, size) ;
        PageInfo<Media> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos",pageInfos);
        mv.setViewName("operation/listMedia");
        return mv;

    }


    /**
     * 删除媒体文件
     * @param mediaId
     */
    @ResponseBody
    @RequestMapping("/delete/media")
    public JSONObject deleteMedia(@RequestParam("id") List<Integer> mediaId,HttpServletRequest request){
        List<Integer> list = mediaId;
        Media media;
        for (int i = 0; i < list.size(); i++) {
            int id = list.get(i);
            media = mediaService.queryById(id);
            FilesDelete.delete(media.getMediaContent(),request);
            mediaService.deleteById(id);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code","1");
        return jsonObject;

    }

}
