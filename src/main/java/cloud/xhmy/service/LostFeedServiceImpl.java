package cloud.xhmy.service;

import cloud.xhmy.dao.LostFeedMapper;
import cloud.xhmy.pojo.LostFeed;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public class LostFeedServiceImpl implements LostFeedService {


    private LostFeedMapper lostFeedMapper;

    public void setLostFeedMapper(LostFeedMapper lostFeedMapper) {
        this.lostFeedMapper = lostFeedMapper;
    }

    @Override
    public int add(LostFeed lost) {
        return this.lostFeedMapper.add(lost);
    }

    @Override
    public int deleteById(int id) {
        return this.lostFeedMapper.deleteById(id);
    }

    @Override
    public List<LostFeed> queryByName(String name, int page, int size) {
        return this.lostFeedMapper.queryByName(name, page, size);
    }

    @Override
    public List<LostFeed> listAll(String author, int page, int size) {
        return this.lostFeedMapper.listAll(author, page, size);
    }

    @Override
    public int modifyById(int id) {
        return this.lostFeedMapper.modifyById(id);
    }

    @Override
    public LostFeed queryById(int id) {
        return this.lostFeedMapper.queryById(id);
    }

    public List<LostFeed> listAllLostFeed(int page, int size) {
        return this.lostFeedMapper.listAllLostFeed(page, size);
    }

    public LostFeed countNumber() {
        return this.lostFeedMapper.countNumber();
    }

    public List<LostFeed> listAllDispose(String result, int page, int size) {
        return this.lostFeedMapper.listAllDispose(result, page, size);
    }

    public List<LostFeed> listQueryAllDispose(String result, String name, int page, int size) {
        return this.lostFeedMapper.listQueryAllDispose(result, name, page, size);
    }
}
