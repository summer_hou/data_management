$(function(){
    $(".naSpan").on("click", "a", function(){
        var navId = $(this).data("id");  //获取data-id的值
        window.location.hash = navId;  //设置锚点
        loadInner(navId);
    });
    function loadInner(navId){
        var navId = window.location.hash;
        var navPathLoad, i;
        switch(navId){
            case "#welcomeNav": navPathLoad = "/admin/welcome"; i = 0; break;
            case "#homeBookNav": navPathLoad = "/admin/homeBook"; i = 1; break;
            case "#homeArticleNav": navPathLoad = "/admin/homeArticle"; i = 2; break;
            case "#blogNav": navPathLoad = "/admin/toEditorArticle"; i = 3; break;
            case "#articleAuditNav": navPathLoad = "/admin/articleAudit"; i = 4; break;
            case "#auditPictureNav": navPathLoad = "/admin/auditPicture"; i = 5; break;
            case "#auditBookNav": navPathLoad = "/admin/auditBook"; i = 6; break;
            case "#auditMediaNav": navPathLoad = "/admin/auditMedia"; i = 7; break;
            case "#auditEBookNav": navPathLoad = "/admin/auditEBook"; i = 8; break;
            case "#uploadOperationNav": navPathLoad = "/admin/uploadOperation"; i = 9; break;
            case "#userPunishNav": navPathLoad = "/admin/userPunish"; i = 10; break;
            case "#blacklistNav": navPathLoad = "/admin/blacklist"; i = 11; break;
            case "#userManageNav": navPathLoad = "/admin/userManage"; i = 12; break;
            case "#borrowDisposeNav": navPathLoad = "/admin/borrowDispose"; i = 13; break;
            case "#subscribeDisposeNav": navPathLoad = "/admin/subscribeDispose"; i = 14; break;
            case "#feedbackDisposeNav": navPathLoad = "/admin/feedbackDispose"; i = 15; break;
            case "#lostFeedDisposeNav": navPathLoad = "/admin/lostFeedDispose"; i = 16; break;
            case "#controllerNav": navPathLoad = "/admin/toController"; i = 17; break;
            case "#articleNav": navPathLoad = "/admin/article"; i = 18; break;
            case "#eBookNav": navPathLoad = "/admin/eBook"; i = 19; break;
            case "#subscribeNav": navPathLoad = "/admin/subscribe"; i = 20; break;
            case "#feedbackNav": navPathLoad = "/admin/feedback"; i = 21; break;
            case "#lostFeedNav": navPathLoad = "/admin/lostFeed"; i = 22; break;
            case "#notesNav": navPathLoad = "/admin/notes"; i = 23; break;
            case "#bookNav": navPathLoad = "/admin/book"; i = 24; break;
            case "#mediaNav": navPathLoad = "/admin/media"; i = 25; break;
            case "#downloadNav": navPathLoad = "/admin/download"; i = 26; break;
            case "#uploadNav": navPathLoad = "/admin/upload"; i = 27; break;
            case "#borrowNav": navPathLoad = "/admin/borrow"; i = 28; break;
            case "#browseNav": navPathLoad = "/admin/browse"; i = 29; break;
            case "#loginLogNav": navPathLoad = "/admin/loginLog"; i = 30; break;
            case "#classifyNav": navPathLoad = "/admin/classify"; i = 31; break;
            case "#eFormatNav": navPathLoad = "/admin/eFormat"; i = 32; break;
            case "#docNav": navPathLoad = "/admin/doc"; i = 33; break;
            case "#readMeNav": navPathLoad = "/admin/readMe"; i = 34; break;
            case "#settingNav": navPathLoad = "/admin/setting"; i = 35; break;
            case "#aboutNav": navPathLoad = "/admin/about"; i = 36; break;
            default: navPathLoad = "/admin/"; i = 0; break;
        }
        $("#content-list").load(navPathLoad); //加载相对应的内容
        // $(".panel-body a").eq(i).addClass("active-nav").siblings().removeClass("active-nav"); //当前列表高亮
    }
    var navId = window.location.hash;
    loadInner(navId);
});
/**
 * --------------------------------------------------------------------------------------------------------
 */
function homeTopNav() {
    //$("#content-list").empty();
    $("#content-list").load("/admin/welcome");
};

function aboutTopNav() {
    //$("#content-list").empty();
    $("#content-list").load("/admin/about");
};

function controllerTopNav() {
    //$("#content-list").empty();
    $("#content-list").load("/admin/toController");
};

function readMeTopNav() {
    //$("#content-list").empty();
    $("#content-list").load("/admin/readMe");
};

function adminBack() {
    //$("#content-list").empty();
    $("#content-list").load(window.history.back());
    // window.history.back();
};
