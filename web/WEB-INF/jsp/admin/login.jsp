<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/7
  Time: 13:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>adminLogin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 网页图标 ico格式 -->
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico">
    <!-- jquery -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>
    <!-- 引入 bootstrap js文件 -->
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- 引入 bootstrap css文件 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css"
          type="text/css">
    <!-- 密码的显示与隐藏 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/password-show-hidden.js"></script>


    <style>
        html, body {
            overflow: hidden;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            background: url("${pageContext.request.contextPath}/static/images/235228-1549900348f5ed.jpg");
            margin: 0 auto;
        }
        .img-back{
            filter:alpha(opacity=50);
            -moz-opacity:0.5;
            -khtml-opacity: 0.5;
            opacity: 0.5;
            width:100%;
            height: 100%;
            position:absolute;
            z-index: 10;
            background: rgba(255, 255, 255, 0.5);

        }
        /* canvas {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background:#000;
            cursor: pointer;
        } */
        div.pos{
            position:relative;
            width: 100%;
            height: 100%;
            z-index:150;
            color: goldenrod;


        }
        /*footer*/
        div.footer {
            position: absolute;
            bottom: 0px;
            right: auto;

            width: 100%;
            min-height: 50px;
            padding-top: 15px;
            color: whitesmoke;
            /* background: rgba(110,100,200,0.8); */
            text-align: center;
            z-index: 100;
        }

        .icp {
            color: white;
            text-decoration: none;
        }

        .icp:hover {
            text-decoration: none;
            color: wheat;
        }

    </style>

</head>
<body>
<div class="img-back">

</div>
<div class="pos">
    <div class="container">
        <div style="margin-top: 120px;">

        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6" style="">
                <h3 style="margin: 30px auto; text-align: center;"><span>管理员登入</span></h3>
                <div style="width: 80%;margin: 0 auto;">
                    <div class="" style="line-height: 2.5em;">

                        <div class="">
                            <label for="email">管理员邮箱</label>

                            <input id="email" type="email" class="form-control" name="email" value="" placeholder="请输入用户邮箱 @dm.net 或者@alike.com"
                                   required onfocus="changeBtnStyle(this.id)">
                        </div>

                        <div class="">
                            <label for="password">管理员密码</label>
                            <span class="float-right" style="float: right;">忘记密码?点击 &nbsp;<a href="${pageContext.request.contextPath}/admin/toForgot">找回</a></span>
                            <input id="password" type="password" class="form-control" name="password" placeholder="请输入六位以上由数字,密码,符号组成的密码"
                                   required data-eye onblur="adminLoginFrom()" onfocus="changeBtnStyle(this.id)">
                            <span id="feedback-message"></span>
                        </div>
                        <!-- <div class="">
                        <label for="chsckCode">验证码</label>
                        <span class="float-right" style="float: right;">看不清，点击图片</span><br>
                        <input id="checkCode" type="text" class="form-control" name="checkCode" style="height: 40px;width: auto;display: inline;" placeholder="请输入验证码">
                        <img src="../img/login-code.png" style="display: inline;margin-bottom: 2px;border-radius: 1px;">
                    </div> -->

                        <div class="" style="margin-top: 20px; text-align: center;">
                            <button id="login-btn" type="button" style="width: 15%; margin: 0 auto;" class="btn btn-primary btn-block"
                                    data-toggle="modal" data-target="#exampleModal">
                                登录
                            </button>
                        </div>
                        <div class="margin-top:20px;text-center">
                            没有账号点击这里? <a href="${pageContext.request.contextPath}/admin/toRegister">创建</a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div></div>
    </div>

    <div class="footer">
        <p>
            <span>© 2020 xhmy.cloud . &nbsp;</span>
            <span>赣ICP证</span> &nbsp;
            <a class="icp" href="https://beian.miit.gov.cn" title="" target="_blank">赣ICP备</a>
            <span>2020012707号</span> &nbsp;
        </p>
    </div>
</div>

<div style="z-index: 1000;">
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document" style="position: fixed;left: 50%;top: 50%; width: 350px; height: 304px; margin-top: -175px; margin-left: -152px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span id="clo" aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">拖动图片验证</h4>
                </div>
                <div class="modal-body">
                    <div id="captcha"></div>
                </div>
            </div>
        </div>
    </div>

</div>






<script src="${pageContext.request.contextPath}/static/js/admin/admin-jigsaw.min.js"></script>
<script>
    function changeBtnStyle(x) {
        document.getElementById("login-btn").style.display="none";
    };
    function adminLoginFrom(){
        $.ajax({
            type: 'post',
            url: '${pageContext.request.contextPath}/admin/login',
            data: {
                'email': $("#email").val(),
                'password': $("#password").val()
            },
            // processData:false,
            // contentType:false,
            dataType: 'json',
            success: function (data) {
                if (data.code == "0") {
                    document.getElementById("feedback-message").innerHTML="<span style='color: red;'>用户名密码不正确！</span>";
                    // document.getElementById("login-btn").style.display="none";
                }else {
                    document.getElementById("feedback-message").innerHTML="<span style='color: green;'>验证通过！</span>";
                    document.getElementById("login-btn").style.removeProperty("display");
                }
            }
        });
    };
    $("#login-btn").click(function () {
        var u = $("#email").val();
        var p = $("#password").val();
        if (p == "" && u == ""){
            document.getElementById("exampleModal").style.display="none";
            alert("用户名、密码不能为空！");
        }else {
            jigsaw.init({
                el: document.getElementById('captcha'),
                onSuccess: function() {
                    //验证成功发送登录请求

                    window.location.href = "${pageContext.request.contextPath}/admin/home";
                },
                onFail: cleanMsg,
                onRefresh: cleanMsg
            });
            function cleanMsg() {
                alert("failure：验证失败！")
            };
        }

    });

</script>

</body>
</html>
