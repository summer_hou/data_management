<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/31
  Time: 23:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>readNotes</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>


    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div style="margin: 5px auto;width: 90%;">
    <div style="text-align: center">
        <h3><span>查看笔记</span></h3>
    </div>
    <from>
        <input type="hidden" name="id" value="${notes.id}" >
    </from>
</div>


<div id="notes-content" style="width: 80%;overflow-y: hidden;margin: 5px auto;">
    <textarea style="display:none;" id="content">${notes.notesContent}</textarea>
</div>

<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/marked.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/prettify.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/raphael.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/underscore.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/sequence-diagram.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/flowchart.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/jquery.flowchart.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/editormd.min.js"></script>



<script type="text/javascript">
    $("#notes-content").load("${pageContext.request.contextPath}/static${notes.notesContent}");
    var testEditor;
    $(function () {
        testEditor = editormd.markdownToHTML("notes-content", {//注意：这里是上面DIV的id
            htmlDecode: "style,script,iframe",
            emoji: true,
            taskList: true,
            tex: true, // 默认不解析
            flowChart: true, // 默认不解析
            sequenceDiagram: true, // 默认不解析
            codeFold: true,
        });});
</script>
</body>
</html>
