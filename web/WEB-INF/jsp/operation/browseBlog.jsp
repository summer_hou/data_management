<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/24
  Time: 17:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>record</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
<%--    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/editor-md/lib/codemirror/codemirror.min.css">--%>
<%--    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/editor-md/lib/codemirror/addon/dialog/dialog.css">--%>
<%--    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/editor-md/lib/codemirror/addon/search/matchesonscrollbar.css">--%>
<%--    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/editor-md/lib/codemirror/addon/fold/foldgutter.css">--%>

</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>
<div style=" width: 90%;margin: auto;">
    <form>
        <input type="hidden" name="id" value="${blog.id}" >
<%--        <td>${blog.blogName}</td>--%>
<%--        <td>${blog.blogTitle}</td>--%>
<%--        <td>${blog.blogTag}</td>--%>
<%--        <td>${blog.blogState}</td>--%>
<%--        <td>${blog.blogTheme}</td>--%>
<%--        <td>${blog.blogTime}</td>--%>
    </form>
    <div style="text-align: center;"><h1>${blog.blogName}</h1><span>作者：${blog.blogAuthor}</span> <span style="margin-left: 100px;margin-right: 100px">主题：${blog.blogTitle}</span> <span>时间：${blog.blogTime}</span></div>
    <div id="article-content">

        <textarea style="display:none;" id="blogContent"></textarea>
<%--        <h3><span>${blog.blogName}</span></h3>--%>
<%--        <textarea style="display:none;" name=""  th:text="${pageContext.request.contextPath}/static${blog.blogContent}"></textarea>--%>
<%--        static${blog.blogContent}      <!--content为获取后台的html格式内容。-->--%>
    </div>
    <div id="browse">

    </div>
    <div id="" style="width: 80%;overflow-y: hidden">

    </div>

</div>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>


<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/codemirror/codemirror.min.js"></script>
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/examples/js/jquery.min.js"></script>--%>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/marked.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/prettify.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/raphael.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/underscore.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/sequence-diagram.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/flowchart.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/jquery.flowchart.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/editormd.min.js"></script>


<script type="text/javascript">
    $("#article-content").load("${pageContext.request.contextPath}/static${blog.blogContent}");
    var testEditor;
    $(function () {
        testEditor = editormd.markdownToHTML("article-content", {//注意：这里是上面DIV的id
            htmlDecode: "style,script,iframe",
            emoji: true,
            taskList: true,
            tex: true, // 默认不解析
            flowChart: true, // 默认不解析
            sequenceDiagram: true, // 默认不解析
            codeFold: true,
        });});
</script>


<script type="text/javascript">




    // $(function () {
    //     editormd.markdownToHTML("");
    // });
    // var browseArticle;

    <%--$(function() {--%>

    <%--    $.get('${pageContext.request.contextPath}/static${blog.blogContent}', function(md){--%>
    <%--        browseArticle = editormd("browse", {--%>
    <%--            htmlDecode: "style,script,iframe",--%>
    <%--            emoji: true,--%>
    <%--            taskList: true,--%>
    <%--            tex: true, // 默认不解析--%>
    <%--            flowChart: true, // 默认不解析--%>
    <%--            sequenceDiagram: true, // 默认不解析--%>
    <%--            codeFold: true,--%>
    <%--        });--%>
    <%--    });--%>
    <%--});--%>
</script>
</body>
</html>
