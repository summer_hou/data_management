package cloud.xhmy.service;

import cloud.xhmy.dao.SubscribeMapper;
import cloud.xhmy.pojo.Subscribe;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public class SubscribeServiceImpl implements SubscribeService {


    private SubscribeMapper subscribeMapper;

    public void setSubscribeMapper(SubscribeMapper subscribeMapper) {
        this.subscribeMapper = subscribeMapper;
    }

    public int addSubscribe(Subscribe subscribe) {
        return this.subscribeMapper.addSubscribe(subscribe);
    }

    public int deleteById(int id) {
        return this.subscribeMapper.deleteById(id);
    }

    public Subscribe queryById(int id) {
        return this.subscribeMapper.queryById(id);
    }

    public List<Subscribe> listAll(String user, int page, int size) {
        return this.subscribeMapper.listAll(user, page, size);
    }

    public List<Subscribe> listAllSubscribe(int page, int size) {
        return this.subscribeMapper.listAllSubscribe(page, size);
    }

    public int push(int id) {
        return this.subscribeMapper.push(id);
    }

    public Subscribe countNumber() {
        return this.subscribeMapper.countNumber();
    }

    public List<Subscribe> queryByName(String name, int page, int size) {
        return this.subscribeMapper.queryByName(name, page, size);
    }
}
