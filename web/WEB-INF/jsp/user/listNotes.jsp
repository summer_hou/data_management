<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/31
  Time: 16:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>


    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/page-helper.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/page-helper.js"></script>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div style="margin: 5px auto;width: 90%;">

    <div class="row">
        <div class="col-md-12 ">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th><input type="checkbox" name="CheckAll" id="CheckAll"/></th>
                    <th>notesName</th>
                    <th>notesAuthor</th>
                    <th>notesContent</th>
                    <th>notesTime</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="notes">
                <c:forEach var="notes" items="${pageInfos.list}">
                    <tr>
                        <td><input type="checkbox" name="Check[]" value="${notes.id}" id="Check[]"/></td>
                        <td>${notes.notesName}</td>
                        <td>${notes.notesAuthor}</td>
                        <td>${notes.notesContent}</td>
                        <td>${notes.notesTime}</td>
                        <td class="text-center">
                            <a href="${pageContext.request.contextPath}/toReadNotes/${notes.id}"
                               class="btn bg-olive btn-xs">查看</a>
                            ||
                            <a href="${pageContext.request.contextPath}/toModifyNotes/${notes.id}"
                               class="btn bg-olive btn-xs">更新</a>
                            ||
                            <a href="javascript:deleteNotes(${notes.id})"
                               class="btn bg-olive btn-xs">删除</a>

                        </td>
                    </tr>
                </c:forEach>

                </tbody>
                <tfoot>

                </tfoot>

            </table>

        </div>
    </div>
    <div class="xhmy_page_div"></div>
    <script type="text/javascript">
        //翻页
        $(".xhmy_page_div").createPage({
            pageNum: ${pageInfos.pages},
            current: 1,
            backfun: function(e) {
                //console.log(e);//回调
            }
        });
        function pageNumber(params) {
            $("#notes").load("${pageContext.request.contextPath}/allNotes/${user.email} #netos >*",{page:params,size:"10"});

        };
    </script>
</div>



<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>

<script type="text/javascript">
    function deleteNotes(did) {
        $.ajax({
            type: 'get',
            url: '${pageContext.request.contextPath}/delete/notes?id='+did,
            processData:false,
            contentType:false,
            dataType: 'json',
            success:function (data) {
                if (data.code == "1"){
                    alert("成功！");
                    window.location.href;
                }else {
                    alert("失败！");
                }
            }
        })
    };
</script>

<script type="text/javascript">
    //全选/全不选
    $("#CheckAll").bind("click", function () {
        $("input[name='Check[]']").prop("checked", this.checked);
    });
    <%--
        //批量删除
        function delAllProduct() {
            if (!confirm("确定要删除吗？")) {
                return;
            }
            var cks = $("input[name='Check[]']:checked");
            var str = "";
            //拼接所有的id
            for (var i = 0; i < cks.length; i++) {
                if (cks[i].checked) {
                    str += cks[i].value + ",";
                }
            }
            //去掉字符串末尾的‘&'
            str = str.substring(0, str.length - 1);
            // var a = str.split('&');//分割成数组
            // console.log(str);return false;
            $.ajax({
                type: 'post',
                url: "${pageContext.request.contextPath}/eBook/{id}",
                dataType: "json",
                data: {
                    filenames: str
                },
                success: function (data) {
                    console.log(data);
                    return false;
                    if (data['status'] == 1) {
                        alert('删除成功！');
                        location.href = data['url'];
                    } else {
                        alert('删除失败！');
                        return false;
                    }
                }
            });
        };--%>
</script>

</body>
</html>
