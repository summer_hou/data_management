package cloud.xhmy.service;

import cloud.xhmy.dao.FeedbackMapper;
import cloud.xhmy.pojo.Feedback;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public class FeedbackServiceImpl implements FeedbackService {

    private FeedbackMapper feedbackMapper;

    public void setFeedbackMapper(FeedbackMapper feedbackMapper) {
        this.feedbackMapper = feedbackMapper;
    }

    public int addFeedback(Feedback feedback) {
        return this.feedbackMapper.addFeedback(feedback);
    }

    public int deleteById(int id) {
        return this.feedbackMapper.deleteById(id);
    }

    public Feedback queryById(int id) {
        return this.feedbackMapper.queryById(id);
    }

    public List<Feedback> listAll(String user, int page, int size) {
        return this.feedbackMapper.listAll(user, page, size);
    }

    public List<Feedback> listAllFeedback(int page, int size) {
        return this.feedbackMapper.listAllFeedback(page, size);
    }

    public int push(int id) {
        return this.feedbackMapper.push(id);
    }

    public Feedback countNumber() {
        return this.feedbackMapper.countNumber();
    }

    public List<Feedback> queryByName(String name, int page, int size) {
        return this.feedbackMapper.queryByName(name, page, size);
    }
}
