<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/3
  Time: 16:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>subscribe</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>
</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>

<div style="width: 90%;margin: auto;">


    <div class="row">
        <div id="" class="col-md-4" style="color: #ffff7f;line-height: 2em;">
            <h3 style="margin: 30px auto;"><span><em>书籍预约说明</em></span></h3>
            <ul><em style="font-size: 16px;margin-bottom: 15px;">预约时务必注意</em></ul>
            <ol>
                <li>预约前通过网络预约时请确认是否有该图书,若不存在可能导致预约失败</li>
                <li>预约时确保预约书名无误</li>
                <li>预约数量不能为空</li>
                <li>预约天数默认为五天（可自行修改），修改后务必记住，避免因错过时间，接受超时处罚。处罚过多可影响个人在本网站的个人体验</li>
                <li>#不确定书名（或不清楚是否存在）点击这里 &nbsp;<a href="#form-seb" style="color: #000000;">查询</a></li>
                <li>预约成功时,返回取书地址（1HF/18#/5f/32或第一层第十八列第五行第三十二列）解释为图书馆层数/图书列数/图书所在列数的层数/图书所在列数的层数的第几列</li>
                <li>查看更多规则点击这里 <a href="${pageContext.request.contextPath}/about">查看</a></li>
            </ol>
        </div>
        <div class="col-md-7">
            <div style="margin: 20px auto;">
                <h3>
                    <sapn>书籍预约</sapn>
                </h3>
            </div>
            <form id="form-subscribe" action="" style="line-height: 2em; font-size: 16px;">
                <div><span>图书书名：</span><input class="form-control" type="text" name="subscribeName" value="${bookName}"></div>
                <div><span>用户：</span><input class="form-control" type="text" name="subscribeUser" value="${user.email}"></div>
                <div><span>预约数量：</span><input class="form-control" type="number" name="subscribeNumber" value="1"
                                              onkeyup="value=value.replace(/[^\d]/g,'')" onblur="value=value.replace(/[^\d]/g,'')" placeholder="默认1"></div>
                <div><span>借阅天数</span><input class="form-control" type="number" name="borrowDay" value="5"
                                             onkeyup="value=value.replace(/[^\d]/g,'')" onblur="value=value.replace(/[^\d]/g,'')" placeholder="默认5天"></div>
                <div><span>押金</span><input class="form-control" type="number" name="deposit" value="30"
                                           onkeyup="value=value.replace(/[^\d]/g,'')" onblur="value=value.replace(/[^\d]/g,'')"></div>
                <div><span>取书时间</span><input class="form-control" type="datetime-local" name="takeTime" value="new Date();"></div>
                <div style="margin: 20px auto; text-align: center;"><input type="button" id="borrow-btn" class="btn btn-primary" value="预约"></div>
            </form>
        </div>
    </div>


</div>

<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>

<script type="text/javascript">
    $("#borrow-btn").click(function () {
        var formData = new FormData(document.getElementById("form-subscribe"));
        $.ajax({
            type:'post',
            url:'${pageContext.request.contextPath}/pushSubscribe',

            data:formData,
            processData:false,
            contentType:false,
            dataType:'json',

            success:function (data) {
                alert("状态："+data.msg+"\n"+"取书地址："+data.address);
            }
        });

    });
</script>
</body>
</html>

