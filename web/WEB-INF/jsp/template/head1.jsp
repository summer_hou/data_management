<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/27
  Time: 18:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/static/css/user/user-template-framework.css">
<link rel="icon" href="${pageContext.request.contextPath}/favicon.ico">
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css">
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/user/user-template-framework.js"></script>
<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/static/css/user/user-template-font.css">
<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/static/css/user/user-template-style.css">
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>

