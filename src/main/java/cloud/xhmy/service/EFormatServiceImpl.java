package cloud.xhmy.service;

import cloud.xhmy.dao.EFormatMapper;
import cloud.xhmy.pojo.EFormat;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public class EFormatServiceImpl implements EFormatService {


    private EFormatMapper eFormatMapper;

    public void setEFormatMapper(EFormatMapper eFormatMapper) {
        this.eFormatMapper = eFormatMapper;
    }

    public int addFormat(EFormat format) {
        return this.eFormatMapper.addFormat(format);
    }

    public int deleteById(int id) {
        return this.eFormatMapper.deleteById(id);
    }

    public List<EFormat> queryByName(String name, int page, int size) {
        return this.eFormatMapper.queryByName(name, page, size);
    }

    public List<EFormat> listAll(int page, int size) {
        return this.eFormatMapper.listAll(page, size);
    }

    public int modifyById(int id) {
        return this.eFormatMapper.modifyById(id);
    }

    public EFormat queryById(int id) {
        return this.eFormatMapper.queryById(id);
    }
}
