package cloud.xhmy.util;

import cloud.xhmy.pojo.Admins;
import cloud.xhmy.pojo.Users;
import cloud.xhmy.service.AdminsService;
import cloud.xhmy.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class PermissionUtil {
    @Autowired
    @Qualifier("usersServiceImpl")
    private static UsersService usersService;
    @Autowired
    @Qualifier("adminsServiceImpl")
    private static AdminsService adminsService;

    public static boolean per(String admin,String user){
        Users perUser = usersService.queryByEmail(user);
        Admins perAdmin = adminsService.queryByEmail(admin);
        if (perUser.getPermissions().equals(1)){
            return false;
        }else {
            return true;
        }


    }
}
