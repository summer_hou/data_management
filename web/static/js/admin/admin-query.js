function searchAuditArticle() {
    var search = $("#search-audit-article").val();
    $("#content-list").load('/admin/auditArticle/0?search='+search);
};
function searchAuditMedia() {
    var search = $("#search-audit-media").val();
    $("#content-list").load('/admin/auditMedia/0?search='+search)
};
function searchAuditEBook() {
    var search = $("#search-audit-eBook").val();
    $("#content-list").load('/admin/auditEBook/0?search='+search)
};
function searchLostFeedDispose() {
    var search = $("#search-dispose-lost").val();
    $("#content-list").load('/admin/LostFeedDispose/0?search='+search)
};
function searchArticle() {
    var search = $("#search-article").val();
    $("#content-list").load('/admin/query/article?search='+search);
};
function searchBlacklist() {
    var search = $("#search-blacklist").val();
    $("#content-list").load('/admin/query/blacklist?search='+search)
};
function searchEBook() {
    var search = $("#search-eBook").val();
    $("#content-list").load('/admin/query/eBook?search='+search)
};
function searchSubscribe() {
    var search = $("#search-subscribe").val();
    $("#content-list").load('/admin/query/subscribe?search='+search)
};
function searchFeedback() {
    var search = $("#search-feedback").val();
    $("#content-list").load('/admin/query/feedback?search='+search);
};
function searchBook() {
    var search = $("#search-book").val();
    $("#content-list").load('/admin/query/book?search='+search)
};
function searchMedia() {
    var search = $("#search-media").val();
    $("#content-list").load('/admin/query/media?search='+search)
};
function searchDownload() {
    var search = $("#search-download").val();
    $("#content-list").load('/admin/query/download?search='+search)
};
function searchUpload() {
    var search = $("#search-upload").val();
    $("#content-list").load('/admin/query/upload?search='+search);
};
function searchBorrow() {
    var search = $("#search-borrow").val();
    $("#content-list").load('/admin/query/borrow?search='+search)
};
function searchLoginLog() {
    var search = $("#search-loginLog").val();
    $("#content-list").load('/admin/query/loginLog?search='+search)
};
function searchLostFeed() {
    var search = $("#search-lostFeed").val();
    $("#content-list").load('/admin/query/lostFeed?search='+search)
};