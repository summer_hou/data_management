package cloud.xhmy.controller;

import cloud.xhmy.pojo.LoginLog;
import cloud.xhmy.service.LoginLogService;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author aliketh.xhmy
 */
@Controller
public class LoginLogController {
    @Autowired
    @Qualifier("loginLogServiceImpl")
    private LoginLogService loginLogService;


    /**
     * 删除
     * @param logId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete/loginLog")
    public JSONObject notes(@RequestParam("id") List<Integer> logId, HttpServletRequest request) {
        List<Integer> list = logId;
        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i < list.size(); i++) {
            int id = list.get(i);
            LoginLog log = loginLogService.queryById(id);
            String u = (String) request.getSession().getAttribute("avatarEmail");
            if (log.getLoginUser().equals(u)){
                jsonObject.put("code","1");
                loginLogService.deleteLogById(id);
            }else {

                jsonObject.put("msg","您无权限删除该记录！");
            }

        }
//        return "user/listNotes";


        return jsonObject;
    }

    /**
     * list all
     * @param loginUser
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/allLoginLog/{user.email}")
    public ModelAndView allLoginLog(@PathVariable("user.email") String loginUser,
                                    @RequestParam(defaultValue = "1") int page,
                                    @RequestParam(defaultValue = "10") int size){
        ModelAndView mv = new ModelAndView();
        PageHelper.startPage(page,size);
        List<LoginLog> list = loginLogService.queryAllLog(loginUser,page,size);
        PageInfo<LoginLog> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos", pageInfos);
        mv.setViewName("user/listLoginLog");
        return mv;
    }



    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */


}
