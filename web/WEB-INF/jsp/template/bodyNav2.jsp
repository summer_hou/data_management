<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/26
  Time: 19:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- 作者：袁金 -->
<!-- 请不要用于非法行为 -->
<div id="wrapper" style="left:0;">
    <!-- 侧边栏 -->
    <div class="sidebar">
        <div class="headSculpture">
            <a href="" data-toggle="modal" data-target="#exampleModal" data-whatever="upAvatar">
            <img src="${pageContext.request.contextPath}/${user.avatar}" alt=""
                 onerror="this.src='${pageContext.request.contextPath}/static/images/avatar/headSculpture.jpg'"></a>
            <p style="color: #000000;">邮箱：<a class="na" href="" onerror="">${user.email}</a></p>
        </div>

        <%--        头像弹框--%>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document" style="position: fixed;left: 50%;top: 50%; width: 340px; height: 340px; margin-top: -170px; margin-left: -170px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span id="clo" aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">跟换头像</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form-upload">
                            <section>
                                <input type="hidden" name="uId" value="${proEmail}">
                                <div class="row">

                                    <div class="" style="">
                                        <label for="avatar"><img id="avatar-img" style="width: 200px; height: 200px ;margin-left: 70px;" src="${pageContext.request.contextPath}/${user.avatar}"
                                                                 alt="" class="img-circle" onerror="this.src='${pageContext.request.contextPath}/static/images/avatar/headSculpture.jpg'"></label>
                                        <input accept="image/*" type="file" name="avatar" id="avatar" value="${avatar}" style="display: none">
                                        <span class="help-block"></span>
                                    </div>

                                </div>

                                <div class="" style="text-align: center;">
                                    <input style=" margin-right: 30px;" id="form-btn" type="button" class="btn btn-primary" value="上传" />

                                    <a href="${pageContext.request.contextPath}/refresh"><input style="" id="form-seb" type="button" class="btn btn-primary"
                                                                                                value="刷新" /></a>

                                </div>
                            </section>

                        </form>

                    </div>
                </div>
            </div>
        </div>



        <div class="option">
            <ul>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-home"></span>
							</span>
                    <!-- <p style=""><a class="na" href="home.html"></a></p> -->
                    <div class="panel-group" id="home" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingHome"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseHome" aria-expanded="true"
                                       aria-controls="collapseHome" class=" na ">
                                        首页
                                    </a>
                                </p>
                            </div>
                            <div id="collapseHome" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingHome" aria-expanded="true">
                                <div class="panel-body">

                                    <span><a class="naCh" href="${pageContext.request.contextPath}/home1">首页一</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/home2">首页二</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/home3">首页三</a></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-pencil"></span>
							</span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="record" role="tablist" aria-multiselectable="false">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingRecord"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseRecord" aria-expanded="flase"
                                       aria-controls="collapseRecord" class="collapsed na">
                                        个人笔记
                                    </a>
                                </p>
                            </div>
                            <div id="collapseRecord" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingRecord" aria-expanded="flase">
                                <div class="panel-body">

                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">添加日志</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">添加博客</a></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-upload"></span>
							</span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingUpload"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseUpload" aria-expanded="flase"
                                       aria-controls="collapseUpload" class="collapsed na">
                                        我的上传
                                    </a>
                                </p>
                            </div>
                            <div id="collapseUpload" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingUpload" aria-expanded="flase">
                                <div class="panel-body">

                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">图片</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">文档</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">压缩包</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">媒体文件</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">可执行文件</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-star"></span>
							</span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="star" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingStar"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseStar" aria-expanded="flase"
                                       aria-controls="collapseStar" class="collapsed na">
                                        我的收藏
                                    </a>
                                </p>
                            </div>
                            <div id="collapseStar" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingStar" aria-expanded="flase">
                                <div class="panel-body">

                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">个人收藏</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">引用收藏</a></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-hdd"></span>
							</span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="dat" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingData"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseData" aria-expanded="flase"
                                       aria-controls="collapseData" class="collapsed na">
                                        个人数据
                                    </a>
                                </p>
                            </div>
                            <div id="collapseData" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingData" aria-expanded="flase">
                                <div class="panel-body">

                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">总体预览</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">上传数据</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">收藏数据</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">笔记数据</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">下载数据</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">用户数据</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">操作数据</a></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-download"></span>
							</span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="down" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingDown"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseDown" aria-expanded="flase"
                                       aria-controls="collapseDown" class="collapsed na">
                                        本地下载
                                    </a>
                                </p>
                            </div>
                            <div id="collapseDown" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingDown" aria-expanded="flase">
                                <div class="panel-body">

                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">图片下载</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">文档下载</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">压缩包下载</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">可执行文件下载</a></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-th-list"></span>
							</span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="cla" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none; background: unset;">
                            <div class="panel-heading" role="tab" id="headingCla"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseCla"
                                       aria-expanded="flase"
                                       aria-controls="collapseCla" class="collapsed na">
                                        分类
                                    </a>
                                </p>
                            </div>
                            <div id="collapseCla" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingCla" aria-expanded="flase">
                                <div class="panel-body">

                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">标签分类</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">格式分类</a></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-file"></span>
							</span>
                    <!-- <p><a class="na" href=""></a></p> -->
                    <div class="panel-group" id="doc" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default"
                             style="padding: 0;margin-left: 15px;border: none;background: unset;">
                            <div class="panel-heading" role="tab" id="headingDoc"
                                 style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDoc"
                                       aria-expanded="flase"
                                       aria-controls="collapseDoc" class="collapsed na">
                                        阅读文档
                                    </a>
                                </p>
                            </div>
                            <div id="collapseDoc" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingDoc" aria-expanded="flase">
                                <div class="panel-body">

                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">项目文档</a></span><br>
                                    <span><a class="naCh" href="${pageContext.request.contextPath}/">环境配置文档</a></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-cog"></span>
							</span>
                    <p style="margin-left: 25px;"><a class="na" href="${pageContext.request.contextPath}/">设置</a></p>
                    <!-- <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default" style="padding: 0;margin-left: 15px;border: none;background: none;background-color: none;">
                            <div class="panel-heading" role="tab" id="headingSett"
                            style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSett" aria-expanded="flase"
                                     aria-controls="collapseSett" class="collapsed na">
                                        设置
                                    </a>
                                </p>
                            </div>
                            <div id="collapseSett" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSett" aria-expanded="flase">
                                <div class="panel-body">

                                        <span><a class="naCh" href="">背景设置</a></span><br>
                                        <span><a class="naCh" href="">1-2</a></span>

                                </div>
                            </div>
                        </div>
                    </div> -->
                </li>
                <li>
							<span class="ply">
								<span class="glyphicon glyphicon-road"></span>
							</span>
                    <p style="margin-left: 25px;"><a class="na" href="${pageContext.request.contextPath}/about">关于</a></p>
                    <!-- <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default" style="padding: 0;margin-left: 15px;border: none;background: none;background-color: none;">
                            <div class="panel-heading" role="tab" id="headingAbout"
                            style="padding: 0;border: unset;background: unset;border-top-right-radius:unset;border-top-left-radius:unset;border-bottom: unset;">
                                <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAbout" aria-expanded="flase"
                                     aria-controls="collapseAbout" class="collapsed na">
                                        关于
                                    </a>
                                </p>
                            </div>
                            <div id="collapseAbout" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAbout" aria-expanded="flase">
                                <div class="panel-body">

                                        <span><a class="naCh" href="">1-1</a></span><br>
                                        <span><a class="naCh" href="">1-2</a></span>

                                </div>
                            </div>
                        </div>
                    </div> -->
                </li>
            </ul>
        </div>

    </div>
    <!-- 侧边栏按钮 -->
    <button class="kj"></button>
</div>

<!-- 内容区域 -->
<div style="width: auto;height: 62px;z-index: 50;">

    <div style="margin-left: 280px;margin-top: 10px; z-index: 60; background: whitesmoke;">
        <nav class="navbar navbar-inverse "
             style="width: 100%; margin-right:50px;z-index: 50; background: rgba(10,00,50,0.8);border-radius: 1px">
            <div class="navbar-header">
                <button style="width: 44px; height: 34px;float: right;border-radius: 3px;" type="button" class="navbar-toggle collapsed"
                        data-toggle="collapse"
                        data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${pageContext.request.contextPath}/home1">DM</a>
            </div>
            <div class="container-fluid" style="">
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.request.contextPath}/home1">主页</a></li>
                        <li><a href="${pageContext.request.contextPath}/about">关于</a></li>
                        <li><a href="${pageContext.request.contextPath}/controller">控制台</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">

                        <li><a href="${pageContext.request.contextPath}/">阅读 <span class="sr-only">(current)</span></a></li>
                        <li>
                            <a style=" margin: 0; padding: 0px;" href="${pageContext.request.contextPath}/">
                                <img style="width: 50px; height: 50px;"
                                     src="${pageContext.request.contextPath}/${user.avatar}"
                                     alt="avatar"
                                     onerror="this.src='${pageContext.request.contextPath}/static/images/avatar/headSculpture.jpg'"
                                     class="img-circle"></a>
                        </li>
                        <li><a href="${pageContext.request.contextPath}/exit">退出</a></li>
                        <li><a href="${pageContext.request.contextPath}/">返回</a></li>
                    </ul>
                </div>
            </div>
        </nav>

    </div>


</div>
