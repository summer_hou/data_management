package cloud.xhmy.service;

import cloud.xhmy.dao.EBooksMapper;
import cloud.xhmy.pojo.EBooks;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public class EBooksServiceImpl implements EBooksService {

    private EBooksMapper eBooksMapper;

    public void setEBooksMapper(EBooksMapper eBooksMapper) {
        this.eBooksMapper = eBooksMapper;
    }

    public int addBook(EBooks book) {
        return this.eBooksMapper.addBook(book);
    }

    public int deleteById(int id) {
        return this.eBooksMapper.deleteById(id);
    }

    public List<EBooks> queryByName(String name, int page, int size) {
        return this.eBooksMapper.queryByName(name, page, size);
    }

    public List<EBooks> queryByMyself(String name, String user, int page, int size) {
        return this.eBooksMapper.queryByMyself(name, user, page, size);
    }

    public List<EBooks> listAll(String shelves,int page, int size) {
        return this.eBooksMapper.listAll(shelves,page, size);
    }

    public int modifyById(EBooks id) {
        return this.eBooksMapper.modifyById(id);
    }

    public EBooks queryById(int id) {
        return this.eBooksMapper.queryById(id);
    }

    public EBooks query(String name) {
        return this.eBooksMapper.query(name);
    }

    public List<EBooks> homeQuery() {
        return this.eBooksMapper.homeQuery();
    }

    public List<EBooks> queryLanguage(String language, int page, int size) {
        return this.eBooksMapper.queryLanguage(language, page, size);
    }

    public List<EBooks> queryClassify(String classify, int page, int size) {
        return this.eBooksMapper.queryClassify(classify, page, size);
    }

    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/


    public List<EBooks> listAllEBook(int page, int size) {
        return this.eBooksMapper.listAllEBook(page, size);
    }

    public List<EBooks> listAllAudit(String state, int page, int size) {
        return this.eBooksMapper.listAllAudit(state, page, size);
    }

    public int push(int id,String state) {
        return this.eBooksMapper.push(id,state);
    }

    public EBooks countNumber() {
        return this.eBooksMapper.countNumber();
    }

    public List<EBooks> listAuditEBook(String state, String name, int page, int size) {
        return this.eBooksMapper.listAuditEBook(state, name, page, size);
    }


}
