package cloud.xhmy.controller;


import cloud.xhmy.pojo.*;
import cloud.xhmy.service.*;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author aliketh.xhmy
 */
@Controller
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    @Qualifier("blogServiceImpl")
    private BlogService blogService;
    @Autowired
    @Qualifier("booksServiceImpl")
    private BooksService booksService;
    @Autowired
    @Qualifier("eBooksServiceImpl")
    private EBooksService eBooksService;
    @Autowired
    @Qualifier("usersServiceImpl")
    private UsersService usersService;
    @Autowired
    @Qualifier("notesServiceImpl")
    private NotesService notesService;
    @Autowired
    @Qualifier("mediaServiceImpl")
    private MediaService mediaService;



    /*-----------------------------------------------------------------------------------------------------------*/
    private static final Log logger = LogFactory.getLog(UploadController.class);
    /*-----------------------------------------------------------------------------------------------------------*/
    /**
     * 本地博客上传
     * @param file
     * @param request
     * @param response
     * @param model
     * @param blogRecord
     * @return
     * @throws IOException
     */
    @RequestMapping("/blog")
    @ResponseBody
    public JSONObject uploadBlog(@RequestParam("scan") MultipartFile file,
                                 HttpServletRequest request,
                                 HttpServletResponse response,
                                 Model model,
                                 @ModelAttribute BlogRecord blogRecord) throws IOException {


        //("/upload")是表示文件上传后的目标文件
        String realPath = request.getSession().getServletContext().getRealPath("/static");
        String contentPath = "/article/mdFile/";
        String path = realPath+contentPath;
        //获取文件名称
        String fileFileName = file.getOriginalFilename();
        String suffix = fileFileName.substring(fileFileName.lastIndexOf("."));
        if (!".md".equalsIgnoreCase(suffix)){
            model.addAttribute("error","抱歉您上传的文件格式不正确：当前仅支持markdown格式");
        }

        //创建File对象，传入目标路径参数和文件名称
        File dir = new File(path);

        if (!dir.exists()) {  //如果dir代表的文件不存在，则创建
            dir.mkdirs();
        }

        String filename = new Date().getTime()+suffix;
        File newFile = new File(dir, filename);

        //如果存在就执行下面操作
        file.transferTo(newFile);//将上传的实体文件复制到指定目录下
        blogRecord.setBlogState("0");
        blogRecord.setBlogContent(contentPath+filename);
        blogRecord.setBlogTime(new Date());
        blogService.addBlog(blogRecord);


        JSONObject jsonObject = new JSONObject();
        model.addAttribute("success","上传成功！");
        jsonObject.put("code","1");
        jsonObject.put("msg","成功！");
        return jsonObject;
//        ModelAndView mv = new ModelAndView();
//        mv.addObject("success","上传成功！");
//        mv.setViewName("operation/editorBlog");
//
//        return mv;


    }





    /*-----------------------------------------------------------------------------------------------------------*/

    /**
     * 保存用户上传笔记
     * @param file
     * @param request
     * @param model
     * @param notes
     * @return
     * @throws IOException
     */
    @RequestMapping("/notes")
    @ResponseBody
    public JSONObject uploadNotes(@RequestParam("scan") MultipartFile file,
                                  HttpServletRequest request,
                                  Model model,
                                  @ModelAttribute Notes notes) throws IOException {


        //("/upload")是表示文件上传后的目标文件
        String realPath = request.getSession().getServletContext().getRealPath("/static");
        String contentPath = "/upload/notes/";
        String path = realPath+contentPath;
        //获取文件名称
        String fileFileName = file.getOriginalFilename();
        String suffix = fileFileName.substring(fileFileName.lastIndexOf("."));
        if (!".md".equalsIgnoreCase(suffix)){
            model.addAttribute("error","抱歉您上传的文件格式不正确：当前仅支持markdown格式");
        }

        //创建File对象，传入目标路径参数和文件名称
        File dir = new File(path);

        if (!dir.exists()) {  //如果dir代表的文件不存在，则创建
            dir.mkdirs();
        }

        String filename = new Date().getTime()+suffix;
        File newFile = new File(dir, filename);

        //如果存在就执行下面操作
        file.transferTo(newFile);//将上传的实体文件复制到指定目录下
        notes.setNotesContent(contentPath+filename);
        notes.setNotesTime(new Date());
        notesService.addNotes(notes);


        JSONObject jsonObject = new JSONObject();
        model.addAttribute("success","上传成功！");
        jsonObject.put("code","1");
        jsonObject.put("msg","成功！");
        return jsonObject;
    }






    /*-----------------------------------------------------------------------------------------------------------*/
    /**
     * book 上传
     * @param request
     * @param model
     * @param file
     * @return
     */
    @ResponseBody
    @RequestMapping("/book")
    public JSONObject oneFileUpload( HttpServletRequest request,
                                     @ModelAttribute Books books,
                                     Model model,@RequestParam("cp") MultipartFile file){

        JSONObject jsonObject = new JSONObject();

        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String contextPath = "/books/picture/";
        String realpath = rootPath+contextPath;
        System.out.println("================================================realpath:=="+realpath);
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        String savePicName =new Date().getTime()+suffix;

        File dirPath = new File(realpath);
        if(!dirPath.exists()){
            dirPath.mkdirs();
        }

        File savePath = new File(dirPath,savePicName);
        //上传
        try {

            file.transferTo(savePath);
            books.setUuid(UUID.randomUUID().toString().replaceAll("-",""));
            books.setCoverPicture(savePicName);
            booksService.addBook(books);

            model.addAttribute("name",fileName);
            jsonObject.put("name",fileName);
            logger.info("管理员用户：["+books.getShelves()+"]成功上传："+ books.getBookName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        jsonObject.put("code","1");
        jsonObject.put("msg","成功！");
        return jsonObject;
    }


    /**
     * ebook 上传
     * @param model
     * @param request
     * @param files
     * @return
     */
    @ResponseBody
    @RequestMapping("/eBook")
    public JSONObject multiFileUpload(Model model,@ModelAttribute EBooks eBooks,
                                      HttpServletRequest request,
                                      List<MultipartFile> files) {

        JSONObject jsonObject = new JSONObject();

        String rootPath = request.getSession().getServletContext().getRealPath("/static");

        String contextPath = "/upload/eBooks/";
        String picturePath = "/upload/picture/";
        String bookRealpath = rootPath+contextPath;
        String pictureRealpath= rootPath+picturePath;
        File picDir = new File(pictureRealpath);
        File booksDir = new File(bookRealpath);
        if(!booksDir.exists() || !picDir.exists()){
            booksDir.mkdirs();
            picDir.mkdir();
        }
        //------------------------------------------------------------------------
        List<MultipartFile> listFiles = files;
        model.addAttribute("list",listFiles);
        listFiles.get(0);
        listFiles.get(1);
        //========================================================================
        /*
        listFiles.get(0);
        listFiles.get(1);
         */
        //------------------------------------------------------------------------
        String pictureNme = listFiles.get(0).getOriginalFilename();
        String bookName =  listFiles.get(1).getOriginalFilename();

        assert pictureNme != null;
        String suffixPic = pictureNme.substring(pictureNme.lastIndexOf("."));
        String suffixBook = bookName.substring(bookName.lastIndexOf("."));

        String savePictureName = new Date().getTime()+suffixPic;
        String saveBookName = UUID.randomUUID().toString().replaceAll("-","")+suffixBook;

        File picFile = new File(pictureRealpath,savePictureName);
        File bookFile = new File(bookRealpath, saveBookName);
        //上传
        try {
            listFiles.get(0).transferTo(picFile);
            listFiles.get(1).transferTo(bookFile);

            //设置保存封面图片路劲
            eBooks.setCoverPicture(picturePath+savePictureName);
            //设置保存电子书的路径，以uuid重命名
            eBooks.setUuid(contextPath+saveBookName);
            eBooks.setFormat(suffixBook);
            eBooks.setAuditStatus("0");
            //保存
            eBooksService.addBook(eBooks);
            logger.info("用户：["+eBooks.getShelves()+"]成功上传："+ eBooks.getShelves());
            jsonObject.put("code","1");
            jsonObject.put("msg","成功上传");
            return jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
        }


//        for (int i = 0; i < listFiles.size(); i++) {
//            MultipartFile file = listFiles.get(i);
//            String fileName = file.getOriginalFilename();
//            assert fileName != null;
//            String suffix = fileName.substring(fileName.lastIndexOf("."));
//            File targetFile = new File(bookRealpath,fileName);
//            //上传
//            try {
//                file.transferTo(targetFile);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }

        model.addAttribute("failure","失败");
        jsonObject.put("code","1");
        jsonObject.put("msg","成功！");
        return jsonObject;
    }



    /*-----------------------------------------------------------------------------------------------------------*/

    /**
     * audio 上传
     * @param request
     * @param model
     * @param file
     * @return
     */
    @ResponseBody
    @RequestMapping("/audio")
    public JSONObject audioUpload( HttpServletRequest request,
                                   @ModelAttribute Media audio,
                                   Model model,
                                   @RequestParam("audio") MultipartFile file){

        JSONObject jsonObject = new JSONObject();

        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String contextPath = "/upload/media/";
        String realpath = rootPath+contextPath;
        System.out.println("================================================realpath:=="+realpath);
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        String savePicName =new Date().getTime()+suffix;

        File dirPath = new File(realpath);
        if(!dirPath.exists()){
            dirPath.mkdirs();
        }

        File savePath = new File(dirPath,savePicName);
        //上传
        try {

            file.transferTo(savePath);
            audio.setMediaState("0");
            audio.setMediaContent(savePicName);
            mediaService.addMedia(audio);

            model.addAttribute("name",fileName);
            jsonObject.put("name",fileName);
            logger.info("管理员用户：["+audio.getMediaUpAuthor()+"]成功上传："+ audio.getMediaName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        jsonObject.put("code","1");
        jsonObject.put("msg","成功！");
        return jsonObject;
    }


    /**
     * video 上传
     * @param model
     * @param request
     * @param file
     * @return
     */
    @ResponseBody
    @RequestMapping("/video")
    public JSONObject videoUpload(Model model,
                                  @ModelAttribute Media video,
                                  HttpServletRequest request,
                                  @RequestParam("video") MultipartFile file) {

        JSONObject jsonObject = new JSONObject();

        // 工作空间的相对地址
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String contextPath = "/upload/media/";
        String realpath = rootPath+contextPath;
        System.out.println("================================================realpath:=="+realpath);
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        String savePicName =new Date().getTime()+suffix;

        File dirPath = new File(realpath);
        if(!dirPath.exists()){
            dirPath.mkdirs();
        }

        File savePath = new File(dirPath,savePicName);
        //上传
        try {

            file.transferTo(savePath);
            video.setMediaState("0");
            video.setMediaContent(savePicName);
            mediaService.addMedia(video);

            model.addAttribute("name",fileName);
            jsonObject.put("name",fileName);
            logger.info("管理员用户：["+video.getMediaUpAuthor()+"]成功上传："+ video.getMediaName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("failure","失败");
        jsonObject.put("code","1");
        jsonObject.put("msg","成功！");
        return jsonObject;
    }



    /*-----------------------------------------------------------------------------------------------------------*/


    /**
     * 上传图片
     *
     * @param request
     * @param
     * @param file
     * @throws IOException
     */
    @RequestMapping("/picture")
    @ResponseBody
    public Map<String ,String> uploadPro(@RequestParam(value = "pro")MultipartFile file,
                                         HttpServletRequest request,
                                         HttpSession session) throws IllegalStateException, IOException {

        String uid = String.valueOf(request.getSession().getAttribute("proEmail"));//获得用户
        Map<String, String> map = new HashMap<>();

        //1.确定保存的文件夹
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String contextPath = "/upload/image/";
        String dirPath = rootPath + contextPath;
        System.out.println("==========================================dirPath=" + dirPath);
        //会在 web 下面创建此文件夹
        System.out.println("dirPath=" + dirPath);

        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        //2.确定保存的文件名
        String orginalFilename = file.getOriginalFilename();
        int beginIndex = orginalFilename.lastIndexOf(".");
        String suffix = "";
        if (beginIndex != -1) {
            suffix = orginalFilename.substring(beginIndex);
        }
        String filename = UUID.randomUUID().toString().replaceAll("-", "") + suffix;
        //创建文件对象，表示要保存的头像文件,第一个参数表示存储的文件夹，第二个参数表示存储的文件
        File dest = new File(dir, filename);
        //执行保存?
        file.transferTo(dest);

        //更新数据表
        String fileUrl = contextPath + filename;
//        String uid = String.valueOf(request.getSession().getAttribute("proEmail"));

        System.out.println("==========================================fileUrl=" + fileUrl);

//        uploadFile.setFileName(filename);
//        uploadFile.setUuid(UUID.randomUUID().toString().replaceAll("-",""));
//        uploadFile.setFileFormat(suffix);
//        uploadFile.setFileUrl(fileUrl);
//        uploadFileService.addFile(uploadFile);
//        logger.info("用户：["+uid+"]成功上传："+ filename);
        map.put("sta", "ok");
        map.put("msg", fileUrl);
        return map;

    }

    /*-----------------------------------------------------------------------------------------------------------*/

    /**
     *
     * editor.md 文件编辑器
     * @param request
     * @param file
     * @param response
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping("/mdImages")
    public JSONObject mdImagesUpload(HttpServletRequest request,
                                     @RequestParam("editormd-image-file") MultipartFile file,
                                     HttpServletResponse response) throws IOException {
        //1.确定保存的文件夹
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String contextPath = "/article/image/";
        String dirPath = rootPath + contextPath;
        System.out.println("==========================================dirPath=" + dirPath);
//        String dirPath = session.getServletContext().getRealPath("/"+"img/avatar/user");
        //会在 web 下面创建此文件夹
        System.out.println("dirPath=" + dirPath);

        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));

        System.out.println("---------------------------"+suffix);//可有可无，自定文件保存名
        String filename = new Date().getTime()+suffix;
        File newFile = new File(dir, filename);
        //直接使用transferTo来保存文件
        file.transferTo(newFile);

        System.out.println("==========================================fileUrl=" + contextPath+filename);
        //返回JSON
        JSONObject res = new JSONObject();
        res.put("url",request.getContextPath()+"/static"+contextPath+filename);
        res.put("success", 1);
        res.put("message", "upload success!");
        return res;
    }


    /*-----------------------------------------------------------------------------------------------------------*/
    /**
     * 上传头像图片
     *
     * @param request
     * @param
     * @param file
     * @throws IOException
     */
    @RequestMapping("/avatar")
    @ResponseBody
    public Map<String ,String> uploadAvatar(@RequestParam(value = "avatar") MultipartFile file,
                                            HttpServletRequest request,
                                            HttpSession session) throws IllegalStateException, IOException{

        Map<String ,String > map = new HashMap<String ,String >();
        //从session中获得用户用户邮箱
        String uid = String.valueOf(request.getSession().getAttribute("avatarEmail"));

        //确定保存的文件夹
        String rootPath = request.getSession().getServletContext().getRealPath("/static");
        String contextPath = "/images/avatar/user/";
        String dirPath = rootPath+contextPath;
        System.out.println("==========================================dirPath"+dirPath);
        //会在 web 下面创建此文件夹
        System.out.println("dirPath="+dirPath);

        File dir = new File(dirPath);
        if(!dir.exists()) {
            dir.mkdirs();
        }

        //2.确定保存的文件名
        String orginalFilename = file.getOriginalFilename();
        int beginIndex = orginalFilename.lastIndexOf(".");
        String suffix ="";
        if(beginIndex!=-1) {
            suffix = orginalFilename.substring(beginIndex);
        }
        String filename = UUID.randomUUID().toString().replaceAll("-","")+suffix;
        //创建文件对象，表示要保存的头像文件,第一个参数表示存储的文件夹，第二个参数表示存储的文件
        File dest = new File(dir,filename);
        //执行保存?
        file.transferTo(dest);

        //更新数据表
        String avatar = contextPath+filename;


        System.out.println("==========================================avatar"+avatar);

        //初始化 Users实体类 并添加Email和avatar的地址位置
        Users pro = new Users();
        pro.setEmail(uid);
        pro.setAvatar(avatar);
        usersService.avatar(pro);

        map.put("sta","ok");
        map.put("msg",filename);
        return map;

    }


    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */

}
