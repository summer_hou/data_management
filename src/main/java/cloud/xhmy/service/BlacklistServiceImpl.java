package cloud.xhmy.service;

import cloud.xhmy.dao.BlacklistMapper;
import cloud.xhmy.pojo.Blacklist;

import java.util.List;

public class BlacklistServiceImpl implements BlacklistService {

    private BlacklistMapper blacklistMapper;

    public void setBlacklistMapper(BlacklistMapper blacklistMapper) {
        this.blacklistMapper = blacklistMapper;
    }

    public int addBlacklist(Blacklist blacklist) {
        return this.blacklistMapper.addBlacklist(blacklist);
    }

    public int deleteById(int id) {
        return this.blacklistMapper.deleteById(id);
    }

    public List<Blacklist> queryByName(String name, int page, int size) {
        return this.blacklistMapper.queryByName(name, page, size);
    }

    public List<Blacklist> listAll(int page, int size) {
        return this.blacklistMapper.listAll(page, size);
    }

    public int modifyById(int id) {
        return this.blacklistMapper.modifyById(id);
    }

    public Blacklist queryById(int id) {
        return this.blacklistMapper.queryById(id);
    }
}
