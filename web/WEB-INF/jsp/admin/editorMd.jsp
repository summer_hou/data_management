<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/22
  Time: 13:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div style="width: 90%;margin: 10px auto;">
    <h2><span>editorMd在线markdown文本编辑器</span></h2>
</div>
<div id="article-editor">
    <textarea style="display:none;" class="form-control" id="mdData" name="editormd"></textarea>
    <textarea class="editormd-html-textarea" name="text" id="editormdData"></textarea>
</div>
<script src="${pageContext.request.contextPath}/plugins/editor-md/editormd.min.js"></script>
<script type="text/javascript">
    $(function () {
        var editor = editormd("article-editor", {
            width: "90%",
            height: 700,
            path: "${pageContext.request.contextPath}/plugins/editor-md/lib/",
            imageUpload: true,
            imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL: "${pageContext.request.contextPath}/upload/mdImages",
            saveHTMLToTextarea : true
        });
    });
</script>