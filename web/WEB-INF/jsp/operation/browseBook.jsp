<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/31
  Time: 23:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>browseBook</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>


</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>


<div style=" width: 90%;margin: auto;">
    <div class="container">
        <div class="row" style="margin: auto;">

            <div class="col-md-3" style=" ">
                <img src="${pageContext.request.contextPath}/static${books.coverPicture}"
                     alt="抱歉图片意外丢失，要图片显示请重新刷新页面"
                     style=" margin: auto; padding: 10px; width: 240px;height: 320px;"
                     onerror="this.src='${pageContext.request.contextPath}/static/upload/picture/CMLS-NO-Cover.png'">
            </div>
            <div class="col-md-9" id="" style="line-height: 2em;">
                <h3>书名：${books.bookName}</h3>
                <smell>作者：${books.author}</smell><br>
                <span><p>简介：${books.introduction}</p></span>
                <span>出版社：${books.publishHouse}</span><br>
                <span>出版时间：${books.publishTime}</span><br>
                <span>语言：${books.language}</span><br>
                <span>定价：${books.price}</span><br>
                <span>类型：${books.classify}</span><br>
                <span>当前图书剩余数量：${books.bookNumber}</span>

            </div>
        </div>
        <div class="row" style="width: 90%;margin: auto;">
            <div style="margin: 20px auto;text-align: center;">
                <span><a class="btn btn-primary" href="${pageContext.request.contextPath}/subscribe/${books.id}">预约纸质图书</a></span>
                <span><a class="btn btn-primary" href="${pageContext.request.contextPath}/borrow/${books.id}">借阅</a></span>
<%--                <span><a class="btn btn-primary" href="">加入收藏</a></span>--%>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
<%--    function sendSubscribe(){--%>
<%--       window.open(encodeURI("http://localhost:8080"+${pageContext.request.contextPath}/subscribe/${books.bookName}));     //encodeURI针对整个参数进行编码--%>
<%--        &lt;%&ndash;window.open(${pageContext.request.contextPath}/subscribe/ + encodeURIComponent(${books.bookName}));  //encodeURIComponent针对单个参数进行编码&ndash;%&gt;--%>

<%--    }--%>
<%--    function sendBorrow(){--%>
<%--//        window.open(encodeURI(url + "?userName=" + userName));     //encodeURI针对整个参数进行编码--%>
<%--        window.open(${pageContext.request.contextPath}/borrow/ + encodeURIComponent(${books.bookName}));  //encodeURIComponent针对单个参数进行编码--%>

<%--    }--%>
</script>




<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>

</body>
</html>