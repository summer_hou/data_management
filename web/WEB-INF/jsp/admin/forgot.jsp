<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/7
  Time: 14:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>adminForgot</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 网页图标 ico格式 -->
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico">
    <!-- jquery -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>
    <!-- 引入 bootstrap js文件 -->
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- 引入 bootstrap css文件 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css"
          type="text/css">
    <!-- 密码的显示与隐藏 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/password-show-hidden.js"></script>

    <style>
        html, body {
            overflow: hidden;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            background: url("${pageContext.request.contextPath}/static/images/235228-1549900348f5ed.jpg");
            margin: 0 auto;
        }
        .img-back{
            filter:alpha(opacity=50);
            -moz-opacity:0.5;
            -khtml-opacity: 0.5;
            opacity: 0.5;
            width:100%;
            height: 100%;
            position:absolute;
            z-index: 10;
            background: rgba(255, 255, 255, 0.5);

        }
        /* canvas {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background:#000;
            cursor: pointer;
        } */
        div.pos{
            position:relative;
            width: 100%;
            height: 100%;
            z-index:150;
            color: goldenrod;


        }
        /*footer*/
        div.footer {
            position: absolute;
            bottom: 0px;
            right: auto;

            width: 100%;
            min-height: 50px;
            padding-top: 15px;
            color: whitesmoke;
            /* background: rgba(110,100,200,0.8); */
            text-align: center;
            z-index: 100;
        }

        .icp {
            color: white;
            text-decoration: none;
        }

        .icp:hover {
            text-decoration: none;
            color: wheat;
        }

    </style>

</head>
<body>



<div class="img-back">

</div>
<div class="pos">
    <div class="container">
        <div style="margin-top: 100px;">

        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h3 style="margin-bottom: 40px; text-align: center;">找回密码</h3>
<%--                <form method="POST">--%>

                    <div class="form-group">
                        <label for="email">注册邮箱</label>
                        <input id="email" type="email" class="form-control" name="email" value="" placeholder="请输入注册时的邮箱" required autofocus>

                    </div>
                    <div class="form-group"><label for="password">输入密码</label>
                        <input id="password" type="password" class="form-control"
                               name="password" placeholder="请输入六位以上由数字,密码,符号组成的密码" required data-eye></div>
                    <div class="form-group"><label for="checkPassword">确认密码</label>
                        <input id="checkPassword" type="password" class="form-control"
                               name="password" placeholder="确认输入的密码" required data-eye><span id="hint"></span></div>
                    <!-- <div class="form-group">
                        <label for="checkCode">验证码</label>
                        <span class="float-right" style="float: right;">看不清，点击图片</span>
                        <input id="checkCode" type="text" class="form-control" name="checkCode" style="width: 50%;" placeholder="请输入验证码">
                    </div> -->

                    <div class="form-group no-margin">
                        <div class="form-group no-margin"><button id="forgot-btn" type="button" style="width: 15%; margin: 0 auto;"
                                                                  data-toggle="modal" data-target="#exampleModal" class="btn btn-primary btn-block">找回 </button></div>
                    </div>
                    <div class="margin-top20 text-center">想起密码返回? <a href="${pageContext.request.contextPath}/admin/toLogin">登录</a></div>
<%--                </form>--%>

            </div>

            <div class="col-md-3"></div>
        </div>

    </div>
    <div class="footer">
        <p>
            <span>© 2020 xhmy.cloud . &nbsp;</span>
            <span>赣ICP证</span> &nbsp;
            <a class="icp" href="https://beian.miit.gov.cn" title="" target="_blank">赣ICP备</a>
            <span>2020012707号</span> &nbsp;
        </p>
    </div>
</div>



<div style="z-index: 1000;">
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document" style="position: fixed;left: 50%;top: 50%; width: 350px; height: 304px; margin-top: -175px; margin-left: -152px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span id="clo" aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">拖动图片验证</h4>
                </div>
                <div class="modal-body">
                    <div id="captcha"></div>
                </div>
            </div>
        </div>
    </div>

</div>


<script src="${pageContext.request.contextPath}/static/js/admin/admin-jigsaw.min.js"></script>
<script>
    jigsaw.init({
        el: document.getElementById('captcha'),
        onSuccess: function() {
            // alert("success")
            // $("#forgot-btn").click(function() {
                //alert(html);
                $.ajax({
                    type: 'post',
                    url: '${pageContext.request.contextPath}/admin/forgot',
                    data: {
                        'email': $("#email").val(),
                        'password': $("#password").val()
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.code == "1") {
                            window.location.href = "${pageContext.request.contextPath}/admin/toLogin";
                        } else {
                            alert(data.msg);
                        }
                    }
                })
            // });
        },
        onFail: cleanMsg,
        onRefresh: cleanMsg
    });

    function cleanMsg() {
        alert("failure：验证失败")
    };
</script>
<script type="text/javascript">
    //验证两次输入密码是否一致
    function checkPassword() {
        var password = document.getElementById("password").value;
        var chkPassword = document.getElementById("checkPassword").value;

        if(password == chkPassword) {
            document.getElementById("hint").innerHTML="<br><font color='green'>两次密码输入一致</font>";
            document.getElementById("submit").disabled = false;

        }else {
            document.getElementById("hint").innerHTML="<br><font color='red'>两次输入密码不一致!</font>";
            document.getElementById("submit").disabled = true;
        }
    }
</script>
</body>
</html>
