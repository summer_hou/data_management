<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/31
  Time: 23:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>modifyBook</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/head.jsp"/>


</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/bodyNav.jsp"/>


<div style="width: 90%; margin: 10px auto;">
    <%--
        <div class="tabbable" id="tabs-736228">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a href="#panel-364990" data-toggle="tab">书籍上架</a>
                </li>
                <li>
                    <a href="#panel-230698" data-toggle="tab">电子书上传</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="panel-364990"  style="width: 100%;m-right: 0px;line-height: 2em;font-size: 16px;">


                    <form id="form-book" enctype="multipart/form-data">
                        <input type="hidden" id=" id" name="bookId" value="${book.id}">
                        <div>
                            <span>封面图片：</span><input id="coverPicture"  name="cp" type="file">
                            <span></span>
                        </div>
                        <div>
                            <span>书名：</span><input class="form-control" id="bookName" name="bookName" value="${book.bookName}" type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>作者：</span><input class="form-control" id="author" name="author" value="${book.author}" type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>出版时间：</span><input class="form-control" id="publishTime" name="publishTime" value="${book.publishTime}" type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>语言：</span><input class="form-control" id="language" name="language" value="${book.language}" type="text">
                            <span></span>
                        </div>

                        <div>
                            <span>出版社：</span><input class="form-control" id="publishHouse" name="publishHouse" value="${book.publishHouse}" type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>上架者：</span><input class="form-control" id="shelves" name="shelves" value="${book.shelves}" readonly type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>简介：</span><input class="form-control" id="introduction" name="introduction" value="${book.introduction}" type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>价格：</span><input class="form-control" id="price" name="price" value="${book.price}" type="text">
                            <span></span>
                        </div>
                        <div>
                            <span>分类：</span><input class="form-control" id="classify" name="classify" value="${book.classify}" type="text">
                            <span></span>
                        </div>

                        <div style="text-align: center; margin: 20px auto;">
                            <input id="saveBook" type="button" class="btn btn-primary" value="上传">
                        </div>
                    </form>
                    <span>${success}</span>
                </div>
                --%>
    <div class="tab-pane" id="panel-230698" style="width: 100%;m-right: 0px;line-height: 2em;font-size: 16px;">

        <form id="from-eBook" method="post" enctype="multipart/form-data">
            <input type="hidden" id="id" name="eBookId" value="${eBook.id}">
            <div>
                <span>封面图片：</span><input name="files" type="file">
                <span></span>
            </div>
            <div>
                <span>书名：</span><input class="form-control" name="bookName" value="${eBook.bookName}" type="text"><br>
                <span></span><input name="files" type="file">
            </div>
            <div>
                <span>作者：</span><input class="form-control" name="author" value="${eBook.author}" type="text">
                <span></span>
            </div>
            <div>
                <span>出版时间：</span><input class="form-control" name="publishTime" value="${eBook.publishTime}"
                                         type="text">
                <span></span>
            </div>
            <div>
                <span>语言：</span><input class="form-control" name="language" value="${eBook.language}" type="text">
                <span></span>
            </div>

            <div>
                <span>出版社：</span><input class="form-control" name="publishHouse" value="${eBook.publishHouse}"
                                        type="text">
                <span></span>
            </div>
            <div>
                <span>上架者：</span><input class="form-control" name="shelves" value="${eBook.shelves}" readonly
                                        type="text">
                <span></span>
            </div>
            <div>
                <span>简介：</span><input class="form-control" name="introduction" value="${eBook.introduction}"
                                       type="text">
                <span></span>
            </div>
            <div>
                <span>简述：</span><input class="form-control" name="description" value="${eBook.description}" type="text">
                <span></span>
            </div>
            <div>
                <span>分类：</span><input class="form-control" name="classify" value="${eBook.classify}" type="text">
                <span></span>
            </div>
            <%--                    <div>--%>
            <%--                        <span>上传状态：</span><input class="form-control" name="auditStatus" value="${eBook.auditStatus}" type="text">--%>
            <%--                        <span></span>--%>
            <%--                    </div>--%>
            <div>
                <span>格式：</span><input class="form-control" name="format" value="${eBook.format}" type="text">
                <span></span>
            </div>
            <div style="text-align: center; margin: 20px auto;">
                <input id="saveEBook" type="submit" class="btn btn-primary" value="修改">
            </div>
        </form>
    </div>
    <%--
            </div>
        </div>
    --%>

</div>


<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/template/footer.jsp"/>

<script type="text/javascript">
    $("#saveBook").click(function () {
        var formData = new FormData(document.getElementById("form-eBook"));
        $.ajax({
            type: 'post',
            url: '${pageContext.request.contextPath}/modifyBook',

            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',

            success: function (data) {
                alert(data);
            }
        })
    });
    $("#saveEBook").click(function () {
        var formData = new FormData(document.getElementById("form-eBook"));
        $.ajax({
            type: 'post',
            url: '${pageContext.request.contextPath}/modifyEBook',
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',

            success: function (data) {
                alert(data);
            }
        })
    });
</script>

</body>
</html>
