<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/24
  Time: 17:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>record</title>


    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/browse-cql.js"></script>

    <style>
        /*===========================================================*/
        /*footer*/
        div.footer {
            position: relative;
            bottom: 0px;
            right: auto;

            color: snow;
            width: 100%;
            min-height: 50px;
            padding-top: 15px;
            background: #081325;
            text-align: center;
            z-index: 50;
        }

        .icp {
            color: white;
        }

        .icp:hover {
            text-decoration: none;
            color: wheat;
        }

    </style>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/editor-md/css/editormd.min.css">

</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    综合图书管理系统<small>&nbsp; &nbsp;浏览页面</small>
                </h1>
                <span style="float: right;margin-top: 10px; font-size: 16px; margin-right: 5px;"><a
                        href="${pageContext.request.contextPath}/toLogin">登录</a> || <a
                        href="${pageContext.request.contextPath}/toRegister">注册</a> || <a
                        href="javascript:goback()">返回</a></span>
            </div>
            <div style="height: 20px"></div>
            <div style=" width: 100% ;margin:0 auto; z-index: 50;">
                <!-- 搜索框 -->
                <div style="margin: 10px auto 30px auto;">
                    <form action="${pageContext.request.contextPath}/browse/retrieve">
                        <input style="width: 90%;height: 38px; padding: 5px; border-radius: 2px; border: 1px solid #666666;"
                               name="search"
                               value="${search}" placeholder="全站搜索（输入要搜索的名称）" type="search">
                        <button style="width: 10%; height: 40px;margin-left: -5px;" type="submit">搜索</button>
                    </form>

                </div>
                <div style="margin-right: 0px; width: auto ;margin-top: -30px; float: right;background: unset;">
                    <div class="btn-group" style="margin-right: -5px;">
                        <button class="btn btn-default" style="border: unset; background: unset;">语言</button>
                        <button style="border: unset; background: unset;" data-toggle="dropdown"
                                class="btn btn-default dropdown-toggle"><span
                                class="caret"></span></button>
                        <ul class="dropdown-menu" style=" left: unset;right: 0;">
                            <li><a href="#">所有语言</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:byLanguage(0)">Chinese</a></li>
                            <li><a href="javascript:byLanguage(1)">English</a></li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <button style="border: unset; background: unset;" class="btn btn-default">分类</button>
                        <button style="border: unset; background: unset;" data-toggle="dropdown"
                                class="btn btn-default dropdown-toggle"><span
                                class="caret"></span></button>
                        <ul class="dropdown-menu" style=" left: unset;right: 0;">
                            <li>
                                <a href="javascript:classify(0)">所有类型</a>
                            </li>
                            <li class="divider">
                            </li>
                            <li><a href="javascript:byClassify(1)">艺术</a></li>
                            <li><a href="javascript:byClassify(2)">商业</a></li>
                            <li><a href="javascript:byClassify(3)">化学</a></li>
                            <li><a href="javascript:byClassify(4)">经济</a></li>
                            <li><a href="javascript:byClassify(5)">教育</a></li>
                            <li><a href="javascript:byClassify(6)">地理</a></li>
                            <li><a href="javascript:byClassify(7)">地质</a></li>
                            <li><a href="javascript:byClassify(8)">历史</a></li>
                            <li><a href="javascript:byClassify(9)">休闲</a></li>
                            <li><a href="javascript:byClassify(10)">法学</a></li>
                            <li><a href="javascript:byClassify(11)">宗教</a></li>
                            <li><a href="javascript:byClassify(12)">技术</a></li>
                            <li><a href="javascript:byClassify(13)">工艺</a></li>
                            <li><a href="javascript:byClassify(14)">文学</a></li>
                            <li><a href="javascript:byClassify(15)">数学</a></li>
                            <li><a href="javascript:byClassify(16)">医学</a></li>
                            <li><a href="javascript:byClassify(17)">物理</a></li>
                            <li><a href="javascript:byClassify(18)">心理学</a></li>
                            <li><a href="javascript:byClassify(19)">计算机</a></li>
                            <li><a href="javascript:byClassify(20)">生物学</a></li>
                            <li><a href="javascript:byClassify(21)">语言学</a></li>
                            <li><a href="javascript:byClassify(22)">社会科学</a></li>
                            <li><a href="javascript:byClassify(23)">体育与运动</a></li>
                            <li><a href="javascript:byClassify(24)">科学(通用)</a></li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style=" width: 90%;margin: auto;">
    <from>
        <input type="hidden" name="id" value="${blog.id}" >
    </from>
    <div style="text-align: center"><h1>${blog.blogName}</h1><span>作者：${blog.blogAuthor}</span> <span style="margin-left: 100px;margin-right: 100px">主题：${blog.blogTitle}</span> <span>时间：${blog.blogTime}</span></div>
    <div id="article-content">
        <textarea style="display:none;" id="blogContent"></textarea>
    </div>

</div>
<div class="footer">
    <p>
        <span>© 2020 xhmy.cloud . &nbsp;</span>
        <span>赣ICP证</span> &nbsp;
        <a class="icp"
           href="https://beian.miit.gov.cn"
           title=""
           target="_blank">赣ICP备</a>
        <span>2020012707号</span> &nbsp;
    </p>
</div>


<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/codemirror/codemirror.min.js"></script>
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/examples/js/jquery.min.js"></script>--%>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/marked.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/prettify.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/raphael.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/underscore.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/sequence-diagram.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/flowchart.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/lib/jquery.flowchart.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/editor-md/editormd.min.js"></script>


<script type="text/javascript">
    $("#article-content").load("${pageContext.request.contextPath}/static${blog.blogContent}");
    var testEditor;
    $(function () {
        testEditor = editormd.markdownToHTML("article-content", {//注意：这里是上面DIV的id
            htmlDecode: "style,script,iframe",
            emoji: true,
            taskList: true,
            tex: true, // 默认不解析
            flowChart: true, // 默认不解析
            sequenceDiagram: true, // 默认不解析
            codeFold: true,
        });});
</script>


<script type="text/javascript">

</script>
</body>
</html>
