package cloud.xhmy.controller;

import cloud.xhmy.pojo.Notes;
import cloud.xhmy.service.NotesService;
import cloud.xhmy.util.files.FilesDelete;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Date;
import java.util.List;

/**
 * @author aliketh.xhmy
 */
@Controller
public class NotesController {
    @Autowired
    @Qualifier("notesServiceImpl")
    private NotesService notesService;


    /**
     * 删除笔记
     *
     * @param notesId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete/notes")
    public JSONObject notes(@RequestParam("id") List<Integer> notesId, HttpServletRequest request) {
        List<Integer> list = notesId;

        Notes notes;
        for (int i = 0; i < list.size(); i++) {
            int id = list.get(i);
            notes = notesService.queryById(id);
            FilesDelete.delete(notes.getNotesContent(), request);
            notesService.deleteById(id);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", "1");
        return jsonObject;

//        return "user/listNotes";
    }

    /**
     * 跳转到笔记页面
     *
     * @return
     */
    @RequestMapping("/toNotes")
    public String toNotes() {
        return "user/notes";
    }

    /**
     * 跳转到笔记修改页面
     *
     * @return
     */
    @RequestMapping("/toModifyNotes/{id}")
    public String toModifyNotes(@PathVariable int id, Model model) {
        System.out.println("====================================" + id);
        Notes notes = notesService.queryById(id);
        model.addAttribute("notes", notes);
        return "user/modifyNotes";
    }

    /**
     * 跳转到笔记预览页面
     *
     * @param id
     * @return
     */
    @RequestMapping("/toReadNotes/{id}")
    public String toReadNotes(@PathVariable int id, Model model) {


        Notes notes = notesService.queryById(id);
        model.addAttribute("notes", notes);
        return "user/readNotes";
    }

    /**
     * 列出个人所有笔记
     *
     * @param author
     * @param page
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("/allNotes/{user.email}")
    public ModelAndView allNotes(@PathVariable("user.email") String author, @RequestParam(defaultValue = "1") int page,
                                 @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Notes> list = notesService.listAll(author, page, size);
        PageInfo<Notes> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos", pageInfos);
        mv.setViewName("user/listNotes");
        return mv;
    }

    /**
     * 保存日记
     *
     * @param notes
     * @param request
     * @param model
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping("/saveNotes")
    public JSONObject saveNotes(@ModelAttribute Notes notes, HttpServletRequest request, Model model) throws IOException {

        //("/")是表示文件上传后的目标文件
        String realPath = request.getSession().getServletContext().getRealPath("/static");
        String contentPath = "/upload/notes/";
        String path = realPath + contentPath;

        //创建File对象，传入目标路径参数和文件名称
        File dir = new File(path);

        if (!dir.exists()) {  //如果dir代表的文件不存在，则创建
            dir.mkdirs();
        }
        System.out.println("===============================" + notes);

        String suffix = ".md";

        Date date = new Date();
        String fileName = date.getTime() + suffix;


        notes.setNotesContent(contentPath + fileName);
        notes.setNotesTime(new Date());
        notesService.addNotes(notes);


        String destFile = path + fileName;
        //换编码格式保存
        OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(destFile), "UTF-8");
        os.write(request.getParameter("data"));
        os.close();
        //不转换编码格式保存
//        FileWriter art = new FileWriter(destFile);
//        art.write(request.getParameter("data"));
//        art.flush();
//        art.close();


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", "1");
        jsonObject.put("msg", "成功！");
        return jsonObject;
//        model.addAttribute("success","保存成功");
//        return "user/notes";
    }

    /**
     * 查询
     *
     * @param name
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/queryNotes/{byName}")
    public ModelAndView queryNotes(@PathVariable("byName") String name,
                                   @RequestParam(defaultValue = "1") int page,
                                   @RequestParam(defaultValue = "10") int size) {
        PageHelper.startPage(page, size);
        ModelAndView mv = new ModelAndView();
        List<Notes> list = notesService.queryByName(name, page, size);
        PageInfo<Notes> pageInfos = new PageInfo<>(list);
        mv.addObject("pageInfos", pageInfos);
        mv.setViewName("user/listNotes");
        return mv;

    }
}
