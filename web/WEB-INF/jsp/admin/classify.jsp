<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/22
  Time: 22:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container">
    <div class="row">

        <div class="col-md-9">
            <h2>引用zlibrary</h2>
            <div class="categoryList" style="text-align:left;overflow:hidden;">
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">art</h2>
                    <ul>
                        <li><a href="Cinema-cat60" title="Search ebooks in category Cinema">Cinema</a></li>
                        <li><a href="Design-Architecture-cat58" title="Search ebooks in category Design: Architecture">Design:
                            Architecture</a></li>
                        <li><a href="Graphic-Arts-cat59" title="Search ebooks in category Graphic Arts">Graphic Arts</a></li>
                        <li><a href="Music-cat61" title="Search ebooks in category Music">Music</a></li>
                        <li><a href="Music-Guitar-cat62" title="Search ebooks in category Music: Guitar">Music: Guitar</a></li>
                        <li><a href="Photo-cat63" title="Search ebooks in category Photo">Photo</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">biology</h2>
                    <ul>
                        <li><a href="Anthropology-cat14" title="Search ebooks in category Anthropology">Anthropology</a></li>
                        <li><a href="Anthropology-Evolution-cat15" title="Search ebooks in category Anthropology: Evolution">Anthropology:
                            Evolution</a></li>
                        <li><a href="Biochemistry-cat19" title="Search ebooks in category Biochemistry">Biochemistry</a></li>
                        <li><a href="Biochemistry-enologist-cat20" title="Search ebooks in category Biochemistry: enologist">Biochemistry:
                            enologist</a></li>
                        <li><a href="Biophysics-cat18" title="Search ebooks in category Biophysics">Biophysics</a></li>
                        <li><a href="Biostatistics-cat16" title="Search ebooks in category Biostatistics">Biostatistics</a></li>
                        <li><a href="Biotechnology-cat17" title="Search ebooks in category Biotechnology">Biotechnology</a></li>
                        <li><a href="Ecology-cat31" title="Search ebooks in category Ecology">Ecology</a></li>
                        <li><a href="Estestvoznananie-cat13" title="Search ebooks in category Estestvoznananie">Estestvoznananie</a></li>
                        <li><a href="Genetics-cat22" title="Search ebooks in category Genetics">Genetics</a></li>
                        <li><a href="Microbiology-cat26" title="Search ebooks in category Microbiology">Microbiology</a></li>
                        <li><a href="Molecular-cat27" title="Search ebooks in category Molecular">Molecular</a></li>
                        <li><a href="Molecular-Bioinformatics-cat28" title="Search ebooks in category Molecular: Bioinformatics">Molecular:
                            Bioinformatics</a></li>
                        <li><a href="Plants-Agriculture-and-Forestry-cat30" title="Search ebooks in category Plants: Agriculture and Forestry">Plants:
                            Agriculture and Forestry</a></li>
                        <li><a href="Plants-Botany-cat29" title="Search ebooks in category Plants: Botany">Plants: Botany</a></li>
                        <li><a href="Virology-cat21" title="Search ebooks in category Virology">Virology</a></li>
                        <li><a href="Zoology-cat23" title="Search ebooks in category Zoology">Zoology</a></li>
                        <li><a href="Zoology-Fish-cat25" title="Search ebooks in category Zoology: Fish">Zoology: Fish</a></li>
                        <li><a href="Zoology-Paleontology-cat24" title="Search ebooks in category Zoology: Paleontology">Zoology:
                            Paleontology</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">business</h2>
                    <ul>
                        <li><a href="Accounting-cat2" title="Search ebooks in category Accounting">Accounting</a></li>
                        <li><a href="E-Commerce-cat11" title="Search ebooks in category E-Commerce">E-Commerce</a></li>
                        <li><a href="Logistics-cat3" title="Search ebooks in category Logistics">Logistics</a></li>
                        <li><a href="Management-cat6" title="Search ebooks in category Management">Management</a></li>
                        <li><a href="Management-Project-Management-cat7" title="Search ebooks in category Management: Project Management">Management:
                            Project Management</a></li>
                        <li><a href="Marketing-cat4" title="Search ebooks in category Marketing">Marketing</a></li>
                        <li><a href="Marketing-Advertising-cat5" title="Search ebooks in category Marketing: Advertising">Marketing:
                            Advertising</a></li>
                        <li><a href="MLM-cat8" title="Search ebooks in category MLM">MLM</a></li>
                        <li><a href="Responsibility-and-Business-Ethics-cat9" title="Search ebooks in category Responsibility and Business Ethics">Responsibility
                            and Business Ethics</a></li>
                        <li><a href="Trading-cat10" title="Search ebooks in category Trading">Trading</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">chemistry</h2>
                    <ul>
                        <li><a href="Analytical-Chemistry-cat297" title="Search ebooks in category Analytical Chemistry">Analytical
                            Chemistry</a></li>
                        <li><a href="Chemical-cat304" title="Search ebooks in category Chemical">Chemical</a></li>
                        <li><a href="Inorganic-Chemistry-cat299" title="Search ebooks in category Inorganic Chemistry">Inorganic
                            Chemistry</a></li>
                        <li><a href="Materials-cat298" title="Search ebooks in category Materials">Materials</a></li>
                        <li><a href="Organic-Chemistry-cat300" title="Search ebooks in category Organic Chemistry">Organic Chemistry</a></li>
                        <li><a href="Pharmacology-cat302" title="Search ebooks in category Pharmacology">Pharmacology</a></li>
                        <li><a href="Physical-Chemistry-cat303" title="Search ebooks in category Physical Chemistry">Physical Chemistry</a></li>
                        <li><a href="Pyrotechnics-and-explosives-cat301" title="Search ebooks in category Pyrotechnics and explosives">Pyrotechnics
                            and explosives</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">computers</h2>
                    <ul>
                        <li><a href="Algorithms-and-Data-Structures-cat71" title="Search ebooks in category Algorithms and Data Structures">Algorithms
                            and Data Structures</a></li>
                        <li><a href="Algorithms-and-Data-Structures-Cryptography-cat72" title="Search ebooks in category Algorithms and Data Structures: Cryptography">Algorithms
                            and Data Structures: Cryptography</a></li>
                        <li><a href="Algorithms-and-Data-Structures-Digital-watermarks-cat75" title="Search ebooks in category Algorithms and Data Structures: Digital watermarks">Algorithms
                            and Data Structures: Digital watermarks</a></li>
                        <li><a href="Algorithms-and-Data-Structures-Image-Processing-cat73" title="Search ebooks in category Algorithms and Data Structures: Image Processing">Algorithms
                            and Data Structures: Image Processing</a></li>
                        <li><a href="Algorithms-and-Data-Structures-Pattern-Recognition-cat74" title="Search ebooks in category Algorithms and Data Structures: Pattern Recognition">Algorithms
                            and Data Structures: Pattern Recognition</a></li>
                        <li><a href="Cryptography-cat82" title="Search ebooks in category Cryptography">Cryptography</a></li>
                        <li><a href="Cybernetics-cat80" title="Search ebooks in category Cybernetics">Cybernetics</a></li>
                        <li><a href="Cybernetics-Artificial-Intelligence-cat81" title="Search ebooks in category Cybernetics: Artificial Intelligence">Cybernetics:
                            Artificial Intelligence</a></li>
                        <li><a href="Databases-cat76" title="Search ebooks in category Databases">Databases</a></li>
                        <li><a href="Information-Systems-cat78" title="Search ebooks in category Information Systems">Information
                            Systems</a></li>
                        <li><a href="Information-Systems-EC-businesses-cat79" title="Search ebooks in category Information Systems: EC businesses">Information
                            Systems: EC businesses</a></li>
                        <li><a href="Lectures-monographs-cat83" title="Search ebooks in category Lectures, monographs">Lectures,
                            monographs</a></li>
                        <li><a href="Media-cat84" title="Search ebooks in category Media">Media</a></li>
                        <li><a href="Networking-cat99" title="Search ebooks in category Networking">Networking</a></li>
                        <li><a href="Networking-Internet-cat100" title="Search ebooks in category Networking: Internet">Networking:
                            Internet</a></li>
                        <li><a href="Operating-Systems-cat85" title="Search ebooks in category Operating Systems">Operating Systems</a></li>
                        <li><a href="Organization-and-Data-Processing-cat86" title="Search ebooks in category Organization and Data Processing">Organization
                            and Data Processing</a></li>
                        <li><a href="Programming-cat87" title="Search ebooks in category Programming">Programming</a></li>
                        <li><a href="Programming-Compilers-cat90" title="Search ebooks in category Programming: Compilers">Programming:
                            Compilers</a></li>
                        <li><a href="Programming-Games-cat89" title="Search ebooks in category Programming: Games">Programming: Games</a></li>
                        <li><a href="Programming-Libraries-API-cat88" title="Search ebooks in category Programming: Libraries API">Programming:
                            Libraries API</a></li>
                        <li><a href="Programming-Modeling-languages-cat91" title="Search ebooks in category Programming: Modeling languages">Programming:
                            Modeling languages</a></li>
                        <li><a href="Programming-Programming-Languages-cat92" title="Search ebooks in category Programming: Programming Languages">Programming:
                            Programming Languages</a></li>
                        <li><a href="Programs-TeX-LaTeX-cat93" title="Search ebooks in category Programs: TeX, LaTeX">Programs: TeX,
                            LaTeX</a></li>
                        <li><a href="Security-cat77" title="Search ebooks in category Security">Security</a></li>
                        <li><a href="Software-Adobe-Products-cat95" title="Search ebooks in category Software: Adobe Products">Software:
                            Adobe Products</a></li>
                        <li><a href="Software-CAD-cat97" title="Search ebooks in category Software: CAD">Software: CAD</a></li>
                        <li><a href="Software-Macromedia-Products-cat96" title="Search ebooks in category Software: Macromedia Products">Software:
                            Macromedia Products</a></li>
                        <li><a href="Software-Office-software-cat94" title="Search ebooks in category Software: Office software">Software:
                            Office software</a></li>
                        <li><a href="Software-Systems-scientific-computing-cat98" title="Search ebooks in category Software: Systems: scientific computing">Software:
                            Systems: scientific computing</a></li>
                        <li><a href="System-Administration-cat101" title="Search ebooks in category System Administration">System
                            Administration</a></li>
                        <li><a href="Web-design-cat70" title="Search ebooks in category Web-design">Web-design</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">economy</h2>
                    <ul>
                        <li><a href="Econometrics-cat310" title="Search ebooks in category Econometrics">Econometrics</a></li>
                        <li><a href="Investing-cat306" title="Search ebooks in category Investing">Investing</a></li>
                        <li><a href="Markets-cat309" title="Search ebooks in category Markets">Markets</a></li>
                        <li><a href="Mathematical-Economics-cat307" title="Search ebooks in category Mathematical Economics">Mathematical
                            Economics</a></li>
                        <li><a href="Popular-cat308" title="Search ebooks in category Popular">Popular</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">education</h2>
                    <ul>
                        <li><a href="Elementary-cat187" title="Search ebooks in category Elementary">Elementary</a></li>
                        <li><a href="Encyclopedia-cat188" title="Search ebooks in category Encyclopedia">Encyclopedia</a></li>
                        <li><a href="International-Conferences-and-Symposiums-cat185" title="Search ebooks in category International Conferences and Symposiums">International
                            Conferences and Symposiums</a></li>
                        <li><a href="self-help-books-cat186" title="Search ebooks in category self-help books">self-help books</a></li>
                        <li><a href="Theses-abstracts-cat184" title="Search ebooks in category Theses abstracts">Theses abstracts</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">geography</h2>
                    <ul>
                        <li><a href="Geodesy-Cartography-cat33" title="Search ebooks in category Geodesy. Cartography">Geodesy.
                            Cartography</a></li>
                        <li><a href="Local-History-cat34" title="Search ebooks in category Local History">Local History</a></li>
                        <li><a href="Local-history-Tourism-cat35" title="Search ebooks in category Local history: Tourism">Local
                            history: Tourism</a></li>
                        <li><a href="Meteorology-Climatology-cat36" title="Search ebooks in category Meteorology, Climatology">Meteorology,
                            Climatology</a></li>
                        <li><a href="Russia-cat37" title="Search ebooks in category Russia">Russia</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">geology</h2>
                    <ul>
                        <li><a href="Hydrogeology-cat39" title="Search ebooks in category Hydrogeology">Hydrogeology</a></li>
                        <li><a href="Mining-cat40" title="Search ebooks in category Mining">Mining</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">history</h2>
                    <ul>
                        <li><a href="American-Studies-cat65" title="Search ebooks in category American Studies">American Studies</a></li>
                        <li><a href="Archaeology-cat66" title="Search ebooks in category Archaeology">Archaeology</a></li>
                        <li><a href="Memoirs-Biographies-cat68" title="Search ebooks in category Memoirs, Biographies">Memoirs,
                            Biographies</a></li>
                        <li><a href="Military-History-cat67" title="Search ebooks in category Military History">Military History</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">housekeeping, leisure</h2>
                    <ul>
                        <li><a href="Aquaria-cat42" title="Search ebooks in category Aquaria">Aquaria</a></li>
                        <li><a href="Astrology-cat43" title="Search ebooks in category Astrology">Astrology</a></li>
                        <li><a href="Beauty-image-cat48" title="Search ebooks in category Beauty, image">Beauty, image</a></li>
                        <li><a href="Benefits-Homebrew-cat52" title="Search ebooks in category Benefits Homebrew">Benefits Homebrew</a></li>
                        <li><a href="Collecting-cat47" title="Search ebooks in category Collecting">Collecting</a></li>
                        <li><a href="Cooking-cat49" title="Search ebooks in category Cooking">Cooking</a></li>
                        <li><a href="Fashion-Jewelry-cat50" title="Search ebooks in category Fashion, Jewelry">Fashion, Jewelry</a></li>
                        <li><a href="Games-Board-Games-cat45" title="Search ebooks in category Games: Board Games">Games: Board Games</a></li>
                        <li><a href="Games-Chess-cat46" title="Search ebooks in category Games: Chess">Games: Chess</a></li>
                        <li><a href="Garden-garden-cat56" title="Search ebooks in category Garden, garden">Garden, garden</a></li>
                        <li><a href="Handicraft-cat54" title="Search ebooks in category Handicraft">Handicraft</a></li>
                        <li><a href="Handicraft-Cutting-and-Sewing-cat55" title="Search ebooks in category Handicraft: Cutting and Sewing">Handicraft:
                            Cutting and Sewing</a></li>
                        <li><a href="Hunting-and-Game-Management-cat51" title="Search ebooks in category Hunting and Game Management">Hunting
                            and Game Management</a></li>
                        <li><a href="Pet-cat44" title="Search ebooks in category Pet">Pet</a></li>
                        <li><a href="Professions-and-Trades-cat53" title="Search ebooks in category Professions and Trades">Professions
                            and Trades</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">jurisprudence</h2>
                    <ul>
                        <li><a href="Criminology-Court-examination-cat312" title="Search ebooks in category Criminology: Court. examination">Criminology:
                            Court. examination</a></li>
                        <li><a href="Law-cat313" title="Search ebooks in category Law">Law</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">linguistics</h2>
                    <ul>
                        <li><a href="Comparative-Studies-cat318" title="Search ebooks in category Comparative Studies">Comparative
                            Studies</a></li>
                        <li><a href="Dictionaries-cat322" title="Search ebooks in category Dictionaries">Dictionaries</a></li>
                        <li><a href="Foreign-cat315" title="Search ebooks in category Foreign">Foreign</a></li>
                        <li><a href="Foreign-English-cat316" title="Search ebooks in category Foreign: English">Foreign: English</a></li>
                        <li><a href="Foreign-French-cat317" title="Search ebooks in category Foreign: French">Foreign: French</a></li>
                        <li><a href="Linguistics-cat319" title="Search ebooks in category Linguistics">Linguistics</a></li>
                        <li><a href="Rhetoric-cat320" title="Search ebooks in category Rhetoric">Rhetoric</a></li>
                        <li><a href="Russian-Language-cat321" title="Search ebooks in category Russian Language">Russian Language</a></li>
                        <li><a href="Stylistics-cat323" title="Search ebooks in category Stylistics">Stylistics</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">literature</h2>
                    <ul>
                        <li><a href="Children-cat106" title="Search ebooks in category Children">Children</a></li>
                        <li><a href="Comics-cat107" title="Search ebooks in category Comics">Comics</a></li>
                        <li><a href="Detective-cat105" title="Search ebooks in category Detective">Detective</a></li>
                        <li><a href="Fantasy-cat112" title="Search ebooks in category Fantasy">Fantasy</a></li>
                        <li><a href="Fiction-cat103" title="Search ebooks in category Fiction">Fiction</a></li>
                        <li><a href="Folklore-cat111" title="Search ebooks in category Folklore">Folklore</a></li>
                        <li><a href="Library-cat104" title="Search ebooks in category Library">Library</a></li>
                        <li><a href="Literary-cat108" title="Search ebooks in category Literary">Literary</a></li>
                        <li><a href="Poetry-cat109" title="Search ebooks in category Poetry">Poetry</a></li>
                        <li><a href="Prose-cat110" title="Search ebooks in category Prose">Prose</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">mathematics</h2>
                    <ul>
                        <li><a href="Algebra-cat114" title="Search ebooks in category Algebra">Algebra</a></li>
                        <li><a href="Algebra-Linear-Algebra-cat115" title="Search ebooks in category Algebra: Linear Algebra">Algebra:
                            Linear Algebra</a></li>
                        <li><a href="Algorithms-and-Data-Structures-cat116" title="Search ebooks in category Algorithms and Data Structures">Algorithms
                            and Data Structures</a></li>
                        <li><a href="Analysis-cat117" title="Search ebooks in category Analysis">Analysis</a></li>
                        <li><a href="Applied-Mathematicsematics-cat137" title="Search ebooks in category Applied Mathematicsematics">Applied
                            Mathematicsematics</a></li>
                        <li><a href="Automatic-Control-Theory-cat139" title="Search ebooks in category Automatic Control Theory">Automatic
                            Control Theory</a></li>
                        <li><a href="Combinatorics-cat126" title="Search ebooks in category Combinatorics">Combinatorics</a></li>
                        <li><a href="Computational-Mathematics-cat120" title="Search ebooks in category Computational Mathematics">Computational
                            Mathematics</a></li>
                        <li><a href="Computer-Algebra-cat128" title="Search ebooks in category Computer Algebra">Computer Algebra</a></li>
                        <li><a href="Continued-fractions-cat133" title="Search ebooks in category Continued fractions">Continued
                            fractions</a></li>
                        <li><a href="Differential-Equations-cat125" title="Search ebooks in category Differential Equations">Differential
                            Equations</a></li>
                        <li><a href="Discrete-Mathematics-cat124" title="Search ebooks in category Discrete Mathematics">Discrete
                            Mathematics</a></li>
                        <li><a href="Dynamical-Systems-cat123" title="Search ebooks in category Dynamical Systems">Dynamical Systems</a></li>
                        <li><a href="Elementary-cat146" title="Search ebooks in category Elementary">Elementary</a></li>
                        <li><a href="Functional-Analysis-cat144" title="Search ebooks in category Functional Analysis">Functional
                            Analysis</a></li>
                        <li><a href="Fuzzy-Logic-and-Applications-cat134" title="Search ebooks in category Fuzzy Logic and Applications">Fuzzy
                            Logic and Applications</a></li>
                        <li><a href="Game-Theory-cat141" title="Search ebooks in category Game Theory">Game Theory</a></li>
                        <li><a href="Geometry-and-Topology-cat121" title="Search ebooks in category Geometry and Topology">Geometry and
                            Topology</a></li>
                        <li><a href="Graph-Theory-cat140" title="Search ebooks in category Graph Theory">Graph Theory</a></li>
                        <li><a href="Lectures-cat129" title="Search ebooks in category Lectures">Lectures</a></li>
                        <li><a href="Logic-cat130" title="Search ebooks in category Logic">Logic</a></li>
                        <li><a href="Mathematicsematical-Physics-cat132" title="Search ebooks in category Mathematicsematical Physics">Mathematicsematical
                            Physics</a></li>
                        <li><a href="Mathematicsematical-Statistics-cat131" title="Search ebooks in category Mathematicsematical Statistics">Mathematicsematical
                            Statistics</a></li>
                        <li><a href="Number-Theory-cat143" title="Search ebooks in category Number Theory">Number Theory</a></li>
                        <li><a href="Numerical-Analysis-cat145" title="Search ebooks in category Numerical Analysis">Numerical Analysis</a></li>
                        <li><a href="Operator-Theory-cat142" title="Search ebooks in category Operator Theory">Operator Theory</a></li>
                        <li><a href="Optimal-control-cat135" title="Search ebooks in category Optimal control">Optimal control</a></li>
                        <li><a href="Optimization-Operations-Research-cat136" title="Search ebooks in category Optimization. Operations Research">Optimization.
                            Operations Research</a></li>
                        <li><a href="Probability-cat119" title="Search ebooks in category Probability">Probability</a></li>
                        <li><a href="Puzzle-cat122" title="Search ebooks in category Puzzle">Puzzle</a></li>
                        <li><a href="Symmetry-and-group-cat138" title="Search ebooks in category Symmetry and group">Symmetry and group</a></li>
                        <li><a href="The-complex-variable-cat127" title="Search ebooks in category The complex variable">The complex
                            variable</a></li>
                        <li><a href="Wavelets-and-signal-processing-cat118" title="Search ebooks in category Wavelets and signal processing">Wavelets
                            and signal processing</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">medicine</h2>
                    <ul>
                        <li><a href="Anatomy-and-physiology-cat148" title="Search ebooks in category Anatomy and physiology">Anatomy
                            and physiology</a></li>
                        <li><a href="Anesthesiology-and-Intensive-Care-cat149" title="Search ebooks in category Anesthesiology and Intensive Care">Anesthesiology
                            and Intensive Care</a></li>
                        <li><a href="Cardiology-cat159" title="Search ebooks in category Cardiology">Cardiology</a></li>
                        <li><a href="Chinese-Medicine-cat160" title="Search ebooks in category Chinese Medicine">Chinese Medicine</a></li>
                        <li><a href="Clinical-Medicine-cat161" title="Search ebooks in category Clinical Medicine">Clinical Medicine</a></li>
                        <li><a href="Dentistry-Orthodontics-cat170" title="Search ebooks in category Dentistry, Orthodontics">Dentistry,
                            Orthodontics</a></li>
                        <li><a href="Dermatology-cat154" title="Search ebooks in category Dermatology">Dermatology</a></li>
                        <li><a href="Diabetes-cat155" title="Search ebooks in category Diabetes">Diabetes</a></li>
                        <li><a href="Diseases-cat150" title="Search ebooks in category Diseases">Diseases</a></li>
                        <li><a href="Diseases-Internal-Medicine-cat151" title="Search ebooks in category Diseases: Internal Medicine">Diseases:
                            Internal Medicine</a></li>
                        <li><a href="Endocrinology-cat176" title="Search ebooks in category Endocrinology">Endocrinology</a></li>
                        <li><a href="ENT-cat167" title="Search ebooks in category ENT">ENT</a></li>
                        <li><a href="Epidemiology-cat177" title="Search ebooks in category Epidemiology">Epidemiology</a></li>
                        <li><a href="Feng-Shui-cat174" title="Search ebooks in category Feng Shui">Feng Shui</a></li>
                        <li><a href="Histology-cat152" title="Search ebooks in category Histology">Histology</a></li>
                        <li><a href="Homeopathy-cat153" title="Search ebooks in category Homeopathy">Homeopathy</a></li>
                        <li><a href="immunology-cat156" title="Search ebooks in category immunology">immunology</a></li>
                        <li><a href="Infectious-diseases-cat157" title="Search ebooks in category Infectious diseases">Infectious
                            diseases</a></li>
                        <li><a href="Molecular-Medicine-cat162" title="Search ebooks in category Molecular Medicine">Molecular Medicine</a></li>
                        <li><a href="Natural-Medicine-cat163" title="Search ebooks in category Natural Medicine">Natural Medicine</a></li>
                        <li><a href="Neurology-cat165" title="Search ebooks in category Neurology">Neurology</a></li>
                        <li><a href="Oncology-cat166" title="Search ebooks in category Oncology">Oncology</a></li>
                        <li><a href="Ophthalmology-cat168" title="Search ebooks in category Ophthalmology">Ophthalmology</a></li>
                        <li><a href="Pediatrics-cat169" title="Search ebooks in category Pediatrics">Pediatrics</a></li>
                        <li><a href="Pharmacology-cat173" title="Search ebooks in category Pharmacology">Pharmacology</a></li>
                        <li><a href="Popular-scientific-literature-cat164" title="Search ebooks in category Popular scientific literature">Popular
                            scientific literature</a></li>
                        <li><a href="Surgery-Orthopedics-cat175" title="Search ebooks in category Surgery, Orthopedics">Surgery,
                            Orthopedics</a></li>
                        <li><a href="Therapy-cat172" title="Search ebooks in category Therapy">Therapy</a></li>
                        <li><a href="Trial-cat171" title="Search ebooks in category Trial">Trial</a></li>
                        <li><a href="Yoga-cat158" title="Search ebooks in category Yoga">Yoga</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">other social sciences</h2>
                    <ul>
                        <li><a href="Cultural-cat191" title="Search ebooks in category Cultural">Cultural</a></li>
                        <li><a href="Ethnography-cat197" title="Search ebooks in category Ethnography">Ethnography</a></li>
                        <li><a href="Journalism-Media-cat190" title="Search ebooks in category Journalism, Media">Journalism, Media</a></li>
                        <li><a href="Philosophy-cat195" title="Search ebooks in category Philosophy">Philosophy</a></li>
                        <li><a href="Philosophy-Critical-Thinking-cat196" title="Search ebooks in category Philosophy: Critical Thinking">Philosophy:
                            Critical Thinking</a></li>
                        <li><a href="Politics-cat192" title="Search ebooks in category Politics">Politics</a></li>
                        <li><a href="Politics-International-Relations-cat193" title="Search ebooks in category Politics: International Relations">Politics:
                            International Relations</a></li>
                        <li><a href="Sociology-cat194" title="Search ebooks in category Sociology">Sociology</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">physical education and sport</h2>
                    <ul>
                        <li><a href="Bike-cat292" title="Search ebooks in category Bike">Bike</a></li>
                        <li><a href="Bodybuilding-cat290" title="Search ebooks in category Bodybuilding">Bodybuilding</a></li>
                        <li><a href="Fencing-cat295" title="Search ebooks in category Fencing">Fencing</a></li>
                        <li><a href="Martial-Arts-cat291" title="Search ebooks in category Martial Arts">Martial Arts</a></li>
                        <li><a href="Sport-fishing-cat294" title="Search ebooks in category Sport fishing">Sport fishing</a></li>
                        <li><a href="Survival-cat293" title="Search ebooks in category Survival">Survival</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">physics</h2>
                    <ul>
                        <li><a href="Astronomy-cat265" title="Search ebooks in category Astronomy">Astronomy</a></li>
                        <li><a href="Astronomy-Astrophysics-cat266" title="Search ebooks in category Astronomy: Astrophysics">Astronomy:
                            Astrophysics</a></li>
                        <li><a href="Crystal-Physics-cat270" title="Search ebooks in category Crystal Physics">Crystal Physics</a></li>
                        <li><a href="Electricity-and-Magnetism-cat287" title="Search ebooks in category Electricity and Magnetism">Electricity
                            and Magnetism</a></li>
                        <li><a href="Electrodynamics-cat288" title="Search ebooks in category Electrodynamics">Electrodynamics</a></li>
                        <li><a href="General-courses-cat278" title="Search ebooks in category General courses">General courses</a></li>
                        <li><a href="Geophysics-cat267" title="Search ebooks in category Geophysics">Geophysics</a></li>
                        <li><a href="Mechanics-cat271" title="Search ebooks in category Mechanics">Mechanics</a></li>
                        <li><a href="Mechanics-Fluid-Mechanics-cat274" title="Search ebooks in category Mechanics: Fluid Mechanics">Mechanics:
                            Fluid Mechanics</a></li>
                        <li><a href="Mechanics-Mechanics-of-deformable-bodies-cat273" title="Search ebooks in category Mechanics: Mechanics of deformable bodies">Mechanics:
                            Mechanics of deformable bodies</a></li>
                        <li><a href="Mechanics-Nonlinear-dynamics-and-chaos-cat275" title="Search ebooks in category Mechanics: Nonlinear dynamics and chaos">Mechanics:
                            Nonlinear dynamics and chaos</a></li>
                        <li><a href="Mechanics-Oscillations-and-Waves-cat272" title="Search ebooks in category Mechanics: Oscillations and Waves">Mechanics:
                            Oscillations and Waves</a></li>
                        <li><a href="Mechanics-Strength-of-Materials-cat276" title="Search ebooks in category Mechanics: Strength of Materials">Mechanics:
                            Strength of Materials</a></li>
                        <li><a href="Mechanics-Theory-of-Elasticity-cat277" title="Search ebooks in category Mechanics: Theory of Elasticity">Mechanics:
                            Theory of Elasticity</a></li>
                        <li><a href="Optics-cat279" title="Search ebooks in category Optics">Optics</a></li>
                        <li><a href="Physics-of-lasers-cat284" title="Search ebooks in category Physics of lasers">Physics of lasers</a></li>
                        <li><a href="Physics-of-the-Atmosphere-cat283" title="Search ebooks in category Physics of the Atmosphere">Physics
                            of the Atmosphere</a></li>
                        <li><a href="Plasma-Physics-cat285" title="Search ebooks in category Plasma Physics">Plasma Physics</a></li>
                        <li><a href="Quantum-Mechanics-cat268" title="Search ebooks in category Quantum Mechanics">Quantum Mechanics</a></li>
                        <li><a href="Quantum-Physics-cat269" title="Search ebooks in category Quantum Physics">Quantum Physics</a></li>
                        <li><a href="Solid-State-Physics-cat286" title="Search ebooks in category Solid State Physics">Solid State
                            Physics</a></li>
                        <li><a href="Spectroscopy-cat280" title="Search ebooks in category Spectroscopy">Spectroscopy</a></li>
                        <li><a href="Theory-of-Relativity-and-Gravitation-cat281" title="Search ebooks in category Theory of Relativity and Gravitation">Theory
                            of Relativity and Gravitation</a></li>
                        <li><a href="Thermodynamics-and-Statistical-Mechanics-cat282" title="Search ebooks in category Thermodynamics and Statistical Mechanics">Thermodynamics
                            and Statistical Mechanics</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">psychology</h2>
                    <ul>
                        <li><a href="Creative-Thinking-cat204" title="Search ebooks in category Creative Thinking">Creative Thinking</a></li>
                        <li><a href="Hypnosis-cat199" title="Search ebooks in category Hypnosis">Hypnosis</a></li>
                        <li><a href="Love-erotic-cat201" title="Search ebooks in category Love, erotic">Love, erotic</a></li>
                        <li><a href="Neuro-Linguistic-Programming-cat202" title="Search ebooks in category Neuro-Linguistic Programming">Neuro-Linguistic
                            Programming</a></li>
                        <li><a href="Pedagogy-cat203" title="Search ebooks in category Pedagogy">Pedagogy</a></li>
                        <li><a href="The-art-of-communication-cat200" title="Search ebooks in category The art of communication">The
                            art of communication</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">religion</h2>
                    <ul>
                        <li><a href="Buddhism-cat206" title="Search ebooks in category Buddhism">Buddhism</a></li>
                        <li><a href="Esoteric-Mystery-cat209" title="Search ebooks in category Esoteric, Mystery">Esoteric, Mystery</a></li>
                        <li><a href="kabbalah-cat207" title="Search ebooks in category kabbalah">kabbalah</a></li>
                        <li><a href="Orthodoxy-cat208" title="Search ebooks in category Orthodoxy">Orthodoxy</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">science (general)</h2>
                    <ul>
                        <li><a href="International-Conferences-and-Symposiums-cat179" title="Search ebooks in category International Conferences and Symposiums">International
                            Conferences and Symposiums</a></li>
                        <li><a href="Science-of-Science-cat180" title="Search ebooks in category Science of Science">Science of Science</a></li>
                        <li><a href="Scientific-and-popular-Journalism-cat182" title="Search ebooks in category Scientific and popular: Journalism">Scientific
                            and popular: Journalism</a></li>
                        <li><a href="Scientific-popular-cat181" title="Search ebooks in category Scientific-popular">Scientific-popular</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">technique</h2>
                    <ul>
                        <li><a href="Aerospace-Equipment-cat212" title="Search ebooks in category Aerospace Equipment">Aerospace
                            Equipment</a></li>
                        <li><a href="Automation-cat211" title="Search ebooks in category Automation">Automation</a></li>
                        <li><a href="Communication-cat234" title="Search ebooks in category Communication">Communication</a></li>
                        <li><a href="Communication-Telecommunications-cat235" title="Search ebooks in category Communication: Telecommunications">Communication:
                            Telecommunications</a></li>
                        <li><a href="Construction-cat236" title="Search ebooks in category Construction">Construction</a></li>
                        <li><a href="Construction-Cement-Industry-cat241" title="Search ebooks in category Construction: Cement Industry">Construction:
                            Cement Industry</a></li>
                        <li><a href="Construction-Renovation-and-interior-design-cat239" title="Search ebooks in category Construction: Renovation and interior design">Construction:
                            Renovation and interior design</a></li>
                        <li><a href="Construction-Renovation-and-interior-design-Saunas-cat240" title="Search ebooks in category Construction: Renovation and interior design: Saunas">Construction:
                            Renovation and interior design: Saunas</a></li>
                        <li><a href="Construction-Ventilation-and-Air-Conditioning-cat238" title="Search ebooks in category Construction: Ventilation and Air Conditioning">Construction:
                            Ventilation and Air Conditioning</a></li>
                        <li><a href="Electronics-cat250" title="Search ebooks in category Electronics">Electronics</a></li>
                        <li><a href="Electronics-Electronics-cat261" title="Search ebooks in category Electronics: Electronics">Electronics:
                            Electronics</a></li>
                        <li><a href="Electronics-Fiber-Optics-cat252" title="Search ebooks in category Electronics: Fiber Optics">Electronics:
                            Fiber Optics</a></li>
                        <li><a href="Electronics-Hardware-cat251" title="Search ebooks in category Electronics: Hardware">Electronics:
                            Hardware</a></li>
                        <li><a href="Electronics-Home-Electronics-cat253" title="Search ebooks in category Electronics: Home Electronics">Electronics:
                            Home Electronics</a></li>
                        <li><a href="Electronics-Microprocessor-Technology-cat254" title="Search ebooks in category Electronics: Microprocessor Technology">Electronics:
                            Microprocessor Technology</a></li>
                        <li><a href="Electronics-Radio-cat256" title="Search ebooks in category Electronics: Radio">Electronics: Radio</a></li>
                        <li><a href="Electronics-Robotics-cat257" title="Search ebooks in category Electronics: Robotics">Electronics:
                            Robotics</a></li>
                        <li><a href="Electronics-Signal-Processing-cat255" title="Search ebooks in category Electronics: Signal Processing">Electronics:
                            Signal Processing</a></li>
                        <li><a href="Electronics-Telecommunications-cat260" title="Search ebooks in category Electronics: Telecommunications">Electronics:
                            Telecommunications</a></li>
                        <li><a href="Electronics-TV-Video-cat259" title="Search ebooks in category Electronics: TV. Video">Electronics:
                            TV. Video</a></li>
                        <li><a href="Electronics-VLSI-cat258" title="Search ebooks in category Electronics: VLSI">Electronics: VLSI</a></li>
                        <li><a href="Energy-cat262" title="Search ebooks in category Energy">Energy</a></li>
                        <li><a href="Energy-Renewable-Energy-cat263" title="Search ebooks in category Energy: Renewable Energy">Energy:
                            Renewable Energy</a></li>
                        <li><a href="Food-Manufacturing-cat229" title="Search ebooks in category Food Manufacturing">Food Manufacturing</a></li>
                        <li><a href="Fuel-Technology-cat243" title="Search ebooks in category Fuel Technology">Fuel Technology</a></li>
                        <li><a href="Heat-cat242" title="Search ebooks in category Heat">Heat</a></li>
                        <li><a href="industrial-equipment-and-technology-cat232" title="Search ebooks in category industrial equipment and technology">industrial
                            equipment and technology</a></li>
                        <li><a href="Industry-Metallurgy-cat231" title="Search ebooks in category Industry: Metallurgy">Industry:
                            Metallurgy</a></li>
                        <li><a href="Instrument-cat230" title="Search ebooks in category Instrument">Instrument</a></li>
                        <li><a href="Light-Industry-cat218" title="Search ebooks in category Light Industry">Light Industry</a></li>
                        <li><a href="Materials-cat219" title="Search ebooks in category Materials">Materials</a></li>
                        <li><a href="Metallurgy-cat221" title="Search ebooks in category Metallurgy">Metallurgy</a></li>
                        <li><a href="Metrology-cat222" title="Search ebooks in category Metrology">Metrology</a></li>
                        <li><a href="Military-equipment-cat214" title="Search ebooks in category Military equipment">Military equipment</a></li>
                        <li><a href="Military-equipment-Weapon-cat215" title="Search ebooks in category Military equipment: Weapon">Military
                            equipment: Weapon</a></li>
                        <li><a href="Missiles-cat233" title="Search ebooks in category Missiles">Missiles</a></li>
                        <li><a href="Nanotechnology-cat224" title="Search ebooks in category Nanotechnology">Nanotechnology</a></li>
                        <li><a href="Oil-and-Gas-Technologies-cat225" title="Search ebooks in category Oil and Gas Technologies">Oil
                            and Gas Technologies</a></li>
                        <li><a href="Oil-and-Gas-Technologies-Pipelines-cat226" title="Search ebooks in category Oil and Gas Technologies: Pipelines">Oil
                            and Gas Technologies: Pipelines</a></li>
                        <li><a href="Patent-Business-Ingenuity-Innovation-cat228" title="Search ebooks in category Patent Business. Ingenuity. Innovation">Patent
                            Business. Ingenuity. Innovation</a></li>
                        <li><a href="Publishing-cat216" title="Search ebooks in category Publishing">Publishing</a></li>
                        <li><a href="Refrigeration-cat249" title="Search ebooks in category Refrigeration">Refrigeration</a></li>
                        <li><a href="Regulatory-Literature-cat227" title="Search ebooks in category Regulatory Literature">Regulatory
                            Literature</a></li>
                        <li><a href="Safety-and-Security-cat223" title="Search ebooks in category Safety and Security">Safety and
                            Security</a></li>
                        <li><a href="Transport-cat244" title="Search ebooks in category Transport">Transport</a></li>
                        <li><a href="Transportation-Aviation-cat245" title="Search ebooks in category Transportation: Aviation">Transportation:
                            Aviation</a></li>
                        <li><a href="Transportation-Cars-motorcycles-cat246" title="Search ebooks in category Transportation: Cars, motorcycles">Transportation:
                            Cars, motorcycles</a></li>
                        <li><a href="Transportation-Rail-cat247" title="Search ebooks in category Transportation: Rail">Transportation:
                            Rail</a></li>
                        <li><a href="Transportation-Ships-cat248" title="Search ebooks in category Transportation: Ships">Transportation:
                            Ships</a></li>
                        <li><a href="Water-Treatment-cat213" title="Search ebooks in category Water Treatment">Water Treatment</a></li>
                    </ul>
                </div>
                <div style="padding:0 0 20px 0" class="col-md-4">
                    <h2 style="text-transform: capitalize;">technology</h2>
                    <ul>
                        <li><a href="Space-Science-cat217" title="Search ebooks in category Space Science">Space Science</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <h2>引用</h2>
            <div class="w310 right">
                <div class="bb1 sideMenu">
                    <div class="tit1">
                        <h2>中图法分类</h2>
                    </div>
                    <dl>
                        <dt><a href="javascript:selectSort('A')">马克思主义、列宁主义...</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('A1')"> 马克思、恩格斯著作</a> </li>
                                <li><a href="javascript:selectSort('A4')"> 毛泽东著作</a> </li>
                                <li><a href="javascript:selectSort('A49')"> 邓小平著作</a> </li>
                                <li><a href="javascript:selectSort('A5')">马克思、恩格斯、列宁... </a> </li>
                                <li><a href="javascript:selectSort('A7')">马克思、恩格斯、列宁... </a> </li>
                                <li><a href="javascript:selectSort('A8')">马克思主义、列宁主义... </a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('B')"> 哲学、宗教</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('B0')"> 哲学理论</a> </li>
                                <li><a href="javascript:selectSort('B1')"> 世界哲学</a> </li>
                                <li><a href="javascript:selectSort('B2')"> 中国哲学</a> </li>
                                <li><a href="javascript:selectSort('B3')"> 亚洲哲学</a> </li>
                                <li><a href="javascript:selectSort('B5')"> 欧洲哲学</a> </li>
                                <li><a href="javascript:selectSort('B7')"> 美洲哲学</a> </li>
                                <li><a href="javascript:selectSort('B80')"> 思维科学</a> </li>
                                <li><a href="javascript:selectSort('B81')"> 逻辑学（论理学）</a> </li>
                                <li><a href="javascript:selectSort('B82')"> 伦理学（道德哲学）</a> </li>
                                <li><a href="javascript:selectSort('B84')"> 心理学</a> </li>
                                <li><a href="javascript:selectSort('B9')"> 宗教</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('C')"> 社会科学总论</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('C0')"> 社会科学理论与方法论</a> </li>
                                <li><a href="javascript:selectSort('C1')"> 社会科学现状与发展</a> </li>
                                <li><a href="javascript:selectSort('C2')"> 社会科学机构、团体、... </a> </li>
                                <li><a href="javascript:selectSort('C3')"> 社会科学研究方法</a> </li>
                                <li><a href="javascript:selectSort('C4')"> 社会科学教育与普及</a> </li>
                                <li><a href="javascript:selectSort('C5')"> 社会科学丛书、文集、... </a> </li>
                                <li><a href="javascript:selectSort('C6')"> 社会科学参考工具书</a> </li>
                                <li><a href="javascript:selectSort('C8')"> 统计学</a> </li>
                                <li><a href="javascript:selectSort('C91')"> 社会学</a> </li>
                                <li><a href="javascript:selectSort('C92')"> 人口学</a> </li>
                                <li><a href="javascript:selectSort('C93')"> 管理学</a> </li>
                                <li><a href="javascript:selectSort('C95')"> 民族学</a> </li>
                                <li><a href="javascript:selectSort('C96')"> 人才学</a> </li>
                                <li><a href="javascript:selectSort('C97')"> 劳动科学</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('D')"> 政治、法律</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('D0')"> 政治理论</a> </li>
                                <li><a href="javascript:selectSort('D1')"> 国际共产主义运动</a> </li>
                                <li><a href="javascript:selectSort('D2')"> 中国共产党</a> </li>
                                <li><a href="javascript:selectSort('D33/37')"> 各国共产党</a> </li>
                                <li><a href="javascript:selectSort('D4')"> 工人、农民、青年、妇... </a> </li>
                                <li><a href="javascript:selectSort('D5')"> 世界政治</a> </li>
                                <li><a href="javascript:selectSort('D6')"> 中国政治</a> </li>
                                <li><a href="javascript:selectSort('D73/77')"> 各国政治</a> </li>
                                <li><a href="javascript:selectSort('D8')"> 外交、国际关系</a> </li>
                                <li><a href="javascript:selectSort('D9')"> 法律</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('E')"> 军事</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('E0')"> 军事理论</a> </li>
                                <li><a href="javascript:selectSort('E1')"> 世界军事</a> </li>
                                <li><a href="javascript:selectSort('E2')"> 中国军事</a> </li>
                                <li><a href="javascript:selectSort('E3/7')"> 各国军事</a> </li>
                                <li><a href="javascript:selectSort('E8')"> 战略学、战役学、战术... </a> </li>
                                <li><a href="javascript:selectSort('E9')"> 军事技术</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('F')"> 经济</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('F0')"> 经济学</a> </li>
                                <li><a href="javascript:selectSort('F1')"> 世界各国经济概况、经... </a> </li>
                                <li><a href="javascript:selectSort('F2')"> 经济计划与管理</a> </li>
                                <li><a href="javascript:selectSort('F3')"> 农业经济</a> </li>
                                <li><a href="javascript:selectSort('F4')"> 工业经济</a> </li>
                                <li><a href="javascript:selectSort('F49')"> 信息产业经济（总论）</a> </li>
                                <li><a href="javascript:selectSort('F5')"> 交通运输经济</a> </li>
                                <li><a href="javascript:selectSort('F59')"> 邮电经济</a> </li>
                                <li><a href="javascript:selectSort('F7')"> 贸易经济</a> </li>
                                <li><a href="javascript:selectSort('F8')"> 财政、金融</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('G')"> 文化、科学、教育、体... </a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('G0')"> 文化理论</a> </li>
                                <li><a href="javascript:selectSort('G1')"> 世界各国文化与文化事... </a> </li>
                                <li><a href="javascript:selectSort('G2')"> 信息与知识传播</a> </li>
                                <li><a href="javascript:selectSort('G3')"> 科学、科学研究</a> </li>
                                <li><a href="javascript:selectSort('G4')"> 教育</a> </li>
                                <li><a href="javascript:selectSort('G6')"> 各级教育</a> </li>
                                <li><a href="javascript:selectSort('G8')"> 体育</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('H')"> 语言、文字</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('H0')"> 语言学</a> </li>
                                <li><a href="javascript:selectSort('H1')"> 汉语</a> </li>
                                <li><a href="javascript:selectSort('H2')"> 中国少数民族语言</a> </li>
                                <li><a href="javascript:selectSort('H3')"> 常用外国语</a> </li>
                                <li><a href="javascript:selectSort('H4')"> 汉藏语系</a> </li>
                                <li><a href="javascript:selectSort('H5')"> 阿尔泰语系（突厥-蒙... </a> </li>
                                <li><a href="javascript:selectSort('H61')"> 南亚语系（澳斯特罗-... </a> </li>
                                <li><a href="javascript:selectSort('H63')"> 南岛语系（马来亚-玻... </a> </li>
                                <li><a href="javascript:selectSort('H67')"> 闪-含语系（阿非罗-... </a> </li>
                                <li><a href="javascript:selectSort('H7')"> 印欧语系</a> </li>
                                <li><a href="javascript:selectSort('H84')"> 大洋州诸语言</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('I')"> 文学</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('I0')"> 文学理论</a> </li>
                                <li><a href="javascript:selectSort('I1')"> 世界文学</a> </li>
                                <li><a href="javascript:selectSort('I2')"> 中国文学</a> </li>
                                <li><a href="javascript:selectSort('I3/7')"> 各国文学</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('J')"> 艺术</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('J0')"> 艺术理论</a> </li>
                                <li><a href="javascript:selectSort('J1')"> 世界各国艺术概况</a> </li>
                                <li><a href="javascript:selectSort('J2')"> 绘画</a> </li>
                                <li><a href="javascript:selectSort('J29')"> 书法、篆刻</a> </li>
                                <li><a href="javascript:selectSort('J3')"> 雕塑</a> </li>
                                <li><a href="javascript:selectSort('J4')"> 摄影艺术</a> </li>
                                <li><a href="javascript:selectSort('J5')"> 工艺美术</a> </li>
                                <li><a href="javascript:selectSort('J6')"> 音乐</a> </li>
                                <li><a href="javascript:selectSort('J7')"> 舞蹈</a> </li>
                                <li><a href="javascript:selectSort('J8')"> 戏剧艺术</a> </li>
                                <li><a href="javascript:selectSort('J9')"> 电影、电视艺术</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('K')"> 历史、地理</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('K0')"> 史学理论</a> </li>
                                <li><a href="javascript:selectSort('K1')"> 世界史</a> </li>
                                <li><a href="javascript:selectSort('K2')"> 中国史</a> </li>
                                <li><a href="javascript:selectSort('K3')"> 亚洲史</a> </li>
                                <li><a href="javascript:selectSort('K4')"> 非洲史</a> </li>
                                <li><a href="javascript:selectSort('K5')"> 欧洲史</a> </li>
                                <li><a href="javascript:selectSort('K6')"> 大洋州史</a> </li>
                                <li><a href="javascript:selectSort('K7')"> 美洲史</a> </li>
                                <li><a href="javascript:selectSort('K81')"> 传记</a> </li>
                                <li><a href="javascript:selectSort('K85')"> 文物考古</a> </li>
                                <li><a href="javascript:selectSort('K89')"> 风俗习惯</a> </li>
                                <li><a href="javascript:selectSort('K9')"> 地理</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('N')"> 自然科学总论</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('N0')"> 自然科学理论与方法论</a> </li>
                                <li><a href="javascript:selectSort('N1')"> 自然科学现状与发展</a> </li>
                                <li><a href="javascript:selectSort('N2')"> 自然科学机构、团体、... </a> </li>
                                <li><a href="javascript:selectSort('N3')"> 自然科学研究方法</a> </li>
                                <li><a href="javascript:selectSort('N4')"> 自然科学教育与普及</a> </li>
                                <li><a href="javascript:selectSort('N5')"> 自然科学丛书、文集、... </a> </li>
                                <li><a href="javascript:selectSort('N6')"> 自然科学参考工具书</a> </li>
                                <li><a href="javascript:selectSort('N8')"> 自然科学调查、考察</a> </li>
                                <li><a href="javascript:selectSort('N91')"> 自然研究、自然历史</a> </li>
                                <li><a href="javascript:selectSort('N94')"> 系统科学</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('O')"> 数理科学和化学</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('O1')"> 数学</a> </li>
                                <li><a href="javascript:selectSort('O3')"> 力学</a> </li>
                                <li><a href="javascript:selectSort('O4')"> 物理学</a> </li>
                                <li><a href="javascript:selectSort('O6')"> 化学</a> </li>
                                <li><a href="javascript:selectSort('O7')"> 晶体学</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('P')"> 天文学、地球科学</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('P1')"> 天文学</a> </li>
                                <li><a href="javascript:selectSort('P2')"> 测绘学</a> </li>
                                <li><a href="javascript:selectSort('P3')"> 地球物理学</a> </li>
                                <li><a href="javascript:selectSort('P4')"> 大气科学（气象学）</a> </li>
                                <li><a href="javascript:selectSort('P5')"> 地质学</a> </li>
                                <li><a href="javascript:selectSort('P7')"> 海洋学</a> </li>
                                <li><a href="javascript:selectSort('P9')"> 自然地理学</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('Q')"> 生物科学</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('Q1')"> 普通生物学</a> </li>
                                <li><a href="javascript:selectSort('Q2')"> 细胞生物学</a> </li>
                                <li><a href="javascript:selectSort('Q3')"> 遗传学</a> </li>
                                <li><a href="javascript:selectSort('Q4')"> 生理学</a> </li>
                                <li><a href="javascript:selectSort('Q5')"> 生物化学</a> </li>
                                <li><a href="javascript:selectSort('Q6')"> 生物物理学</a> </li>
                                <li><a href="javascript:selectSort('Q7')"> 分子生物学</a> </li>
                                <li><a href="javascript:selectSort('Q81')"> 生物工程学（生物技术... </a> </li>
                                <li><a href="javascript:selectSort('Q91')"> 古生物学</a> </li>
                                <li><a href="javascript:selectSort('Q93')"> 微生物学</a> </li>
                                <li><a href="javascript:selectSort('Q94')"> 植物学</a> </li>
                                <li><a href="javascript:selectSort('Q95')"> 动物学</a> </li>
                                <li><a href="javascript:selectSort('Q96')"> 昆虫学</a> </li>
                                <li><a href="javascript:selectSort('Q98')"> 人类学</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('R')"> 医药、卫生</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('R1')"> 预防医学、卫生学</a> </li>
                                <li><a href="javascript:selectSort('R2')"> 中国医学</a> </li>
                                <li><a href="javascript:selectSort('R3')"> 基础医学</a> </li>
                                <li><a href="javascript:selectSort('R4')"> 临床医学</a> </li>
                                <li><a href="javascript:selectSort('R5')"> 内科学</a> </li>
                                <li><a href="javascript:selectSort('R6')"> 外科学</a> </li>
                                <li><a href="javascript:selectSort('R71')"> 妇产科学</a> </li>
                                <li><a href="javascript:selectSort('R72')"> 儿科学</a> </li>
                                <li><a href="javascript:selectSort('R73')"> 肿瘤学</a> </li>
                                <li><a href="javascript:selectSort('R74')"> 神经病学与精神病学</a> </li>
                                <li><a href="javascript:selectSort('R75')"> 皮肤病学与性病学</a> </li>
                                <li><a href="javascript:selectSort('R76')"> 耳鼻咽喉科学</a> </li>
                                <li><a href="javascript:selectSort('R77')"> 眼科学</a> </li>
                                <li><a href="javascript:selectSort('R78')"> 口腔科学</a> </li>
                                <li><a href="javascript:selectSort('R79')"> 外国民族医学</a> </li>
                                <li><a href="javascript:selectSort('R8')"> 特种医学</a> </li>
                                <li><a href="javascript:selectSort('R9')"> 药学</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('S')"> 农业科学</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('S1')"> 农业基础科学</a> </li>
                                <li><a href="javascript:selectSort('S2')"> 农业工程</a> </li>
                                <li><a href="javascript:selectSort('S3')"> 农学（农艺学）</a> </li>
                                <li><a href="javascript:selectSort('S4')"> 植物保护</a> </li>
                                <li><a href="javascript:selectSort('S5')"> 农作物</a> </li>
                                <li><a href="javascript:selectSort('S6')"> 园艺</a> </li>
                                <li><a href="javascript:selectSort('S7')"> 林业</a> </li>
                                <li><a href="javascript:selectSort('S8')"> 畜牧、动物医学、狩猎... </a> </li>
                                <li><a href="javascript:selectSort('S9')"> 水产、渔业</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('T')"> 工业技术</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('TB')"> 一般工业技术</a> </li>
                                <li><a href="javascript:selectSort('TD')"> 矿业工程</a> </li>
                                <li><a href="javascript:selectSort('TE')"> 石油、天然气工业</a> </li>
                                <li><a href="javascript:selectSort('TF')"> 冶金工业</a> </li>
                                <li><a href="javascript:selectSort('TG')"> 金属学与金属工艺</a> </li>
                                <li><a href="javascript:selectSort('TH')"> 机械、仪表工业</a> </li>
                                <li><a href="javascript:selectSort('TJ')"> 武器工业</a> </li>
                                <li><a href="javascript:selectSort('TK')"> 能源与动力工程</a> </li>
                                <li><a href="javascript:selectSort('TL')"> 原子能技术</a> </li>
                                <li><a href="javascript:selectSort('TM')"> 电工技术</a> </li>
                                <li><a href="javascript:selectSort('TN')"> 无线电电子学、电信技... </a> </li>
                                <li><a href="javascript:selectSort('TP')"> 自动化技术、计算机技... </a> </li>
                                <li><a href="javascript:selectSort('TQ')"> 化学工业</a> </li>
                                <li><a href="javascript:selectSort('TS')"> 轻工业、手工业</a> </li>
                                <li><a href="javascript:selectSort('TU')"> 建筑科学</a> </li>
                                <li><a href="javascript:selectSort('TV')"> 水利工程</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('U')"> 交通运输</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('U1')"> 综合运输</a> </li>
                                <li><a href="javascript:selectSort('U2')"> 铁路运输</a> </li>
                                <li><a href="javascript:selectSort('U4')"> 公路运输</a> </li>
                                <li><a href="javascript:selectSort('U6')"> 水路运输</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('V')"> 航空、航天</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('V1')"> 航空、航天技术的研究... </a> </li>
                                <li><a href="javascript:selectSort('V2')"> 航空</a> </li>
                                <li><a href="javascript:selectSort('V4')"> 航天（宇宙航行）</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('X')"> 环境科学、安全科学</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('X1')"> 环境科学基础理论</a> </li>
                                <li><a href="javascript:selectSort('X2')"> 社会与环境</a> </li>
                                <li><a href="javascript:selectSort('X3')"> 环境保护管理</a> </li>
                                <li><a href="javascript:selectSort('X4')"> 灾害及其防治</a> </li>
                                <li><a href="javascript:selectSort('X5')"> 环境污染及其防治</a> </li>
                                <li><a href="javascript:selectSort('X7')"> 废物处理与综合利用</a> </li>
                                <li><a href="javascript:selectSort('X8')"> 环境质量评价与环境监... </a> </li>
                                <li><a href="javascript:selectSort('X9')"> 安全科学</a> </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt><a href="javascript:selectSort('Z')"> 综合性图书</a>
                        </dt>
                        <dd>
                            <ul>
                                <li><a href="javascript:selectSort('Z1')"> 丛书</a> </li>
                                <li><a href="javascript:selectSort('Z2')"> 百科全书、类书</a> </li>
                                <li><a href="javascript:selectSort('Z4')">论文集、全集、选集、...</a> </li>
                                <li><a href="javascript:selectSort('Z5')">年鉴、年刊</a> </li>
                                <li><a href="javascript:selectSort('Z8')">图书目录、文摘、索引</a> </li>
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>

    </div>
</div>