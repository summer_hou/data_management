package cloud.xhmy.interceptor;

import cloud.xhmy.pojo.Admins;
import cloud.xhmy.pojo.Users;
import cloud.xhmy.service.AdminsService;
import cloud.xhmy.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * @author aliketh.xhmy
 * 拦截全部请求
 */
public class AllInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String uri = request.getRequestURI();
        if (uri.isEmpty()){
            response.sendRedirect(request.getContextPath()+"/x404");
        }

        if (uri.contains("Login") || uri.contains("login")) {
            //登录请求，直接放行
            return true;
        } else {

            //判断用户是否登录
            if (request.getSession().getAttribute("user") != null) {
                //说明已经登录，放行
                return true;
//                if (!uri.contains("admin")){
//                    return true;
//                }
//                else {
//                    response.sendRedirect(request.getContextPath()+"/x401");
//                }
            } else {
                //没有登录，跳转到登录界面
                response.sendRedirect(request.getContextPath() + "/toLogin");
            }

        }
//        }catch (){
//
//        }
        //默认拦截

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        String uri = response.toString();
        if (uri == null){
            response.sendRedirect(request.getContextPath()+"/x404");
        }

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
