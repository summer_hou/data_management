/**
 *
 * 简单使用
 * html
 * <td><a class="btn btn-primary" onclick="delAllProduct()">删除</a></td>
 *
 <table>

 <thead>
 <tr>
 <th><input type="checkbox" name="CheckAll" id="CheckAll"/>选择</th>
 <th>名称</th>
 <th>操作</th>
 <th>其他</th>
 </tr>
 </thead>
 <tbody>


 <!-- 遍历model中的files -->
 <c:forEach items="${files}" var="filenames">
 <tr>
 <td class="chk"><label>
 <input type="checkbox" name="Check[]" value="${filenames}" id="Check[]"/>
 </label></td>
 <td><span>${filenames}</span></td>
 <td><a href="${pageContext.request.contextPath }/delete?filenames=${filenames}">删除</a>
 &nbsp;||&nbsp;
 <a href="${pageContext.request.contextPath }/download?filename=${filenames}">下载</a></td>
 <td></td>
 </tr>
 </c:forEach>
 </tbody>

 </table>
 *
 */


//全选/全不选
$("#CheckAll").bind("click", function () {
    $("input[name='Check[]']").prop("checked", this.checked);
});

//批量删除
function delAllProduct() {
    if (!confirm("确定要删除吗？")) {
        return;
    }
    var cks = $("input[name='Check[]']:checked");
    var str = "";
    //拼接所有的id
    for (var i = 0; i < cks.length; i++) {
        if (cks[i].checked) {
            str += cks[i].value + ",";
        }
    }
    //去掉字符串末尾的‘&'
    str = str.substring(0, str.length - 1);
    // var a = str.split('&');//分割成数组
    // console.log(str);return false;
    $.ajax({
        type: 'post',
        url: "${pageContext.request.contextPath}/delete",
        dataType: "json",
        data: {
            filenames: str
        },
        success: function (data) {
            console.log(data);
            return false;
            if (data['status'] == 1) {
                alert('删除成功！');
                location.href = data['url'];
            } else {
                alert('删除失败！');
                return false;
            }
        }
    });
};