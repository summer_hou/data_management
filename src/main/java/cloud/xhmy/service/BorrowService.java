package cloud.xhmy.service;

import cloud.xhmy.pojo.Borrow;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface BorrowService {
    /**
     * 增加
     * @param borrow
     * @return
     */
    int addBorrow(Borrow borrow);

    /**
     * 根据id删除
     * @param id
     * @return
     */
    int deleteById(int id);


    /**
     * 根据id查询,返回
     * @param id
     * @return
     */
    Borrow queryById(int id);


    /**
     * 查询全部,返回list集合
     * @param page
     * @param size
     * @return
     */
    List<Borrow> listAll(String user, int page, int size);

    /**
     * 归还
     *
     * @param borrow
     * @return
     */
    int backBorrow(Borrow borrow);

    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */

    /**
     * 查询全部,返回list集合
     * @param page
     * @param size
     * @return
     */
    List<Borrow> listAllBorrow(int page, int size);

    /**
     * 列出归还
     *
     * @param page
     * @param size
     * @return
     */
    List<Borrow> listAllTReturn(int page, int size);

    /**
     * 列出未归还
     * @param page
     * @param size
     * @return
     */
    List<Borrow> listAllFReturn(int page, int size);

    /**
     * 列出在阅状态
     * @param page
     * @param size
     * @return
     */
    List<Borrow> listAllOn(int page, int size);

    /**
     * 统计
     * @return
     */
    Borrow countNumber();

    /**
     * 模糊查询
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<Borrow> queryByName(String name,int page, int size);
}
