package cloud.xhmy.controller;

import cloud.xhmy.pojo.Books;
import cloud.xhmy.pojo.EBooks;
import cloud.xhmy.service.BooksService;
import cloud.xhmy.service.EBooksService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ClassifyController {

    @Autowired
    @Qualifier("booksServiceImpl")
    private BooksService booksService;
    @Autowired
    @Qualifier("eBooksServiceImpl")
    private EBooksService eBooksService;

    /**
     * 跳转到分类
     * @return
     */
    @RequestMapping("/admin/classify")
    public String classify(){
        return "admin/classify";
    }

    /**
     * 跳转到电子书分类
     * @return
     */
    @RequestMapping("/admin/toEFormat")
    public String toEFormat(){
        return "admin/eFormat";
    }


    //-------------------------------------------------------------------------------------------------------

    /**
     * 查询
     * @param name
     * @param model
     * @return
     */
    @RequestMapping("/retrieve")
    public String queryBook(@RequestParam("search") String name,Model model){
       model.addAttribute("search",name);
        return "operation/query";

    }

    //-------------------------------------------------------------------------------------------------------


    /**
     * 语言分类
     * @param clq
     * @param model
     * @return
     */
    @RequestMapping("/language")
    public String language(@RequestParam("clq") String clq , Model model){
        model.addAttribute("search",clq);
        return "operation/LQuery";
    }

    /**
     * 分类
     * @param clq
     * @param model
     * @return
     */
    @RequestMapping("/classify")
    public String cla(@RequestParam("clq") String clq , Model model){
        model.addAttribute("search",clq);
        return "operation/CQuery";
    }

    /**
     * books
     * 语言分类
     * @param language
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/booksLanguage")
    public ModelAndView booksLanguage(@RequestParam("clq") String language ,
                               @RequestParam(defaultValue = "1" ) int page,
                               @RequestParam(defaultValue = "10") int size){
        ModelAndView mv = new ModelAndView();
        PageHelper.startPage(page,size);
        List<Books> list = booksService.queryLanguage(language ,page,size);
        PageInfo<Books> pageInfo = new PageInfo<>(list);

        mv.addObject("pageInfoBook" ,pageInfo);
        mv.addObject("search",language);
        mv.setViewName("operation/query/LQuery-book");
        return mv;
//        JsonObject jsonObject = new JsonObject();
//
//        return jsonObject;
    }

    /**
     * books
     * 语言分类
     * @param classify
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/booksClassify")
    public ModelAndView booksClassify(@RequestParam("clq")String classify ,
                                 @RequestParam(defaultValue = "1" ) int page,
                                 @RequestParam(defaultValue = "10") int size){
        ModelAndView mv = new ModelAndView();
        PageHelper.startPage(page,size);
        List<Books> list = booksService.queryClassify(classify ,page,size);
        PageInfo<Books> pageInfo = new PageInfo<>(list);

        mv.addObject("pageInfoBook" ,pageInfo);
        mv.addObject("search",classify);
        mv.setViewName("operation/query/CQuery-book");
        return mv;
//        JsonObject jsonObject = new JsonObject();
//
//        return jsonObject;
    }

    /**
     * ebooks
     * 语言分类
     * @param language
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/eBooksLanguage")
    public ModelAndView eBooksLanguage(@RequestParam("clq")String language ,
                                      @RequestParam(defaultValue = "1" ) int page,
                                      @RequestParam(defaultValue = "10") int size){
        ModelAndView mv = new ModelAndView();
        PageHelper.startPage(page,size);
        List<EBooks> list = eBooksService.queryLanguage(language ,page,size);
        PageInfo<EBooks> pageInfo = new PageInfo<>(list);

        mv.addObject("pageInfoEBook" ,pageInfo);
        mv.addObject("search",language);
        mv.setViewName("operation/query/LQuery-eBook");
        return mv;
//        JsonObject jsonObject = new JsonObject();
//
//        return jsonObject;
    }

    /**
     * ebooks
     * 语言分类
     * @param classify
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/eBooksClassify")
    public ModelAndView eBooksClassify(@RequestParam("clq")String classify ,
                                      @RequestParam(defaultValue = "1" ) int page,
                                      @RequestParam(defaultValue = "10") int size){
        ModelAndView mv = new ModelAndView();
        PageHelper.startPage(page,size);
        List<EBooks> list = eBooksService.queryClassify(classify ,page,size);
        PageInfo<EBooks> pageInfo = new PageInfo<>(list);

        mv.addObject("pageInfoEBook" ,pageInfo);
        mv.addObject("search",classify);
        mv.setViewName("operation/query/CQuery-eBook");
        return mv;
//        JsonObject jsonObject = new JsonObject();
//
//        return jsonObject;
    }
}
