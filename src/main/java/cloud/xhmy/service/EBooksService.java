package cloud.xhmy.service;

import cloud.xhmy.pojo.Books;
import cloud.xhmy.pojo.EBooks;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author aliketh.xhmy
 */
public interface EBooksService {

    /**
     * 增加书籍
     * @param book
     * @return
     */
    int addBook(EBooks book);

    /**
     * 删除书籍
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 查询书籍
     * @param name
     * @param page
     * @param size
     * @return
     */
    List<EBooks> queryByName(String name, int page, int size);

    /**
     * 查询个人书籍
     * @param name
     * @param user
     * @param page
     * @param size
     * @return
     */
    List<EBooks> queryByMyself(String name,String user, int page, int size);

    /**
     * 列出所有书籍
     * @param page
     * @param size
     * @return
     */
    List<EBooks> listAll(String shelves,int page, int size);

    /**
     * 修改书籍
     * @param id
     * @return
     */
    int modifyById(EBooks id);

    /**
     * 用于修改
     * @param id
     * @return
     */
    EBooks queryById(int id);

    /**
     * 单独查询
     * @param name
     * @return
     */
    EBooks query(String name);

    /**
     * 首页展示
     * @return
     */
    List<EBooks> homeQuery();

    /**
     * 语言查询
     * @param language
     * @param page
     * @param size
     * @return
     */
    List<EBooks> queryLanguage(String language,int page,int size);


    /**
     * 分类查询
     * @param classify
     * @param page
     * @param size
     * @return
     */
    List<EBooks> queryClassify(String classify,int page,int size);

    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    /**
     * 管理员
     */

    /**
     * 查询所有
     * @param page
     * @param size
     * @return
     */
    List<EBooks> listAllEBook(int page, int size);

    /**
     * 查询所有等待审核
     * @param state
     * @param page
     * @param size
     * @return
     */
    List<EBooks> listAllAudit(String state, int page, int size);

    /**
     * 通过审核
     * @param id
     * @return
     */
    int push(int id, String state);

    /**
     * 统计
     * @return
     */
    EBooks countNumber();

    /**
     * 审核列表
     *
     * @param state
     * @param page
     * @param size
     * @return
     */
    List<EBooks> listAuditEBook(String state,String name,int page,int size);


}
