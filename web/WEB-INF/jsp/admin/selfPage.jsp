<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/7
  Time: 21:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container" style="margin-top: 40px;color: #ffff7f;">
    <div class="row clearfix">
        <div class="col-md-4">
            <h3><em>说明</em></h3>
            <ol></ol>
            <ol>
                <li>当要修改时页面红色字体提示</li>
                <li>需要谨慎执行注销操作</li>
                <li>实名认证后联允许再进行实名</li>
                <li>修改联系电话时去请确认是否为本人联系方式</li>
            </ol>
        </div>
        <div class="col-md-8 column">
            <div class="tabbable" id="tabs-736228">
                <ul class="nav nav-tabs ">
                    <li class="active">
                        <a href="#panel-364990" style="color: goldenrod;" data-toggle="tab">基本信息</a>
                    </li>
                    <li>
                        <a href="#panel-230698" style="color: goldenrod;" data-toggle="tab">修改密码</a>
                    </li>
                    <li class="">
                        <a href="#panel-364995" style="color: goldenrod;" data-toggle="tab">实名认证</a>
                    </li>
                    <li>
                        <a href="#panel-230696" style="color: goldenrod;" data-toggle="tab">跟换手机</a>
                    </li>
                    <li>
                        <a href="#panel-230686" style="color: goldenrod;" data-toggle="tab">其他</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="panel-364990" style="line-height: 2.5em;font-size: 16px;">
                        <h3 style="margin: 20px auto;"><span>用户基本信息</span></h3>

                        <div>
                            <span>用户头像：</span>
                            <span><a href=""> <img style="width: 100px;height: 100px;border-radius: 50px;"
                                                   src="${pageContext.request.contextPath}/static${user.avatar}"
                                                   alt="${user.avatar}"></a></span>
                        </div>
                        <div>
                            <span>用户注册id：</span>
                            <span>${user.uid}</span>
                        </div>
                        <div>
                            <span>用户邮箱：</span>
                            <span>${user.email}</span>
                        </div>
                        <div>
                            <span>绑定手机：</span>
                            <span>${user.telephone}</span>
                        </div>
                        <div>
                            <span>是否实名：</span>
                            <span>${torf}</span>
                        </div>

                        <div>
                            <span>注册时间：</span>
                            <span>${user.createTime}</span>
                        </div>
                        <div>
                            <span></span>
                            <span></span>
                        </div>

                    </div>
                    <%--
                        <h3 style="margin: 20px auto;"><span>添加用户</span></h3>
                        <form id="add-user" action="">
                            <div style="line-height: 2em ;font-size: 16px;">
                                <form action="${pageContext.request.contextPath}/register">
                                    <div>
                                        <span>用户邮箱：</span>
                                        <input class="form-control" type="email" name="email" id="email" value="${user.email}">
                                    </div>
                                    <div>
                                        <span>密 &nbsp; &nbsp; &nbsp; 码：</span>
                                        <input class="form-control" type="password" name="password" id="password" value="${user.password}">
                                    </div>
                                    <div>
                                        <span>手机号码：</span>
                                        <input class="form-control" type="tel" name="telephone" id="telephone" value="${user.telephone}">
                                    </div>
                                    <div>
                                        <span>用&nbsp; 户 名：</span>
                                        <input class="form-control" type="text" name="usName" id="usName" value="${user.usName}">
                                    </div>
                                </form>
                            </div>
                        </form>
                        --%>


                    <div class="tab-pane" id="panel-230698">
                        <h3 style="margin: 30px auto;"><span>修改密码</span></h3>
                        <span style="color: red;"><em><b>密码由数字，字母，符号，特殊字符，中的其中三种组成且长度不小于8位，不超过32位</b></em></span>

                        <div style="line-height: 2em ;font-size: 16px;">
                            <form id="admin-form-modify-pwd">
                                <input type="hidden" name="uid" value="${user.uid}">
                                <div>
                                    <%--                                    <span>输入用户邮箱：</span>--%>
                                    <%--                                    <input class="form-control" type="email" name="email">--%>
                                </div>
                                <div>
                                    <span>输入旧密码：</span>
                                    <input class="form-control" type="password" name="oldPassword" required data-eye>
                                </div>
                                <div>
                                    <span>输入新密码：</span>
                                    <input class="form-control" id="newPassword" type="password" name="newPassword"
                                           required
                                           data-eye>
                                </div>
                                <div>
                                    <span>确认新密码：</span>
                                    <input class="form-control" id="checkNew" type="password" name=""
                                           onkeyup="checkPassword()" required data-eye>
                                </div>
                                <div style="margin: 20px auto; text-align: center;">
                                    <input type="button" class="btn btn-primary" value="提交修改" onclick="formModifyPwd()"><br>
                                    <span style="color: red;" id="pwdMsg">${msg}</span><span id="hint"></span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="panel-364995">
                        <h3 style="margin: 30px auto;"><span>实名认证</span></h3>
                        <!-- <span><em><b>实名认证</b></em></span> -->
                        <div style="line-height: 2em;font-size: 16px;">
                            <form id="admin-form-real-user">
                                <input type="hidden" name="uid" value="${user.uid}">
                                <div>
                                    <span>真实姓名：</span>
                                    <input class="form-control" type="text" value="" name="realName">
                                </div>
                                <div>
                                    <span>证件号:</span>
                                    <input class="form-control" id="idNumber" type="text" name="idNumber"
                                           onkeyup="regIdNumber()">
                                </div>
                                <div style="margin: 30px auto;text-align: center;">
                                    <input class="btn btn-primary" type="button" value="提交"
                                           onclick="formRealUser()"><br>
                                    <span style="color: red;" id="idMsg">${realMsg}</span>
                                </div>

                            </form>

                        </div>


                    </div>
                    <div class="tab-pane" id="panel-230696">
                        <h3 style="margin: 30px auto;">
                            <span>更改手机号</span>
                        </h3>
                        <span style="color: red">未绑定则两者填相同号码</span>
                        <!-- <span style="color: red;"><em><b>更改手机时，系统将发送动态验证码到原手机先解绑，请确保原手机可以接受信息。若无法接受请联系服务员或管理员，提供相应身份或账号信息证明</b></em></span> -->
                        <div style="line-height: 2em;font-size: 16px;">
                            <form id="admin-dorm-modify-tel">
                                <input type="hidden" name="uid" value="${user.uid}">
                                <div>
                                    <span>原手机号：</span>
                                    <input class="form-control " type="text" value="${user.telephone}"
                                           name="oldTelephone">
                                </div>

                                <div>
                                    <!-- <span>验证码：</span>
                            < input type="submit" value="点击获取">-->
                                </div>

                                <div>
                                    <span>新手机号：</span>
                                    <input class="form-control" id="newTelephone" type="text" name="newTelephone"
                                           oninput="checkNumber()">
                                </div>

                                <div style="margin: 30px ; text-align: center;">
                                    <input type="button" class="btn btn-primary" value="跟换"
                                           onclick="formModifyTel()"><br>
                                    <span style="color: red;" id="telMsg">${telMsg}</span>
                                </div>
                            </form>

                        </div>
                    </div>
                    <div class="tab-pane" id="panel-230686">
                        <h3 style="margin: 30px auto;"><span>其他</span></h3>
                        <span style="color: red;"><em><b>注销用户时，系统将删除用户的个人信息，需要时可重新注册</b></em></span>
                        <form action="${pageContext.request.contextPath}/unsubscribe" method="post">
                            <div>
                                <input type="hidden" name="uid" value="${user.uid}">
                                <input type="submit" class="btn btn-primary" value="注销本账户">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    //验证两次输入密码是否一致
    function checkPassword() {
        var password = document.getElementById("newPassword").value;
        var chkPassword = document.getElementById("checkNew").value;

        if (password == chkPassword) {
            document.getElementById("hint").innerHTML = "<br><font color='green'>两次密码输入一致</font>";
            document.getElementById("submit").disabled = false;

        } else {
            document.getElementById("hint").innerHTML = "<br><font color='red'>两次输入密码不一致!</font>";
            document.getElementById("submit").disabled = true;
        }
    }
</script>


<script type="text/javascript">
    function checkNumber() {
        var num = document.getElementById("newTelephone").value;
        var telRegx = /^1(3|4|5|6|7|8|9)\d{9}$/;
        // var telRegx =/^((13[0-9])|(14[5,6,7,9])|(15[^4])|(16[5,6])|(17[0-9])|(18[0-9])|(19[1,8,9]))\\d{8}$/;
        if (num == "") {
            // document.getElementById('telMsg').innerText = "请输入";
            alert("请输入手机号！")
        } else {
            if (telRegx.test(num) == false) {
                document.getElementById('telMsg').innerText = "手机号有误，请输入正确的手机号";
                // alert("手机号有误，请输入正确的手机号！")
            }
        }

    };

    function regIdNumber() {
        var num = document.getElementById("idNumber").value;
        // var telRegx = /(^\d{15}$)|(^\d{17}([0-9]|X)$)/ ;
        var telRegx = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{4}$/
        if (telRegx.test(num) == false) {
            document.getElementById('idMsg').innerText = "身份证号有误，请输入正确的身份证号";
        }
    };
    //密码验证

</script>

<script type="text/javascript">
    function formModifyPwd() {
        var formData = new FormData(document.getElementById("admin-form-modify-pwd"));
        $.ajax({
            type: 'post',
            url: '${pageContext.request.contextPath}/admin/modifyPwd',
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (data) {
                if (data.code == "1") {
                    alert("成功！");
                } else {
                    alert("失败！");
                }
            }
        })
    };

    function formModifyTel() {
        var formData = new FormData(document.getElementById("admin-form-modify-tel"));
        $.ajax({
            type: 'post',
            url: '${pageContext.request.contextPath}/admin/modifyTel',
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (data) {
                if (data.code == "1") {
                    alert("成功！");
                } else {
                    alert("失败！");
                }
            }
        })
    };

    function formRealUser() {
        var formData = new FormData(document.getElementById("admin-form-real-user"));
        $.ajax({
            type: 'post',
            url: '${pageContext.request.contextPath}/admin/realUser',
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (data) {
                if (data.code == "1") {
                    alert("成功！");
                } else {
                    alert("失败！");
                }
            }
        })
    };
</script>
