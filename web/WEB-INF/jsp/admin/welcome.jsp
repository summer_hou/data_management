<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/11/22
  Time: 22:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div style="width: 90%;margin: 10px auto;text-align: center;">
    <div>
        <h2><span>欢迎管理员【<span style="color: orangered;">${user.email}</span>】登录综合图书管理系统/后台管理系统</span></h2>
    </div>
    <div style="max-width: 600px;margin: 0 auto;">
        <h4><span>Welcome the administrator [<span style="color: orangered;">${user.email}</span>] to log in the integrated library management
						system/background management system</span></h4>
    </div>
    <div style="text-align: center;min-width: 400px;margin: 20px auto;">
        <div style="display: inline;">
            <img style="width: 200px; height: 200px;border-radius: 100px; display: inline;"
                 src="${pageContext.request.contextPath}/static/images/logo.png" alt="ioc">
        </div>
        <div style="display: inline;margin-left: 60px;">
            <img style="width: 200px; height: 200px;border-radius: 100px;"
                 src="${pageContext.request.contextPath}/static/images/logo.jpg" alt="logo">

        </div>
<%--        <div>--%>
<%--            <span>ioc</span><span style="margin-left: 250px;">logo</span>--%>
<%--        </div>--%>
    </div>
    <div>
        <h2><span>综合图书管理系统</span></h2>
    </div>
    <div>
        <h2><span>Comprehensive Library Management System</span></h2>
    </div>

</div>