package cloud.xhmy.service;

import cloud.xhmy.dao.ClassifyMapper;
import cloud.xhmy.pojo.Classify;

import java.util.List;

public class ClassifyServiceImpl implements ClassifyService {
    private ClassifyMapper classifyMapper;

    public void setClassifyMapper(ClassifyMapper classifyMapper) {
        this.classifyMapper = classifyMapper;
    }

    public int addClassify(Classify classify) {
        return this.classifyMapper.addClassify(classify);
    }

    public int deleteById(int id) {
        return this.classifyMapper.deleteById(id);
    }

    public List<Classify> queryByName(String name, int page, int size) {
        return this.classifyMapper.queryByName(name, page, size);
    }

    public List<Classify> listAll(int page, int size) {
        return this.classifyMapper.listAll(page, size);
    }

    public int modifyById(int id) {
        return this.classifyMapper.modifyById(id);
    }

    public Classify queryById(int id) {
        return this.classifyMapper.queryById(id);
    }
}
