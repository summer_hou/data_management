package cloud.xhmy.pojo;

/**
 * 书籍类型
 * bookType
 * @author aliketh.xhmy
 */
public class BookType {

    private int id;
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
