<%--
  Created by IntelliJ IDEA.
  User: aliketh.xhmy
  Date: 2020/10/26
  Time: 19:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/static/css/user/user-template-framework.css" >
<link rel="icon"
      href="${pageContext.request.contextPath}/favicon.ico">
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/jquery/jquery-3.4.1.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/bootstrap/dist/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/static/bootstrap/dist/css/bootstrap.min.css">
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/user/user-template-framework.js"></script>
<style>
    body {
        background: rgba(64, 58, 109, 0.7);
        /*background-image: linear-gradient(100deg, #89ca54, #fbcdab);*/
        /*background-image: linear-gradient(90deg, #bce8f1, #fbcdab);*/
        <%--background: url(${pageContext.request.contextPath}/static/images/230718-159456643848d4.jpg) no-repeat;--%>
    }
    /*.img-back{*/
    /*    filter:alpha(opacity=50);*/
    /*    -moz-opacity:0.5;*/
    /*    -khtml-opacity: 0.5;*/
    /*    opacity: 0.5;*/
    /*    width:100%;*/
    /*    height: 100%;*/
    /*    position:absolute;*/
    /*    background: rgba(255, 255, 255, 0.5);*/

    /*}*/
</style>
